# Livro online de literatura em Libras

Versão digital do [livro escrito por Rachel Sutton-Spence](http://files.literaturaemlibras.com/Literatura_em_Libras_Rachel_Sutton_Spence.pdf), o desenho do app é de João Paulo de Abreu (abreu.joaopaulo EM gmail.com) e Mariana Dornelles (marianadcdesign EM gmail.com). Implementação por [Lucas Piteco](https://eupiteco.gitlab.io/pagina-pessoal/) e Victor Savas (vicotorsavas EM gmail.com)
