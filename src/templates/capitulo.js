import React, {useEffect} from 'react'
import { graphql } from 'gatsby'
import Layout from '../components/Layout'
import Container from '../components/Container'
import ContentHeader from '../components/livro/ContentHeader'
import BarraLateral from '../components/livro/BarraLateral'
import NavLateral from '../components/livro/NavLateral'
import NavMobile from '../components/livro/NavMobile'
import CorpoLivro from '../components/leitura-online/CorpoLivro'
import {theme} from '../styles/theme'
import offsetScript from '../../static/scripts/offsetAnchor'
import { useLocation } from "@reach/router"

import * as S from './capituloStyles'

const Texto = ({data}) => {
  const {markdownRemark: {
    html, frontmatter: { parte, capitulo, pdf, video }
  }} = data

  const parteAtual = {
    capitulo,
    parte,
    pdf,
    video,
  }

  const location = useLocation()

  useEffect(() => {
    const hash = location.hash !== ""
    const hasVideo = parte !== 0
    offsetScript(hash, hasVideo)
    //eslint-disable-next-line
  }, [])

  return (
    <Layout>
      <Container>
        <S.Wrapper>
          <article>
            {parte !== 0 &&<ContentHeader {...{capitulo, parte}} />}
            <CorpoLivro paleta={theme[parte].paleta} html={html}/>
          </article>
          {parte !== 0 &&<BarraLateral parteAtual={parteAtual}/>}
          <NavLateral {...parteAtual}/>
          <NavMobile {...parteAtual}/>
        </S.Wrapper>
      </Container>
    </Layout>
  )
}

export const query = graphql`
  query Texto($slug: String!) {
    markdownRemark(fields: {slug: {eq: $slug}}) {
      html
      frontmatter {
        capitulo
        parte
        pdf
        video
      }
    }
  }
`

export default Texto
