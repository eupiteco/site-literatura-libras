import styled from 'styled-components'
import { mediaQueries } from '../styles/theme'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column-reverse;
    & > .nav-lateral {display: none;}
    & > .nav-mobile {display: block;}
  ${mediaQueries.lgUp} {
    flex-direction: row;
    & > .nav-lateral {display: block;}
    & > .nav-mobile {display: none;}
  }
`
