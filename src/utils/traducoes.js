import oLivroGif from '../assets/traducoes/teste2.gif'
import oLivroPng from '../assets/traducoes/teste2.png'
import sobreGif from '../assets/traducoes/cara_indiano.gif'
import sobrePng from '../assets/traducoes/cara_indiano.png'

export const oLivro = {
  previa: oLivroPng,
  traducao: oLivroGif,
}
export const sobre = {
  previa: sobrePng,
  traducao: sobreGif,
}
