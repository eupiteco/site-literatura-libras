import React, {createContext, useContext} from 'react'
import { useStaticQuery, graphql } from 'gatsby'

const TraducaoContext = createContext()

export const TraducaoProvider = ({children}) => {
  const {
    access,
    author,
    download,
    libras,
    livro,
    online,
    principal,
    rachel_gustavo,
    vimeo,
    youtube,
    sobre,
    logo
  } = useStaticQuery(graphql`
    query {
      access: file(name: {eq: "icon_access"}) { childImageSharp {
          fluid(maxWidth: 256, quality: 90) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      author: file(name: {eq: "icon_author"}) {
        childImageSharp {
          fluid(maxWidth: 256, quality: 90) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      download: file(name: {eq: "icon_download"}) {
        childImageSharp {
          fluid(maxWidth: 256, quality: 90) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      livro: file(name: {eq: "icon_livro"}) {
        childImageSharp {
          fluid(maxWidth: 256, quality: 90) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      libras: file(name: {eq: "icon_Libras"}) {
        childImageSharp {
          fluid(maxWidth: 256, quality: 90) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      online: file(name: {eq: "icon_online"}) {
        childImageSharp {
          fluid(maxWidth: 256, quality: 90) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      principal: file(name: {eq: "icon_principal"}) {
        childImageSharp {
          fluid(maxWidth: 256, quality: 90) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      rachel_gustavo: file(name: {eq: "icon_Rachel_and_Gustavo"}) {
        childImageSharp {
          fluid(maxWidth: 256, quality: 90) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      vimeo: file(name: {eq: "icon_vimeo"}) {
        childImageSharp {
          fluid(maxWidth: 256, quality: 90) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      youtube: file(name: {eq: "icon_youtube"}) {
        childImageSharp {
          fluid(maxWidth: 256, quality: 90) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      sobre: file(name: {eq: "icon_sobre"}) {
        childImageSharp {
          fluid(maxWidth: 256, quality: 90) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      logo: file(name: {eq: "favicon"}) {
        childImageSharp {
          fluid(maxWidth: 256, quality: 90) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  return <TraducaoContext.Provider value={{
    access: access.childImageSharp.fluid,
    author: author.childImageSharp.fluid,
    download: download.childImageSharp.fluid,
    libras: libras.childImageSharp.fluid,
    livro: livro.childImageSharp.fluid,
    online: online.childImageSharp.fluid,
    principal: principal.childImageSharp.fluid,
    rachel_gustavo: rachel_gustavo.childImageSharp.fluid,
    sobre: sobre.childImageSharp.fluid,
    vimeo: vimeo.childImageSharp.fluid,
    youtube: youtube.childImageSharp.fluid,
    logo: logo.childImageSharp.fluid,
  }}>{children}</TraducaoContext.Provider>
}

export function useTraducao() {
  const context = useContext(TraducaoContext)
  if (!context) throw new Error("useTraducao must be used within a TraducaoProvider");
  return context
}
