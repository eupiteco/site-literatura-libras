import React, {createContext, useContext} from 'react'
import { useStaticQuery, graphql } from 'gatsby'

const ContentContext = createContext()

export const ContentProvider = ({children}) => {

  const {allMarkdownRemark: {edges}, ...img} = useStaticQuery(graphql`
    query {
      allMarkdownRemark(
        sort: {fields: frontmatter___capitulo, order: ASC}
      ) {
        edges {
          node {
            frontmatter {
              capitulo
              titulo
              parte
              pdf
            }
            headings {
              value
              depth
              id
            }
            fields {
              slug
            }
          }
        }
      }
      imgP1: file(relativePath: {eq: "parte-1.png"}) {
        childImageSharp {
          fluid(maxWidth: 256, quality: 90) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      imgP2: file(relativePath: {eq: "parte-2.png"}) {
        childImageSharp {
          fluid(maxWidth: 256, quality: 90) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      imgP3: file(relativePath: {eq: "parte-3.png"}) {
        childImageSharp {
          fluid(maxWidth: 256, quality: 90) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      imgP4: file(relativePath: {eq: "parte-4.png"}) {
        childImageSharp {
          fluid(maxWidth: 256, quality: 90) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  const conteudo = edges.map(({
    node: {
      frontmatter: {capitulo, titulo, parte, pdf},
      headings,
      fields: {slug}
    }}) => ({
    parte,
    content: {
      titulo, capitulo, slug, headings, pdf
    }
  }))

  const conteudoComImagem = conteudo.map(c => c.parte > 0 ? ({...c, image: img[`imgP${c.parte}`].childImageSharp.fluid}) : c)

  const preTexto = conteudo.filter(c => c.parte === 0)
  const parte1 = conteudoComImagem.filter(c => c.parte === 1)
  const parte2 = conteudoComImagem.filter(c => c.parte === 2)
  const parte3 = conteudoComImagem.filter(c => c.parte === 3)
  const parte4 = conteudoComImagem.filter(c => c.parte === 4)

  return( 
    <ContentContext.Provider value={[preTexto, parte1, parte2, parte3, parte4]}>
      {children}
    </ContentContext.Provider>
  )
}

export function useContent() {
  const context = useContext(ContentContext)
  if (!context) throw new Error("useContent must be used within a ContentProvider");
  return context
}
