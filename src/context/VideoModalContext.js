import React, {useState, createContext, useContext} from 'react'
import VideoModal from '../components/modals/VideoModal'

const VideoModalContext = createContext()

const contextDefaultData = {
  isOpen: false,
  content: {
    type: "",
    videoId: "",
  },
}

export const VideoModalProvider = ({children}) => {
  const [ content, setContent ] = useState(contextDefaultData.content)
  const [ isOpen, setIsOpen ] = useState(contextDefaultData.isOpen)

  const openModal = (content) => {
    setContent(content)
    setIsOpen(true)
  }

  const closeModal = () => {setIsOpen(false)}

  return (
    <VideoModalContext.Provider value={{
      openModal,
    }}>
      {children}
      <VideoModal
        onClose={closeModal}
        isOpen={isOpen}
        type={content.type}
        id={content.videoId}
      />
    </VideoModalContext.Provider>
  )
}

export function useVideoModal() {
  const context = useContext(VideoModalContext)
  if (!context) throw new Error("useVideoModal must be used within a VideoModalProvider");
  const { isOpen, content, openModal, closeModal } = context
  return { isOpen, content, openModal, closeModal }
}
