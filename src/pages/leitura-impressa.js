import React from 'react'
import SEO from "../components/seo"
import Layout from "../components/Layout"
import Pagina from "../components/leitura-online/Pagina"

const PageLeituraOnline = () => {

  return (
    <Layout>
      <SEO title="Leitura online" />
      <Pagina impresso/>
    </Layout>
  )
}

export default PageLeituraOnline
