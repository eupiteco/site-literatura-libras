import React from "react"
import SEO from "../components/seo"
import Layout from '../components/Layout'
import HomeHero from "../components/sections/HomeHero"
import HomeOLivro from "../components/sections/HomeOLivro"
import HomeLivroLibras from "../components/sections/HomeLivroLibras"
import HomeSobre from "../components/sections/HomeSobre"

const IndexPage = () => {

  return (
    <Layout>
      <SEO title="Home" />
      <HomeHero />
      <HomeOLivro/>
      <HomeLivroLibras/>
      <HomeSobre/>
    </Layout>
  )
}
 
export default IndexPage
