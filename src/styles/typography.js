import styled from 'styled-components'
import {mediaQueries,cores} from './theme'

const {
  cinzaTexto,
  azul,
  azulClaro,
} = cores

const estiloBase = `
    font-family: "Source Sans Pro", sans-serif;
    text-align: left;
    margin-bottom: 8px;
`

const estiloLista = `
    font-size: 18px;
    line-height: 1.4;
    color: ${cinzaTexto};
    font-weight: regular;
`


const tooltipBorder = `15px`

export const StyledCorpoLivro = styled.div `
  max-width: 720px;

p {
    font-size: 18px;
    line-height: 1.4;
    color: ${cinzaTexto};
    font-weight: regular;
    margin-bottom: 16px;
}

em {
  font-style: italic;
  font-weight: 600;
}

strong {
  font-weight: bold;
}

blockquote {
  border-left: 1px solid;
  border-color: ${p => p.cores.corMedia};
  padding-left: 16px;
  margin-left: 16px;
  & > p {
    font-size: 16px;
  }
}
h2 {
  ${estiloBase}
  color: ${p => p.cores.corBase};
  font-weight: bold;
  font-size: 26px;
  line-height: 1.2;
  margin-top: 12px;
  margin-bottom: 24px;
  ${mediaQueries.lgUp} {
    font-size: 32px;
  }
}

h3 {
    ${estiloBase}
    font-size: 20px;
    line-height: 32px;
    color: ${p => p.cores.corMedia};
    font-weight: 900;
    text-transform: uppercase;
    margin-top: 12px;
  }

  h4 {
    ${estiloBase}
    font-size: 18px;
    line-height: 20px;
    color: ${p => p.cores.corClara};
    font-weight: 900;
    text-transform: uppercase;
    margin-top: 12px;
  }

  h5 {
    ${estiloBase}
    font-size: 18px;
    line-height: 20px;
    font-weight: 600;
    color: ${p => p.cores.corClara};
  }

  h6 {
    ${estiloBase}
    font-size: 14px;
    line-height: 16px;
    color: ${p => p.cores.corClara};
    text-decoration: underline;
  }

  a {
    color: ${p => p.cores.corBase};
    text-decoration: none;
    font-weight: 600;
    font-style: italic;
  }

  ul {
    ${estiloBase}
    ${estiloLista}
    list-style: disc;
    
    li {
        margin-left: 12px;
        &::marker {
          color: ${p => p.cores.corMedia};
          font-size: 12px;
        }
    }
  }

  ol {
    ${estiloBase}
    ${estiloLista}
    list-style: decimal;
    > li {
      margin-bottom: 16px;
      &::marker {
      color: ${p => p.cores.corMedia};
    }
}
    }

  ol > li > p,
  ul > li > p {
    margin-bottom: 0;
  }
  .lista-ordenada {
    font-size: 16px;
    line-height: 18px;
    color: ${azul};
    font-weight: 900;
  }

.destaque-item + p span {
  color: ${p => p.cores.corBase};
  font-weight: 600;
}


  p + h2,
  p + h3, 
  p + h4,
  p + h5,
  p + h6 {
      margin-top: 16px;
  }

// legenda de imagem centralizada
.gatsby-resp-image-wrapper + em {
  text-align: center;
  display: block;
}

.grid {
  display: grid;
  min-width: 300px;
  text-align: center;
  margin: 0 auto;
  place-items: center;
  background-color: ${p => p.cores.corMedia};
  border: solid 1px ${p => p.cores.corMedia};
  grid-gap: 1px;
  grid-template-columns: repeat(5, 1fr);
  grid-template-rows: repeat(5,60px);
  ${mediaQueries.mdUp} {
    grid-template-rows: repeat(5,80px);
  }
  & > div {
    background: white;
    display: flex;
    width: 100%;
    height: 100%;
    justify-content: center;
    align-items: center;
  }
}
// classes modificadoras daqui pra baixo
.legenda + p {
  text-align: center;
  font-size: 16px;
}

.poesia + blockquote {
  border: none;
  & > p:not(:last-child) {
    margin-bottom: 4px;
  }
}

p a {
  word-break: break-word;
}

.float + p {
  float: left;
  width: 52px;
  margin-right: 12px;
  ${mediaQueries.mdUp} {
    width: 120px;
    margin-right: 24px;
  }
}
.identado + * {
  padding-left: 16px;
}
.lista-de-videos {
  border: 1px solid;
  border-color: ${p => p.cores.corMedia};
  margin-left: 48px;
  margin-bottom: 16px;
  ${mediaQueries.mdUp} {
    margin-left: 120px;
  margin-bottom: 24px;
  }
  & + ul > li,
  & + ol > li {
    margin-left: 82px;
    ${mediaQueries.mdUp} {
      margin-left: 168px;
    }
  }
  & + p + ul > li,
  & + p + ol > li {
    margin-left: 82px;
    ${mediaQueries.mdUp} {
      margin-left: 168px;
    }
  }

  & + p {
    margin-left: 82px;
    ${mediaQueries.mdUp} {
      margin-left: 168px;
    }
  }
}

.lista-de-videos.sem-linha {
  border:0 ;
}

.lista-com-letras + ol {
  counter-reset: list;
}
.lista-com-letras + ol > li {
  list-style: none;
}
.lista-com-letras + ol > li:before {
  color: ${p => p.cores.corMedia};
  content: counter(list, lower-alpha) ") ";
  counter-increment: list;
}

.lista-com-fases + ol {
  counter-reset: list;
}
.lista-com-fases + ol > li {
  list-style: none;
}

.lista-com-fases + ol > li:before {
  color: ${p => p.cores.corMedia};
  content: counter(list, decimal) "ª Fase - ";
  counter-increment: list;
}

a[href="#tooltip"] {
  position: relative;
  white-space: nowrap;
  vertical-align: super;
  font-size: 12px;
  &:before {
    content: "";
    display: block;
    padding: 20px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%)
  }
}

.tooltip {
  display: none;
  padding: 14px;
  background-color: #fff;
  color: ${cinzaTexto};
  border-radius: 10px;
  border: 2px solid ${p => p.cores.corBase};
  position: absolute;
  -webkit-transition: opacity 0.5s;
  -moz-transition: opacity 0.5s;
  -ms-transition: opacity 0.5s;
  -o-transition: opacity 0.5s;
  transition: opacity 0.5s;
  box-shadow: 5px 5px 5px rgb(0 0 0 / 50%);
  width: 250px;
  z-index: 999;
  font-size: 14px;
  &-texto {
    display: none;
  }

  /* The bordered part of the triangle */
    
    &::before,
    &::after {
      content: "";
      position: absolute;
      border-left: ${tooltipBorder} solid transparent;
      border-right: ${tooltipBorder} solid transparent;
      top: 100%;
      left: 11%;
      margin-left: -${tooltipBorder};
    }

    &::before {
      border-top: 20px solid ${p => p.cores.corBase};
      margin-top: 1px;
    }

    /* The white fill of the triangle */
    &::after {
      border-top: 20px solid white;
      margin-top: -2px;
      z-index: 1;
    }
  }

.video-modal_backdrop {
  display: none;
  width: 100vw;
  height: 100vh;
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 999;
  background-color: #000C;
  align-items: center;
  justify-content: center;
}

.video-modal_close-button {
  border: none;
  position: absolute;
  top: 22px;
  right: 42px;
  background: transparent;
  font-size: 48px;
  font-weight: 700;
  color: #DDD;
  padding: 6px;
}
`




export const Paragrafo = styled.p`
${estiloBase}
font-size: 11px;
line-height: 15px;
color: ${cinzaTexto};
font-weight: regular;

`

export const Destaque = styled.p`
${estiloBase}
font-size: 11px;
line-height: 15px;
  color: ${p => p.color};
font-weight: bold;
`

export const Link = styled.a`
${estiloBase}
font-size: 10,5px;
line-height: 12,6px;
  color: ${p => p.color};
font-weight: semibold italic;
`

export const Titulo = styled.h1`
${estiloBase}
font-size: 15px;
line-height: 12px;
  color: ${p => p.color};
font-weight: 900;
text-transform: uppercase;
`

export const TituloSemNumero = styled(Titulo)`
  color: ${p => p.color};
${estiloBase}
font-size: 15px;
line-height: 12px;
font-weight: 900;
text-transform: uppercase;
`

export const TituloSublinhado = styled.h2`
  display: inline-block;
  font-size: 46px;
  line-height: 30px;
  font-weight: 600;
  border-bottom: 2px solid ${azulClaro};
  margin-bottom: 8px;
  padding-bottom: 8px;
`


export const SubTituloSublinhado = styled(TituloSublinhado)`
  font-size: 30px;
  line-height: 33px;
  font-weight: 400;
`

export const SubTitulo = styled.h2`
  ${estiloBase}
  font-size: 30px;
  line-height: 33px;
  color: ${p => p.color};
  font-weight: 900;
  text-transform: uppercase;
`

export const SubTitulo2 = styled.h3`
  ${estiloBase}
  font-size: 13px;
  line-height: 14px;
  color: ${p => p.color};
  font-weight: semiBold;
`

export const SubTitulo3 = styled.h4`
  ${estiloBase}
  font-size: 11px;
  line-height: 14px;
  color: ${p => p.color};
  font-weight: regular;
`

export const TextoVerde = styled(SubTitulo3)`
  font-weight: light;
  color: ${p => p.color};
`

export const DestaqueSublinhado = styled(Destaque)`
  text-decoration: underline;
`

export const TituloAtividade = styled.h3`
  ${estiloBase}
  font-size: 15px;
  line-height: 12px;
  color: ${p => p.color};
  font-weight: italic;
`

export const ListAtividadeNumeral = styled.ul`
  ${estiloBase}
  ${estiloLista}
  list-style: decimal;
`

export const ListAtividadeAlfabetica = styled(ListAtividadeNumeral)`
  ${estiloBase}
  ${estiloLista}
  list-style-type:lower-alpha;
  margin-inline-start: 0px;
`

export const legendaFigura = styled.h4`
  font-family: source serif pro;
  text-align: left;
  font-size: 8px;
  line-height: 15px;
  text-align: text;
  font-weight: regular;
  color: ${cinzaTexto};
`
