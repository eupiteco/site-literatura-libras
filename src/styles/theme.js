export const cores = {
  ciano: "#1C7FD2",
  cianoMedio: "#3086B6",
  cianoClaro: "#2DA0CA",
  verde: "#125026",
  verdeMedio: "#23742A",
  verdeClaro: "#13951E",
  azul: "#0E4988",
  azulMedio: "#2A629E",
  azulClaro: "#4C7FB7",
  amarelo: "#936D03",
  amareloMedio: "#A47E16",
  amareloClaro: "#B39031",
  cinzaTexto: "#323232"
}

export const breakPoints = {
  sm: 576,
  md: 768,
  lg: 992,
  xl: 1200,
  xxl: 1400,
}

export const mediaQueries = {
  smUp: `@media (min-width:${breakPoints.sm}px)`,
  smDown: `@media (max-width:${breakPoints.sm - 0.02}px)`,
  mdUp: `@media (min-width:${breakPoints.md}px)`,
  mdDown: `@media (max-width:${breakPoints.md - 0.02}px)`,
  lgUp: `@media (min-width:${breakPoints.lg}px)`,
  lgDown: `@media (max-width:${breakPoints.lg - 0.02}px)`,
  xlUp: `@media (min-width:${breakPoints.xl}px)`,
  xlDown: `@media (max-width:${breakPoints.xl - 0.02}px)`,
  xxlUp: `@media (min-width:${breakPoints.xxl}px)`,
  xxlDown: `@media (max-width:${breakPoints.xxl - 0.02}px)`,
}

export const theme = [
  {
    paleta: {
      corBase: "#555",
      corMedia: "#777",
      corClara: "#888",
    },
    conteudo: {
      titulo: "Capítulos pré-textuais",
    },
  },
  {
    paleta: {
      corBase: cores.azul,
      corMedia: cores.azulMedio,
      corClara: cores.azulClaro,
    },
    conteudo: {
      titulo: "Alguns elementos fundamentais de literatura em libras",
      pdf: "http://files.literaturaemlibras.com/6.%20LITERATURA%20SURDA%20BRASILEIRA-parte%201-Literatura%20em%20Libras.pdf",
    },
  },
  {
    paleta: {
      corBase: cores.amarelo,
      corMedia: cores.amareloMedio,
      corClara: cores.amareloClaro,
    },
    conteudo: {
      titulo: "A produção de narrativas e contos em libras",
      pdf: "http://files.literaturaemlibras.com/6.%20LITERATURA%20SURDA%20BRASILEIRA-parte%201-Literatura%20em%20Libras.pdf",
    },
  },
  {
    paleta: {
      corBase: cores.verde,
      corMedia: cores.verdeMedio,
      corClara: cores.verdeClaro,
    },
    conteudo: {
      titulo: "Elementos de linguagem estética",
      pdf: "http://files.literaturaemlibras.com/6.%20LITERATURA%20SURDA%20BRASILEIRA-parte%201-Literatura%20em%20Libras.pdf",
    },
  },
  {
    paleta: {
      corBase: cores.ciano,
      corMedia: cores.cianoMedio,
      corClara: cores.cianoClaro,
    },
    conteudo: {
      titulo: "A relação entre a sociedade e a literatura em libras",
      pdf: "http://files.literaturaemlibras.com/6.%20LITERATURA%20SURDA%20BRASILEIRA-parte%201-Literatura%20em%20Libras.pdf",
    },
  },
]
