import React from 'react'
import PropTypes from 'prop-types'
import DownloadFile from '../../../assets/icons/DownloadFile'
import { useContent } from '../../../context/ContentContext'
import { theme, cores } from '../../../styles/theme'
import * as S from './styles'

const SumarioLateral = ({parte, pdf}) => {

  const conteudo = useContent() 

  return (
    <S.Wrapper
      className="nav-lateral"
      allowMultipleExpanded
      preExpanded={[parte]}
      noVideo={parte === 0}
    >
      <S.Sticky>
        {pdf &&
          <S.DownloadLink href={pdf} target="_blank" rel="noreferrer">
            Download do capítulo
            <S.IconWrapper>
              <DownloadFile color={cores.azulMedio} large/>
            </S.IconWrapper>
          </S.DownloadLink>
        }
        {conteudo.map((parte, i) => {
          const { paleta: { corBase, corMedia} } = theme[parte[0].parte]
          return parte[0].parte !== 0 ? (
            <S.Item key={i} uuid={parte[0].parte}>
              <S.ItemHeading color={corBase}>
                <S.ItemButton>
                  {parte[0].parte}
                </S.ItemButton>
              </S.ItemHeading>
              <S.ItemPanel>
                <ul>
                  {parte.map(({content: {capitulo, slug}}) => {
                    return (
                      <li key={capitulo}>
                        <S.Capitulo
                          color={corMedia}
                          activeStyle={{ color: corBase, fontStyle: 'italic' }}
                          to={`/leitura-online${slug}`}
                        >capítulo {capitulo}</S.Capitulo>
                      </li>
                    )
                  })}
                </ul>
              </S.ItemPanel>
            </S.Item>
          ) : null
        })}
      </S.Sticky>
    </S.Wrapper>
  )
}

SumarioLateral.propTypes = {
  capitulo: PropTypes.number,
  parte: PropTypes.number,
  pdf: PropTypes.string,
}
export default SumarioLateral
