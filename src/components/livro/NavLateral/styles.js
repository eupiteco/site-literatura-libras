import styled, { keyframes } from "styled-components"
import { Link } from "gatsby"
import { cores, mediaQueries } from "../../../styles/theme"
import {
  Accordion,
  AccordionItem,
  AccordionItemButton,
  AccordionItemHeading,
  AccordionItemPanel,
} from "react-accessible-accordion"

const noVideoSideBar = {
  right: 0,
  position: "absolute",
  marginRight: "0 !important",
}
export const Wrapper = styled(Accordion)`
  margin-left: 24px;
  margin-top: 24px;
  min-width: 88px;
  text-align: center;
  margin-right: calc(-50vw + 472px);
  ${p => (p.noVideo ? noVideoSideBar : {})}
  ${mediaQueries.xlUp} {
    margin-right: calc(-50vw + 562px);
    ${p => (p.noVideo ? noVideoSideBar : {})}
  }
  ${mediaQueries.xxlUp} {
    margin-right: calc(-50vw + 652px);
    ${p => (p.noVideo ? noVideoSideBar : {})}
  }
`
export const Item = styled(AccordionItem)``

export const ItemHeading = styled(AccordionItemHeading)`
  color: ${p => p.color};
  font-size: 21px;
  line-height: 40px;
  font-weight: 600;
`

export const ItemButton = styled(AccordionItemButton)`
  cursor: pointer;
`

export const Capitulo = styled(Link)`
  color: ${p => p.color};
  font-size: 12px;
  line-height: 21px;
  text-decoration: none;
  ${p =>
    p.active && {
      fontWeight: 600,
      fontStyle: "italic",
    }}
`

export const DownloadLink = styled.a`
  font-size: 14px;
  color: ${cores.azulMedio};
  text-align: left;
  display: block;
`

export const IconWrapper = styled.div`
  border: 1px solid;
  border-color: ${cores.azulMedio};
  margin-top: 8px;
  margin-bottom: 32px;
  padding: 8px;
  padding-left: 16px;
  border-top-left-radius: 10px;
  border-bottom-left-radius: 10px;
  border-right: 0;
`
export const DownloadIcon = styled.img`
  width: 36px;
  height: auto;
`

const Fadein = keyframes`
  from { opacity: 0;}
  to { opacity: 1;}
`

export const ItemPanel = styled(AccordionItemPanel)`
  animation: ${Fadein} 0.2s ease-in;
`

export const Sticky = styled.div`
  ${mediaQueries.lgUp} {
    position: sticky;
    top: 140px;
  }
`
