import React, {useState} from 'react'
import { theme } from '../../../styles/theme'
import DownloadFile from '../../../assets/icons/DownloadFile'
import Chevron from '../../../assets/icons/Chevron'
import * as S from './styles' 

function addZero(n) {
  if (n.length === 1) return "0"+n
  return n
}

const NavMobile = ({capitulo, parte, pdf}) => {

  const [ showMenu, setShowMenu ] = useState(false)
  const toggleMenu = () => setShowMenu(!showMenu)

  const prevChapter = addZero((capitulo-1).toString())
  const nextChapter = addZero((capitulo+1).toString())

  const { paleta: { corBase, corMedia, corClara} } = theme[parte]

  return (
    <S.Wrapper color={corMedia} show={showMenu}>
      <S.ToggleButton onClick={toggleMenu} colors={{corClara, corBase}} open={showMenu}>
        <S.ToggleIconWrapper open={showMenu}>
          <Chevron color={corBase} />
        </S.ToggleIconWrapper>
        <span>{showMenu ? '' : 'Menu'}</span>
      </S.ToggleButton>
      <S.TopSection>
        <S.LinkSumario className="cor-base" to="/leitura-online">
          <S.TextoSumario>Sumário</S.TextoSumario>
        </S.LinkSumario>
      </S.TopSection>
      <S.BottomSection>
        <S.PrevLink to={"/leitura-online/parte"+prevChapter} disabled={capitulo <= 1} className="cor-base">
          <div>Capítulo Anterior</div>
          <S.PrevIconWrapper>
            <Chevron color={corMedia}/>
          </S.PrevIconWrapper>
        </S.PrevLink>
        {pdf &&
          <S.PrintLink href={pdf} target="_blank" className="cor-base">
            <S.PrintIconWrapper>
              <DownloadFile color={corMedia}/>
            </S.PrintIconWrapper>
            Dowdnload do capítulo
          </S.PrintLink>
        }
        <S.NextLink to={"/leitura-online/parte"+nextChapter} disabled={capitulo >= 24} className="cor-base">
          <div>Próximo Capítulo</div>
          <S.NextIconWrapper>
            <Chevron color={corMedia}/>
          </S.NextIconWrapper>
        </S.NextLink>
      </S.BottomSection>
    </S.Wrapper>
  )
}

export default NavMobile
