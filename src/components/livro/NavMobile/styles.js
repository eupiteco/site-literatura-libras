import styled from 'styled-components'
import { mediaQueries } from '../../../styles/theme'
import {Link} from 'gatsby'

const navLinkStyle = p => p.disabled ? 
  ({
    cursor: 'unset',
    pointerEvents: 'none',
    opacity: '0.3',
  }) : ({})

const ChevronIconWrapper = styled.div`
  display: flex;
  width: 38px;
  height: 38px;
  border: 2px solid;
  border-radius: 99px;
  border-color: ${p => p.color};
  margin-top: 8px;
  justify-content: center;
  align-items: center;
  & > svg {
    margin-top: 4px;
  }
`

export const Wrapper = styled.div`
  position: fixed;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 99;
  box-shadow: 0 -2px 2px rgba(0, 0, 0, 0.3);
  padding: 40px 32px 12px;
  background: white;
  transform: translateY(${p => p.show ? '0' : '101%'});
  transition: transform 0.2s linear;
  & .cor-base {
    color: ${p => p.color}!important;
  }
  ${mediaQueries.mdUp} {padding: 40px 162px 18px;}
  ${mediaQueries.lgUp} {display: none;}
`

export const ToggleButton = styled.button`
  position: absolute;
  display: flex;
  top: ${p => p.open ? -16 : -76}px;
  left: 50%;
  z-index: 999;
  border: none;
  border-radius: 99px;
  color: ${p => p.colors.corBase};
  flex-direction: column;
  align-items: center;
  transform: translateX(-50%);
  padding: 12px;
  background-color: ${p => p.colors.corClara}DD;
  transition: transform 0.2s linear;
`

export const ToggleIconWrapper = styled.div`
  transform: rotate(${p => p.open ? "0" : "180deg" });
`

export const TopSection = styled.div`
  margin-bottom: 12px;
`

export const BottomSection = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
`

export const LinkSumario = styled(Link)`
  display: flex;
  margin: 0 auto;
  justify-content: center;
  align-items: center;
`

export const TextoSumario = styled.span`
  font-size: 20px;
  margin-left: 12px;
`

const NavLink = styled(Link)`
  display: flex;
  margin-bottom: 4px;
  width: min-content;
  font-size: 16px;
  flex-direction: column;
  align-items: center;
`

export const PrevLink = styled(NavLink)`
  ${p => navLinkStyle(p)}
`

export const PrevIconWrapper = styled(ChevronIconWrapper)`
  transform: rotate(90deg);
`

export const NextLink = styled(NavLink)`
  ${p => navLinkStyle(p)}
`

export const NextIconWrapper = styled(ChevronIconWrapper)`
  transform: rotate(-90deg);
`

export const PrintLink = styled.a`
  font-size: 12px;
  width: 60px;
  text-align: center;
`

export const PrintIconWrapper = styled.div`
  border: 1px solid;
  border-color: ${p => p.color};
  border-radius: 4px;
  padding: 4px;
  margin-bottom: 4px;
`
