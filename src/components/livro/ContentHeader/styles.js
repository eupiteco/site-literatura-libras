import styled from 'styled-components'
import { mediaQueries } from '../../../styles/theme'
import Img from 'gatsby-image'

export const Wrapper = styled.div``

export const Imagem = styled(Img)`
  width: 100%;
  max-width: 96px;
  margin: 0 auto 16px;
  ${mediaQueries.lgUp} {
    max-width: 220px;
  }
`

export const Titulo = styled.h2`
  font-size: 26px;
  margin-bottom: 24px;
  line-height: 1.2;
  text-align: center;
  text-transform: uppercase;
  font-weight: 900;
  ${mediaQueries.lgUp} {
    font-size: 40px;
  }
`

export const Divisor = styled.hr`
  border-top: 1px solid;
`
