import React from 'react'
import PropTypes from 'prop-types'
import { theme } from '../../../styles/theme'
import {useContent} from '../../../context/ContentContext'
import * as S from './styles'

const ContentHeader = ({parte, capitulo}) => {
  const {image} = useContent()[parte][0]
  const { paleta: { corBase}, conteudo: {titulo}} = theme[parte]
  return (
    <S.Wrapper>
      <S.Imagem fluid={image}/>
      <S.Titulo style={{color: corBase}}>Parte {parte} - {titulo}</S.Titulo>
      <S.Divisor style={{borderColor: corBase}}/>
    </S.Wrapper>
  )
}

ContentHeader.propTypes = {
  parte: PropTypes.number.isRequired,
  capitulo: PropTypes.number.isRequired,
}

export default ContentHeader
