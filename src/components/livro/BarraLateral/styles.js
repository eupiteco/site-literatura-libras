import styled from 'styled-components'
import { Link } from 'gatsby'
import { mediaQueries } from '../../../styles/theme'

export const Wrapper = styled.aside`
  width: 100vw;
  position: sticky;
  top: 70px;
  background: white;
  box-shadow: 0 2px 2px rgba(0, 0, 0, 0.3);
  margin: 0 -16px 24px;
  transform: translateY(${p => p.show ? '0' : '-101%'});
  transition: transform 0.3s linear;
  ${mediaQueries.smUp} { margin: 0 calc(-50vw + 260px) 24px; }
  ${mediaQueries.mdUp} { margin: 0 calc(-50vw + 340px) 24px; }
  ${mediaQueries.lgUp} {
    transform: translateY(0);
    max-width: max-content;
    box-shadow: none;
    position: static;
    margin: 0;
    margin-left: 32px;
  }
`
export const Breadcrumbs = styled.ol`
  display: none;
  text-align: right;
  margin-bottom: 16px;
  & > li {
    margin-right: 12px;
    &:not(:first-child):before {
      content: ">";
      margin-right: 12px;
    }
  }
  ${mediaQueries.lgUp} {
    display: flex;
  }
`

export const BreadLink = styled(Link)`
  color: ${p => p.color};
`

export const VideoContainer = styled.div`
  position: relative;
  max-width: 480px;
  margin: 0 auto;
  ${mediaQueries.lgUp} {
    min-width: ${p => p.grande ? '470px' : '360px'};
    margin: 0;
    position: sticky;
    top: 140px;
  }
`

export const ToggleButton = styled.button`
  position: absolute;
  display: flex;
  bottom: ${p => p.open ? -26 : -76}px;
  left: 50%;
  z-index: 999;
  border: none;
  border-radius: 99px;
  color: ${p => p.colors.corBase};
  flex-direction: column;
  align-items: center;
  transform: translateX(-50%);
  padding: 12px;
  background-color: ${p => p.colors.corClara}DD;
  transition: transform 0.2s linear;
  ${mediaQueries.lgUp} {display: none;}
`

export const ToggleIconWrapper = styled.div`
  transform: rotate(${p => p.open ? "180deg" : "0" });
`
