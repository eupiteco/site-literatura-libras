import React, {useState} from 'react'
import PropTypes from 'prop-types'
import { theme } from '../../../styles/theme'
import EmbededVideo from '../../../components/shared/EmbededVideo'
import Chevron from '../../../assets/icons/Chevron'
import * as S from './styles'

const BarraLateral = ({parteAtual, videoId, grande}) => {
  const { paleta: { corBase, corClara} } = theme[parteAtual.parte]
  const [ showVideo, setShowVideo ] = useState(true)
  const toggleVideo = () => setShowVideo(!showVideo)
  return (
    <S.Wrapper show={showVideo}>
      {parteAtual && (
          <nav aria-label="breadcrumbs">
        <S.Breadcrumbs>
            <li>
              <S.BreadLink
                to={`/leitura-online#parte-${parteAtual.parte}`}
                color={corBase}
              >
                Parte {parteAtual.parte}
              </S.BreadLink>
            </li>
            <li>
              <span aria-current="location">
                Capítulo {parteAtual.capitulo}
              </span>
            </li>
        </S.Breadcrumbs>
          </nav>
      )}
      <S.VideoContainer grande={grande}>
        <EmbededVideo type="youtube" id={videoId || parteAtual.video}/>
        <S.ToggleButton onClick={toggleVideo} colors={{corClara, corBase}} open={showVideo}>
          <span>{showVideo ? '' : 'Vídeo'}</span>
          <S.ToggleIconWrapper open={showVideo}>
            <Chevron color={corBase} />
          </S.ToggleIconWrapper>
        </S.ToggleButton>
      </S.VideoContainer>
    </S.Wrapper>
  )
}

BarraLateral.propTypes = {
  parteAtual: PropTypes.shape({
    capitulo: PropTypes.number,
    parte: PropTypes.number
  }),
  grande: PropTypes.bool,
  id: PropTypes.string,
}

export default BarraLateral
