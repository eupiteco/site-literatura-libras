import styled from 'styled-components'

export const Backdrop = styled.div`
  width: 100vw;
  height: 100vh;
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  display: flex;
  z-index: 99;
  background-color: #000C;
  align-items: center;
  justify-content: center;
`

export const CloseButton = styled.button`
  border: none;
  position: absolute;
  top: 12px;
  right: 12px;
  background: transparent;
  font-size: 48px;
  font-weight: 700;
  color: #DDD;
  padding: 6px;
`
