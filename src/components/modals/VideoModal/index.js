import React, {useEffect} from 'react'
import PropTypes from 'prop-types'
import EmbededVideo, {embeddedVideoType} from '../../shared/EmbededVideo'
import * as S from './styles'

const handleEsc = (e, f) => {
  if (e.key === "Escape") f()
}

const VideoModal = ({isOpen, onClose, type, id}) => {

  useEffect((onClose) => {
    document.addEventListener("keydown", e => handleEsc(e, onClose))
    return (
      document.removeEventListener("keydown", e => handleEsc(e, onClose))
    )
  }, [])

  const embeddedVideoProps = {type, id}
  
  return isOpen ? (
    <S.Backdrop onClick={onClose}>
      <EmbededVideo {...embeddedVideoProps} />
      <S.CloseButton onClick={onClose}>&times;</S.CloseButton>
    </S.Backdrop>
  ) : null
}

VideoModal.propTypes = {
  ...embeddedVideoType,
  onClose: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired
}

export default VideoModal
