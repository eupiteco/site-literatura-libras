import React from 'react'
import * as S from './styles'
import Container from '../../Container'
import {cores} from '../../../styles/theme'
import EmbededVideo from '../../shared/EmbededVideo'
import TituloSessao from '../../shared/TituloSessao'
import { useStaticQuery, graphql } from 'gatsby'
import { useTraducao } from '../../../context/TraducaoContext'
import Image from 'gatsby-image'

const HomeSobre = () => {

  const { sobre } = useTraducao() 
  const {bg} = useStaticQuery(graphql`
    query {
      bg: file(relativePath: { eq: "arabesco-home-2.png" }) {
        childImageSharp {
          fixed(width: 203, quality: 90) {
            ...GatsbyImageSharpFixed_tracedSVG
          }
        }
      }
    }
  `)


  return (
    <S.SectionRelative id="sobre">
      <S.DecorationContainer>
        <Image fixed={bg.childImageSharp.fixed} />
      </S.DecorationContainer>
      <Container>

      <TituloSessao color={cores.verdeMedio} traducao={sobre}>Sobre</TituloSessao>


        <S.Wrapper>
        <S.TituloSessao>O livro</S.TituloSessao>
        <S.GrupoBloco>
          <S.Bloco>
            <S.VideoWrapper>
              <EmbededVideo type="vimeo" id="516237560" />
            </S.VideoWrapper>
          </S.Bloco>
            <S.Bloco>
              <S.TituloLivro>Literatura em Libras</S.TituloLivro>
              <S.Texto>Neste livro, “Literatura em Libras”, celebramos a riqueza, a criatividade e a genialidade da Libras. Apresentamos alguns conceitos fundamentais da literatura em Libras, detalhando a produção de narrativas, os elementos da linguagem estética de Libras e a inter-relação entre a sociedade e esta literatura. Tentamos entender melhor como ela é enquanto conhecemos as obras literárias de Libras, analisamos os exemplos e comentamos sobre eles. Por isso, sugerimos que você assista as obras literárias indicadas antes de ler/ver os capítulos.</S.Texto>
            </S.Bloco>
          </S.GrupoBloco>
          <S.TituloSessao>A escritora</S.TituloSessao>
          <S.GrupoBloco>
            <S.Bloco>
            <S.VideoWrapper>
                <EmbededVideo type="vimeo" id="516236498" />
              </S.VideoWrapper>
            </S.Bloco>
            <S.Bloco>
              <S.TituloLivro>Rachel Sutton-Spence</S.TituloLivro>
              <S.Texto>Rachel Sutton-Spence é professora no departamento de Língua Brasileira de Sinais na Universidade Federal de Santa Catarina desde 2013. Ela é inglesa. Fez seu doutorado em linguística aplicada em BSL (Língua de Sinais Britânica) e começou pesquisar a literatura surda e o folclore linguístico da comunidade surda na Inglaterra em 1997. Desde o primeiro encontro com a literatura em Libras em 2004 ela começou a conhecer os artistas surdos brasileiros e hoje pesquisa, publica, leciona sobre esta língua, sempre com o objetivo de valorizar a literatura surda, os artistas que a produzem e os públicos que participam dela.</S.Texto>
            </S.Bloco>
          </S.GrupoBloco>
          <S.TituloSessao>Apresentação</S.TituloSessao>
          <S.GrupoBloco>
            <S.Bloco>
              <S.VideoWrapper>
                <EmbededVideo type="vimeo" id="516198735" />
              </S.VideoWrapper>
            </S.Bloco>
            <S.Bloco>
              <S.TituloLivro>Rachel e Gustavo se apresentam</S.TituloLivro>
              <S.Texto>A autora do livro Rachel Sutton-Spence e o tradutor para Libras Gustavo Gusmão se apresentam.</S.Texto>
            </S.Bloco>
          </S.GrupoBloco>
        </S.Wrapper>
      </Container>
    </S.SectionRelative>
  )
}

export default HomeSobre
