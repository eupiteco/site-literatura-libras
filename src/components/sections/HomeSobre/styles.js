import styled from 'styled-components'
import {mediaQueries} from '../../../styles/theme'
import {cores} from '../../../styles/theme'


const {
  verdeMedio,
  } = cores


export const SectionRelative = styled.div `
  position: relative;
`


export const DecorationContainer = styled.div `
  display: none;

  ${mediaQueries.smUp} {
    display: block;
    position: absolute;
    right: 0;
    top: 0;
    z-index: 5;
  }
`

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;

  * { z-index: 10; }

  ${mediaQueries.lgUp} {
    flex-direction: row;
  }
`

export const Bloco = styled.div`
  min-width: 50%;

  ${mediaQueries.lgUp} {
    &:nth-child(odd) {
      padding-right: 16px;
    }  

    &:nth-child(even) {
      padding-left: 16px;
    }  
  }

`

export const BlocoFooter = styled.div`
  margin: 24px 32px;

  button {
    width: 80%;
    padding: 24px 32px;
  }
`

export const TituloLivro = styled.h3`
  font-size: 18px;
  line-height: 28px;
  font-weight: 500;
  margin-bottom: 16px;
  display: inline-block;
  border-bottom: 1px solid ${verdeMedio};


  ${mediaQueries.lgUp} {
    font-size: 24px;
    line-height: 30px;
    font-weight: 600;
    padding-top: 20px;
    border-bottom: 2px solid ${verdeMedio};
  }
`

export const TituloSessao = styled.h3`

  display: none;
  
  ${mediaQueries.lgUp} {
    font-size: 28px;
    line-height: 30px;
    font-weight: 600;
    margin-bottom: 16px;
    display: inline-block;
  }
`

export const Texto = styled.p`
  font-size: 18px;
  line-height: 26px;
  color: #333333;
`


export const VideoWrapper = styled.div`
  margin: 0 auto;
  margin-bottom: 16px;
  iframe {
    max-width: 100%;
    max-height: 51vw;
  }
  ${mediaQueries.lgUp} {
    padding-top: 18px;
    margin-bottom: 32px;
    iframe {
      width: 100%;
    }
  }
`

export const BlocoPadding = styled(Bloco)`
  ${mediaQueries.xlUp} {
    margin-left: 144px;
  }
`

export const GrupoBloco = styled.div`
  display: flex;
  flex-direction: column;
  min-width: 100%;
  margin-bottom: 16px;

  ${mediaQueries.lgUp} {
    flex-direction: row;
    margin-bottom: 32px;
  }
`