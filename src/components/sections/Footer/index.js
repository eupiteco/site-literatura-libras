import React from 'react'
import styled from 'styled-components'
import {cores} from '../../../styles/theme'
import Container from '../../Container'
import { useStaticQuery, graphql } from 'gatsby'
import Image from 'gatsby-image'
import {mediaQueries} from '../../../styles/theme'


const {
  azulMedio,
  } = cores



export const FooterStyles = styled.div `
  width: 100%;

  color: ${azulMedio};
  position: relative;

  h2 {
    font-size: 0.875rem;
    font-weight: 600;
  }

  p {
    font-size: 0.5rem;
    font-style: italic;
  }


  ${mediaQueries.smUp} {
    -webkit-box-shadow: -0.1px -4px 4px 0 rgba(0, 0, 0, 0.08); 
    box-shadow: -0.1px -4px 4px 0 rgba(0, 0, 0, 0.08);
    
    h2 {
      font-size: 1rem;
    }

    p {
      font-size: 0.875rem;
    }
  }
`

export const FooterWrapper = styled.div `
  padding: 32px 0 80px 0;
  display: flex;
  flex-direction: row;
  justify-content: space-between;

  * {z-index:10;}
`

export const DecorationContainer = styled.div `

  display: block;
  position: absolute;
  left: 0;
  bottom: 0;
  z-index: 5;

  ${mediaQueries.smUp} {
    display: block;
    position: absolute;
    left: 0;
    bottom: 0;
    z-index: 5;
  }
`

const Footer = () => {

  const {bg} = useStaticQuery(graphql`
    query {
      bg: file(relativePath: { eq: "arabesco-footer.png" }) {
        childImageSharp {
          fixed(width: 229, quality: 90) {
            ...GatsbyImageSharpFixed_tracedSVG
          }
        }
      }
    }
  `)

  return (
    <>
    <FooterStyles>
      <DecorationContainer>
          <Image fixed={bg.childImageSharp.fixed} />
      </DecorationContainer>
      <Container>
        <FooterWrapper>
          <h2>Literatura em Libras de Rachel Sutton-Spence</h2>
          <p>Publicado em 28/06/2021</p>
        </FooterWrapper>
      </Container>
    </FooterStyles>
    </>
  )
}

export default Footer
