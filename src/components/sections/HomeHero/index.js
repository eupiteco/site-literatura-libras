import React from 'react'
import * as S from './styles'
import BgAzul from '../../shared/BgAzul'
import EmbededVideo from '../../shared/EmbededVideo'
import Container from '../../Container'

const HomeHero = () => {

  return (
    <>
      <BgAzul mobileHeight="400px">
        <Container>
          <S.Wrapper>
            <S.VideoWrapper>
              <EmbededVideo type="vimeo" id="516241035" />
            </S.VideoWrapper>
            <div>
              <S.Bloco>
                <S.TituloHero>O livro Literatura em Libras</S.TituloHero>
                <S.Texto>Bem vindo ao livro “Literatura em Libras”! É um livro bilingue sobre a arte linguística da comunidade surda brasileira – narrativas, poemas, piadas e mais. Publicado pela editora Arara Azul, está disponível gratuitamente para você ler, estudar e compartilhar.</S.Texto>
              </S.Bloco>
              <S.BlocoPadding>
                <S.TituloHero>Como acessá-lo?</S.TituloHero>
                <S.Texto>Neste site, você pode acessar os capítulos em Libras no YouTube e Vimeo. Também pode baixar os arquivos em pdf do texto em português e imprimi-los. Cada capítulo tem hiperlinks ou códigos QR onde se encontram as obras literárias em outras plataformas. Você pode ler o mesmo texto nas páginas aqui.</S.Texto>
              </S.BlocoPadding>
            </div>
            </S.Wrapper>
          </Container>
        </BgAzul>
    </>
  )
}

export default HomeHero
