import styled from 'styled-components'
import {mediaQueries} from '../../../styles/theme'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  ${mediaQueries.lgUp} {
    flex-direction: row;
    justify-content: flex-start;
    align-items: flex-start;
    margin-top: -90px;
  }
`

export const VideoWrapper = styled.div`
  width: 100%;
  padding-top: 20px;
  margin-bottom: 32px;

  iframe {
    max-width: 100%;
    max-height: 51vw;
  }
  
  ${mediaQueries.lgUp} {
    width: 50%;
    margin-right: 40px;
    padding-top: 0;
    margin-bottom: 0;

    iframe {
      max-width: unset;
    }
  }
`

export const Bloco = styled.div`
  margin-bottom: 40px;
  ${mediaQueries.lgUp} {
    max-width: 400px;
  }
`

export const BlocoPadding = styled(Bloco)`
  ${mediaQueries.xlUp} {
    margin-left: 144px;
  }
`

export const TituloHero = styled.h3`
  font-size: 18px;
  line-height: 28px;
  font-weight: 600;
  margin-bottom: 16px;

  ${mediaQueries.lgUp} {
    font-size: 28px;
    line-height: 30px;
  }
`

export const Texto = styled.p`
  font-size: 18px;
  line-height: 38px
`
