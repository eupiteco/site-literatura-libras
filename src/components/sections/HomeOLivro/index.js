import React from 'react'
import * as S from './styles'
import {cores} from '../../../styles/theme'
import Container from '../../Container'
import EmbededVideo from '../../shared/EmbededVideo'
import TraducaoMini from '../../shared/TraducaoMini'
import Button from '../../shared/Button'
import TituloSessao from '../../shared/TituloSessao'
import { useStaticQuery, graphql } from 'gatsby'
import { useTraducao } from '../../../context/TraducaoContext'
import Image from 'gatsby-image'


const HomeOLivro = () => {

  const {livro, online, download} = useTraducao()

  const {bg} = useStaticQuery(graphql`
    query {
      bg: file(relativePath: { eq: "arabesco-home-1.png" }) {
        childImageSharp {
          fixed(width: 115, quality: 90) {
            ...GatsbyImageSharpFixed_tracedSVG
          }
        }
      }
    }
  `)

  return (
    <S.SectionRelative>
      <S.DecorationContainer>
        <Image fixed={bg.childImageSharp.fixed} />
      </S.DecorationContainer>
      <Container>
        <TituloSessao color={cores.azulMedio} traducao={livro}>O Livro</TituloSessao>
        <S.ContentWrapper>
          <S.Bloco>
            <div>
              <S.VideoWrapper>
                <EmbededVideo type="vimeo" id="516240136" />
              </S.VideoWrapper>
              <S.TituloLivro>Ler o livro online</S.TituloLivro>
              <S.Texto>Para acessar os 24 capítulos do livro e os outros materiais em português para leitura online, clique aqui. Eles podem ser lidos em computadores, tablets ou celulares e possuem hiperlinks para se assistir aos vídeos indicados.</S.Texto>
            </div>
            <S.BlocoFooter>
              <TraducaoMini tamanho="m" traducao={online}/>
              <Button to="/leitura-online">
                  Ler online
              </Button>
            </S.BlocoFooter>
          </S.Bloco>

          <S.Bloco>
          <div>
              <S.VideoWrapper>
                <EmbededVideo type="vimeo" id="516239882" />
              </S.VideoWrapper>
              <S.TituloLivro>Baixar o livro em PDF</S.TituloLivro>
              <S.Texto>Você pode baixar todos 24 capítulos do livro e os outros materiais em português ou fazer uma seleção deles para leitura offline. Basta clicar aqui.</S.Texto>
            </div>
            <S.BlocoFooter>
            <TraducaoMini tamanho="m" traducao={download}/>
              <Button to="leitura-impressa">
                  Baixar PDF
              </Button>
            </S.BlocoFooter>
          </S.Bloco>
        </S.ContentWrapper>
      </Container>
    </S.SectionRelative>
  )
}

export default HomeOLivro
