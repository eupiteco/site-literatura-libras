import styled from 'styled-components'
import {mediaQueries} from '../../../styles/theme'
import {cores} from '../../../styles/theme'

const {
  azulClaro,
  } = cores

export const SectionRelative = styled.div `
  position: relative;
  padding-top: 32px; 
`

export const DecorationContainer = styled.div `
  display: none;

  ${mediaQueries.smUp} {
    display: block;
    position: absolute;
    left: 0;
    top: 50%;
    transform: translateY(-50%);
    z-index: 5;
  }
`

export const ContentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;

  * { z-index: 10; }

  ${mediaQueries.lgUp} {
    flex-direction: row;
  }
`

export const Titulo = styled.div`
  margin-bottom: 16px;
  ${mediaQueries.lgUp} {
    margin-bottom: 32px;
  }

  h2 {
    font-size: 22px;
    font-weight: 600;
    line-height: 22px;
    margin-bottom: 8px;
    padding-bottom: 8px;
    align-self: flex-end;

    ${mediaQueries.lgUp} {
      display: inline-block;
      font-size: 46px;
      line-height: 30px;
      border-bottom: 2px solid ${azulClaro};

    }
  }
`

export const Bloco = styled.div`
  margin-bottom: 40px;
  min-width: 320px;

  
  ${mediaQueries.lgUp} {
    min-width: 400px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    width: 50%;

    &:nth-child(odd) {
      padding-right: 40px;
    }
  
    &:nth-child(even) {
      padding-left: 40px;
    }
  }
`


export const BlocoFooter = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: 48px 0;
  a {
    flex-grow: 1;
    margin-left: 16px;
  }
  ${mediaQueries.lgUp} {
    margin: 24px 32px;
    a{ 
      margin-left: 16px;
    }
  }
`

export const TituloLivro = styled.h3`
  font-size: 18px;
  line-height: 28px;
  font-weight: 500;
  margin-bottom: 16px;
  display: inline-block;
  border-bottom: 1px solid ${azulClaro};


  ${mediaQueries.lgUp} {
    font-size: 24px;
    line-height: 30px;
    font-weight: 600;
    border-bottom: 2px solid ${azulClaro};
  }
`

export const Texto = styled.p`
  font-size: 18px;
  line-height: 26px;
  color: #333333;
`

export const VideoWrapper = styled.div`
  margin: 0 auto;
  margin-bottom: 16px;
  iframe {
    max-width: 100%;
    max-height: 51vw;
  }
  ${mediaQueries.lgUp} {
    padding-top: 18px;
    margin-bottom: 32px;
    iframe {
      width: 100%;
    }
  }
`


export const BlocoPadding = styled(Bloco)`
  ${mediaQueries.xlUp} {
    margin-left: 144px;
  }
`
