import styled from 'styled-components'
import { mediaQueries } from '../../../styles/theme'

export const Wrapper = styled.div`
  display: flex;
  justify-content: space-between; 
  align-items: center;
  ${mediaQueries.lgUp} {
    padding-top: 12px;
    padding-bottom: 12px;
  }
`
