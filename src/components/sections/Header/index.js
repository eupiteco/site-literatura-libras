import React from 'react'
import Container from '../../Container/'
import HeaderLogo from '../../header/HeaderLogo'
import HeaderNav from '../../header/HeaderNav'
import * as S from './styles'

const Header = () => {
  return (
    <div style={{
      borderBottom: "1px solid black",
      backgroundColor: 'white',
      width: '100vw',
      zIndex: 999,
      position: "fixed",
    }}>
      <Container>
        <S.Wrapper>
          <HeaderLogo />
          <HeaderNav />
        </S.Wrapper>
      </Container>
    </div>
  )
}

export default Header
