import styled from 'styled-components'
import {mediaQueries} from '../../../styles/theme'
import {cores} from '../../../styles/theme'


const {
  azulClaro,
  } = cores


export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  ${mediaQueries.lgUp} {
    flex-direction: row;
  }
`

export const Bloco = styled.div`
  
min-width: 50%;

${mediaQueries.lgUp} {
  &:nth-child(odd) {
    padding-right: 16px;
  }  

  &:nth-child(even) {
    padding-left: 16px;
  }  
}
`


export const BlocoFooter = styled.div`

  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: 48px 0;

  a {
    flex-grow: 1;
    margin-left: 16px;
  }


  ${mediaQueries.lgUp} {
    margin: 24px 32px;
    
    a{ 
      margin-left: 16px;
    }

  }
`


export const TituloLivro = styled.h3`
  font-size: 18px;
  line-height: 28px;
  font-weight: 500;
  margin-bottom: 16px;
  display: inline-block;
  border-bottom: 1px solid ${azulClaro};


  ${mediaQueries.lgUp} {
    font-size: 24px;
    line-height: 30px;
    font-weight: 600;
    border-bottom: 2px solid ${azulClaro};
  }
`

export const Texto = styled.p`
  font-size: 18px;
  line-height: 26px;
  color: #333333;
`

export const VideoWrapper = styled.div`
  margin: 0 auto;
  margin-bottom: 16px;
  iframe {
    max-width: 100%;
    max-height: 51vw;
  }
  ${mediaQueries.lgUp} {
    padding-top: 18px;
    margin-bottom: 32px;
    iframe {
      width: 100%;
    }
  }
`

export const BlocoPadding = styled(Bloco)`
  ${mediaQueries.xlUp} {
    margin-left: 144px;
  }
`

export const GrupoBloco = styled.div`
  display: flex;
  flex-direction: column;
  min-width: 100%;
  margin: 16px 0;

  ${mediaQueries.lgUp} {
    flex-direction: row;
    margin: 32px 0;
  }
`