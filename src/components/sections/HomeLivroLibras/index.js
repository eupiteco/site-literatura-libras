import React from 'react'
import * as S from './styles'
import Container from '../../Container'
import {cores} from '../../../styles/theme'
import BgAzul from '../../shared/BgAzul'
import EmbededVideo from '../../shared/EmbededVideo'
import TraducaoMini from '../../shared/TraducaoMini'
import Button from '../../shared/Button'
import TituloSessao from '../../shared/TituloSessao'
import { useTraducao } from '../../../context/TraducaoContext'

const HomeLivroLibras = () => {

  const { libras, youtube, vimeo } = useTraducao()
  return (
    <>
      <BgAzul mobileHeight="400px">
        <Container>
          <TituloSessao color={cores.azulMedio} traducao={libras}>Livro em Libras</TituloSessao>
          <S.Wrapper>
            <S.GrupoBloco>
              <S.Bloco>
                <S.VideoWrapper>
                  <EmbededVideo type="vimeo" id="516238472" />
                </S.VideoWrapper>
              </S.Bloco>
              <S.Bloco>
                <S.TituloLivro>Livro completo em Libras</S.TituloLivro>
                <S.Texto>A tradução para Libras foi feita pelo artista surdo Gustavo Gusmão. Você pode acessar os capítulos em Libras aqui. Os vídeos no YouTube têm cada subseção marcada para navegar facilmente dentro dos capítulos. Você pode controlar a velocidade dos vídeos.  Os vídeos no Vimeo estão disponíveis para baixar. No texto que acompanha cada vídeo se encontram os hiperlinks para as obras literárias relevantes.</S.Texto>
                <S.BlocoFooter>
                  <TraducaoMini tamanho="m" traducao={youtube}/>
                  <Button href="https://www.youtube.com/playlist?list=PL1FZ-Ods_um2_gOYoasJB6G5baeZes_LJ">
                    Livro em libras - YouTube
                  </Button>
                </S.BlocoFooter>
                <S.BlocoFooter> 
                  <TraducaoMini tamanho="m" traducao={vimeo}/>
                  <Button href="https://vimeo.com/showcase/6790274">
                    Livro em libras - Vimeo
                  </Button>
                </S.BlocoFooter>
              </S.Bloco>
            </S.GrupoBloco>
          </S.Wrapper>
        </Container>
      </BgAzul>
    </>
  )
}

export default HomeLivroLibras
