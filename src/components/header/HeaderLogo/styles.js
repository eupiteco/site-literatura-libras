import styled from 'styled-components'
import {cores} from '../../../styles/theme'
import {mediaQueries} from '../../../styles/theme'

const {azulClaro} = cores

export const Wrapper = styled.div`
  display: flex;
  align-items: flex-end;
`

export const Titulo = styled.h1`
  color: black;
  display: inline-block;
  font-size: 18px;
  line-height: 14px;
  font-weight: 700;
  margin-bottom: 8px;

${mediaQueries.lgUp} {
    font-size: 36px;
    line-height: 40px;
    border-bottom: 2px solid ${azulClaro};
  }
`

export const TagLine = styled.p`
  color: black;
  font-size: 12px;
  line-height: 9px;
  font-weight: 300;
  font-family: 'Source Serif Pro', serif;

  ${mediaQueries.lgUp} {
    font-size: 24px;
    line-height: 28px;
  }
`
