import React from 'react'
import { Link } from 'gatsby'
import TraducaoMini from '../../shared/TraducaoMini'
import { useTraducao } from '../../../context/TraducaoContext'
import * as S from './styles'

const HeaderLogo = () => {
  const { logo } = useTraducao()
  return (
    <Link to="/">
      <S.Wrapper>
        <TraducaoMini tamanho="g" traducao={logo}/>
        <div style={{marginLeft: 16}}>
          <S.Titulo>Literatura em Libras</S.Titulo>
          <S.TagLine>de Rachel Sutton-Spence</S.TagLine>
        </div>
      </S.Wrapper>
    </Link>
  )
}

export default HeaderLogo
