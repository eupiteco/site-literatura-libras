import styled from 'styled-components'
import { cores } from '../../../styles/theme'
import { Link } from 'gatsby'
import {mediaQueries} from '../../../styles/theme'
import BackgroundImage from 'gatsby-background-image'

import { 
  Menu,
  MenuItem,
  MenuButton,
} from '@szhsin/react-menu'

const headerItemStyle = `
  display: flex;
  flex-direction: column;
  align-items: center;
  & > div {
    color: black;
    margin-top: 8px;
    font-size: 18px;
    font-family: "Source Sans Pro", sans-serif;
  }
  &.active > div {
    font-weight: 600;
    color: ${cores.azulMedio};
  }
`

export const Nav = styled.nav`
  display: flex;
  align-items: center;
  margin: auto 0;

`
export const NavItem = styled.div`
  margin: 0 8px;
  ${mediaQueries.lgDown} {
    display: none;
  }
`
export const HeaderLink = styled(Link)`
  ${headerItemStyle}
`

export const MenuLink = styled(HeaderLink)`
  flex-direction: row;
`

export const MenuText = styled.div`
  padding-left: 12px;
`

export const HeaderMenu = styled.div`
  ${headerItemStyle}
`

export const ExternalLink = styled.a`
  display: flex;
  align-items: center;
  & > div {
    color: black;
    margin-top: 8px;
    font-size: 18px;
    font-family: "Source Sans Pro", sans-serif;
  }
  &.active > div {
    font-weight: 600;
    color: ${cores.azulMedio};
  }
`

export const Button = styled(MenuButton)`
  border: 0;
  color: black;
  padding: 0;
  line-height: 1;
  &:hover {
    background: transparent;
  }
`

export const StyledMenu = styled(Menu) `
  top: 10px;
  margin-top: 20px;
  border-radius: 0;
  background: #FFFFFFee;
  padding: 0;
`

export const Item = styled(MenuItem)`
  min-width: 300px;
  background: transparent;
  margin: 0 8px;
  padding: 12px 16px;

  &:not(:last-child) {
    border-bottom: 2px solid ${cores.azulMedio};
  }
`

export const MobileButton = styled.button`
  color: #333;
  font-size: 24px;
  background: transparent;
  padding: 20px;
  border: none;
  ${mediaQueries.lgUp} {
    display: none;
  }
`

export const CloseMobileButton = styled(MobileButton)`
  position: absolute;
  top: 12px;
  right: 0;
`

export const MobileNavPanel = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  right: 0;
  height: 100vh;
  box-shadow: -10px 0 10px rgba(0, 0, 0, 0.3);
  transform: translateX(${p => p.open ? "0" : "101%"});
  transition: transform 0.2s linear;
  background-repeat: no-repeat;
  ${mediaQueries.lgUp} {
    display: none;
  }
`

export const MobileNavBg = styled(BackgroundImage)`
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  height: 100vh;
  padding: 32px 0;
  background: ${cores.azulClaro}; 
  
  :before {
    background-repeat: no-repeat;
  }
`

export const MobileNav = styled.div `
    list-style: none;

    li {
      border-bottom: 1px solid #fff;
      font-size: 18px;
      font-weight: 600;

      &:last-child {
        border-bottom: 0;
      }
      & > a {
        display: block;
        padding: 24px 64px;
        color: black;
      }
    }
` 

