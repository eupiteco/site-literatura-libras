import React, {useState, useRef, useEffect} from 'react'
import { Link, useStaticQuery, graphql } from 'gatsby'
import TraducaoMini from '../../../components/shared/TraducaoMini'
import { useTraducao } from '../../../context/TraducaoContext'
import * as S from './styles'
import '@szhsin/react-menu/dist/index.css';

const NavLink = ({children, traducao, ...props}) => (
  <S.HeaderLink {...props} activeClassName="active">
    <TraducaoMini tamanho="p" traducao={traducao} />
    <div>{children}</div>
  </S.HeaderLink>
)

const MenuLink = ({children, traducao, ...props}) => (
  <S.MenuLink {...props}>
    {traducao && <TraducaoMini tamanho="p" traducao={traducao} />}
    <S.MenuText>{children}</S.MenuText>
  </S.MenuLink>
)

const HeaderMenu = ({children, traducao}) => (
  <S.HeaderMenu>
    <TraducaoMini tamanho="p" traducao={traducao} />
    <div>{children}</div>
  </S.HeaderMenu>
)
const ExternalLink = ({children, traducao, ...props}) => (
  <S.ExternalLink {...props}>
    <TraducaoMini tamanho="p" traducao={traducao} />
    <S.MenuText>{children}</S.MenuText>
  </S.ExternalLink>
)

function handleClickOutside(ev, ref, callBack) {
  if (ref.current && !ref.current.contains(ev.target)) callBack()
}

const HeaderNav = () => {

  const [ openMobile, setOpenMobile ] = useState(false)
  const openMenu = () => setOpenMobile(true)
  const closeMenu = () => setOpenMobile(false)
  const wrapperRef = useRef(null)
  const {principal, livro, libras, vimeo, youtube, sobre, online, download} = useTraducao()

  useEffect(() => {
    document.addEventListener('mouseup', (e) => handleClickOutside(e, wrapperRef, closeMenu))
    document.addEventListener('keyup', (e) => { if (e.key === 'Escape') closeMenu() })
    return () => {
      document.removeEventListener('mouseup', (e) => handleClickOutside(e, wrapperRef, closeMenu))
      document.removeEventListener('keyup', (e) => { if (e.key === 'Escape') closeMenu() })
    }
  }, [wrapperRef])

  const {bg} = useStaticQuery(graphql`
    query {
      bg: file(relativePath: { eq: "fundo-menu-mobile.png" }) {
        childImageSharp {
          fluid(maxWidth: 2400, quality: 90) {
            ...GatsbyImageSharpFluid_tracedSVG
          }
        }
      }
    }
  `)

  return (
    <>
      <S.Nav>
        <S.NavItem>
          <NavLink to="/" traducao={principal}>Home</NavLink>
        </S.NavItem>
        <S.NavItem>
          <S.StyledMenu menuButton={<S.Button><HeaderMenu traducao={livro}>O Livro</HeaderMenu></S.Button>}>
            <S.Item>
              <MenuLink to="/leitura-online" traducao={online}>
                Ler o livro online
              </MenuLink>
            </S.Item>
            <S.Item>
              <MenuLink to="/leitura-impressa" traducao={download}>
                Baixar o livro em PDF
              </MenuLink>
            </S.Item>
          </S.StyledMenu>
        </S.NavItem>
        <S.NavItem>
          <S.StyledMenu menuButton={<S.Button><HeaderMenu traducao={libras}>Libras</HeaderMenu></S.Button>}>
            <S.Item>
              <ExternalLink target="_blank" traducao={youtube} href="https://www.youtube.com/playlist?list=PL1FZ-Ods_um2_gOYoasJB6G5baeZes_LJ">
                Acesso ao canal no YouTube
              </ExternalLink>
            </S.Item>
            <S.Item>
              <ExternalLink target="_blank" traducao={vimeo} href="https://vimeo.com/showcase/6790274">
                Acesso ao canal Vimeo
              </ExternalLink>
            </S.Item>
          </S.StyledMenu>
        </S.NavItem>
        <S.NavItem>
          <NavLink to="/#sobre" traducao={sobre}>Sobre</NavLink>
        </S.NavItem>
        <S.MobileButton onClick={openMenu}>
          <span>&#8801;</span>
        </S.MobileButton>
      </S.Nav>
      <S.MobileNavPanel open={openMobile} ref={wrapperRef}>
        <S.MobileNavBg fluid={bg.childImageSharp.fluid} Tag="div">
          <S.CloseMobileButton onClick={closeMenu}>
            <span>&times;</span>
          </S.CloseMobileButton>
          <S.MobileNav>
            <li><Link to="/" activeClassName="active">Home</Link></li>
            <li><Link to="/leitura-online" activeClassName="active">Ler o livro online</Link></li>
            <li><Link to="/leitura-impressa" activeClassName="active">Baixar o livro em PDF</Link></li>
            <li><a href="https://www.youtube.com/playlist?list=PL1FZ-Ods_um2_gOYoasJB6G5baeZes_LJ" target="_blank" rel="noreferrer">Livro em libras - YouTube</a></li>
            <li><a href="https://vimeo.com/showcase/6790274" target="_blank" rel="noreferrer">Livro em libras - Vimeo</a></li>
          </S.MobileNav>
        </S.MobileNavBg>
      </S.MobileNavPanel>
    </>
  )
}

export default HeaderNav
