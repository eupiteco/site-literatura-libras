import React from 'react'
import styled from 'styled-components'
import {mediaQueries} from '../../styles/theme'
import "fontsource-source-serif-pro/300.css"
import "fontsource-source-sans-pro/400.css"
import "fontsource-source-sans-pro/600.css"
import "fontsource-source-sans-pro/700.css"
import "fontsource-source-sans-pro/900.css"
import {ContentProvider} from "../../context/ContentContext"
import {TraducaoProvider} from "../../context/TraducaoContext"
import GlobalStyles from '../../styles/global-styles'
import Header from '../sections/Header'
import Footer from '../sections/Footer'

const ContentWrapper = styled.div`
  padding-top: 71px;
  ${mediaQueries.lgUp} {
    padding-top: 130px;
  }
`
const Layout = ({children}) => {

  return (
    <>
      <TraducaoProvider>
        <GlobalStyles />
        <Header />
        <ContentProvider>
          <ContentWrapper>
            {children}
          </ContentWrapper>
        </ContentProvider>
        <Footer />
      </TraducaoProvider>
    </>
  )
}

export default Layout
