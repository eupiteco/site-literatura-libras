import React, {useEffect} from 'react'
import styled from 'styled-components'
import { mediaQueries } from '../../../styles/theme'
import Container from "../../../components/Container"
import Sumario from '../../../components/leitura-online/Sumario'
import offsetScript from '../../../../static/scripts/offsetAnchor'
import { useLocation } from "@reach/router"

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column-reverse;
  ${mediaQueries.lgUp} {
    margin-top: 24px;
    flex-direction: row;
  }
`

const Pagina = ({impresso}) => {

  const location = useLocation()

  useEffect(() => {
    const hash = location.hash !== ""
    offsetScript(hash, true)
    //eslint-disable-next-line
  }, [])

  return (
    <>
      <Container>
        <Wrapper>
          <Sumario impresso={impresso}/>
        </Wrapper>
      </Container>
    </>
  )
}

export default Pagina
