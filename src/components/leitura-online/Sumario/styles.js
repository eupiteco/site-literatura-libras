import styled from 'styled-components'

export const Wrapper = styled.div`
`

export const IconWrapper = styled.div`
  display: inline-block;
  padding-bottom: 12px;
  margin-left: 12px;
`

export const TituloSessao = styled.div`
  display: flex;
  align-items: flex-end;
  margin-bottom: 46px;
`
