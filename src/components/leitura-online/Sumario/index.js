import React from 'react'
import PropTypes from 'prop-types'
import { useContent } from '../../../context/ContentContext'
import {cores} from '../../../styles/theme'
import { TituloSublinhado } from '../../../styles/typography' 
import DownloadFile from '../../../assets/icons/DownloadFile'
import Parte from '../Parte'
import * as S from './styles'

const Sumario = ({impresso}) => {

  const conteudo = useContent()

  return (
    <S.Wrapper>
      {impresso ? (
        <S.TituloSessao>
          <TituloSublinhado>Download do Livro completo</TituloSublinhado>
          <a href="http://files.literaturaemlibras.com/Literatura_em_Libras_Rachel_Sutton_Spence.pdf" target="_blank" rel="noreferrer">
            <S.IconWrapper>
              <DownloadFile color={cores.azulMedio} large />
            </S.IconWrapper>
          </a>
        </S.TituloSessao>
      ) : (
        <S.TituloSessao>
          <TituloSublinhado>Leitura online</TituloSublinhado>
        </S.TituloSessao>
      )}
      {conteudo.map((parte, i) => <Parte conteudo={parte} key={i} impresso={impresso}/>)}
    </S.Wrapper>
  )
}

Sumario.propTypes = {
  impresso: PropTypes.bool,
}

export default Sumario
