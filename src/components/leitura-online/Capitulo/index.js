import React from 'react'
import PropTypes from 'prop-types'
import {
  AccordionItem,
  AccordionItemHeading,
  AccordionItemState,
} from 'react-accessible-accordion'
import Chevron from '../../../assets/icons/Chevron'
import DownloadFile from '../../../assets/icons/DownloadFile'
import * as S from './styles'

const Capitulo = ({conteudo, paleta, impresso}) => {

  const { slug, capitulo, titulo, headings, pdf } = conteudo
  const { corClara, corMedia} = paleta
  return impresso ? (
    <S.CapituloWrapper color={corMedia} href={pdf} target="_blank" rel="noreferrer">
      <span>
        {conteudo.parte !== 0 &&  capitulo + "."} {titulo}
      </span>
        <DownloadFile color={corMedia} />
    </S.CapituloWrapper>
  ) : (
    <AccordionItem>
      <S.CapituloWrapper color={corMedia}>
        <S.Capitulo to={`/leitura-online${slug}`} color={corMedia}>
          {conteudo.parte !== 0 &&  capitulo + "."} {titulo}
        </S.Capitulo>
        <AccordionItemHeading>
          <S.ItemButton style={{cursor: "pointer"}}>
            <AccordionItemState>
              {({expanded}) => (
                <S.AccordionToggle color={corMedia} expanded={expanded} >
                  <Chevron color={corMedia}/>
                </S.AccordionToggle>
              )}
            </AccordionItemState>
          </S.ItemButton>
        </AccordionItemHeading>
      </S.CapituloWrapper>
      <S.ItemPanel>
        { headings
            .filter( ({depth}) => depth === 3 || depth === 4 )
            .map( ({value, id, depth}, i) => depth === 3 ? (
              <S.Cabecalho key={i} to={`/leitura-online${slug.slice(0, -1)}#${id}`} color={corMedia}>{value}</S.Cabecalho>
            ) : (
              <S.SubCabecalho key={i} to={`/leitura-online${slug.slice(0, -1)}#${id}`} color={corClara}>{value}</S.SubCabecalho>
            )
        )}
      </S.ItemPanel>
    </AccordionItem>
  )
}

Capitulo.propTypes = {
  capitulo: PropTypes.any,
  paleta: PropTypes.exact({
    corBase: PropTypes.string,
    corMedia: PropTypes.string,
    corClara: PropTypes.string,
  })
}
export default Capitulo
