import styled, { keyframes } from 'styled-components'
import { Link } from 'gatsby'
import {
  AccordionItemPanel,
  AccordionItemButton,
} from 'react-accessible-accordion'

export const CapituloWrapper = styled.a`
  display: flex;
  justify-content: space-between;
  border-bottom: solid 1px;
  border-color: ${p => p.color};
  padding-bottom: 8px;
  margin-top: 16px;
  margin-bottom: 16px;
  font-size: 16px;
  line-height: 1.2;
  font-weight: 700;
  color: ${p => p.color};
  text-transform: uppercase;
`
export const Capitulo = styled(Link)`
  color: ${p => p.color};
`

export const Cabecalho = styled(Link)`
  display: block;
  margin-bottom: 12px;
  font-size: 16px;
  line-height: 1.2;
  font-weight: 600;
  color: ${p => p.color};
`

export const SubCabecalho = styled(Cabecalho)`
  padding-left: 24px;

`

export const ItemButton = styled(AccordionItemButton)`
  cursor: pointer;
`

export const AccordionToggle = styled.div`
  display: flex;
  width: 32px;
  height: 32px;
  border: 2px solid;
  border-color: ${p => p.color};
  border-radius: 99px;
  align-items: center;
  justify-content: center;
  transform: rotateZ(${p => p.expanded ? "180deg" : "0"});
  transition: transform .2s linear;
`

const Fadein = keyframes`
  from { opacity: 0;}
  to { opacity: 1;}
`

export const ItemPanel = styled(AccordionItemPanel)`
  animation: ${Fadein} 0.2s ease-in;
  margin-bottom: 24px;
`
