import React from 'react'
import Image from 'gatsby-image'
import { Accordion } from 'react-accessible-accordion'
import Capitulo from '../Capitulo'
import { theme } from '../../../styles/theme'
import * as S from './styles'

const Parte = ({conteudo, impresso}) => {

  const {paleta, conteudo: { titulo}} = theme[conteudo[0].parte]
  const {corBase} = paleta
  return (
    <div style={{marginBottom: 64}}>
      {conteudo[0].parte !== 0 && (
        <div id={`parte-${conteudo[0].parte}`}>
          <S.CabecalhoContainer>
            <S.CabecalhoImagem>
              <Image fluid={conteudo[0].image} />
            </S.CabecalhoImagem>
            <S.Texto color={corBase}>
              <S.CabecalhoParte>Parte {conteudo[0].parte} -&nbsp;</S.CabecalhoParte>
              <div>
                {titulo}
              </div>
            </S.Texto>
          </S.CabecalhoContainer>
        </div>
      )}
      {impresso ? (
        <>
          {conteudo.map((capitulo, i) => {
            return capitulo.content.pdf ? (
              <Capitulo paleta={paleta} conteudo={{...capitulo.content, parte: capitulo.parte}} key={i} impresso={impresso}/>
            ) : null
          })}
        </>
      ) : (
        <Accordion
          allowMultipleExpanded
          allowZeroExpanded
        >
          {conteudo.map((capitulo, i) => {
            return(
              <Capitulo paleta={paleta} conteudo={{...capitulo.content, parte: capitulo.parte}} key={i}/>
            )
          })}
        </Accordion>
      )}
    </div>
  )
}

export default Parte
