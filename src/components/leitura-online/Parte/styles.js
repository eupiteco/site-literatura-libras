import styled from 'styled-components'
import {mediaQueries} from '../../../styles/theme'

export const CabecalhoContainer = styled.div`
  display: flex;
  flex-wrap: nowrap;
`
export const CabecalhoImagem = styled.div`
  min-width: 60px;
  margin-right: 8px;
  ${mediaQueries.mdUp} {
    min-width: 96px;
    margin-right: 16px;
  }
`

export const Texto = styled.h2`
  display: flex;
  flex-wrap: nowrap;
  font-size: 16px;
  line-height: 1.1;
  color: ${p => p.color};
  font-weight: 900;
  text-transform: uppercase;
  ${mediaQueries.mdUp} {
    font-size: 30px;
  }
`

export const CabecalhoParte = styled.div`
  min-width: max-content;
`
