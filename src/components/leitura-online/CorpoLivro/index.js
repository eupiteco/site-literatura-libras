import React, {useEffect} from 'react'
import {addTooltipListeners, removeTooltipListeners} from '../../../../static/scripts/tooltip'
import {addYoutubeListeners, removeYoutubeListeners} from '../../../../static/scripts/youtube-modal'
import {addVimeoListeners, removeVimeoListeners} from '../../../../static/scripts/vimeo-modal'
import {StyledCorpoLivro} from '../../../styles/typography'

const tooltip = `
  <div class="tooltip">
  </div>
`

const videoModal = `
  <div class="video-modal_backdrop vimeo">
    <iframe class="video-modal_iframe vimeo" src="" width="560" height="315" frame-border="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
    <button class="video-modal_close-button vimeo">&times;</button>
  </div>
  <div class="video-modal_backdrop youtube">
    <iframe class="video-modal_iframe youtube" width="560" height="315" src="" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <button class="video-modal_close-button youtube">&times;</button>
  </div>
`

const CorpoLivro = ({html, paleta}) => {

  useEffect(function () {
    addTooltipListeners()
    addYoutubeListeners()
    addVimeoListeners()
    return function() {
      removeTooltipListeners()
      removeYoutubeListeners()
      removeVimeoListeners()
    }
  },[html])

  return (
    <StyledCorpoLivro cores={paleta} dangerouslySetInnerHTML={{
        __html: html+ tooltip+videoModal
      }}
    ></StyledCorpoLivro>
  )
}

export default CorpoLivro
