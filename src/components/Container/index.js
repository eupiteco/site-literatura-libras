import React from 'react'
import styled from 'styled-components'
import {mediaQueries} from '../../styles/theme'

export const ContainerStyles = styled.div `
    width: 100%;
    padding-left: 16px;
    padding-right: 16px;
    margin-right: auto;
    margin-left: auto;
    
    ${mediaQueries.smUp} {max-width: 540px;}
    ${mediaQueries.mdUp}  {max-width: 720px;}
    ${mediaQueries.lgUp}  {max-width: 960px;}
    ${mediaQueries.xlUp}  {max-width: 1140px;}
    ${mediaQueries.xxlUp}  {max-width: 1320px;}
`


const Container = ({children}) => {
  
  return (
    <>
        <ContainerStyles>
            {children}
        </ContainerStyles>
    </>
  )
}

export default Container
