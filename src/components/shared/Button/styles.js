import styled from 'styled-components'
import videoMask from '../../../assets/traducoes/ video-mask.svg'

const tamanhos = {
  p: {
    width: 61,
    height: 57,
  },
  m: {
    width: 92,
    height: 87,
  },
  g: {
    width: 113,
    height: 105,
  },
}

const cssImagem = `
  height: 100% !important;
  width: auto !important;
  position: absolute;
  left: 50%;
  top: 0;
  transform: translate(-50%);
`
export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  &:hover {
    & .previa {
      display: none !important;
    }
  }
`

export const Hover = styled.div`
  ${props => tamanhos[props.tamanho]}
  position: relative;
  mask: url(${videoMask});
  mask-repeat: no-repeat;
  mask-size: contain;
  overflow: hidden;
`

export const Traducao = styled.img`
  ${cssImagem}
`
