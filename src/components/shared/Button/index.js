import React from 'react'
import styled from 'styled-components'
import { Link } from 'gatsby'
import {cores} from '../../../styles/theme'
import {mediaQueries} from '../../../styles/theme'


const {
  azulMedio,
  } = cores

const buttonStyle = `

    border: 1px solid ${azulMedio};
    background: #fff;
    color: ${azulMedio};
    border-radius: 50px;
    padding: 13px 24px;
    text-align: center;
    font-size: 18px;
    font-weight: 600;
    -webkit-box-shadow: 1.3px 1.5px 4.7px 0.4px rgba(0, 0, 0, 0.41); 
    box-shadow: 1.3px 1.5px 4.7px 0.4px rgba(0, 0, 0, 0.41);
    display: block; 


    ${mediaQueries.lgUp} {
      display: inline-block;
      padding: 24px 32px;
      border-radius: 20px;
      margin: 16px;
      width: 100%;
      border: 2px solid ${azulMedio};
      background: ${azulMedio};
      color: #fff;

      &:hover {
        background: #fff;
        color: ${azulMedio};
      }
    }

`
export const LinkStyles = styled(Link)`${buttonStyle}`
export const AStyles = styled("a")`${buttonStyle}`

const Button = ({children, ...props}) => {
  
  return props.to ? (
        <LinkStyles {...props}>
            {children}
        </LinkStyles>
  ) : (
        <AStyles {...props}>
            {children}
        </AStyles>
  )
}


export default Button
