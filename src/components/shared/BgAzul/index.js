import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import * as S from './styles'

const BgAzul = ({children}) => {

  const {bgDesktop, bgMobile } = useStaticQuery(graphql`
    query {
      bgDesktop: file(relativePath: { eq: "mancha-fundo.png" }) {
        childImageSharp {
          fluid(maxWidth: 2400, quality: 90) {
            ...GatsbyImageSharpFluid_tracedSVG
          }
        }
      }

      bgMobile: file(relativePath: { eq: "mancha-fundo-mobile.png" }) {
        childImageSharp {
          fluid(maxWidth: 2400, quality: 90) {
            ...GatsbyImageSharpFluid_tracedSVG
          }
        }
      }
    }
  `)

  return (
    <>
      <S.StyledBg>
        <S.BgContainer>
          <S.BgDesktop fluid={bgDesktop.childImageSharp.fluid} Tag="div"></S.BgDesktop>
        </S.BgContainer>
        <S.BgContainer>
          <S.BgMobile fluid={bgMobile.childImageSharp.fluid} Tag="div"></S.BgMobile>
        </S.BgContainer>
        <S.ContentContainer>
          {children}
        </S.ContentContainer>
      </S.StyledBg>
    </>
  )
}

export default BgAzul
