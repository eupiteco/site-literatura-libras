import styled from 'styled-components'
import {mediaQueries} from '../../../styles/theme'
import BackgroundImage from 'gatsby-background-image'

export const StyledBg = styled.div`
  width: 100vw;
  position: relative;
`

export const ContentContainer = styled.div`
  padding: 30px 0;
  ${mediaQueries.mdUp} {
    padding: 200px 40px;
  }
  
  z-index: 100;

  * {
    z-index: 100;
  }
`


export const BgDesktop = styled(BackgroundImage) `
  display: none;
  background-size: cover;
  background-position: center;
  height: 100%;

  ${mediaQueries.mdUp} {
    display: block;
  }
`


export const BgMobile = styled(BackgroundImage) `
  display: block;
  background-size: cover;
  background-position: center;
  height: 100%;

  ${mediaQueries.mdUp} {
    display: none;
  }
`


export const BgContainer = styled.div`
  display: block;
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
`