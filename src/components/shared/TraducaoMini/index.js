import React from 'react'
import PropTypes from 'prop-types'
import * as S from './styles'

const TraducaoMini = ({traducao,tamanho}) => {
  return (
    <S.Wrapper tamanho={tamanho}>
        <S.Traducao fluid={traducao} />
    </S.Wrapper>
  )
}

TraducaoMini.propTypes = {
  tamanho: PropTypes.oneOf(["p", "m", "g"]).isRequired,
}

export default TraducaoMini
