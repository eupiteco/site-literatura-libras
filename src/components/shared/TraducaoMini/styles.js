import styled from 'styled-components'
import Img from 'gatsby-image'
import { mediaQueries } from '../../../styles/theme'
import videoMask from '../../../assets/traducoes/ video-mask.svg'

const tamanhos = {
  pMobile: {
    minWidth: 44,
    height: 42,
  },
  mMobile: {
    minWidth: 52,
    height: 48,
  },
  gMobile: {
    minWidth: 52,
    height: 48,
  },
  p: {
    minWidth: 61,
    height: 57,
  },
  m: {
    minWidth: 92,
    height: 87,
  },
  g: {
    minWidth: 113,
    height: 105,
  },
}

export const Wrapper = styled.div`
  ${p => tamanhos[`${p.tamanho}Mobile`]}
  position: relative;
  mask: url(${videoMask});
  mask-repeat: no-repeat;
  mask-size: contain;
  overflow: hidden;
  ${mediaQueries.lgUp} {
    ${p => tamanhos[p.tamanho]}
  }
`

export const Traducao = styled(Img)`
  height: 100% !important;
  width: auto !important;
  position: absolute;
  left: 50%;
  top: 0;
  transform: translate(-50%);
`
