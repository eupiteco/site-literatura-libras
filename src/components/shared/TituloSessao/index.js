import React from 'react'
import styled from 'styled-components'
import {mediaQueries} from '../../../styles/theme'
import TraducaoMini from '../../shared/TraducaoMini'

const Wrapper = styled.div` 
  display: flex;
  align-items: flex-end;
  margin-bottom: 24px;

  h2 {
    font-size: 22px;
    font-weight: 600;
    line-height: 22px;
    margin-bottom: 4px;
    margin-left: 12px;
    ${mediaQueries.lgUp} {
      display: inline-block;
      padding-bottom: 8px;
      margin-bottom: 8px;
      margin-left: 22px;
      font-size: 46px;
      line-height: 30px;
      border-bottom: 2px solid;
      border-color: ${p => p.color};
      
    }
  }

  ${mediaQueries.lgUp} {
    margin-bottom: 56px;
  }

`

const TituloSessao = ({children, color, traducao}) => {
  return (
    <Wrapper color={color}>
      {traducao && <TraducaoMini tamanho="g" traducao={traducao} />}
      <h2>{children}</h2>
    </Wrapper>
  )
}

export default TituloSessao
