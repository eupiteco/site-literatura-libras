import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const VideoContainer = styled.div`
  box-shadow: 0 4px 6px 0 rgba(0, 0, 0, 0.2)
  margin-bottom: -3px;
  position: relative;
  padding-bottom: 56.25%;
  padding-top: 30px;
  height: 0;
  overflow: hidden;
` 

const Video = styled.iframe`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`

const EmbededVideo = ({type, id,}) => {

  return (
    <VideoContainer>
      {type === "vimeo" ? (
        <Video title="vimeo-embed" src={`https://player.vimeo.com/video/${id}?transparent=0`} frameBorder="0" allow="autoplay; fullscreen; picture-in-picture" allowFullScreen></Video>
      ) : (
        <Video title="youtube-embed" style={{marginBottom: -3}} src={`https://www.youtube.com/embed/${id}`} frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></Video>
      )}
    </VideoContainer>
  )
}

export const embeddedVideoType = {
  type: PropTypes.oneOf(['vimeo', 'youtube', '']).isRequired,
  id: PropTypes.string.isRequired,
}

EmbededVideo.propTypes = embeddedVideoType

export default EmbededVideo
