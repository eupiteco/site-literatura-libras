---
capitulo: 21
parte: 4
titulo: As mulheres e a literatura em Libras
pdf: "http://files.literaturaemlibras.com/CP21_As_mulheres_e_a_literatura_em_Libras.pdf"
video: gp6GWm748i8
---

## 21. As mulheres e a literatura em Libras

<div class="float"></div>

![objetivo](/images/objetivo-parte4.png)

### 21.1. Objetivo

Neste capítulo, investigaremos a participação das mulheres na literatura
em Libras e a sua contribuição à literatura, apesar da desigualdade
existente na representação delas em produções ao vivo e gravadas
disponibilizadas na internet. A literatura feita por mulheres surdas se
encaixa nas categorias de literatura surda, literatura em Libras,
literatura feminina e literatura brasileira (SUTTON-SPENCE, 2019).

<div class="float"></div>

![lista de videos](/images/videos-parte4.png)

<div class="lista-de-videos"></div>

Veremos exemplos de quatro vídeos:

* [*Antônio Silvino o Rei dos Cangaceiros*](#ytModal-57xLeX74Vu0)
(Leandro Gomes de Barros), traduzido por Klícia de Araújo Campos.

* *[As Brasileiras](#vimeoModal-242326425),* de Klícia
Campos e Anna Luiza Maciel.

* *[A Economia](#vimeoModal-267272909),* de Sara Theisen Amorim e
Ângela Eiko Okumura.

* *[Saci](#ytModal-4UBwn9242gA),* de Fernanda Machado.

### 21.2. Desigualdade na representação de mulheres na literatura em Libras 

Muitas vezes, não percebemos que as descrições de literatura têm uma
tendência sutil de serem dominadas pelos homens. Apesar de as mulheres
serem aproximadamente 50% da humanidade, eles aparecem muito mais nas
coleções de literatura em línguas de sinais, nas pesquisas sobre essa
literatura e nas redes sociais, onde encontramos vídeos das produções
literárias. Um levantamento de algumas antologias, coleções e de alguns
eventos mostra um pouco da situação atual da área da literatura em
Libras, onde vemos uma desigualdade histórica entre os gêneros, mas que
parece estar diminuindo atualmente no Brasil.

Sabemos que as mulheres não têm paridade de representação nas artes
performáticas. Lauzen (2017) afirma que, em 2014, apenas 12% dos
protagonistas nos 100 filmes mais assistidos de Hollywood eram mulheres.
Uma pesquisa sobre literatura infantil escrita na língua inglesa
(TAYLOR, 2018) apresenta um levantamento de quase 300 livros publicados
nos últimos 100 anos e mostra que 25% deles não tem personagens
femininas e apenas 14% as contemplam e, nesses casos, elas falam e têm
um papel de líder. Dos outros, 38% tem personagens femininas com falas,
mas com papel passivo e 23% tem personagens femininas, mas que não se
pronunciam em cena.

Nas línguas de sinais, vemos uma realidade parecida. O documentário
“[The Heart of the Hydrogen
Jukebox](#ytModal-aJ0Y-luT5_w)”, realizado por
Nathan Lerner e Feigel (2009) (Nathan Lerner é mulher, Feigel é homem),
descreve os pioneiros literários surdos que criaram poesias em ASL entre
as décadas de 1940 e 1980. O documentário mostra a importância das
pessoas que criaram e desenvolveram o gênero da literatura sinalizada.
Nove artistas masculinos são apresentados: Robert Panara, Bernard Bragg,
Patrick Graybill, Clayton Valli, Peter Cook, Jim Cohn, Allen Ginsberg, e
o duplo “Flying Words Project” formado por Peter Cook e Kenny Lerner
(homens). Apenas três artistas femininas são apresentadas: Dorothy
Miles, Ella Mae Lentz e Debbie Rennie. Há uma proporção de três homens
para cada mulher.

O site [Culturasurda.net](https://culturasurda.net/poesia/) é uma das
maiores coleções brasileiras on-line de produções em línguas de sinais,
com 78 poemas. Destes, 62% são apresentados por homens e 35% por
mulheres (mais 4% são apresentados pelos dois gêneros). A pesquisa de
doutorado de Janaína Peixoto (2016) apresenta um corpus de poesia em
Libras em que 16 dos 70 poemas foram interpretados por mulheres (23%).
Na coleção de poemas que formou a base da antologia poética em BSL
(língua de sinais britânica)[47](#tooltip) feita em 2012, cinco homens têm sua
própria *playlist* em comparação a três mulheres. Os homens também têm
mais exemplos de poesia postados. Dos 100 poemas no canal, 65% foram
feitos por homens e 35% por mulheres. Há uma proporção de dois poemas
masculinos para cada poema de mulher.

Sutton-Spence e colegas (2017) descreveram o papel de diversos pioneiros
de literatura em Libras. No artigo, vemos seis homens (Carlos Goés,
Silas Queiroz, Augusto Schallenburger, Nelson Pimenta, Bruno Ramos e
Sandro Pereira) e três mulheres (Marlene Prado, Kelly Piedade de Ávila e
Fernanda Machado). Há uma proporção de dois homens para cada mulher.

É muito mais comum encontrar artistas surdos masculinos participando
como convidados em festivais e eventos “ao vivo”. No “Festival de
Folclore Sinalizado: Os craques da Libras”, em 2014, organizado na
Universidade Federal de Santa Catarina – UFSC –, dos seis artistas
convidados para darem oficinas e apresentarem um show de poemas, cinco
eram homens (na Figura 12, da esquerda para a direita: Richard Carter,
Sandro Pereira, Rimar Romano, Nelson Pimenta e Bruno Ramos). Fernanda
Machado é a única mulher.

![Cartaz do primeiro festival de folclore sinalizado](/images/p4-c21-1.png)

<div class="legenda"></div>

Figura 12: Cartaz do primeiro festival de folclore sinalizado - 2014

<div class="legenda"></div>

Fonte da imagem: Martin Haswell, acervo particular.

No segundo Festival Internacional de Folclore Surdo, no ano de 2016 em
Florianópolis, as organizadoras se esforçaram muito para atingir uma
paridade de participação entre mulheres e homens. Na Figura 13, da
esquerda para a direita vemos: Atiyah Asmal (África do Sul), Rosani
Suzin (Brasil), Donna Williams (Reino Unido)**,** Susan Njeyiyana
(África do Sul), Silas Queiroz (Brasil), Marlene Prado (Brasil), Carlos
Alberto Goés (Brasil), Rosana Grasse (Brasil), Carolina Hessel (Brasil),
Ella Mae Lentz (EUA), Peter Cook (EUA), Renata Heinzelmann (Brasil) e
Johanna Mesch (Suécia), Richard Carter (Reino Unido). Isso mostra que,
com um esforço, pudemos atingir uma igualdade conforme esperado.

![segundo festival de folclore surdo](/images/p4-c21-2.png)

<div class="legenda"></div>

Figura 13: Cartaz do segundo festival de folclore surdo - 2016

<div class="legenda"></div>

Fonte da imagem: Martin Haswell, acervo particular.

Apesar de ser possível essa paridade, podemos questionar por que as
mulheres não têm maior visibilidade na literatura sinalizada. Esse é um
assunto que merece mais pesquisas, no entanto, podemos destacar e
enaltecer a contribuição das mulheres no Brasil para a literatura em
Libras, como artistas, autoras, professoras, pesquisadoras e curadoras.

### 21.3. Mulheres surdas autoras de textos escritos 

As mulheres surdas brasileiras sempre estiveram presentes nos textos
escritos de literatura surda, e têm ainda uma maior presença nas
produções autobiográficas e nas literaturas infantil e juvenil.

A respeito de autobiografias escritas em português, Müller e Karnopp
(2015), pesquisaram as narrativas de autoria surda. Das dez obras do
levantamento, oito foram escritas por autoras surdas (restando um autor
homem e uma antologia com contribuições de homens e mulheres).

A literatura infantil é frequentemente ligada a autoras, talvez por
causa da relação forte entre o cuidado materno e a educação das
crianças, tradicionalmente feitos por mulheres. Perry Nodelman (1988, p.32) afirma que “A literatura infantil é certamente uma atividade das
mulheres. A maior parte da escrita e edição de livros infantis é feita
por mulheres, a maioria dos bibliotecários infantis são mulheres e a
maioria dos estudiosos da literatura infantil são mulheres” (tradução
nossa).

Assim, esperamos encontrar nas pesquisas de livros infantis para surdos
muitos textos escritos por mulheres e vídeos de narrativas em Libras com
contadoras. Lodenir Karnopp (2008) encontrou um equilíbrio nos textos
escritos por homens e mulheres juntos (por exemplo, “Cinderela Surda” e
“Rapunzel Surda”). “A cigarra surda e as formigas” (OLIVEIRA; BOLDO, 2003) e “Tibi e Joca” (BISOL, 2001), ambos com público-alvo infantil,
foram escritos por mulheres.

As pioneiras em tradução literária começaram as traduções de literatura
para Libras no final do século XX. Hoje em dia, as traduções infantis
mais conhecidas talvez sejam as das Fábulas de Esopo, recontadas por
Nelson Pimenta (com Ana Regina Campello) em 1999. Porém, já em 1992, a
tradutora surda Marlene Pereira do Prado traduziu uma história infantil
com a pesquisadora Clélia Ramos, e as duas lançaram a tradução de “Alice
no País das Maravilhas” (RAMOS, 2000). Heloise Gripp traduziu
“Chapeuzinho Vermelho” para a coleção INES e, mais recentemente,
Carolina Hessel, no site [Mãos
Aventureiras](https://www.ufrgs.br/maosaventureiras/), conta muitas
histórias traduzidas em Libras.

### 21.4. As pioneiras da literatura em línguas de sinais

A história da literatura surda mundial ainda não foi muito pesquisada
(embora Mourão, 2016, descreva muito da história da literatura surda
brasileira). Porém, os pioneiros da literatura surda contavam com a
presença de mulheres como artistas surdas pioneiras. Apesar de, muitas
vezes, as publicações literárias de épocas passadas terem sido criadas
por homens, existem mulheres surdas que atuaram como precursoras no
campo literário. Por exemplo, já falamos d**o primeiro poema conhecido
em língua de sinais registrado em filme**, [A morte de
Minnehaha](http://hsldb.georgetown.edu/films/film-view.php?film=deathofminnehaha&signer=Erd).
Vale destacar, que foi produzido em 1913 por uma mulher, Mary Williamson
Erd, que era professora da escola de surdos *Michigan School for the
Deaf*.

A primeira **pesquisa linguística** sobre poesia de origem surda em
língua de sinais (por Klima e Bellugi, 1979) analisou os poemas criados
em ASL por uma mulher – Dorothy Miles. Dorothy era poeta, mas também
**professora** pioneira da literatura surda. Sabemos que, sem a
divulgação das informações sobre os mecanismos de poesia, a pesquisa e a
criação não podem se desenvolver. Outras mulheres, professoras, também
asseguraram a implantação da literatura surda nos contextos acadêmicos.
No Brasil, a professora pesquisadora Lodenir Karnopp implantou a
literatura surda no primeiro curso de Letras Libras a distância da UFSC,
em 2008. Ela contou um pouco sobre isso:

> Lembro que sugeri em uma reunião a proposta dessa disciplina. Isso
> estava relacionado com a experiência que tive na ULBRA, com a
> publicação dos livros "Cinderela Surda", Rapunzel Surda...
> Inicialmente me enviaram uma disciplina intitulada Literatura Visual,
> mas sugeri que o nome fosse Literatura Surda - e foi aceito. ... Meu
> objetivo era defender que o estudo de qualquer língua deve também
> contemplar o estudo da literatura daquela língua. Esse foi meu
> argumento naquela época. Língua e Literatura! (comunicação por e-mail,
> 2017).

Com essa disciplina, formaram-se mais de mil professores no Brasil na
área de literatura surda, e foi uma mulher que iniciou esse processo.

As **pesquisadoras** também são imprescindíveis para se estabelecer a
literatura surda no cânone literário de um país. Heidi Rose pesquisou a
literatura surda em ASL em 1992. Para dar outros exemplos, no Reino
Unido as pesquisadoras Rachel Sutton-Spence e Michiko Kaneko publicaram
o primeiro livro com foco inteiro na literatura surda e sinalizada
(SUTTON-SPENCE; KANEKO, 2016). Na Suécia, a professora surda Johanna
Mesch faz pesquisas na Universidade de Estocolmo junto a suas produções
literárias, criando uma coleção de poesia em língua de sinais sueca –
Swedish Sign Language *–* SSL. No Brasil, ao lado de pesquisadores
masculinos, podemos destacar as seguintes autoras (entre muitas outras):
Carolina Hessel Silveira, Fernanda Machado, Janaína Peixoto, Lodenir
Karnopp, Marilyn Mafra Klamt, Renata Heidermann e Ronice Müller de
Quadros.

Vimos, neste livro, a importância de registrar as produções literárias
em Libras, mas para a promoção da literatura é importante também
organizar os materiais de uma forma acessível a fim de se promover os
trabalhos dos poetas. Nesse sentido, no campo da **curadoria** se
destaca a importância das mulheres. Já falamos do documentário realizado
por Miriam Nathan Lerner, “[The Heart of the Hydrogen
Jukebox](#ytModal-aJ0Y-luT5_w)”; foi ela quem
entrou na biblioteca do Instituto Técnico Nacional dos Surdos, em
Rochester, nos EUA (em inglês “The National Technical Institute for the
Deaf”), pegou caixas de vídeos em VHS e assistiu a horas dessas
gravações já esquecidas para reconstruir a história da poesia em ASL e
criar aquele documentário – com a ajuda da bibliotecária Joan Naturale.

As mulheres no Brasil criaram coleções, como a de Janaína Peixoto
(2016), e a primeira antologia de poesia em Libras, de Fernanda Machado,
em 2018. Nesse mesmo ano começou outro projeto de antologia de
literatura em Libras, coordenado por Ronice Müller de Quadros. Com esses
recursos, professores, alunos, pesquisadores e poetas podem aprender
sobre a poesia sinalizada.

A respeito de **artistas surdos brasileiros**, ao lado dos pioneiros em
cada fase da nossa história da literatura em Libras sempre há mulheres:


<div class="lista-com-fases"></div>

1. Carlos Goés, **Marlene Prado** e Silas Queiroz
2. Augusto Schallenburger, **Kelly Piedade de Ávila**, Nelson
Pimenta
3. Bruno Ramos, **Fernanda Machado**, Sandro Pereira

Hoje, graças a essas mulheres, outras entraram no campo da literatura em
Libras.

### 21.5. Temas típicos e características da literatura feminina em Libras

As artistas surdas de Libras fazem literatura surda e feminina. Podemos
dizer que essas duas são bastante parecidas. Vimos, no capítulo 8, que
diversas pesquisas já mostraram que a literatura surda muitas vezes
aborda tópicos relacionados ao mundo surdo e à experiência surda,
falando de opressão, resistência e liberdade. Por outro lado, Nodelman
(1988, p.33) destacou que os aspectos característicos da escrita
feminina não são apenas uma resposta à repressão, “mas sim como uma
visão diferente, uma maneira alternativa de descrever a realidade”
(tradução nossa).

Sugerimos, então, que as narrativas e os poemas das artistas surdas
podem oferecer uma alternativa para que todos os surdos – homens ou
mulheres – possam ver a realidade do mundo surdo.

A literatura feminina é caracterizada por histórias em que o
protagonista é feminino e a maioria dos personagens é feminina,
incluindo a família e os amigos. Geralmente se concentram numa
protagonista mulher que enfrenta uma situação difícil que afeta a vida
das mulheres, como por exemplo: um conflito pessoal, um problema
familiar, ser mãe, a vida amorosa, os problemas de amizade ou conflitos
entre trabalho e vida pessoal. Dado isso, a literatura surda feminina
pode oferecer uma descrição alternativa da realidade do mundo das
mulheres surdas.

A subjetividade da literatura em Libras já foi comprovada e sabemos que
a apresentação dessa literatura exige a incorporação dos personagens. No
capítulo 7 abordamos que algumas obras focam no “eu” do poeta ou
contador da história. Os estudos críticos literários destacam claramente
que o “eu” do poeta não é o mesmo “eu” do autor. Assim, um autor
masculino pode criar uma obra literária na voz de um “eu” feminino, e
não há nada que impeça uma poeta surda de criar e apresentar um poema ou
história em Libras com o “eu” do poeta homem. Mas isso não é comum,
talvez por causa da visibilidade do corpo feminino da artista. Como a
literatura surda está “dentro do corpo” da atriz, quando o protagonista
é feminino, o corpo feminino da artista expõe diretamente a sua forma.

Nas obras das artistas surdas, vemos que a maioria das protagonistas é
de mulheres, em parte devido à importância do corpo na literatura surda
sinalizada (ROSE, 1992; 2006), em que o corpo feminino da artista surda
incorpora personagens femininas. Quando uma narrativa inclui personagens
femininas, não há necessidade de a artista indicar que o corpo mostra
uma mulher, porém quando existe uma dúvida, ela pode exagerar os traços
linguísticos e performáticos para identificar o ser feminino. Veremos
quatro exemplos disso.

Narrativas sinalizadas por mulheres podem apresentar o eu-poético (isto
é, a voz que fala no poema) explicitamente por meio do corpo feminino.
No poema [*As Brasileiras*](#vimeoModal-242326425), de Klícia Campos e
Anna Luiza Maciel, as duas poetas dizem “sou” brasileira, antes de cada
uma falar da outra “você é nordestina” e “você é paulistana” e
finalmente “eu sou paulistana” e “eu sou nordestina”. O poema é
apresentado de uma forma incorporada, onde as poetas mostram a
experiência de pertencerem a regiões brasileiras diferentes. Elas nunca
declaram que são mulheres, mas isso é percebido através do contexto e da
apresentação.

Por outro lado, no poema dueto [A Economia](#vimeoModal-267272909), de
Sara Theisen Amorim e Ângela Eiko Okumura, as poetas não declararam se o
“eu” é masculino ou feminino, embora os corpos das duas sugiram que
possam ser personagens femininas. A incorporação do protagonista e do
outro personagem é neutra para que o público não tenha a indicação sobre
o gênero do “eu” apresentado. Observamos que uma tradução do dueto para
o português exigirá uma decisão ao final quando se fala da demissão do
protagonista (“demitido!” ou “demitida!” e “falido” ou “falida”,
dependendo da escolha).

Quando o corpo da mulher mostra um ser não humano, isso não implica que
ela esteja representando uma mulher. *Saci,* de
Fernanda Machado, mostra uma onça, um papagaio e uma árvore. Fernanda
incorpora os três seres não humanos, e não há sugestão de que eles sejam
masculinos ou femininos. Porém, ela também incorpora o personagem do
Saci, que é um homem - e nisso vemos uma terceira possibilidade, a do
protagonista ser masculino e da artista ser mulher. Na performance de
Fernanda, apenas nosso conhecimento de folclore brasileiro nos leva a
entender que ela incorpora um personagem masculino.

Em contrapartida, na tradução do poema de cordel “[Antônio Silvino o Rei dos
Cangaceiros](#ytModal-57xLeX74Vu0)”,
de Leandro Gomes de Barros, Klícia de Araújo Campos incorpora o
protagonista masculino Antônio Silvino. Sendo ele um cangaceiro macho, a
tradutora exagera os movimentos do corpo e a expressão facial para
superar a suposição natural de que o corpo da mulher mostra um corpo
feminino.

<div class="float"></div>

![resumo](/images/resumo-parte4.png)

### 21.6. Resumo

Vimos que as mulheres surdas no Brasil são bem representadas em algumas
esferas da literatura surda escrita, mas as artistas surdas ainda não
participam da literatura em Libras no mesmo grau em que os homens. No
entanto, elas têm sido pioneiras como professoras, pesquisadoras,
tradutoras e curadoras nas áreas de literatura em Libras (e em outras
línguas de sinais). O fato de o corpo ser feminino pode destacar
personagens femininas na poesia, dependendo do contexto, do conteúdo e
da forma da linguagem, o que não impede a artista surda de incorporar
personagens masculinos.

<div class="float"></div>

![atividade](/images/atividade-parte4.png)

### 21.7. Atividade

Assista a cinco obras artísticas em Libras criadas e apresentadas por
mulheres.

* Qual o assunto principal de cada obra? Elas têm mais a ver com a
literatura surda ou com a literatura feminina (ou com ambas; ou com
nenhuma)?

* As artistas incorporam personagens humanos masculinos ou femininos, ou
seres não humanos de qualquer (ou nenhum) gênero?

* Como você sabe o gênero do protagonista?

* As artistas incorporam os personagens masculinos? Como?

<span class="tooltip-texto" id="tooltip-47">
Hoje disponíveis no canal do YouTube <a href="https://www.youtube.com/user/signmetaphor/playlists">
    Signmetaphor</a> acessado em 19/03/2020
</span>

