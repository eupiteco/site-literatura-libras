---
capitulo: 14
parte: 3
titulo: Gêneros, tipos e formas de poesia em Libras
pdf: "http://files.literaturaemlibras.com/CP14_Generos_tipos_e_formas_de_poesia_em_Libras.pdf"
video: nLMtNCh88Tk
---

## 14. Gêneros, tipos e formas de poesia em Libras

<div class="float"></div>

![objetivo](/images/objetivo-parte3.png)

### 14.1. Objetivo

Neste capítulo, apresentaremos e refletiremos sobre alguns tipos de
poemas em línguas de sinais. Uma palavra muito presente nas discussões
acerca das categorias de poesia é “gênero”. Vimos nos capítulos 03 e 07
que esse conceito ainda não é bem definido, tampouco há uma
categorização “correta” ou “errada”, mas simplesmente “útil”. Para
conhecer uma seleção de poemas de tipos diferentes, traremos alguns
categorizados de acordo com seu estilo, sua forma, origem e seu
conteúdo.

O campo da poesia em Libras está crescendo muito rápido e ainda não está
bem delimitado. Com as mudanças atuais, fica complicado determinar se um
texto é ou não um poema, e fica ainda mais difícil inseri-lo em um
gênero de poesia. Muitos poetas e artistas criam o seu próprio estilo,
sem basear seus trabalhos numa tradição poética conhecida. Os
pesquisadores americanos Klima e Bellugi (1979) também afirmaram que o
mesmo aconteceu nos EUA nos anos 70. Cada vez que um artista criava uma
nova forma de poesia, levava um tempo para as pessoas aceitarem (ou não)
a produção. Em 1939, o surdo americano Eric Malzkuhn (conhecido como
“Malz”) criou um tipo de poesia para a tradução do poema
“Jabberwocky”[35](#tooltip) em ASL. Um trecho dessa tradução está disponível no
documentário “*[The Heart of the Hydrogen
Jukebox](#ytModal-aJ0Y-luT5_w)*”, no tempo de
00:12:42. Ele afirmou: “As pessoas ainda não estavam preparadas para
isso. Um terço do público achava que eu era louco, outro terço achava
que eu era um gênio. O último terço não conseguia se decidir se me
achava louco ou gênio”[36](#tooltip). O poeta Peter Cook relatou que “Antes de
Malz, a língua de sinais era muito formal e controlada. Malz era
completamente diferente, ele tomou outro caminho.”

Em 1995, o pesquisador Alec Ormsby afirmou que em ASL

> *nenhum conjunto de fórmulas poéticas tradicionais é transmitido de
> geração para geração. Vários usos padronizados e folclóricos da ASL
> são transmitidos, mas estes não são considerados textos poéticos, seja
> dentro da comunidade surda ou fora dela. Até agora, as estruturas da
> poesia em ASL, como Klima e Bellugi observam são, em geral,
> individuais e não convencionais (p. 169, tradução nossa). *

Ormsby observou que, por isso, os poetas de ASL “têm poucas convenções
que os unem”[37](#tooltip). Ele usou, na sua exposição, a palavra em inglês
“bind”, a qual remete a dois sentidos: unir e amarrar. As convenções
poéticas em qualquer cultura podem unir poetas, criando um gênero coeso
com convenções entendidas que apoiam e guiam ou podem os amarrar,
limitando as suas criatividades. Quase 25 anos depois dessa afirmação,
vemos que em Libras essa situação já mudou. Hoje, existem convenções que
unem os poetas, dando-lhes apoio e abrindo novos caminhos poéticos para
se seguir, mas que não devem “amarrá-los”.

<div class="float"></div>

![lista de videos](/images/videos-parte3.png)

<div class="lista-de-videos"></div>

Neste capítulo, vamos falar de muitos poemas. Já conhecemos a maioria,
mas também há alguns que ainda não vimos:

* *[Amar é também
silenciar](#ytModal-Oz5YqANdunU),* de Rafael
Lopes dos Santos (Rafael Lelo).

* *[Arrumar, Passear](#ytModal-uciVF5oMqkc),* de Jéssie Rezende.

* *[Árvore](#vimeoModal-267272296),* de André Luiz
Conceição.

* *[Ave 1 x 0 Minhoca](#vimeoModal-267277312),* de Marcos
Marquioto.

* *[As Brasileiras](#vimeoModal-242326425),* de Anna
Luiza Maciel e Klícia Campos.

* *[Cinco Sentidos](#ytModal-AyDUTifxCzg),* de Nelson Pimenta.

* *[Como Veio Alimentação](#ytModal-nMOTYprbYoY),* de Fernanda
Machado.

* *[A Economia](#vimeoModal-267272909),* de Sara Theisen Amorim e
Ângela Eiko Okumura.

* *[Feliz Natal](#ytModal-SSzlEALHo80),* de Rosana Grasse.

* *[Five Senses](#ytModal--qLcuxfdoYY),* de Paul Scott (em BSL).

* *[Fruit](#ytModal-l4yXP3Z4gqs),* em português “Frutas” – renga.

* *[Homenagem Santa Maria](#ytModal-9LtOP-LLx0Y)/RS,* de Alan
Henry Godinho.

* *[Jaguadarte](#ytModal-DSSXTh0wriU),*
de Aulio Nóbrega.

* *[Lei de Libras](#vimeoModal-267274663),* de Sara
Theisen Amorim e Anna Luiza Maciel.

* *[Lutas surdas](#ytModal-aOQx2YMj6Xc),* de Alan Henry Godinho.

* *[Mãos do Mar](#ytModal-Njy8l1Qnnko),* de Alan
Henry Godinho.

* *[Meu Ser é nordestino](#ytModal-t4SLooMDTiw),*
de Klícia Campos.

* [*A morte de Minnehaha*](http://hsldb.georgetown.edu/films/film-view.php?film=deathofminnehaha&signer=Erd), de Mary
Williamson Erd., 1913.

* *[Peixe](#ytModal-LEDC479z_vo),* de Renato Nunes.

* *[Poema dueto
curto](#ytModal-_FmzDgviY5s),* de
Daltro Roque Carvalho da Silva Junior e Victória Hidalgo Pedroni.

* *[Um poema
maluco](#ytModal-GnDTdgxLGoo),* de
Daltro Roque Carvalho da Silva Junior.

* *[Saci](#ytModal-4UBwn9242gA),* de Fernanda Machado.

* [*The Fastest Hand in the West*](#ytModal-YkRvss7OsGM), de Jake
Schwall.

* *[Tinder](#vimeoModal-267275098),* de Anna Luiza
Maciel.

* [*V & V*](#vimeoModal-325444221), de Fernanda Machado.

* *[Voo Sobre Rio](#ytModal-YaAy0cbjU8o),* de Fernanda Machado.

* *[World II](#ytModal-WuYo2d51lKA),* em português “O mundo II” -
renga.

### 14.2. Gêneros definidos pela origem dos poemas

Alguns gêneros de poesia em Libras vêm do folclore surdo, outros tipos
de poema surgem de tradições poéticas de outras culturas, seja da
cultura de poesia de ouvintes ou de surdos estrangeiros.

#### 14.2.1. Traduções

Os poemas traduzidos são de um gênero que é definido pela sua origem.
Muitas pessoas entendem que não é possível traduzir um poema porque (nas
famosas palavras atribuídas ao poeta americano Robert Frost) a poesia “é
aquilo que é perdido na tradução tanto em prosa como em verso” (tradução
nossa). É possível reproduzir um poema escrito de uma língua para outra
por meio da tradução, mas as formas poéticas, o efeito artístico das
palavras e os sentidos culturais serão tão distintos que o tradutor sabe
que não pode criar o mesmo efeito na nova língua por meio de palavras
equivalentes. Isso não impede a tradução de poemas entre diversas
línguas, e muitas dessas traduções enriquecem a cultura da língua alvo.
Geralmente, no Brasil, as traduções de poemas para o português são
feitas por poetas com habilidades nas tradições poéticas da cultura
brasileira. Os irmãos Haroldo e Augusto de Campos, por exemplo, eram
poetas que traduziam muitos poemas para a língua portuguesa.

Podemos ver poemas em Libras que são traduções. A língua de origem pode
ser o português ou outra língua de sinais. As traduções são feitas por
poetas surdos bilíngues ou por tradutores ouvintes. Atualmente, no
Brasil, não há muito a exigência de que um tradutor seja poeta e alguns
tradutores nem sabem bem as normas poéticas da comunidade surda. Por
isso, nem todas as traduções para a Libras são poéticas.

Historicamente, as traduções de poemas escritos eram muito importantes
para a expressão estética das línguas de sinais. Embora o conteúdo não
seja criado pelo poeta surdo, a forma da linguagem na tradução criou uma
forma de Libras poética que faz parte das raízes da poesia da Libras
atual. Por isso veremos elementos parecidos com os de poemas escritos em
outros gêneros de poesia em Libras. Antigamente, esse era o único tipo
de poesia feita por surdos em língua de sinais em alguns países porque
as pessoas acreditavam que poemas eram coisas que não pertenciam às
comunidades surdas. Até hoje, a tradução de poemas do português para a
Libras é um gênero feito por poetas surdos e tradutores (existem muitos
exemplos na internet). As traduções dão algumas sugestões sobre o que é
um poema em Libras.

O poema em sinais mais antigo já registrado é a tradução de um poema
escrito intitulado *[A morte de Minnehaha](http://hsldb.georgetown.edu/films/film-view.php?film=deathofminnehaha&signer=Erd)*. Foi
criado por Mary Williamson Erd em 1913, traduzido do poema *Hiawatha,*
escrito em inglês por Henry Longfellow. É uma interpretação que mescla
dança, teatro e ASL estética.

Já vimos que as traduções podem abrir novos caminhos para os poemas em
sinais. A tradução de *Jabberwocky* (em português, *Jaguadarte*) de
inglês para ASL, em 1939, liberou os artistas da época para
experimentarem com a forma. Nos anos 80, os alunos do colégio NTID, nos
EUA, brincavam com os sinais criando imagens semelhantes às imagens de
*Jabberwocky*. Um desses alunos era Peter Cook, um dos poetas de ASL
mais conhecidos hoje em dia. O poema de Malz também influenciou os
poetas brasileiros. O *Jaguadarte* em Libras,
interpretado por Aulio Nóbrega (do qual já falamos), foi inspirado na
tradução de Malz.

Mas ainda são poucas as traduções de poemas de outras línguas de sinais
para a Libras. Isso pode acontecer em parte porque muitos poemas têm uma
forma tão visual que o público os entende sem a necessidade de tradução.
Mas existem exemplos dessas traduções (ver
capítulo 08). O poema [*Cinco Sentidos*](#ytModal-AyDUTifxCzg)
em Libras é uma tradução feita por Nelson Pimenta de [*Five
Senses*](#ytModal--qLcuxfdoYY), poema de Paul Scott em BSL.

#### 14.2.2. Poemas-homenagem

Poemas-homenagem são categorizados pela origem. Nesses casos, uma pessoa
pode simplesmente copiar e reapresentar um poema de uma outra pessoa. Da
forma mais extrema é um desafio muito grande porque o sinalizante não
copia simplesmente os sinais, mas sim, a forma de apresentação deles,
com o ritmo, as expressões faciais e os movimentos do corpo parecidos
com os do original. Fazer esse tipo de homenagem é uma tarefa muito boa
para os aprendizes de poesia, porque na reprodução do poema eles
descobrem a complexidade e a riqueza da performance original.

No entanto, criar poemas-homenagem não significa apenas copiar um poema,
mas sim adaptá-lo ou tomá-lo como base para criar um novo poema. Isso
faz parte da tradição folclórica surda, para a qual as produções
culturais da comunidade são parte do coletivo e não pertencem a um
indivíduo apenas. As piadas e os contos de fadas, por exemplo, não têm
um autor nas tradições folclóricas brasileiras, e cada pessoa pode
recontar e adaptar esses artefatos culturais do seu próprio jeito. Para
a comunidade surda, os poemas também fazem parte da comunidade. Por
exemplo, [*Tree*](#ytModal-Lf92PlzMAXo), de Paul Scott, foi
adaptado por Fernanda Machado em [Saci](#ytModal-4UBwn9242gA) e
por André Luiz Conceição em
[*Árvore*](#vimeoModal-267272296). Os dois poemas são
diferentes do original, mas mantém conceitos linguísticos fundamentais
deste como o uso de classificadores e de incorporação, a criação de
classificadores originais, a manutenção do antebraço na horizontal (como
a terra enquanto imagina a presença da árvore) e a passagem do tempo
através do movimento circular do sol.

#### 14.2.3. Poemas malucos

Os poemas malucos também surgem de outro poema. É um gênero de poesia
lúdico e engraçado. Primeiro, o poeta escolhe um poema, por exemplo um
bem conhecido, recentemente apresentado ao público ou criado pelo
próprio poeta. Em seguida, são selecionados cinco sinais aleatoriamente.
O poeta pode solicitar os sinais ao público ou escolhê-los
aleatoriamente a partir de um dicionário. Depois, as regras do jogo
exigem que ele use esses cinco sinais no mesmo poema, criando um poema
com um sentido diferente. As imagens estranhas e até esquisitas geradas
por esse jogo são engraçadas e divertidas e criam seu próprio efeito
estético. Veja como exemplo, o poema maluco de Daltro Roque Carvalho da
Silva Junior. Começa com um [poema dueto
curto](#ytModal-_FmzDgviY5s)
(feito com Victória Hidalgo Pedroni). Com a
escolha aleatória das palavras *aerodeslizador, porta, vento,
dentaduras* e *cuecas* ele criou [um poema
maluco](#ytModal-GnDTdgxLGoo).

### 14.3. Gêneros de apresentação do poema

Os gregos antigos dividiam os poemas nas categorias lírico, dramático e
épico, dependendo da “voz” do poema, ou seja, da sua forma de
apresentação. Os líricos eram apresentados como se o poeta falasse e
compartilhasse seus pensamentos e sentimentos com o público. Nos poemas
dramáticos, os personagens falavam por si só e nos épicos havia uma
mistura entre a fala do narrador e a dos personagens. É importante
entender que, apesar de ter uma longa tradição, essa divisão muitas
vezes não é tão útil hoje para se categorizar a literatura em Libras. A
literatura surda em Libras não precisa seguir essas mesmas categorias,
porque os contextos socioculturais e históricos são diferentes de outros
contextos literários. O conceito de “voz”, por exemplo, é diferente, mas
vemos que existem poemas que têm elementos parecidos com esses gêneros
tradicionais.

#### 14.3.1. Poemas líricos

Os poemas líricos são muito comuns nas tradições literárias europeias,
porém os encontramos menos nas línguas de sinais. Esse gênero de poema
tem diversas características nas tradições de literaturas europeias.
Antigamente, eles eram cantados (e acompanhados historicamente por uma
lira ou harpa – daí surgiu o nome **lírico**), mas hoje podemos dizer
que um poema lírico é caracterizado estruturalmente por rimas e em
conteúdo pela expressão de opiniões, experiências ou emoções pessoais,
normalmente falando do “eu”. Muitas canções que fazem parte da música
popular brasileira, por exemplo, são compostas em primeira pessoa[38](#tooltip),
usam o pronome “eu” e falam muito do amor e dos desejos do personagem. É
importante entender que esse “eu” não se refere ao autor, nem ao cantor,
mas a um personagem que apresenta as emoções de um “eu-poético”. Quando
Elis Regina canta, em *Romaria,* “Sou caipira, Pirapora/Nossa Senhora de
Aparecida/Ilumina a mina escura e funda/O trem da minha vida”,
entendemos que ela não é uma caipira. Nem pensamos que o autor, Renato
Teixeira, seja o caipira que sofreu essas experiências. A letra da
música lírica fala das emoções de um “eu” imaginário caipira, que não as
do “eu” autor ou artista.

Os poemas líricos que seguem esses critérios não são tão comuns em
Libras. Muitos exemplos de poesia lírica em outras línguas de sinais vêm
dos anos 70 e 80, uma época em que a poesia sinalizada era mais
influenciada pelas línguas orais. Como vimos no capítulo 13 (e veremos
mais no capítulo 18), o conceito de rima não é igual em Libras e em
português e por isso a rima não faz parte necessariamente de um gênero
de poesia lírica em Libras. No entanto, existem poemas com o ponto de
vista de uma pessoa só e pode-se considerar que nesse caso o “eu” fala
das opiniões e dos desejos do próprio artista. Às vezes, o poeta não usa
o sinal <span style="font-variant:small-caps;">eu</span> e não está
claro se ele incorpora o “eu” ou outras pessoas. Por isso, até os poemas
em Libras que têm elementos líricos também têm aspectos mais dramáticos.

Em ASL, temos exemplos desse tipo de poema daquela época, como *Lone and
Sturdy Tree* (em português, “Árvore solitária e firme”), de Clayton
Valli (*[The Heart of the Hydrogen Jukebox](#ytModal-aJ0Y-luT5_w)* -
01:32:13), e *Defiance* (em português, “Desafio” ou “Rebeldia”), de
Dorothy Miles *[The Heart of the Hydrogen Jukebox](#ytModal-aJ0Y-luT5_w)* 00:24:26).

Um exemplo de poema lírico em Libras, que fala de assuntos mais pessoais
é [*Meu Ser é Nordestino*](#ytModal-t4SLooMDTiw), de Klícia Campos.Nele, a poeta descreve emoções como
angústia, sofrimento e opressão, mas também coragem, determinação e
amor. O poema usa configurações de mãos repetidas, criando um efeito
parecido com o conceito de rima em português. O poema dura 00:01:42, mas
é apenas nos últimos 16 segundos que ela usa um sinal que se refere ao
“eu”. No poema inteiro, entendemos que a poeta mostra o “eu” do poema,
mas apenas por incorporação. Não importa se a própria autora e atriz
Klícia andou a cavalo na caatinga ou não, com um fuzil nas costas, o
poema ainda assim mostra o “eu”.

Um poema lírico em Libras, que fala de amor, é *[Amar é também silenciar](#ytModal-Oz5YqANdunU),*, de Rafael
Lopes dos Santos (que usa o pseudônimo Rafael
Lelo). Embora ele não use o sinal <span
style="font-variant:small-caps;">eu,</span> o “eu” é apresentado pela
incorporação de um personagem que é o eu-poético.

Um tópico muito rico da poesia lírica em ASL feita nos anos 70 e 80 não
fala sobre emoções românticas e o amor, mas sim sobre aquelas ligadas à
experiência de se aprender a língua de sinais (podemos dizer que falam
do seu amor pelos sinais) e, por outro lado, das frustrações da educação
oralista. Vemos isso também nos poemas líricos em Libras. O poema
*[Mãos do Mar](#ytModal-Njy8l1Qnnko),*, de Alan
Henry Godinho, fala do sofrimento da experiência de se receber uma
educação baseada no oralismo e da libertação com a Libras. O poema
*[Lei de Libras](#vimeoModal-267274663),*, de Sara Theisen Amorim e Anna
Luiza Maciel, descreve as emoções e o prazer de quando as poetas usam a
Libras. Cada artista apresenta as emoções como se fossem suas por meio
de incorporação, mas entendemos que podem ser as de qualquer pessoa
surda brasileira que ame a Libras. O último sinal é “nós”, significando
que as emoções pertencem às personagens do poema.

*[Homenagem Santa Maria](#ytModal-9LtOP-LLx0Y)/RS,* de Alan Henry Godinho, é um poema elegíaco
que descreve o luto. Este é normalmente visto como um subgênero dos
poemas líricos.

#### 14.3.2. Poemas narrativos: dramáticos e épicos

Conforme Peters (2000) e Rose (1992 e 2006), os poemas em línguas de
sinais vêm da tradução de contação de histórias. Por isso, muitos poemas
contam-nas por meio de uma linguagem poética, mas com elementos mais
teatrais ou mais narrados. Poemas como *[Tinder](#vimeoModal-267275098)*, de Anna Luiza Maciel, e [*Voo Sobre
Rio*](#ytModal-YaAy0cbjU8o), de Fernanda Machado, contam
histórias curtas apresentadas com pouca narração. Este mescla a
descrição narrada do passeio da ave com a apresentação do encontro entre
os dois pássaros que namoram.

Poemas narrativos podem falar de assuntos históricos e de eventos
culturais. *[Lutas surdas](#ytModal-aOQx2YMj6Xc)*, de Alan Henry
Godinho, começa com a fala pessoal do surdo como cidadão brasileiro e
passa para uma narração dramática que conta sobre a luta da comunidade
para manter o Instituto Nacional de Educação de Surdos - INES. Por causa
do ritmo do poema, da repetição, dos sinais criativos e das metáforas
(entre outros elementos poéticos), dizemos que é um poema, porém que
narra mais e descreve menos.

### 14.4. Gêneros de poemas com parâmetros de língua delimitados 

Todos os poemas em Libras devem seguir algumas regras poéticas para
criar um efeito poético. No entanto, o gênero de alguns poemas é
definido pelo objetivo de seguir uma regra específica relacionada à
forma, por exemplo a limitação de um parâmetro dos sinais como o de uma
configuração de mão, movimento, local no espaço ou orientação.

Alguns poemas de forma delimitada são muito simples. Por exemplo, para
os aprendizes de poesia em Libras, é necessário apenas criar uma
história rítmica com alguns sinais da mesma configuração de mão. Um
exemplo de poema simples é [*Feliz Natal*](#ytModal-SSzlEALHo80), de
Rosana Grasse, em que a poeta usa a mesma configuração de mão, a da
letra F do alfabeto manual, para todos os sinais, embora normalmente
eles não usem essa configuração de mão para suas produções. É um poema
curto que ela utiliza para desejar às amigas um feliz natal. O sinal
<span style="font-variant:small-caps;">feliz</span> em Libras é um tipo
de empréstimo do português em que a configuração de mão tem a forma da
letra manual F e o movimento é o da letra Z. Usar a configuração de mão
em F no poema inteiro dá um tom de felicidade especial a todos os
sinais.

Outros poemas são mais extensos, mais criativos e mais desenvolvidos.
*[V & V](#vimeoModal-325444221)* de Fernanda Machado, é um poema
delimitado, que exige apenas o uso da configuração de mão na forma de V,
sempre com simetria de reflexo, com uma mão invertida em relação à
outra. Além de ser um poema estético agradável, cria um grande desafio
físico para a sua performance.

Em outros poemas, o poeta pode escolher um padrão de desenvolvimento
delimitado, que faz parte do método do poema. Em *[Cinco Sentidos](#ytModal-AyDUTifxCzg)*, de Nelson Pimenta, cada novo trecho deve focar
no próximo dedo da mão não dominante – do polegar ao indicador, ao
médio, ao anelar, até chegar no mindinho. Fugir do padrão quebra o
efeito poético. Em [*Como Veio
Alimentação*](#ytModal-nMOTYprbYoY), de Fernanda Machado, o
poema também segue o número de dedos, de um (indicador) a dois
(indicador e médio), numa ordem crescente até chegar nos cinco dedos com
a mão aberta.

Já vimos no capítulo 07 que a literatura em Libras inclui as histórias
de ABC, de números e de configuração de mão. Muitas delas são contadas
mais em prosa do que em forma poética. Porém, vimos que as diferenças
entre prosa e poesia estão dispostas numa escala, e algumas histórias
têm um tipo de ritmo, metáfora e criatividade que as colocam mais perto
do conceito de poesia. A repetição na história de ABC [*Arrumar, Passear*](#ytModal-uciVF5oMqkc), de Jéssie Rezende, tem um ritmo regular que
cria um efeito mais poético.

Outros jogos poéticos em Libras usam uma sequência de configurações de
mãos baseada nas letras manuais de uma palavra. O *acróstico* (em que
cada sinal tem a configuração da letra de uma palavra) é um exemplo
desse tipo de jogo. Normalmente, os sinais devem ter uma relação
temática com o sentido da palavra. Assim, os sinais num poema acróstico
de um gato, devem seguir as configurações de mão das letras G, A, T e O,
sempre falando do aspecto visual ou do comportamento do animal. O poeta
americano Clayton Valli (1995) criou alguns poemas complexos desse tipo,
como *Something not Right* (“Algo que não está certo”). Neste, cada
sinal tem a configuração de mão das letras que soletram “education”
(“educação”) e falam das falhas do sistema educacional dos surdos.

Em poemas de perspectivas múltiplas, o objetivo
é “colocar a linguagem no primeiro plano” e criar uma desfamiliarização
ao mostrar cenas habituais através de perspectivas fora do comum. Vimos,
no capítulo 10, que narrativas fílmicas em Libras criam diversas
perspectivas, dos planos mais aproximados aos mais distantes. Quando
esse recurso é produzido com outros elementos poéticos, tais como
brevidade, ritmo, metáfora e sinais criativos, tem-se o seu próprio
gênero de forma. O poema *[Tinder](#vimeoModal-267275098)*, de Anna
Luiza Maciel, é um exemplo. [*Ave 1 x 0
Minhoca*](#vimeoModal-267277312), de Marcos Marquioto,
é criado com o objetivo de seguir alternâncias entre a perspectiva da
minhoca e a da ave. O poema é curto e gera imagens fortes, com um ritmo
que repete as mesmas ações dos animais, cada vez com classificadores
diferentes.

Dentro da categoria de poemas com parâmetros de língua delimitados vemos
os poemas de VV, em que o poeta vai além da Libras e apresenta uma forma
de mímica adaptada para língua de sinais. Esses poemas, muitas vezes,
têm um ritmo forte com muita repetição e simetria, normalmente narram um
evento, mas também têm descrições detalhadas.

### 14.5. Gêneros de forma adaptados de outros gêneros

#### 14.5.1. Haicai
O haicai é um gênero em Libras que vem de um gênero da comunidade
ouvinte, mas que foi adaptado para as necessidades das línguas de
sinais. Esses poemas são curtos (algumas pessoas exigem o máximo de seis
sinais) e apresentam uma imagem visual muito forte, sem enredo, muitas
vezes falando da natureza ou de uma estação do ano. São de origem
japonesa, cujas regras exigem que sejam divididos em três versos, cada
um com um número específico de sílabas. Muitas vezes, os poetas quebram
as regras da forma para criar poemas com outros efeitos, mas elas
existem, e é um prazer ver o desvio da sua forma padrão.

Os haicais têm sido criados em ASL nos EUA desde a década de 70, sendo
originalmente traduzidos de haicais escritos em inglês, depois surgindo
como produções originais em ASL (KLIMA; BELLUGI, 1979). Eles se adaptam
muito bem às línguas de sinais, porque com eles podemos criar imagens
visuais a partir de poucos sinais. Os primeiros haicais em ASL seguiam a
necessidade de serem apenas descritivos e não terem enredo. Porém, os
poetas de línguas de sinais começaram a criar haicais com ação e com
isso surgiram narrativas muito curtas e muito visuais. A pesquisadora e
professora Michiko Kaneko trabalha com poetas surdos no Reino Unido para
criar haicais em BSL desde 2007. O desenvolvimento do gênero lá se deu
da sugestão de que poderia ter um desfecho inesperado. O poeta britânico
Paul Scott trouxe essa ideia para o Brasil quando ministrou uma oficina
para poetas surdos brasileiros em 2013. Hoje, muitos haicais em Libras
têm um desfecho imprevisível ou fortemente emocional.

O poema *[Peixe](#ytModal-LEDC479z_vo)*, de Renato Nunes, é um bom
exemplo de haicai em Libras. Dura apenas 14 segundos e tem seis ou sete
sinais (a divisão entre os sinais não é fácil na poesia). Usa apenas a
mão aberta como configuração de mão. Fala da água e de um peixe,
seguindo a regra de se falar sobre a natureza. Fala do sol, sugerindo o
verão. Usa apenas os classificadores com expressão facial intensa para
criar uma imagem forte. O desfecho inesperado acontece quando o peixe
bate no vidro do aquário e entendemos que ele não está livre na
natureza.

#### 14.5.2. Renga

A renga também vem da tradição japonesa, sendo tradicionalmente uma
série de poemas de haicai. A renga foi introduzida e promovida na Europa
no ano de 2010 pela professora Michiko Kaneko e pelo poeta ouvinte
reconhecido pelos poemas de renga em inglês Alan Summers. Hoje esse
gênero existe em muitos países, por exemplo na Irlanda (veja
*Fruit* - em português “Frutas”), no Reino
Unido (veja [*World II*](#ytModal-WuYo2d51lKA) - em português “O
mundo II”), Suécia e África do Sul. Também temos renga brasileira em
Libras. Veja um exemplo no link
<https://repositorio.ufsc.br/handle/123456789/176319>.

Renga é um poema coletivo e colaborativo. Um grupo de pessoas
(normalmente de cinco a dez) cria um poema em conjunto, cada um
contribuindo na sua vez. Cada pessoa sinaliza seu próprio haicai, ou
pelo menos apresenta um trecho poético de alguns sinais, antes de passar
para a próxima pessoa que vai continuar com a trama, muitas vezes
começando com um sinal que usa a mesma configuração de mão do último
sinal usado pela pessoa anterior. O momento da transição entre duas
pessoas é muito importante na renga e deve ser planejado cuidadosamente.
Nesse momento, os dois participantes podem interagir, criando um dueto
curto (veja a próxima seção), em que os sinais são ligados pela forma e
pelo sentido.

Como é um produto de um grupo de pessoas trabalhando juntas, a renga não
é apenas um artefato, mas também um evento e um processo. Esse tipo de
poesia se encaixa bem nas tradições surdas por ser coletiva, e é uma
forma muito boa para aprendizes menos confiantes, porque estes podem
apresentar um poema com o apoio do grupo.

#### 14.5.3. Duetos

Embora normalmente esperemos ver um poema apresentado por apenas um
artista, os duetos criam efeitos poéticos especiais por serem
apresentados por duas (ou mais) pessoas. Elas podem estar lado a lado,
como no poema *[As Brasileiras](#vimeoModal-242326425)*, de Anna Luiza
Maciel e Klícia Campos. Os artistas de um dueto podem interagir
fisicamente (por exemplo, nesse poema, as duas poetas criam juntas o
contorno do Brasil com as mãos em contato e cada uma toca no peito da
outra para dizer “você”). Além disso, o conteúdo que os dois
participantes de um dueto sinalizam pode ter uma relação oposta ou algum
tipo de contraste. No poema [*As
Brasileiras*](#vimeoModal-242326425), as duas pessoas
contrastam as características do interior rural do Nordeste com as da
grande cidade de São Paulo. Vemos uma igrejinha humilde e uma catedral
magnífica, poucas pessoas e uma multidão, uma cena de seca e uma de
chuva. Os artistas podem, também, interagir com os sinais para criar um
efeito poético. Em [*As
Brasileiras*](#vimeoModal-242326425), também, a
Paulistana joga para fora os resíduos de um copo de água e a Nordestina,
ao lado dela na cena, mas a milhares de quilômetros de distância no
poema, pega a água em seu copo.

As pessoas podem, ainda, estar uma por trás da outra, como no exemplo do
poema *[A Economia](#vimeoModal-267272909)*, de Sara Theisen Amorim e
Ângela Eiko Okumura (ver também capítulo 15).

###  14.6. Pelejas ou batalhas poéticas

As pelejas ou batalhas poéticas são um tipo de dueto, em que o objetivo
é criar uma imagem de uma ideia cada vez maior do que a anterior. Um
exemplo bem conhecido no Brasil é o do ASL Cowboys – The Fastest Hands
in the West (em português “As mãos mais rápidas d’oeste”). Veja a versão
homenagem de Jake Schwall [*The Fastest Hand in the
West*](#ytModal-YkRvss7OsGM).

Lembramos que os poemas existem numa escala entre poesia e prosa, e
nesse exemplo podemos ver que, apesar de ter uma estrutura narrativa, a
peleja segue um ritmo forte e usa os classificadores de uma forma muito
criativa e visual. Pode contar sobre objetos cada vez maiores (como as
armas no poema dos cowboys), situações mais esquisitas, emoções mais
fortes ou sentimentos mais intensos. Podemos dizer que, de certa forma,
a luta entre as mãos no poema [*V & V*](#vimeoModal-325444221) de
Fernanda Machado é uma peleja entre as mãos, que vai cada vez criando
uma imagem mais intensa – até que elas caem.

<div class="float"></div>

![resumo](/images/resumo-parte3.png)

###  14.7. Resumo

Esse capítulo não falou de todos os gêneros de poesia em Libras. Do
rápido desenvolvimento pelo qual a poesia em Libras tem passado, podemos
esperar cada vez mais gêneros surgindo no Brasil, criados pelos artistas
brasileiros ou emprestados de línguas de sinais ou de línguas orais de
outros países. Mostramos que há muitos tipos diferentes de poemas que
podemos apreciar e que não existe uma maneira só de categorizar toda a
riqueza da poesia em Libras. Vimos que a origem dos poemas varia e que a
tradução era - e ainda é - uma fonte de poemas. Os próprios gêneros
poéticos também podem ter origem em outras culturas (surdas ou ouvintes)
adaptados para a Libras. O papel do poeta e do artista na apresentação
de poemas cria obras mais narrativas ou teatrais. No entanto, tudo
contribui para um tecido entrelaçado de poemas visuais de muitos
gêneros.

<div class="float"></div>

![atividade](/images/atividade-parte3.png)

### 14.8. Atividade

**Vamos criar uma renga!**

Quais procedimentos você vai seguir? Dê uma olhada nas “regras” de renga
detalhadas nesse capítulo e assista a algumas rengas na internet para se
inspirar. Lembramos que qualquer poeta pode quebrar as “regras” por
motivos poéticos a qualquer momento.

<span class="tooltip-texto" id="tooltip-35">
Em inglês “Jabberwocky”, escrito por Lewis Carroll, traduzido
    para português por Augusto de Campos com o título “Jaguadarte”.
</span>

<span class="tooltip-texto" id="tooltip-36">
Entrevista no documentário <a href="https://www.youtube.com/watch?v=aJ0Y-luT5_w" target="_blank">
    The Heart of the Hydrogen Jukebox</a>, de Nathan
    Lerner e Feigel, 2009, no tempo de 00:14:11. Tradução nossa.
</span>

<span class="tooltip-texto" id="tooltip-37">
"ASL poets have few conventions to bind them" (p. 173, tradução
    nossa).
</span>

<span class="tooltip-texto" id="tooltip-38">
No sentido gramatical, a primeira pessoa do singular significa a
    pessoa que fala.
</span>
