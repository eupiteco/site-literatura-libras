---
parte: 0
capitulo: 25
titulo: Pós textos
pdf: http://files.literaturaemlibras.com/Literatura_em_Libras_Pos_textos.pdf
---

## Pós textos

### 25. Referências

ALBRES, Neiva de Aquino. Tradução de literatura infanto-juvenil para 
língua de sinais: dialogia e polifonia em questão. **Revista Brasileira
de Linguística Aplicada**, vol.14, n.4, 2014. DOI:
<http://dx.doi.org/10.1590/1984-639820145540>

ALBRES, Neiva de Aquino; COSTA, Mairla. P. P.;
ADAMS, Harrison. G. Contar um conto com encantamento: a construção de
sentidos e efeitos da tradução para libras. **Revista Diálogos**
(RevDia), Dossiê temático “Educação, Inclusão e Libras”, v. 6, n. 1,
2018.

ANDRADE Betty Lopes L’Astorina de. **A tradução de obras literárias em Língua Brasileira de Sinais – Antropomorfismo em foco.** Dissertação
(Mestrado em Tradução) - Programa de Pós-Graduação em Estudos da
Tradução da Universidade Federal de Santa Catarina, Florianópolis, 2015.

BAHAN, Ben. Comment on Turner. **Sign Language Studies**, n. 83, p.
241-249, 1994.

BAHAN, Ben. ‘Face-to-face tradition in the American Deaf community. In
H-Dirksen Bauman, Jennifer Nelson and Heidi Rose (org.). **Signing the Body Poetic**. California: University of California Press, 2006.

BALDWIN, Stephen. **Pictures in the Air: The Story of the National Theatre of the Deaf** Washington DC, Gallaudet University Press, 2009.

BARROS, Thatiane do Prado. **Experiência de Tradução Poética de Português/Libras: Três Poemas de Drummond.** Dissertação (Mestrado em
Estudos da Tradução) - Universidade de Brasília. Brasília, 2015.

BARROS, R.O.; VIEIRA, S. Z. The Relationship between Text and Image on
Literary Productions in Libras. **Sign Language Studies**. Volume 20,
Number 3, Spring 2020.

BARRY, Peter. **Beginning Theory: An introduction to literary and cultural theory.** Manchester: Manchester University Press, 2017.

BASILIO, Ester Vitória Anchieta. 
**Incorporação e partição do corpo: o espaço sub-rogado no discurso narrativo de uma tradução de literatura infantil do português para a Libras.** Dissertação (Mestrado em
tradução). Programa de Pós-Graduação em Estudos da Tradução da
Universidade Federal de Santa Catarina. 2017.

BAUMAN, H-Dirksen; NELSON, Jennifer; ROSE, Heidi (org.). **Signing the Body Poetic.** California: University of California Press, 2006.

BAUMAN, H-Dirksen. Getting out of line: toward a visual and cinematic
poetics of ASL. In: BAUMAN, H-Dirksen; NELSON, Jennifer; ROSE, Heidi
(org.). **Signing the Body Poetic**. California: University of
California Press, 2006.

BECHTER, F. The Deaf Convert Culture and Its Lessons for Deaf Theory.
In: BAUMAN, H.-D. L. (org.). **Open Your Eyes: Deaf Studies Talking**,
Minneapolis: University of Minnesota Press, 2008. p. 60–82.

BENEDICT, Barbara. **The eighteenth-century anthology and the construction of the expert reader.** Poetics 28, 2001. P. 377-397.

BERGSON, Henri. **O Riso: ensaio sobre a significação do cômico.** Rio
de Janeiro: Zahar Editores. Tradução publicada em 1983, \[Original,
publicado em francês, 1900\].

BIENVENU, Martina J. Reflections of Deaf culture in Deaf humor. In:
ERTING, Carol J.; JOHNSON, Robert C.; SMITH, Dorothy L.; SNIDER Bruce C.
(org.), **The Deaf way: perspectives from the International Conference on Deaf Culture.** Washington, DC: Gallaudet University Press, 1994. P.
16 –23.

BISOL, Cláudia. **Tibi e Joca: uma história de dois mundos.** Porto
Alegre: Mercado Aberto, 2001.

BOUCHAUVEAU, Guy. Deaf humor and culture. In: ERTING, Carol J.; JOHNSON,
Robert C.; SMITH, Dorothy L.; SNIDER Bruce C. (org.). **The Deaf way:
perspectives from the International Conference on Deaf Culture**,
Washington, DC: Gallaudet University Press, 1994. P. 24 –30.

BOLDO Jaqueline, SUTTON-SPENCE Rachel. 2020. Libras Humor: Playing with
the Internal Structure of Signs. **Sign Language Studies**. Volume 20,
Number 3, Special Issue on Sign Language Poetry. pp. 411-433

CAMPELLO, Ana Regina. Pedagogia Visual / Sinal na Educação dos Surdos.
In: QUADROS, Ronice Müller de; PERLIN, Gladis (org.). **Estudos Surdos II**. Petrópolis, RJ: Arara Azul, 2007. P. 100-131.

CAMPOS, Klícia de Araújo. **Literatura de cordel em Libras: os desafios de tradução da literatura nordestina pelo tradutor surdo**. Dissertação (Mestrado em Tradução) - Programa de
Pós-Graduação em Estudos da Tradução da Universidade Federal de Santa
Catarina, Florianópolis, 2017.

CASTRO, Nelson Pimenta. **A tradução de fábulas seguindo aspectos imagéticos da linguagem cinematográfica e da língua de sinais.**
Dissertação (Mestrado em Estudos da Tradução) - Programa de Pós
Graduação em Estudos da Tradução da Universidade Federal de Santa
Catarina, Florianópolis, 2012.

CHRISTIE, Karen; WILKINS, Dorothy. Themes and symbols in ASL poetry:
resistance, affirmation and liberation. **Deaf Worlds**, 22, p. 1–49,
2007.

CLARK, John Lee. (org.). **Deaf American Poetry**. Washington DC:
Gallaudet University Press, 2009.

CLÜVER, Claus. Da Transposição intersemiótica. In: ARBEX, Márcia.
**Poéticas do visível: Ensaios sobre a escrita e a imagem.** UFMG, 2006. P. 107-166.

CUXAC, C. **La Langue des Signes Françaises; les voies de
l’iconicite.** Paris: Ophrys, 2000.

CUXAC, C.; SALLANDRE, M.-A. Iconicity and arbitrariness in French Sign
Language: Highly iconic structures, degenerated iconicity and
diagrammatic iconicity. In: PIZZUTO, E.; PIETRANDREA, P.; SIMONE, R.
(org.), **Verbal and signed languages: Comparing structure, constructs
and methodologies.** Berlin: de Gruyter, 2008. P. 13–33.

DI LEO, Jeffrey. (org.). **On anthologies: Politics and Pedagogy.**
Lincoln: University of Nebraska Press, 2004. P. 1-30.

DUDIS, Paul. Body partitioning and real-space blends. **Cognitive Linguistics**, 15(2), p. 223-238, 2004. DOI: 10.1515/cogl.2004.009

DUNDES, Alan. What Is Folklore? In: DUNDES, Alan (org.). **The Study of
Folklore.** Englewood Cliffs, NJ: Prentice Hall, 1965. P. 1-6.

ELIOT, Thomas Stearns. **‘Introduction’ in Paul Valéry, Collected Works
of Paul Valéry,** Volume 7: The Art of Poetry. New York: Vintage, 1958.

EVANGELISTA, Kácio Lima. **Ser**. Fortaleza, CE \[s.n.\], 2018.

FARIA, Maria Alice. **Como usar a literatura infantil na sala de aula**. São Paulo, SP: Editora Contexto. 2016.

FELÍCIO, Márcia. **Interpretação simultânea artística de contos em línguas de sinais.** Tese (Doutorado em Estudos de Tradução) - Programa
de Pós-Graduação em Estudos da Tradução da Universidade Federal de Santa
Catarina, Florianópolis, UFSC, 2017.

FRISHBERG, Nancy. Signers of tales: the case of literary status of an
unwritten language. **Sign Language Studies**, 17(59), 1988. P.
149–170.

GONZALES, Maribel. **Being and becoming a Deaf educator: the
construction of Deaf educators’ roles and pedagogies in Chilean Deaf
schools.** Tese - University of Bristol, 2017.

HALL, Stephanie. **The Deaf Club is Like a Second Home: An Ethnography
of Folklore Communication in American Sign Language.** Tese -
University of Pennsylvania. 1989

HEBERLE, Viviane. Texto, discurso, gêneros textuais e práticas sociais
na sociedade contemporânea: tributo a José Luiz Meurer. **Cadernos de
Linguagem e Sociedade - Papers on Language and Society**, 12(1), p.
155-168, 2011. Disponível em:
<https://www.researchgate.net/publication/256199767_Texto_discurso_generos_textuais_e_praticas_sociais_na_sociedade_contemporanea_tributo_a_Jose_Luiz_Meurer>

HEINZELMAN, Renata. **Pedagogia cultural em poemas da Língua Brasileira
de Sinais.** Dissertação (Mestrado em Educação) - Faculdade de
Educação, Programa de Pós-Graduação em Educação da Universidade Federal
do Rio Grande do Sul, Porto Alegre, 2014.

HESSEL Carolina Silveira; KARNOPP Lodenir. Humor na cultura surda:
análise de piadas. **Textura**, v. 18 n. 37, maio/ago, 2016.

HESSEL, Carolina Silveira; KARNOPP, Lodenir Becker; ROSA, Fabiano Souto.
**Cinderela Surda.** Canoas: Ed. ULBRA, 2003.

HESSEL, Carolina Silveira; KARNOPP, Lodenir Becker; ROSA, Fabiano Souto.
**Rapunzel Surda.** Canoas: Ed. ULBRA, 2003.

HESSEL, Carolina Silveira. **Literatura Surda: análise da circulação de
piadas clássicas em Línguas de Sinais.** Tese (Doutorado em Educação) -
Faculdade de Educação da Universidade Federal do Rio Grande do Sul,
Porto Alegre, 2015.

HOEK, L. H. A transposição intersemiótica: por uma classificação
pragmática. In: ARBEX, Márcia. **Poéticas do visível: Ensaios sobre a
escrita e a imagem.** Belo Horizonte: UFMG, 2006. p. 167- 190.

HOPKINS, David. **On Anthologies.** Cambridge Quarterly, 37 (3). 2008.
P. 285-304.

JESUS, João Ricardo Bispo. **Literatura em língua de sinais: a
performance do escritor surdo Maurício Barreto.** Dissertação (Mestrado
em Letras). Programa de pós-graduação em Literatura e Cultura da
Universidade Federal da Bahia. 2019.

KARNOPP, Lodenir. **Literatura Visual**. 2008b. Disponível em:
[www.libras.ufsc.br/colecaoLetrasLibras/eixoFormacaoEspecifica/literaturaVisual/](http://www.libras.ufsc.br/colecaoLetrasLibras/eixoFormacaoEspecifica/literaturaVisual/)

KARNOPP, Lodenir. Produções culturais de surdos- análise de literatura
surda. **Cadernos de Educação*:** Educação de Surdos, Ano 19, n. 36, p.
155-174, 2010. Disponível em:
<http://periodicos.ufpel.edu.br/ojs2/index.php/caduc/article/viewFile/1605/1488>

KARNOPP, Lodenir Becker; HESSEL SILVEIRA, Carolina. Humor na literatura
surda. **Educar em Revista.** no.spe-2, 2014. DOI:
<http://dx.doi.org/10.1590/0104-4060.37013>

KARNOPP, Lodenir. **Literatura Surda.** Universidade Federal de Santa
Catarina: Licenciatura em Letras-Libras na Modalidade a Distância.
2008a.

KARNOPP, Lodenir. Produções culturais em língua brasileira de sinais
(Libras). **Letras de Hoje,** Porto Alegre, v. 48, n. 3, p. 407-413,
2013.

KARNOPP, Lodenir; KLEIN, Madalena; LUNARDI-LAZZARIN, Márcia Lise (Org.).
**Cultura Surda na contemporaneidade: negociações, intercorrências e
provocações.** Canoas: Editora ULBRA, 2011.

KLAMT, Marilyn Mafra; MACHADO, Fernanda de Araújo; QUADROS, Ronice
Muller de. Simetria e ritmo na poesia em língua de sinais. In Ronice
Müller de Quadros & Markus Weininger (Org.). **Estudos da Língua
Brasileira de Sinais**, Vol III, p. 211-226. 2014.

KLIMA, Edward; Ursula BELLUGI. **The Signs of Language Cambridge**,
M.A.: Harvard University Press, 1979.

KORTE, Barbara. Flowers for the Picking: Anthologies of Poetry in
(British) Literary and Cultural Studies. In: Korte, Barbara; SCHNEIDER,
Ralf; LETHBRIDGE, Stefanie (org.). **Anthologies of British Poetry:
Critical Perspectives from Literary and Cultural Studies.** Amsterdam,
Atlanta GA: Rodopi, 2000. P. 1-32.

KRENTZ, Christopher. The camera as printing press; How film has
influenced ASL literature. In: BAUMAN, H-Dirksen; NELSON, Jennifer;
ROSE, Heidi (org.). **Signing the Body Poetic**. California:
University of California Press, 2006.p 51-70.

LABOV, W. **Padrões Sociolinguísticos.** São Paulo: Parábola \[1972\],
2008.

LADD, Paddy. **Understanding Deaf Culture: In Search of Deafhood.**
Clevedon: Multilingual Matters. 2003.

LAKOFF, George; JOHNSON, Mark. **Metáforas da vida cotidiana.**
Mercado de Letras, 2002.

LANE, Harlan; PILLARD, Richard C.; HEDBERG, Ulf. **The People of the
Eye: Deaf Ethnicity and Ancestry.** Oxford University Press. 2010.

LAUZEN, Martha M. **It’s a Man’s (Celluloid) World: Portrayals of
Female Characters in the Top 100 Films of 2016**. Report compiled for
Center for the Study of Women in Television and Film, San Diego State
University. 2017. Disponível em:
<https://womenandhollywood.com/resources/statistics/>

LEECH, Geoffrey. **A Linguistic Guide to English Poetry.** London:
Longman, 1969.

LIDDELL, Scott. Four functions of a locus: reexamining the structure of
space in ASL. In: LUCAS, C. (org.). **Sign Language Research:
Theoretical Issues.** Washington, DC: Gallaudet University Press), 1990. P. 176–198.

LIDDELL, Scott K. **Grammar, gesture, and meaning in American Sign
Language.** Cambridge: Cambridge University Press, 2003.

LIDDELL, Scott; JOHNSON, Robert. E. American Sign Language: The
Phonological Base. **Sign Language Studies**, v. 64, p. 197–277, 1989.

MACHADO, Fernanda de Araújo. **Antologia da poética de Língua de Sinais
Brasileira.** Tese (Doutorado em Estudos da Tradução) - Pós-Graduação
em Estudos da Tradução da Universidade Federal de Santa Catarina, 2017.

MACHADO, Fernanda de Araujo. **Poesia Árvore de Natal.** (DVD). Rio de
Janeiro: LSB vídeo, 2005.

MACHADO, Fernanda de Araújo. **Simetria na Poética Visual na Língua de
Sinais Brasileira**. 149 f. Dissertação (Mestrado em Estudos da
Tradução) - Programa de Pós-Graduação em Estudos da Tradução,
Universidade Federal de Santa Catarina, 2013.

MARTINEAU, W. H. A model of the social functions of humor. In:
GOLDSTEIN, J.; McGHEE, P. (org.). **The psychology of humor:
Theoretical perspectives and empirical issues.** New York, NY: Academic
Press, 1972. P. 101–125.

MATOS, Lucyene da C. Vieira Machado. **Narrar e pensar as narrativas
surdas Caixabas: o outro surdo no processo de pensar uma pedagogia.**
In: QUADROS, Ronice Muller de (org.). Estudos Surdos III. Rio de
Janeiro: Arara Azul, 2008. P. 210 -259

MECKLER, D. C. **On difficulty in the arts.** 2007. Disponível em:
<http://accounts.smccd.edu/mecklerd/mus202/4difficulties.htm>

MEURER J. L.; DELLAGNELO Adriana Kuerten. **Análise do Discurso.**
Universidade Federal de Santa Catarina: Licenciatura em Letras-Libras na
Modalidade a Distância, 2008.

MILES, Dorothy. **Gestures: Poetry in Sign Language.** Northridge, CA:
Joyce Motion Picture Co, 1976.

MORGADO, Marta. Literatura em língua gestual. In: KARNOPP, L.; KLEIN,
M.; LUNARDI-LAZZARIN, M. (org.). **Cultura Surda na
contemporaneidade.** Canoas RS: Editora ULBRA, 2011. P. 151-172.

MOURÃO, Claudio Henrique Nunes. **Literatura
Surda: Experiência das Mãos Literárias.** Tese (Doutorado em Educação) - Programa de Pós-Graduação em Educação da Universidade Federal do Rio
Grande do Sul, 2016.

MOURÃO, C. **Literatura Surda: produções
culturais de surdos em língua de sinais.** In: KARNOPP, L.; KLEIN, M.;
LUNARDI-LAZZARIN, M. (org.). Cultura Surda na contemporaneidade. Canoas:
Editora ULBRA, 2011. P. 71-90.

MÜLLER, Janete Inês; KARNOPP, Lodenir Becker. Tradução cultural em
educação: experiências da diferença em escritas de surdos. **Educ.
Pesqui**., São Paulo, v. 41, n. 4, p. 1041-1054, 2015. DOI:
<http://dx.doi.org/10.1590/s1517-97022015031750>

MULROONEY, K.J. **Extraordinary from the Ordinary: Personal Experience
Narratives in American Sign Language.** Washington D.C.: Gallaudet
University Press, 2009.

NASCIMENTO, Sandra P. **Representações Lexicais da Língua de Sinais
Brasileira. Uma Proposta Lexicográfica.** Tese de doutorado.
Universidade de Brasília, 2009.

NATHAN LERNER, Miriam; FEIGEL, Don. **The Heart of the Hydrogen
Jukebox.** (DVD). New York: Rochester Institute of Technology, 2009.

NODELMAN, Perry. **Children’s Literature as Women’s Writing.**
Children’s Literature Association Quarterly, v. 13, n. 1, pp. 31-34,
1988.

NONHEBEL, Annika; CRASBORN, Onno; VAN DER KOOIJ, Els. **Sign language
transcription conventions for the ECHO project.** 2003. Disponível em:
<https://www.researchgate.net/publication/237538700_Sign_language_transcription_conventions_for_the_ECHO_Project>

**O Som das Palavras - Antologia Literária.** 2003. Rio de Janeiro:
Editora Litteris, 2003.

OLIVEIRA, Carmen; BOLDO, Jaqueline. **A** **cigarra surda e as
formigas***. Porto Alegre: Corag, s.d.

OLIVEIRA, Ronise. **Meus sentimentos em folhas.** Rio de Janeiro:
Editora Litteris, 2005.

OLRIK, Axel. Epic laws of folk narrative. In: DUNDES, Alan (org.) **The
Study of Folklore.** Englewood Cliffs, NJ: Prentice Hall, 1909
\[1965\].

ORMSBY, Alec. **The poetry and poetics of American Sign Language.**
Tese. Stanford University, 1995.

PADDEN, Carol. The relation between space and grammar in ASL verb
morphology. In: LUCAS, C. (org.). **Sign Language Research: Theoretical
Issues.** Washington, DC: Gallaudet University Press, 1990. P. 118–132.

PARAN, A. The role of literature in instructed foreign language learning
and teaching: An evidence based survey. **Language Teaching**, 41 (4),
p. 465 –96, 2008.

PEIXOTO, Janaina. **O registro da beleza nas mãos: a tradição de
produções poéticas em Língua de Sinais no Brasil.** Tese (Doutorado em
Letras) - Programa de Pós-Graduação em Letras da Universidade Federal da
Paraíba, 2016.

PETERS, Cynthia. **Deaf American Literature: From Carnival to the
Canon.** Washington, D.C.: Gallaudet University Press, 2000.

PIMENTA, Nelson. **Literatura em LSB**, LSB vídeo (DVD). Rio de
Janeiro: Editora Abril e Dawn Sign Press, 1999.

POL, C. **Deaf humor. A theater performance in Italian Sign
Language.** Dissertação. Università Ca' Foscari Venezia, Venice, 2014.
Disponível em: <http://hdl.handle.net/10579/5425>

QUADROS, Ronice Muller de; SUTTON-SPENCE, R. Poesia em Língua de Sinais:
Traços da identidade surda. In: QUADROS, Ronice Muller de (org.).
**Estudos Surdos 1.** Petrópolis, Rio de Janeiro: Editora Azul, 2006.
P. 110-165.

RAMOS, Tânia Regina Oliveira. **Introdução aos estudos da
literatura.** Licenciatura em Letras-Libras na Modalidade a Distância,
2008.

RAMOS, Clélia Regina. **Tradução Cultural: Uma
proposta de trabalho para surdos e ouvintes.** Reflexões sobre trabalho
de tradução de textos da literatura para a LIBRAS, realizado na
Faculdade de Letras da UFRJ entre os anos de 1992 a 2000. Rio de
Janeiro: Arara Azul, 2000. Disponível em:
[www.editora-arara-azul.com.br](http://www.editora-arara-azul.com.br)

RASKIN, Victor. **Semantic mechanisms of humor.** Dordrecht: Kluwer
Academic, 1985.

REILLY, Charles; REILLY, Nipapon.**The Rising of Lotus Flowers:
Self-education by Deaf Children in Thai Boarding Schools**. Washington,
D.C.: Gallaudet University Press, 2005.

RIGO, Natália Schleder. **Tradução de canções de LP para LSB:
identificando e comparando recursos tradutórios empregados por
sinalizantes surdos e ouvintes**.* Dissertação (Mestrado em Estudos da
Tradução) - Universidade Federal de Santa Catarina -Programa de
Pós-Graduação em Estudos da Tradução, Florianópolis, 2013.

ROSA, Fabiano Souto. **O que o currículo de Letras Libras ensina sobre
literatura surda?** Tese apresentada ao Programa de Pós-Graduação em
Educação, da Universidade Federal de Pelotas, 2017.

ROSA, Fabiano Souto; KARNOPP, Lodenir Becker. **Patinho Surdo.**
Canoas: ULBRA, 2005.

ROSA, Fabiano Souto; KLEIN, Madalena. **O que sinalizam os professores
surdos sobre literatura surda em livros digitais.** In: KARNOPP, L.;
KLEIN, M.; LUNARDI-LAZZARIN, M. (org.). **Cultura Surda na
contemporaneidade: negociações, intercorrências e provocações** Canoas:
Editora ULBRA, 2011. P. 91-112.

ROSE, Heidi. The poet in the poem in the
performance: the relation of body, self, and text in ASL literature. In:
BAUMAN, H-Dirksen; NELSON, Jennifer; ROSE, Heidi (org.). **Signing the
Body Poetic.** California: University of California Press, 2006. P.
130-146.

ROSE, Heidi. **A Critical Methodology for Analyzing American Sign
Language Literature.** Tese. Arizona State University, 1992.

RUTHERFORD, Susan. **A Study of Deaf American Folklore.** Silver
Spring, MD: Linstok Press, 1993.

RUTHERFORD, Susan. Funny in Deaf — Not in hearing. In: WILCOX, Sherman
(org.). **American Deaf culture: An anthology**. Silver Spring, MD:
Linstok Press, 1989. P. 65–82.

RYAN, Stephen. **‘Let’s Tell an ASL Story’ in Gallaudet University
College for Continuing Education.** Conference Proceedings, April
22-25. Washington, D.C.: Gallaudet University Press, 1993. p145-150

SALES, Taísa Aparecida Carvalho (org). **Onze histórias e um segredo,
desvendando as lendas Amazônicas.** Manaus: Damir Pacheco de Souza,
2016.

SCHALLENBERGER, Augusto. **Ciberhumor nas comunidades surdas.**
Dissertação (Mestrado em Educação) - UFRGS/FACED/PPGEDU. Porto Alegre,
2010.

SCOTT, Paul. Do Deaf children eat deaf carrots? In: MATHUR, Gaurav;
NAPOLI, Donna Jo (org.). **Deaf around the World: the impact of
language.** Oxford: Oxford University Press, 2011. P. 359-366.

SPOONER, Ruth Anna. 2016. **Languages, Literacies, and Translations:
Examining Deaf Students’ Language Ideologies Through English-to-ASL
Translations of Literature**. Tese de doutorado. Ann Arbor, MI,
University of Michigan.

STRÖBEL, Karin Lilian. História dos surdos: representações “mascaradas”
das identidades surdas. In: QUADROS, Ronice Muller de; PERLIN, Gladis
(org.). **Estudos Surdos II.** Petrópolis: Editora Arara Azul. 2007.
P. 18-37.

SUTTON-SPENCE Rachel. Literatura surda feita por mulheres. In: Silva,
Arlene Batista da; DALVI, Maria Amélia; DUTRA, Paulo; SALGUEIRO,
Wilberth (org.). **Literatura e artes, teoria e crítica feitas por
mulheres**. Instituto Brasil Multicultural de Educação e Pesquisa –
IBRAMEP: Campos dos Goytacazes, 2019. P. 142-166.

SUTTON-SPENCE, Rachel. Teaching Sign Language Literature in L2/Ln
Classrooms. In: ROSEN Russel (org.) **Routledge handbook of sign
language pedagogy**. Abingdon: Routledge. 2020. P. 233-246.

SUTTON-SPENCE, Rachel; MACHADO, Fernanda de Araújo; CAMPOS, Klícia de
Araújo, FELÍCIO, Márcia; VIEIRA, Saulo; CARVALHO, Daltro; BOLDO,
Jaqueline. Artistas surdos contam suas histórias: quais foram suas
influências? **Revista Brasileira de Vídeo Registros em Libras**, nº
003, 2017. Disponível em:
<http://revistabrasileiravrlibras.paginas.ufsc.br/publicacoes/edicao-no-0032017/>

SUTTON-SPENCE, Rachel; MACHADO, Fernanda de Araújo; CAMPOS, Klícia de
Araújo, FELÍCIO, Márcia; VIEIRA, Saulo; CARVALHO, Daltro; BOLDO,
Jaqueline. Os craques da Libras: a importância de um festival de
folclore sinalizado. **Revista Sinalizar**, v.1, n.1, p. 78-92. 2016.

SUTTON-SPENCE, Rachel. Por que precisamos de poesia sinalizada em
educação bilíngue?" Educar em Revista. O dossiê temático Educação
Bilíngue para Surdos: políticas e práticas. FERNANDES, Sueli (org).
**Educ. rev.** no.spe-2, p. 111-127, 2014. DOI:
<http://dx.doi.org/10.1590/0104-4060.37009>

SUTTON-SPENCE, Rachel; NAPOLI Donna Jo. Deaf Jokes and Sign Language
Humour. **International Journal of Humor Research**, 3, 311-338, 2012.
DOI: <https://doi.org/10.1515/humor-2012-0016>

SUTTON-SPENCE, Rachel; BOYES BRAEM, Penny. Comparing the products and
the processes of creating sign language poetry and pantomimic
improvisations. **Journal of Nonverbal Behavior**, 37(3), p. 245–280,
2013.

SUTTON-SPENCE, Rachel; KANEKO, Michiko. **Introducing Sign Language
Literature: Creativity and Folklore.** Basingstoke: Palgrave Press,
2016.

SUTTON-SPENCE, Rachel; MACHADO, Fernanda de Araújo. Poetry in Libras..
In: QUADROS, Ronice Muller de; STUMPF, Marianne Rossi (org.). Brazilian
Sign Language Studies. Florianópolis, Editora Insular, Volume IV, p.
187-220, 2018.

SUTTON-SPENCE, Rachel; MACHADO, Fernanda de Araújo. Considerações sobre
a criação de antologias de poemas em línguas de sinais. In: QUADROS,
Ronice Muller de; STUMPF, Marianne Rossi (org.). **Estudos da Língua
Brasileira de Sinais**.* Florianópolis, Editora Insular, Volume IV, p.
187-220, 2018.

SUTTON-SPENCE, Rachel; QUADROS, Ronice Müller de. Performance Poética em
Sinais: o que a audiência precisa para entender a poesia em sinais. In:
QUADROS, Ronice Muller de; STUMPF, Marianne; LEITE, Tarcísio de Arantes
(org.). **Série Estudos de Língua de Sinais**, Volume II.
Florianópolis: Editora Insular, 2014. P. 207-228.

SUTTON-SPENCE, Rachel; RAMSEY Claire. What we should teach Deaf
Children: Deaf Teachers' Folk Models in Britain, the U.S. and Mexico.
**Deafness and Education International**, 12/3, p. 149-176. 2010.

TAUB, Sarah. **Language from the Body: Iconicity and Metaphor in
American Sign Language.** Cambridge: Cambridge University Press, 2001.

TAUB, Sarah. Iconicity and metaphor. In: PFAU, R.; STEINBACH, M.; WOLL,
B. (org.). **Sign language. An international handbook (HSK—Handbooks of
Linguistics and Communication),** Science 37. Berlin: Mouton de
Gruyter. 2012. P. 400- 412.

TAYLOR, Debbie. **Agenda: Hearts and Minds.** MsLexia, Jun-Aug, p. 5-
9, 2018.

VALLI, Clayton. **Poetry in Motion**.* Burtonsville, MD: Sign Media
Inc, 1990.

VALLI, Clayton. **Poetics of American Sign Language Poetry.** Tese -
Union Institute Graduate School, 1993.

VALLI, Clayton. **ASL Poetry: Selected Works of Clayton Valli.**
\[DVD\] San Diego, CA: Dawn Sign Press, 1995.

VIEIRA, André Guirland. Do Conceito de Estrutura Narrativa à sua
Crítica. **Psicologia: Reflexão e Crítica**, 14(3), p. 599-608, 2001.

VIEIRA, Saulo Zulmar*. **A produção narrativa em libras: uma análise dos
vídeos em língua brasileira de sinais e da sua tradução intersemiótica a
partir da linguagem cinematográfica.** Dissertação (Mestrado em Estudos
da Tradução). Pós Graduação em Estudos da Tradução da Universidade
Federal de Santa Catarina, Florianópolis, 2016.

VIEIRA-MACHADO Lucyenne Matos da C. Narrar e pensar as narrativas surdas
Capixabas: o outro surdo no processo de pensar uma pedagogia. In:
QUADROS, Ronice Muller de (org.). **Estudos Surdos III**. Petrópolis,
RJ: Editora Arara Azul, 2008. P. 208-257

WEININGER, M. J.; SUTTON-SPENCE, R. Quando múltiplos olhares geram
diferentes experiências de tradução ao português de um poema em Libras:
o caso de “Homenagem Santa Maria” de Godinho 2013*. **Cadernos de
Tradução** da Universidade Federal de Santa Catarina, vol.35, nº2,
2015. Disponível em:
<https://periodicos.ufsc.br/index.php/traducao/article/view/2175968.2015v35nesp2p458>

WEYL, Hermann. **Symmetry.** Princeton, N.J.: Princeton University
Press. 1952.

WILCOX, Phyllis Perrin. **Metaphor in American Sign Language**.
Washington, DC: Gallaudet University Press. 2000.

WILLIAMS, Jeffrey. Anthology Disdain. In: DI LEO, Jeffrey (org.). **On
anthologies: Politics and Pedagogy**. Lincoln: University of Nebraska
Press, 2004. P. 207 – 221

ZIRALDO. **Flicts**. São Paulo: Editora Melhoramentos, 2009.

### 26. Lista de Vídeos indicados no texto

[*O Alienista*](#ytModal-51J5IRcE_vQ),
de Machado de Assis

[*Amar é também silenciar*](#ytModal-Oz5YqANdunU), de Rafael
Lopes dos Santos (Rafael Lelo)

[*Animais números*](#vimeoModal-348189766) de
Juliana Lohn

[*Anjo Caído*](#vimeoModal-209842983) de Fernanda Machado

[*Antônio Silvino o Rei dos Cangaceiros*](#ytModal-57xLeX74Vu0),
(Leandro Gomes de Barros), de Klícia de Araujo Campos

[*O Armário*](#vimeoModal-348652466) de Juliana Lohn

[*Arrumar, Passear...* *de A à Z Letras*](#ytModal-uciVF5oMqkc)
de Jéssie Rezende

[*Árvore*](#vimeoModal-267272296) de André Luiz
Conceição

[*Associação*](#ytModal-yd1VVhdsT3Q)
de Fernanda Machado

[*Ave 1 x 0 Minhoca*](#vimeoModal-267277312) de Marcos
Marquioto

[*Bolinha de Ping-pong*](#ytModal-VhGCEznqljo) de
Rimar Segala

*[As Borboletas](#ytModal-mi8bnByv4S4),*
de Wilson Santos Silva

[*As Brasileiras*](#vimeoModal-242326425) de Klícia
Campos e Anna Luiza Maciel

[*Passeio no Rio de Janeiro*](#vimeoModal-203292043) de Leonardo
Adonis de Almeida e Marcelo William da Silva

[*Chapeuzinho Vermelho*](#ytModal-JuCVU9rGUa8), de Heloise Grippe,

[*O Churrasco da banana surda*](#vimeoModal-360265158) de
Marina Teles.

[*Cinco Sentidos*](#ytModal-AyDUTifxCzg) de Nelson Pimenta,

[*Clóvis Albuquerque dos Santos*](#ytModal-uP3q5kOcXxs), de Manaus

[*Coleção de narrativas didáticas curtas*](https://vimeo.com/showcase/6241328) de Marina Teles

[*Como Veio Alimentação*](#ytModal-nMOTYprbYoY), de Fernanda
Machado

[*O Conto VV*](#ytModal-0WfXyP4DoIg), de
Lúcio Macedo

[*O Curupira*](#vimeoModal-292526263) de Fábio de Sá

[*Doll*](#ytModal-5iM7zbis68w) (em português, *A Boneca*), de
Paul Scott

[*A Economia*](#vimeoModal-267272909) de Sara Theisen Amorim e
Ângela Eiko Okumura

[*O Encontro*](#ytModal-eDHKhLjyIGw) de Fabio de Sá

[*Eu x Rato*](#ytModal-UmsAxQB5NQA) de Rodrigo
Custodio da Silva

[*Galinha Fantasma*](#ytModal-isyT8-mgCDM)* de
Bruno Ramos

[*Farol da Barra*](#ytModal-VXcKgO-jD9A) de Mauricio Barreto

[*The Fastest Hand in the West*](#ytModal-YkRvss7OsGM),* de Jake
Schwall

[*Fazenda: Vaca*](#ytModal-NtN98y67ukM)* de Rimar
Segala

[*Feliz Natal*](#ytModal-SSzlEALHo80), de Rosana Grasse

[*Five Senses*](#ytModal--qLcuxfdoYY), de Paul Scott

[*A Formiga Indígena*](#vimeoModal-355984518) surda* de
Marina Teles

[*Fruit*](#ytModal-l4yXP3Z4gqs) - em português “Frutas”, renga

[*Golf Ball*](#ytModal-Gl3vqLeOyEE) de Stefan Goldschmidt

[*The Heart of the Hydrogen Jukebox*](#ytModal-aJ0Y-luT5_w) Nathan-Lerner e
Feigel

[*Hino Nacional*](#ytModal-jHfBUvskvbw), de Bruno Ramos

[*Homenagem Santa Maria/RS*](#ytModal-9LtOP-LLx0Y) de Alan Henry
Godinho,

[*Jaguadarte*](#ytModal-DSSXTh0wriU)
de Aulio Norbrega

[*Julgar a Prostituta*](#ytModal-SyG9yCkP_Qc) de Maurício Barreto

[*Lei de Libras*](#vimeoModal-267274663), de Sara
Theisen Amorim e Anna Luiza Maciel

[*Leoa Guerreira*](#ytModal-rfnKoCXmSg4) de Vanessa Lima

[*O Lobinho Bom*](#ytModal-HXD1YszZdp8) de Carolina
Hessel

[*Lutas surdas*](#ytModal-aOQx2YMj6Xc) de Alan Henry Godinho

[*O Macaco surdo fazendo música*](#vimeoModal-355992442),
de Marina Teles

[*Mãos Aventureiras*](https://www.ufrgs.br/maosaventureiras/), de Carolina
Hessel

[*Mãos do Mar*](#ytModal-Njy8l1Qnnko), de Alan
Henry Godinho

[*Mãos em Fúrias*](#ytModal-8sYWSq2pwhg), por Eduardo Tótoli,
Luciano Canesso Dyniewicz, Elissane Zimmerman Dyniewicz e Carlos
Alexandre Silvestri, dirigido por Giuliano Robert

[*Meu Ser é Nordestino*](#ytModal-t4SLooMDTiw) de
Klicia Campos

[*Mirror*](#ytModal-p0B8ztiVI4s) (O Espelho) de Richard Carter

[*O Modelo do Professor Surdo*](#ytModal-rverroKm8Bg), de Wilson
Santos Silva,

[*O Morcego surdo no busão*](#vimeoModal-356028141) de
Marina Teles,

[*A Morte de Minnehaha*](http://hsldb.georgetown.edu/films/film-view.php?film=deathofminnehaha&signer=Erd)”
de Mary Williamson em 1913

[*O Negrinho do Pastoreio*](#vimeoModal-306082977) de
Roger Prestes

[*Números em Libras*](#ytModal-ad8k-rIq7Sw) de Maurício Barreto

[*Paraná*](#vimeoModal-267276733) de Marcos Marquioto

[*Party*](#ytModal-s8BLY1Nm7qQ) (em português *A Festa*) de
Johanna Mesch,

[*O Passarinho diferente*](#ytModal-P_DR60BJuFI), de
Nelson Pimenta

[*Pássaro números*](#vimeoModal-348080802) de
Juliana Lohn

[*A Pedra Rolante*](#ytModal-kPXWu5UCTzk), de Sandro Pereira

[*Peixe*](#ytModal-LEDC479z_vo) de Renato Nunes

[*O Pequeno Príncipe*](#ytModal-foMiwFlVHCc) Projeto
Acessibilidade em Bibliotecas Pública

[*Um poema dueto curto*](#ytModal-355978488) de
Daltro Roque Carvalho da Silva Junior e Victória Hidalgo Pedroni.

[*Um poema maluco*](#ytModal-GnDTdgxLGoo) de
Daltro Roque Carvalho da Silva Junior.

[*Príncipe Procurando Amor*](#ytModal-D4hPR6DCcDA) (em inglês,
*Prince Looking for Love*) de Richard Carter

[*A Rainha das Abelhas*](#ytModal-nXI4aO2_G3E) de
Mariá de Rezende Araújo

[*Saci*](#ytModal-4UBwn9242gA) de Fernanda Machado

*[O Sapo e o Boi](#ytModal-23HQFq0A4AY),*
de Nelson Pimenta

[*O Símbolo do Olodum*](#ytModal-_jhW0K4NN0A), de Priscila
Leonnor

[*Slow Motion Portrait*](#ytModal-rWowtWJSd4E) (em português
“Retrato em câmera lenta”), de Tony Bloem

[*Tinder*](#vimeoModal-267275098) de Anna Luiza Maciel

[*Tree*](#ytModal-Lf92PlzMAXo) de Paul Scott

[*Unexpected moment*](#ytModal-hD48RQLQurg) (“Momento
inesperado”), de Amina Ouahid e Jamila Ouahid

[*V & V*](#vimeoModal-325444221) de Fernanda Machado

[*A Vaca surda de salto alto*](#vimeoModal-356033857) de
Marina Teles

[*Vagalume*](#ytModal-JG8xrh1az_g) de Tom Min Alves

[*O Vernáculo visual e o uso de classificadores em línguas de sinais*](#ytModal-lfNVeK-24cA) de Peter Cook

[*Voo Sobre Rio*](#ytModal-YaAy0cbjU8o) de Fernanda Machado

[*World II*](#ytModal-WuYo2d51lKA) - em português “O mundo II”,
renga

### 27. Lista de artistas de literatura em Libras (e outras línguas de sinais) indicados no texto 

Alan Henry Godinho [*Homenagem Santa Maria/RS*](#ytModal-9LtOP-LLx0Y);
[*Lutas surdas*](#ytModal-aOQx2YMj6Xc); [*Mãos do Mar*](#ytModal-Njy8l1Qnnko)

Amina Ouahid e Jamila Ouahid [*Unexpected moment*](#ytModal-hD48RQLQurg) (“Momento inesperado”)

André Luiz Conceição [*Árvore*](#vimeoModal-267272296)

Ângela Eiko Okumura eSara Theisen Amorim [*A Economia*](#vimeoModal-267272909)

Anna Luiza Maciel e Klícia Campos [*As Brasileiras*](#vimeoModal-242326425)

Anna Luiza Maciel e Sara Theisen Amorim [*Lei de Libras*](#vimeoModal-267274663),

Anna Luiza Maciel [*Tinder*](#vimeoModal-267275098)

Jake Schwall [*The Fastest Hand in the West*](#ytModal-YkRvss7OsGM),

Aulio Norbrega [*Jaguadarte*](#ytModal-DSSXTh0wriU)

Bruno Ramos [*Galinha Fantasma*](#ytModal-isyT8-mgCDM)

[*Hino Nacional*](#ytModal-jHfBUvskvbw)

Carolina Hessel [*Mãos Aventureiras*](https://www.ufrgs.br/maosaventureiras/); 
[*O Lobinho Bom*](#ytModal-HXD1YszZdp8)

Cézar Pedrosa de Oliveira [*X Men Apocalipse, Mercúrio*](#ytModal-Yh_qXzHYkfs)

[*Clóvis Albuquerque dos Santos*](#ytModal-uP3q5kOcXxs), de Manaus

Daltro Roque Carvalho da Silva Junior e Victória Hidalgo Pedroni [*Um poema dueto curto*](#ytModal-_FmzDgviY5s)

Daltro Roque Carvalho da Silva Junior [*Um poema maluco*](#ytModal-GnDTdgxLGoo)

Eduardo Tótoli, Luciano Canesso Dyniewicz, Elissane Zimmerman Dyniewicz
e Carlos Alexandre Silvestri, dirigido por Giuliano Robert [*Mãos em Fúrias*](#ytModal-8sYWSq2pwhg)

Fábio de Sá [*O Curupira*](#vimeoModal-292526263); [*O Encontro*](#ytModal-eDHKhLjyIGw)

Fernanda Machado [*Anjo Caído*](#vimeoModal-209842983);
[*Associação*](#ytModal-yd1VVhdsT3Q);
[*Como Veio Alimentação*](#ytModal-nMOTYprbYoY);
[*Saci*](#ytModal-4UBwn9242gA); [*V & V*](#vimeoModal-325444221); [*Voo Sobre Rio*](#ytModal-YaAy0cbjU8o)

[*Fruit*](#ytModal-l4yXP3Z4gqs) - em português “Frutas”, renga

Heloise Grippe, [*Chapeuzinho Vermelho*](#ytModal-JuCVU9rGUa8)

Jéssie Rezende [*Arrumar, Passear...* *de A à Z Letras*](#ytModal-uciVF5oMqkc)

Johanna Mesch [*Party*](#ytModal-s8BLY1Nm7qQ) (em português *A Festa*)

Juliana Lohn [*Animais números*](#vimeoModal-348189766); 
[*O Armário*](#vimeoModal-348652466);
[*Pássaro números*](#vimeoModal-348080802)

Kácio Evangelista Lima Comunidade

Klícia Campos [*Antônio Silvino o Rei dos Cangaceiros*](#ytModal-57xLeX74Vu0),
(Leandro Gomes de Barros); [*Meu Ser é Nordestino*](#ytModal-t4SLooMDTiw)

Klícia Campos e Anna Luiza Maciel [*As Brasileiras*](#vimeoModal-242326425)

Leonardo Adonis de Almeida e Marcelo William da Silva [*Passeio no Rio de Janeiro*](#vimeoModal-203292043)

Lúcio Macedo [*O Conto VV*](#ytModal-0WfXyP4DoIg)

Machado de Assis [*O Alienista*](http://acessibilidadeembibliotecas.culturadigital.br/2016/11/16/o-alienista-de-machado-de-assis-e-mais-uma-obra-disponivel-com-recursos-de-acessibilidade/),

Marcelo William da Silva e Leonardo Adonis de Almeida [*Passeio no Rio de Janeiro*](#vimeoModal-203292043)

Marcos Marquioto [*Ave 1 x 0 Minhoca*](#vimeoModal-267277312);
[*Paraná*](#vimeoModal-267276733)

Mariá de Rezende Araújo [*A Rainha das Abelhas*](#ytModal-nXI4aO2_G3E)

Marina Teles [*A Formiga Indígena*](#vimeoModal-355984518) surda; 
[*A Vaca surda de salto alto*](#vimeoModal-356033857); 
[*O Churrasco da banana surda*](#vimeoModal-360265158);

[*O Macaco surdo fazendo música*](#vimeoModal-355992442);
[*O Morcego surdo no busão*](#vimeoModal-356028141)

Mary Williamson Erd [*A Morte de Minnehaha*](http://hsldb.georgetown.edu/films/film-view.php?film=deathofminnehaha&signer=Erd)

Mauricio Barreto [*Farol da Barra*](#ytModal-VXcKgO-jD9A);
[*Julgar a Prostituta*](#ytModal-SyG9yCkP_Qc); [Números em
Libras](#ytModal-ad8k-rIq7Sw)

Nathan-Lerner e Feigel [*The Heart of the Hydrogen Jukebox*](#ytModal-aJ0Y-luT5_w)

Nelson Pimenta [*O Passarinho diferente*](#ytModal-P_DR60BJuFIh);* 
[*O Sapo e o Boi*](#ytModal-23HQFq0A4AY);
[*Cinco Sentidos*](#ytModal-AyDUTifxCzg)

Paul Scott [*Doll*](#ytModal-5iM7zbis68w) (em português, *A
Boneca*); [*Five Senses*](#ytModal--qLcuxfdoYY);
[*Tree*](#ytModal-Lf92PlzMAXo)

Peter Cook [*O Vernáculo visual e o uso de classificadores em línguas de sinais*](#ytModal-lfNVeK-24cA)

Priscila Leonnor [*O Símbolo do Olodum*](#ytModal-_jhW0K4NN0A)

Projeto Acessibilidade em Bibliotecas Pública [*O Pequeno Príncipe*](#ytModal-foMiwFlVHCc)

Rafael Lopes dos Santos (Rafael Lelo) [*Amar é também silenciar*](#ytModal-Oz5YqANdunU)

Renato Nunes [*Peixe*](#ytModal-LEDC479z_vo)

Richard Carter [O Espelho*](#ytModal-p0B8ztiVI4s) (em inglês
*Mirror*); [*Príncipe Procurando Amor*](#ytModal-D4hPR6DCcDA)
(em inglês, *Prince Looking for Love*)

Rimar Segala [*Bolinha de Ping-pong*](#ytModal-VhGCEznqljo);
[*Fazenda: Vaca*](#ytModal-NtN98y67ukM)

Rodrigo Custódio da Silva [*Eu x Rato*](#ytModal-UmsAxQB5NQA)

Roger Prestes [*O Negrinho do Pastoreio*](#vimeoModal-306082977)

Rosana Grasse [*Feliz Natal*](#ytModal-SSzlEALHo80)

Sandro Pereira [*A Pedra Rolante*](#ytModal-kPXWu5UCTzk)

Sara Theisen Amorim e Ângela Eiko Okumura [*A Economia*](#vimeoModal-267272909)

Sara Theisen Amorim e Anna Luiza Maciel [*Lei de Libras*](#vimeoModal-267274663),

Stefan Goldschmidt [*Golf Ball*](#ytModal-Gl3vqLeOyEE)

Tom Min Alves [*Vagalume*](#ytModal-JG8xrh1az_g)

Tony Bloem [*Slow Motion Portrait*](#ytModal-rWowtWJSd4E) (em
português “Retrato em câmera lenta”)

Vanessa Lima [*Leoa Guerreira*](#ytModal-rfnKoCXmSg4)

Victória Hidalgo Pedroni e Daltro Roque Carvalho da Silva Junior [*Um poema dueto curto*](#ytModal-_FmzDgviY5s)

Wilson Santos Silva [*As Borboletas*](#ytModal-mi8bnByv4S4);
[*O Modelo do Professor Surdo*](#ytModal-rverroKm8Bg)

[*World II*](#ytModal-WuYo2d51lKA) - em português “O mundo II”,
renga
