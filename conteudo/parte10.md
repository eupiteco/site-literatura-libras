---
capitulo: 10
parte: 2
titulo: Literatura cinematográfica e o Vernáculo Visual (VV)
pdf: "http://files.literaturaemlibras.com/CP10_Literatura_cinematografica_e_o_vernaculo_visual_(VV).pdf"
video: CKDM6AEG0r8
---

## 10. Literatura cinematográfica e o Vernáculo Visual (VV)

<div class="float"></div>

![objetivo](/images/objetivo-parte2.png)

### 10.1 Objetivo

Neste capítulo, veremos dois elementos da literatura em Libras usados
para se construir e apresentar poemas e histórias visualmente: as
técnicas cinematográficas e as mímicas - ou técnicas teatrais. No
capítulo 07, conhecemos o Vernáculo Visual (VV). Frisamos que ele não
acontece em todos os poemas e narrativas, mas é uma opção atualmente
muito comum e valorizada pela comunidade surda brasileira.



<div class="float"></div>

![lista de videos](/images/videos-parte2.png)

<div class="lista-de-videos"></div>

Vamos, então, pensar mais sobre as técnicas cinematográficas e mímicas
em Libras antes de retomarmos o Vernáculo Visual. Para ver alguns
exemplos, vamos assistir a alguns novos vídeos e voltar para outros já
conhecidos:

* *[Bolinha de Ping-pong](#ytModal-VhGCEznqljo),* de Rimar Segala.
* *[O conto VV](#ytModal-0WfXyP4DoIg),* de Lúcio Macedo.
* *[O Curupira](#vimeoModal-292526263),* de Fábio de Sá.
* *[Eu x Rato](#ytModal-UmsAxQB5NQA),* de Rodrigo Custódio da Silva.
* *[Galinha Fantasma](#ytModal-isyT8-mgCDM),* de Bruno Ramos.
* *[Fazenda: Vaca](#ytModal-NtN98y67ukM),* de Rimar Segala.
* [*A* *Formiga Indígena*](#vimeoModal-355984518) *Surda,* de Marina Teles.
* [*Leoa Guerreira*](#ytModal-rfnKoCXmSg4), de Vanessa Lima.
* *[Meu Ser é Nordestino](#ytModal-t4SLooMDTiw),* de Klícia Campos.
* *[O Jaguadarte](#ytModal-DSSXTh0wriU),* de Aulio Nóbrega.
* *[A Rainha das Abelhas](#ytModal-nXI4aO2_G3E),* de Mariá de Rezende Araújo.
* *[Saci](#ytModal-4UBwn9242gA),* de Fernanda Machado.
* *[Tinder](#vimeoModal-267275098),* de Anna Luiza Maciel.

### 10.2 Histórias cinematográficas

Já sabemos que os filmes influenciam muito a literatura em Libras.
Embora sejam feitos por pessoas ouvintes e tenham muitos diálogos não
acessíveis aos surdos[24](#tooltip), são a forma de entretenimento cultural mais
popular entre a comunidade surda. Muitos artistas de Libras falam da
influência das películas no desenvolvimento de suas habilidades, pois
têm o hábito de assistir aos filmes e depois recontá-los em Libras para
os amigos. Essa tradição parece ser mundial, sendo descrita por autores
dos Estados Unidos, Reino Unido e até da Tailândia, que falam sobre as
origens da literatura surda (RUTHERFORD, 1993; LADD, 2003; REILLY;
REILLY, 2005). Os filmes de aventura e ação, como os de faroeste ou os
da Marvel (tais como *X-Men*) fornecem muitas oportunidades para se
criar histórias visuais em Libras.

É importante observar que as histórias em quadrinhos e os jogos de
videogame também apresentam elementos visuais semelhantes aos dos filmes
por contarem histórias de ação, mas não serão foco de discussão neste
capítulo.

Vimos, no capítulo 07, que existem elementos visuais nas narrativas que
têm paralelos nos filmes. O impacto vem particularmente do contraste de
imagens como: perto-longe, alto-baixo, esquerda-direita e rápido-lento.
O pesquisador americano Dirksen Bauman (2006) mostrou o valor de se
descrever a arte em língua de sinais como se fosse uma forma de filme.
No Brasil, as pesquisas de Nelson Pimenta (2012), ator e artista de
Libras, e de Saulo Vieira (2016), descrevem como usamos as técnicas
fílmicas na produção de narrativas em Libras para mostrar as diversas
perspectivas de um evento visual.

Nas contações de histórias cinematográficas, é como se o artista tivesse
uma "câmera em mente" ou como se o público visse a apresentação
projetada pelo artista numa tela. A partir desse olhar, os sinais podem
mostrar “close-ups”, planos médios e “long-shots”. Vemos os sinais e as
cenas a partir da perspectiva do narrador ou do personagem – ou de ambos
(através de uma “montagem”). O sinalizante pode dar um zoom ou diminuir
esse zoom e mudar o ângulo das tomadas.

A linguagem do cinema tem a sua própria
gramática. Os espectadores de um filme entendem como as tomadas e as
cenas são criadas e como devem ser percebidas. Essa gramática
cinematográfica dos filmes tem muitos paralelos com a gramática visual
da Libras. Bauman afirma que podemos usar a linguagem dos filmes para
descrever qualquer discurso sinalizado, não apenas o literário, porque
toda a produção de uma língua de sinais é uma performance visual que
acontece dentro do espaço e do tempo, como num filme. Essa é uma
perspectiva muito útil para se compreender o impacto do visual namos falar dos seguintes vídeos na nossa exploração do
assunto:
linguísticos de classificação dos sinais.

Em termos fílmicos, **aproximar** a imagem (transferir a imagem pelo
corpo) significa a **transferência do personagem** na ideia de
transferência de Cuxac e Sallandre. Sob o ponto de vista linguístico
mais tradicional, isso é chamado de **incorporação.**

Aproximação **intermediária** e **distanciamento** (transferir a imagem
para as mãos) é um tipo de **transferência de situação** que usa os
**classificadores de pessoa e objeto**, como são conhecidos sob o ponto
de vista linguístico tradicional.

Aproximação intermediária e distanciamento também é denominado
**transferência do tamanho e forma** e **classificadores** desse tipo.

A montagem em termos fílmicos mescla ou combina diversas imagens. Na
teoria de Cuxac, vemos isso como uma **combinação de duas
transferências** e, nas outras teorias linguísticas, isso se chama
**espaço dividido** (DUDIS, 2004).

#### 10.2.1 Tamanho e distância da imagem 

##### Mostrar a vista panorâmica

No início de filmes, é comum vermos um panorama amplo, mostrando a vista
inteira da cena. É isso que a poeta faz na abertura do poema [*Voo Sobre
Rio*](#ytModal-YaAy0cbjU8o), ao mostrar através de
classificadores os pontos principais da cidade pela qual o pássaro
passa.
*[Jaguadarte](#ytModal-DSSXTh0wriU)*
abre com uma descrição completa da vista do guerreiro, do elmo, da
couraça, do escudo e da lança. Depois, vemos um panorama da paisagem por
meio dos classificadores que destacam os vales, as árvores e as rochas
até a caverna com as estalactites pingando. Também Klícia Campos cria
uma vista panorâmica no início do seu poema *[Meu ser é
nordestino](#ytModal-t4SLooMDTiw),* que veremos
em maiores detalhes no capítulo 15.

##### Aproximar a imagem

O poema *[Voo Sobre Rio](#ytModal-YaAy0cbjU8o)* começa com a
imagem de um planeta muito distante. Nos aproximamos do planeta e os
sinais mostram-no girando e se aproximando cada vez mais, do ponto de um
dedo aos dedos fechados de uma mão, depois aos dedos de duas mãos
juntas, até as duas mãos separadas.

Durante o voo, o pássaro passa pela estátua do Cristo Redentor e vemos
novamente uma aproximação da imagem. No início, dois dedos cruzados
(classificadores ou transferência de situação) estão longe do corpo da
poeta. As mãos se aproximam do corpo, dando um zoom, e ela abre os
braços para incorporar a estátua. Em outro trecho, aproximando-se do Pão
de Açúcar, as mãos fechadas e juntas se abrem e se separam, aumentando o
tamanho da imagem, para dar um zoom novamente.

Em
[*Jaguadarte*](#ytModal-DSSXTh0wriU),
o classificador é de um humano em movimento a pé, representando o
guerreiro a passar pelo campo em sua busca. Quando ele entra na caverna,
os dedos da mão fechada que pareciam representar uma rocha se abrem para
criar uma caverna nessa rocha, para permitir a entrada da forma de
classificador do guerreiro. A mão não dominante, em seguida, passa sobre
a cabeça do artista para criar um efeito de modo que o público saiba que
está vendo o guerreiro incorporado. Dessa maneira, vemos o equivalente a
um plano de close-up. Quando o monstro acorda, vemos a incorporação dele
inteiro no corpo e os braços e as mãos do artista se tornam os membros e
as garras do monstro - o que geralmente é considerado uma tomada de
close-up. No entanto, o poeta nos leva ainda mais perto quando as suas
mãos se tornam apenas a boca e os dentes do monstro.

Ressaltamos que o público de uma performance literária ao vivo (e em
muitas gravações) sempre pode ver o corpo inteiro do artista, e as
pessoas sabem qual é a parte do corpo que mostra a informação
linguística necessária para a imagem visual. Quando o artista Aulio
mostra o guerreiro pelo classificador a distância, o público entende que
o corpo dele é o corpo do narrador e que a principal ação está nas mãos.
Quando o guerreiro entra na caverna, toda a informação dada pelo corpo
de Aulio é importante para a história porque este se tornou o corpo do
guerreiro por incorporação. Essa habilidade para entender o que importa
e o que não importa do corpo é fundamental para compreender tais
mudanças de perspectiva.

No vídeo [*X-Men Apocalipse - Mercúrio*](https://youtu.be/Yh_qXzHYkfs), de
Cézar Pedrosa de Oliveira, os sinais seguem exatamente as tomadas do
filme e a edição do vídeo também revela isso. No momento do close-up nos
pés de Mercúrio ou da abelha na flor, o quadro do artista mostra apenas
as mãos dele – não vemos o restante do seu corpo. Numa performance ao
vivo, o público sabe que deve focar apenas nas mãos; no vídeo, a edição
permite que vejamos apenas as mãos.

##### Aproximação intermediária

No poema [*Voo Sobre Rio*](#ytModal-YaAy0cbjU8o), de Fernanda
Machado, temos três aproximações dos pássaros. Uma mais de perto, que
mostra mais detalhes, é feita por incorporação, onde o corpo e a cabeça
do humano são os mesmos do pássaro e os braços abertos e estendidos são
as suas asas. A perspectiva mais distante, mostrando poucos detalhes do
pássaro além do local e a forma de movimento, é feita com as duas mãos
entrelaçadas, que são as asas. Porém, há a opção de uma aproximação
intermediária, que Fernanda usa na parte central do poema, na qual o
antebraço é o corpo do pássaro e a mão com os dedos fechados para criar
um “bico” é a cabeça[25](#tooltip).

##### Distanciar

No final do conto
[*Jaguadarte*](#ytModal-DSSXTh0wriU),
o monstro fica “superaproximado” e os sinais são feitos muito perto da
câmera. Essa é uma maneira de terminar uma história com uma emoção muito
forte. No entanto, outras narrativas têm um desfecho mais suave e o
distanciamento é uma opção para revelar a finalização. No poema [*Voo
Sobre Rio*](#ytModal-YaAy0cbjU8o), vemos as imagens se
distanciando, os pássaros voam, vão diminuindo, e Fernanda mostra isso
através da redução da incorporação em transição para o uso de um sinal
classificador para os pássaros. O planeta também diminui da forma
contrária em que cresceu, terminando com uma ponta de dedo.

##### Alternar

A alternância entre os tipos de transferência e as imagens dos
personagens e objetos cria variação estética em uma narrativa. Também
permite ao narrador descrever o contexto em que os personagens
interagem. Podemos ver o movimento deles no espaço da narrativa através
da distância, mas também a forma física, o comportamento e as emoções
através da aproximação.

No filme *X-Men Apocalipse*, a edição do filme original da Marvel
alterna as tomadas – perto e longe, focando em Mercúrio e no que ele vê.
O artista Cézar Pedrosa de Oliveira segue essas tomadas na obra *X-Men
Apocalipse - Mercúrio*. Quando o filme mostra Mercúrio a distância,
Cézar usa um classificador de distância e, quando o filme apresenta uma
tomada em close-up, ele usa a transferência de pessoa para incorporar o
personagem.

Podemos também alternar entre dois personagens para ver a interação. Em
[*Jaguadarte*](#ytModal-DSSXTh0wriU),
o monstro e o guerreiro lutam. Vemos o ataque do monstro de duas
perspectivas: a do monstro e como esse ataque atinge o escudo do
guerreiro. No poema [*Voo Sobre Rio*](#ytModal-YaAy0cbjU8o), o
pássaro macho bate na cabeça da fêmea sem querer. Isso é mostrado com
classificadores num plano médio e alterna com uma tomada aproximada que
apresenta a fêmea esfregando a cabeça. O macho faz um carinho nela por
meio de um classificador intermediário.

##### Câmera lenta

Em termos fílmicos, os sinais podem ser acelerados ou apresentados em
câmera lenta. Nos dois textos analisados aqui não temos exemplos dessa
alteração de velocidade, mas já vimos essa estratégia utilizada no
capítulo 05, com a cena do rato pulando dentro da sala de ferramentas em
*Eu x Rato* e com a bolinha na narrativa
*Bolinha de Ping-pong*.

### 10.3 Isso é mímica?

Muitas pessoas que não conhecem a literatura em Libras pensam que ela é
uma forma de mímica. Às vezes, ambas parecem ser iguais, mas as
convenções literárias da Libras e da mímica, bem como as habilidades de
seus públicos, são diferentes. Não é sempre útil tentar usar as teorias
criadas pelos gêneros textuais dos ouvintes para descrever a Libras
literária, mas vale a pena entender algumas diferenças entre mímica (uma
forma de teatro silencioso que Marcel Marceau ensinou a Bernard Bragg –
ver capítulo 07) e a forma de Libras literária que parece ser a mais
visual. Uma pesquisa feita por Rachel Sutton-Spence e Penny Boyes Braem
(2014) comparou as produções dos artistas surdos e dos mímicos
profissionais e encontrou muitas diferenças.

O espaço de apresentação da cena visual é muito diferente: o da mímica é
muito maior do que o da Libras literária. Os
mímicos usam o corpo inteiro e os artistas se deslocam para mostrar os
movimentos, como o de caminhada por exemplo. Já em Libras, geralmente
nos sinais, nada é articulado abaixo dos quadris, os artistas ficam no
mesmo lugar e movem apenas as mãos e o corpo para representar movimento
ou deslocamento. A mímica usa todo o espaço vertical, inclusive o chão,
mas em Libras, além de apresentações excepcionais, os artistas não
deixam os sinais caírem abaixo dos quadris.

O tipo de transferência também é diferente nas duas técnicas. Na mímica,
vemos quase sempre a transferência de pessoa (podemos dizer que é um
tipo de incorporação), enquanto os poetas e artistas de Libras também
usam esse recurso, porém com a diferença de frequentemente misturarem
essa incorporação com transferências de forma (ou seja, misturar a
incorporação com a narração por classificadores). Isso permite que os
artistas de sinais façam uma troca de perspectivas em sinais que os
mímicos não usam, entre a pessoa e a forma. Geralmente, há menos
personagens nas histórias pantomímicas e ainda menos pe rsonagens são
mostrados. Por exemplo, numa narrativa em Libras pode haver um homem,
uma mulher e um cachorrinho e o contador pode incorporar os três -
prestando atenção no espaço ao redor de cada personagem para manter a
coerência entre eles. O homem olhando para baixo e para a direita vê o
cachorro, e o cachorro olhando para cima e para a esquerda vê o homem.
Essa coerência cria uma boa experiência visual. Os mímicos geralmente
mantêm um personagem só, embora os olhos mostrem as informações sobre os
lugares dos personagens. Se já incorporou o homem, podem imaginar o
cachorro (por exemplo, puxando a coleira que o homem segura) e a mulher
(a quem o homem sorriu, levantando o chapéu e oferecendo uma flor), mas
é incomum os mímicos mostrarem todos os personagens e, se mostram, eles
se movimentam muito mais no palco, se deslocando para lugares distintos
e mantendo uma perspectiva por mais tempo do que os artistas de línguas
sinais.

Não é que os mímicos sejam incapazes de criar múltiplos personagens ou
de usar transferência de forma, mas o seu público é que não consegue
entender essa dinâmica, porque não está acostumado a ver essas trocas ou
compreender o uso do espaço. Já um público surdo entende facilmente o
espaço para as múltiplas perspectivas.

Um bom teste para comprovar as diferenças entre os mímicos ouvintes e os
poetas surdos, apresentado por Sutton-Spence e Boyes Braem, abordou como
os dois tipos de artistas incorporam um sapo e um lápis. O mímico
profissional Don McLeod incorporou um sapo com o corpo inteiro. Pensando
no peso do animal, no ritmo e no movimento; o corpo dele se tornou o
corpo do sapo ao fechar os olhos e mexer a boca e a língua como um sapo
faz. McLeod disse:

> comece com ritmos (língua, movimento da cabeça), tempos, padrões de
> movimento e a essência básica de um sapo; então evolua lentamente de
> um sapo (agachado) para uma pessoa parecida com um sapo
> (vertical)[26](#tooltip)

O artista surdo britânico Richard Carter também incorporou o sapo, mas
usou as mãos para criar as partes do corpo que o sapo tem e o humano não
tem: como os olhos arredondados, o papo e a língua comprida. Nas imagens
abaixo, vemos que o mímico abaixou a cabeça, fechou os olhos e mostrou a
língua. Em comparação, Richard usou a mão esquerda para representar o pé
do sapo e a mão direita virou a língua pegando uma mosca. A língua
humana é mais curta do que a língua do sapo, mas a mão permite uma
extensão. Ele mostra também sua própria língua para incorporar o sapo e
isso ajuda na identificação da mão como a língua (Figura 3).

![Artistas fazendo mímica de um lápis](/images/p2-c10-1.png)

<div class="legenda"></div>

Figura 3: Incorporação de um sapo (mímico e artista surdo)

Três mímicos profissionais – Emily Mayne, Dennis Schaller e Don McLeod –
incorporaram um lápis com imaginação e criatividade, cada um deles
usando o corpo inteiro (Figura 4). O movimento de escrita foi feito no
chão, que usaram como a folha de um papel. Para Emily, a ponta do lápis
estava nos pés, o que permitiu a ela escrever no chão; para Dennis e
Don, estava na cabeça, o que criou um problema para a escrita. Dennis
criou a ponta do lápis com as mãos e Don colocou a cabeça dentro do
apontador e girou sua manivela, exemplo raro de perspectivas múltiplas
entre os mímicos.

![Artistas fazendo mímica de um lápis](/images/p2-c10-2.png)

<div class="legenda"></div>

Figura 4: Opções para incorporação de um lápis (mímicos)

Por outro lado, o artista surdo Richard Carter usou apenas as mãos e a
cabeça para mostrar o lápis; e a mão para mostrar a folha de papel
(Figura 5). As mãos mostraram a ponta do lápis, igual a Dennis, mas o
rosto também mostrou a ponta do objeto. Para representar a escrita num
papel, Richard colocou o rosto na palma da mão. Para apontar o lápis,
ele colocou o rosto entre as duas mãos, onde fica o apontador. O público
surdo entende que a mão significa o papel e o apontador também, mas o
público ouvinte vai ter mais dificuldade para compreender essa troca de
referentes.

![Artistas fazendo mímica de um lápis](/images/p2-c10-3.png)

<div class="legenda"></div>

Figura 5: Incorporação de um lápis com classificadores (artista surdo)

### 10.4 Vernáculo Visual - VV

Retomando o assunto dos contos em VV e lembrando da descrição de Peter
Cook apresentada no capítulo 07, podemos ver que neles se usam as
técnicas de mímica e teatro, mas também elementos cinematográficos.
Tudo, porém, é baseado no uso da Libras como uma língua visual.

Já vimos no capítulo 07, também, que um dos primeiros exemplos de VV
criado por Bernard Bragg foi um conto curto sobre um caçador que saiu
com o cão para pegar um pato. É um conto lento e suave. 
Esse estilo contido e moderado dele era mais típico das normas
literárias de ASL naquele tempo. Existem exemplos em Libras dessa forma
também, como *[O Modelo do Professor
Surdo](#ytModal-rverroKm8Bg),* de Wilson Santos Silva[27](#tooltip), e
*[Tinder](#vimeoModal-267275098),* de Anna Luiza
Maciel.

No entanto, a tendência atual de VV em Libras parte de outra
perspectiva, haja vista que muitos contos atualmente são feitos por
homens ou adolescentes sobre tópicos de ação, tais como lutas entre
guerreiros, extraterrestres esquisitos e corridas de carro, construídos
com muita energia e ritmo intenso. Nada impede que as mulheres criem em
VV (e existem contadoras de VV reconhecidas), mas esse é um gênero
atualmente mais ligado aos homens.

[O conto VV](#ytModal-0WfXyP4DoIg), de
Lúcio Macedo, é um bom exemplo de VV em Libras, pois é muito fílmico.
Nele, o contador não usa nenhum sinal do vocabulário da língua. Às vezes
ele usa o corpo inteiro para mostrar o movimento dos personagens e até
se desloca para estar em dois lugares diferentes quando incorpora dois
papéis. Por outro lado, Macedo usa classificadores com a incorporação,
que fazem parte da Libras. A história é complexa, fala sobre duas
pessoas, ocorre em dois tempos e em dois lugares diferentes, mas é
sempre compreensível por causa das apresentações visuais.

No entanto, existem histórias apresentadas pela técnica VV sobre outros
assuntos. Maurício Barreto utilizou-a quando contou a história bíblica
[Julgar a Prostituta](#ytModal-SyG9yCkP_Qc), uma das mais
dramáticas do Novo Testamento. Foi, sem dúvida, uma oportunidade para um
artista de Libras recontar uma história de forma extremamente visual,
com todos os elementos do VV e com técnicas cinematográficas como as
trocas de tomada (“close-up” e “long-shot”, rápida e lenta) e a troca de
papéis, muito rápidas e bruscas, mostrando a multidão, a prostituta e
Jesus. Barreto usa alguns sinais do vocabulário em Libras,
classificadores e incorporação, com um ritmo destacado e muita repetição
para criar um conto como se fosse um filme.

### 10.5 Resumo

<div class="float"></div>

![resumo](/images/resumo-parte2.png)

Nesse capítulo, vimos a forte influência de técnicas de cinema nas
narrativas em Libras. A linguagem visual dos filmes influencia uma certa
forma de contar histórias. Apesar de parecer mímica, os artistas surdos
que contam histórias visuais em Libras usam técnicas de Libras com
técnicas cinematográficas para criar imagens fortes nas histórias e nos
poemas, utilizando o corpo inteiro para entreter um público que entende
bem a gramática desse tipo de literatura surda.

<div class="float"></div>

![atividade](/images/atividade-parte2.png)

### 10.6 Atividade

Assista à [*Bolinha de
Ping-pong*](#ytModal-VhGCEznqljo), de Rimar
Segala.

1. Dê exemplos de momentos em que ele, como o contador, usa ângulos
diferentes para apresentar imagens tais como *alto* e *baixo*,
*esquerda* e *direita*, *rastreamento* e como mostra o ponto de vista de
diferentes personagens.

1. Procure exemplos de sinais:

    * com diferentes distâncias (perto, longe ou intermediária)
    * com diferentes comprimentos (curto, longo, muito curto, muito longo,
ou mediano)
    * com velocidades diferentes (velocidade “normal”, “rápido” ou “câmera
lenta”)

1. Procure exemplos de “edição” em que as tomadas são apresentadas em
uma determinada ordem para gerar emoção.


<span class="tooltip-texto" id="tooltip-24">
Nos tempos antigos, os filmes eram mudos e os surdos participavam
    muito deles. Charlie Chaplin, ator consagrado dos filmes mudos,
    influenciou muitos artistas surdos e é uma referência até hoje.
</span>

<span class="tooltip-texto" id="tooltip-25">
Existe mais uma opção para mostrar o pássaro ainda mais afastado,
    em que dois dedos entrelaçados são as asas do pássaro a uma grande
    distância, mas Fernanda não usou essa possibilidade no poema.
</span>

<span class="tooltip-texto" id="tooltip-26">
“Start with rhythms (tongue, head movement), tempos, movement
    patterns and basic essence of a frog; then evolve it slowly from a
    frog (crouching) into a frog-like person (upright).”
</span>

<span class="tooltip-texto" id="tooltip-27">
Não é 100% sem sinais. Tem um sinal de Libras nesta produção:
    <span style="font-variant:small-caps;">gesto</span>.
</span>
