---
capitulo: 19
parte: 3
titulo: O humor em Libras
pdf: "http://files.literaturaemlibras.com/CP19_O_humor_em_Libras.pdf"
video: 70OHt6shc5Y
---

## 19. O humor em Libras

<div class="float"></div>

![objetivo](/images/objetivo-parte3.png)

### 19.1. Objetivo

Em um capítulo anterior, tratamos do conteúdo do humor. Neste capítulo,
no contexto de linguagem estética, falaremos do humor em Libras cujo
impacto ocorre primeiramente em função da forma da linguagem. Isto é, se
o humor for expresso de qualquer outra forma, ele se perde. Vamos focar
especificamente em três áreas que geram risos em Libras: a imitação de
pessoas, de animais e de objetos; os jogos com a estrutura interna dos
sinais; e o humor bilíngue, que mescla português e Libras (KLIMA;
BELLUGI, 1979; MORGADO, 2011; BOLDO, SUTTON-SPENCE, 2020).

O humor se encontra em todos os gêneros de literatura em Libras (poesia,
histórias e teatro). Entender os seus embasamentos na língua de sinais
ajuda-nos a compreender esse elemento fundamental da literatura. No
humor visual, a Libras e os gestos se encontram e trabalham em conjunto.
Os elementos da Libras, tais como classificadores e incorporação com
expressão facial e outros não manuais exagerados criam o humor. Muitas
vezes, este é baseado na presença de “espírito” ou na originalidade da
representação e da incorporação.

<div class="float"></div>

![lista de vídeos](/images/videos-parte3.png)

<div class="lista-de-videos"></div>

Para explorar o humor em Libras, vamos usar as seguintes produções:

* *[Árvore](#vimeoModal-267272296),* de André Luiz
Conceição.

* *[A Pedra Rolante](#ytModal-kPXWu5UCTzk),* de Sandro Pereira.

* *[Bolinha de Ping-Pong](#ytModal-VhGCEznqljo),* de Rimar Segala.

* *[Cinco Sentidos](#ytModal-AyDUTifxCzg),* de Nelson Pimenta.

* *[Eu x Rato](#ytModal-UmsAxQB5NQA),* de Rodrigo
Custódio da Silva.

* *[Galinha
Fantasma](#ytModal-isyT8-mgCDM),*
de Bruno Ramos.

* [*Golf Ball*](#ytModal-Gl3vqLeOyEE), de Stefan Goldschmidt.

* *[O Jaguadarte](#ytModal-DSSXTh0wriU),* de Aulio Nóbrega.

* *[Um Morcego Surdo No Busão](#vimeoModal-356028141),*
de Marina Teles.

* *[Passeio no Rio de Janeiro](#vimeoModal-203292043),* de Leonardo
Adonis de Almeida e Marcelo William da Silva.

* *[Saci](#ytModal-4UBwn9242gA),* de Fernanda Machado.

* *[Tinder](#vimeoModal-267275098),* de Anna Luiza Maciel.

* *[Tree](#ytModal-Lf92PlzMAXo),* de Paul Scott.

* *[A Vaca Surda de Salto Alto](#vimeoModal-356033857),*
de Marina Teles.

* *[Voo sobre Rio](#ytModal-YaAy0cbjU8o),* de Fernanda Machado.

###  19.2. Tipos de humor em Libras

Morgado (2011) descreveu cinco formas de humor em LGP, a língua de
sinais de Portugal: 

1) imitações de filmes, de pessoas, de animais e de
objetos apresentados por expressões faciais e corporais;

2) jogos de
linguagem, geralmente contos curtos que brincam com formas de mão,
especialmente o alfabeto manual ou números;

3) jogos com movimento;

4) jogos de linguagem sobre tópicos tabus; e

5) piadas, desenhos animados e
anedotas humorísticas.

No capítulo 07, vimos as narrativas de ABC e, no capítulo 12, falamos
das piadas e anedotas humorísticas. Aqui, vamos pensar sobre as
imitações e os jogos de linguagem que brincam com os parâmetros dos
sinais.

No humor das línguas faladas não é muito comum se criar palavras (embora
esse tipo humorístico exista) mas, nas línguas de sinais, a habilidade
de inventar sinais visuais apropriados é altamente valorizada. Entre os
jogos de Libras com os parâmetros fundamentais dos sinais (configuração
de mão, locação, movimento, orientação da mão e elementos não manuais),
os mais importantes para o humor são os de configuração de mão e os de
elementos não manuais. Ainda precisamos de muito mais pesquisas sobre o
humor em Libras, mas já há investigações em outras línguas de sinais que
podem contribuir com os estudos do humor na língua brasileira de sinais.

### 19.3. Imitações

Morgado (2011) descreve a primeira categoria de humor como “imitações de
filmes, pessoas, animais e objetos apresentados através de expressões
faciais e corporais”. A imitação e a incorporação são fundamentais ao
humor em Libras. O humorista britânico Hal Draper falou sobre as
imitações quando afirmou:

> Eu acho que o fundamental é combinar os sinais com a expressão facial,
> o movimento do corpo e a maneira em que as pessoas surdas podem se
> transformar em uma coisa. No humor dos ouvintes, eu acho que a ênfase
> é baseada nas palavras em inglês, mas para os surdos está relacionada
> à incorporação, expressão facial e coisas desse tipo. (comunicação
> pessoal, 2005, tradução nossa)

Falando de humor e de risos em geral, o filósofo francês Henri Bergson
afirmou:

> Suponhamos agora ideias expressas no estilo que lhes convém e
> encaixadas assim no seu ambiente natural. Se imaginamos um dispositivo
> que lhes permite serem transpostas a um novo ambiente, conservando as
> relações que mantêm entre si, ou, em outras palavras, se as levamos a
> se exprimir em estilo inteiramente diferente e transpostas em
> tonalidade diferente, será então a linguagem que estará produzindo a
> comédia, ela é que será cômica (BERGSON, 1924, tradução da edição de
> 1983, p. 59).

Por isso Bergson criou uma regra geral: *“Obteremos um efeito cômico ao
transpor a expressão natural de uma ideia para outra tonalidade”*
(grifos no original*,* BERGSON, 1924, tradução da edição de 1983, p. 59)

Isso é o que acontece nas incorporações humorísticas dos artistas e
humoristas surdos em Libras. O humorista escolhe uma perspectiva nova e
inesperada de uma coisa familiar e bem conhecida para gerar a risada por
apresentá-la dentro do novo contexto do seu corpo. Como vimos no
capítulo 17, na discussão sobre antropomorfismo, Bergson observa que as
coisas não humanas não geram risadas em si, mas rimos se detectarmos
algum elemento humano no não humano. Quando o artista apresenta
características não humanas em Libras por meio de suas próprias
características humanas, a atitude ou a ação humana se torna cômica.
Lembremos da bola de golfe incorporada por Stefan Goldschmidt, que é
engraçada por expressar emoções humanas. Bergson declara outra regra:
“RIMO-NOS SEMPRE QUE UMA PESSOA NOS DÊ A IMPRESSÃO DE SER UMA COISA”.
(grifos no original. BERGSON, 1924, tradução da edição de 1983, p. 30)

É por isso que o antropomorfismo gera tanta risada em Libras.

### 19.4. Criatividade conceitual

Antes de mais nada, no humor em Libras a pessoa precisa decidir qual
aspecto de uma imagem visual apresentar e alterar. Um exemplo seria
escolher uma perspectiva diferente de uma imagem com a qual todos já
estão acostumados e criar um desvio dessa normalidade. Começa-se com
questões que nunca pensamos em perguntar: o que aquele homem tem dentro
da sua pasta que não é papel? Se eu tivesse apenas 10cm de altura, como
seria a minha vida? Se aquele animal pudesse sinalizar, como faria? Se
um queijo fosse surdo, como se comportaria?

No poema *[Cinco Sentidos](#ytModal-AyDUTifxCzg)*, traduzido para Libras por Nelson Pimenta, vemos a resposta
à pergunta “Se os sentidos fossem humanos, como eles seriam?”. No poema
*[Tree](#ytModal-Lf92PlzMAXo)*, de Paul Scott, e nas versões *[Saci](#ytModal-4UBwn9242gA)*, de Fernanda Machado, e
[*Árvore*](#vimeoModal-267272296), de André Luiz
Conceição, perguntamos “Se a árvore fosse humana, como seria?”. A
pergunta em *[Um Morcego Surdo no
Busão](#vimeoModal-356028141),* de Marina Teles, é “Se
um morcego pegasse um ônibus, o que aconteceria?”

Chamamos essa imaginação de “espírito”. Para falar do assunto
inesperado, o sinalizante seleciona construções de classificadores
inesperadas (mas ainda assim lógicas) para representá-lo. Ele irá
trabalhar com a expressão facial, frequentemente exagerada, e deve
criá-la para objetos ou animais que normalmente não têm expressões
faciais. O sinalizante permite ao público achar graça da “lógica do
absurdo” em que existe um mundo no qual a maioria das coisas é como no
nosso mundo, mas uma coisa é diferente. Isso significa dizer que o
público aceita algumas mudanças de realidade e outras não. Aceita que um
relógio possa sinalizar, dentro do contexto do humor, mas não que o
relógio tenha quatro ponteiros. Uma tartaruga pode conversar em Libras,
mas ainda é lenta e pesada e não pode pular e correr como uma lebre.
Lembramos que Bergson explica que a transferência gera risos quando
levamos uma coisa para um novo contexto e repetimos o evento original
nele. O equilíbrio entre manter os elementos principais e mudar apenas o
que for necessário no novo contexto para gerar o riso faz parte do
“espírito”.

Por isso vemos o humor na interação entre os pássaros em *[Voo sobre Rio](#ytModal-YaAy0cbjU8o),* de Fernanda
Machado. Eles se comportam da maneira típica dos pássaros, mas também
mostram comportamentos humanos dentro das formas dos bichos. Ver a mão
sinalizar que o macho come o grão com o bico, mas ver no mesmo momento a
incorporação no rosto humano cria um choque entre o mundo das aves e o
mundo dos humanos que é engraçado. Outro “espírito” que gera humor no
poema acontece no abraço dos pássaros. Já nos acostumamos à ideia de que
cada ave é representada pelo classificador de média-distância com o
antebraço e a mão fechada na forma de um bico, mas quando eles se
abraçam, os dois classificadores criam a forma de um coração. O prazer
em ver a imagem e entender que é feita pelos dois classificadores sempre
gera risadas.

### 19.5. Expressão facial e movimentos corporais exagerados 

Vemos frequentemente o uso de exagero no humor em Libras. Quando ele
acontece, sugerimos que alguma coisa ou qualidade é mais do que ela na
verdade é. Podemos exagerar uma coisa para ela ser grande demais, ou o
tamanho pequeno pode ser exagerado para algo parecer ainda menor.
Bergson observa que a transposição de um conceito para outro ambiente
pode envolver uma mudança de escala, especialmente de pequena à grande,
e isso é o uso do exagero. Quando o exagero for especialmente algum tipo
de distorção daquilo que é normal, o resultado frequentemente é cômico.

> Falar das pequenas coisas como se fossem grandes é, de modo geral,
> exagerar. O exagero é cômico quando é prolongado e sobretudo quando é
> sistemático: de fato, é o caso quando surge como processo de
> transposição. Faz rir tanto que alguns autores chegaram a definir o
> cômico pelo exagero (BERGSON, 1983, p. 60).

Geralmente, os sinais humorísticos mostram elementos maiores e são
feitos com movimentos ampliados e mais fortes. Sinais maiores criam um
efeito cômico do qual o objetivo é exagerar ou caricaturar, expandindo
uma distorção para torná-la mais saliente.

Sinais menores também criam humor ao se reduzir um aspecto de algo,
depreciar alguma coisa ou diminuir seu poder através do ridículo ou da
incongruência. Tudo isso acontece pelo processo de “inversão” descrito
por Bergson. O que foi grande agora parece pequeno e, portanto, é
bem-humorado. Veremos isso nos sinais de vocabulário, ainda neste
capítulo.

Esse exagero é tão comum nas peças cômicas que quase todas elas têm
exageros de alguma forma. Destacamos alguns exemplos: nas histórias *[Um
Morcego Surdo no Busão](#vimeoModal-356028141)* e *[A
Vaca Surda de Salto Alto](#vimeoModal-356033857),* de
Marina Teles, a expressão facial é intensificada especialmente para
divertir o público infantojuvenil. Na história de Rodrigo Custódio da
Silva, [*Eu x Rato*](#ytModal-UmsAxQB5NQA), a
expressão facial e os movimentos são exagerados para criar mais humor
quando o narrador explica que não pode alcançar o pau para matar o rato
e quando o bicho pula perto do rosto do personagem depois de ter seu
rabo liberado. No poema , de Nelson Pimenta, o movimento dos sinais é
exagerado e a expressão facial é muito maior do que o normal, ainda mais
intensificada pela proximidade da câmera. No poema de perspectiva , de
Anna Luiza Maciel, as expressões faciais e os movimentos do corpo são
exagerados. Vemos isso também em , de Rimar Segala, na descrição
exagerada dos dois jogadores e ainda em [*A Pedra
Rolante*](#ytModal-kPXWu5UCTzk), contada por Sandro Pereira.

O exagero pode ser acompanhado por sinais mais lentos. Eles podem também
ser realizados lentamente para que o público tenha tempo de apreciar a
sinalização exagerada e engraçada. A sinalização em câmera lenta é
também uma fonte de entretenimento. Embora sintamos a dor da , rimos ao
ver o jogo em câmera lenta, com a expressão não manual e o movimento da
cabeça exagerados.

A caricatura é um tipo de exagero que dá ênfase aos aspectos visuais de
uma pessoa, muitas vezes com a intenção de zombar. Isso acontece, por
exemplo, com o rosto ou outra parte do corpo, ou, ainda, pode ser uma
imitação exagerada do jeito que uma pessoa sinaliza, fala ou se move.
Pode-se exagerar suas roupas, em acessórios ou qualquer outro elemento
visual do seu comportamento. Vemos caricatura em personagens de
profissionais familiares às pessoas surdas (tais como médicos,
professores, assistentes sociais e pessoas ouvintes em geral), ou de
qualquer pessoa ou coisa.

No entanto, devemos lembrar que o exagero em si não é cômico. Por
exemplo, nos contos , de Aulio Nóbrega, e em [*Galinha
Fantasma*](#ytModal-isyT8-mgCDM),
de Bruno Ramos, os movimentos do corpo e a expressão são exagerados, mas
com o objetivo de criar uma sensação de medo e não de causar o riso.
Para criar um efeito cômico, ele precisa enfatizar a incongruência, ou
os elementos de algum tipo de distorção, transposição ou inversão
descritos por Bergson.

No exagero, a incorporação e o uso criativo de classificadores são muito
importantes. Hal Draper afirmou:

> Lembro de um amigo vindo para a Associação de Surdos numa noite em que
> estava nevando. Ele disse que estava caminhando para a Associação e
> uma pessoa estava atrás dele e tinha escorregado e caído. Se eu
> estivesse falando e contando isso a você, você pensaria “OK e daí?”.
> Mas a maneira como ele explicou com os classificadores, o movimento
> corporal e a expressão facial de andar feliz e descuidado, conversando
> e brincando sobre ir à Associação, tomando uma cerveja e vendo o homem
> andar e escorregar e cair bruscamente, aquilo foi muito impressionante
> para mim, porque eu pude realmente me colocar no lugar do homem’
> \[sinalizou literalmente “ter empatia” a fim de expressar “trocar de
> sentimentos com ele”\]. É tão fluido e expressivo!”. (HAL DRAPER,
> comunicação pessoal, março de 2005, tradução nossa)

Exageramos também os personagens não humanos. Já falamos do
antropomorfismo em Libras (no capítulo 17). Vimos que valorizamos as
habilidades de mostrar animais e objetos (como avião, árvore ou celular)
como se fossem humanos em Libras e, muitas vezes, as características são
exageradas. As expressões faciais que apresentam as reações e o
comportamento desses objetos não humanos fazem parte do humor em língua
de sinais. E a graça de ver o desconhecido se tornar familiar (ou o
familiar se tornar desconhecido?) é o que faz ficar engraçado.

### 19.6. Estrutura interna do sinal

Um tipo de humor em Libras brinca com a estrutura do sinal, alterando um
parâmetro e mantendo os outros. Esse tipo de humor revitaliza os
parâmetros no sinal de maneira original e significativa. “Revitalizar”
significa que o parâmetro já tem um significado, que é originalmente
icônico, mas que ninguém presta mais atenção a ele (veja no capítulo 04,
em que Cuxac e Sallandre, 2008, falam de intenção ilustrativa). Quando
uma pessoa está resfriada e espirra, sinalizamos <span
style="font-variant:small-caps;">saúde!</span> Esse sinal tem
configuração de mão, movimento e locação convencional, motivados
historicamente pelo fato de o médico fazer exames com um estetoscópio no
peito do paciente. Mas, sinalizar <span
style="font-variant:small-caps;">saúde!,</span> mantendo todas as
estruturas habituais, porém com a locação no nariz e não no peito,
*revitaliza* o sinal <span
style="font-variant:small-caps;">saúde,</span> porque agora a saúde tem
seu foco no nariz. Embora o médico não coloque o estetoscópio no nariz,
o humor vem do novo lugar com o novo sentido <span
style="font-variant:small-caps;">saúde-do-nariz</span>.

#### 19.6.1. Configuração de mão

Existem muitos exemplos de mudança de configuração de mão de um sinal
para revitalizar o sentido do parâmetro. Lembramos que o humor muitas
vezes é criado pelo exagero do tamanho. Podemos brincar com o número de
dedos na configuração de mão para mostrar que quanto mais dedos forem
usados para articular o sinal, maior a sua intensidade. Sabemos que os
sinais <span style="font-variant:small-caps;">difícil</span> e <span
style="font-variant:small-caps;">ponto-de-interrogação</span> são feitos
com um único dedo, mas, quando são realizados com quatro dedos, mostram
que algo é muito difícil ou gera muitos pontos de interrogação,
respectivamente (com o significado “esta é uma questão importante e
problemática”). O sinal <span
style="font-variant:small-caps;">paciência</span> é feito com dois
dedos, mas para mostrar muita paciência humoristicamente, utilizamos
todos os dedos, o que dobra o tamanho do sinal e assim “dobra” a
paciência (Figura 6).

![Paciência (citação e exagerado)](/images/p3-c19-paciencia.png)

<div class="legenda"></div>

Figura 6: Paciência (citação e exagerado)

Podemos brincar também com a configuração de mão para a redução
exagerada de sinais. O sinal <span
style="font-variant:small-caps;">aplauso</span> em Libras é apresentado
ao bater palmas ou acenar com as mãos. Para aplaudir um pouco, o
sinalizante pode simplesmente bater os dedos mindinhos ou acenar com
eles (Figura 7). Uma pessoa sinalizou puxando dois fios de cabelo da
cabeça e tocando-os delicadamente juntos, criando aplausos muito
pequenos. Para que isso tenha um efeito máximo, o movimento do sinal
também deve ser reduzido e a expressão facial deve mostrar a ideia de
redução. O sinal <span style="font-variant:small-caps;">entender</span>
é feito com todos os dedos. Para sinalizar que se entendeu só um
pouquinho, pode-se usar o dedo mindinho, mantendo os outros parâmetros.

![Aplauso (citação e exagerado)](/images/p3-c19-aplauso.png)

<div class="legenda"></div>

Figura 7: Aplauso (citação e exagerado)

As diferentes partes da mão podem representar diferentes pontos de
articulação. O sinal <span
style="font-variant:small-caps;">ocupado</span> é feito com dois dedos
em “V”, no local da garganta. Para exagerar e dizer que realmente se
está ocupado – é impossível interromper – é possível utilizar duas mãos
em contato com a garganta. Mas podemos reduzir o sinal humoristicamente
também. As pontas dos dedos de um “V” em garras podem contatar o polegar
da mesma mão, mostrando que alguém está ocupado, mas a pessoa não sente
muito por isso. O polegar representa a garganta para reduzir o tamanho
do sinal. A expressão facial que acompanha o sinal também mostra que a
pessoa não está muito incomodada (Figura 8).

![Ocupado (citação e exagerado)](/images/p3-c19-ocupado.png)

<div class="legenda"></div>

Figura 8: Ocupado (citação e exagerado)

#### 19.6.2. Locação

Já vimos que a nova locação do sinal <span
style="font-variant:small-caps;">saúde</span> pode gerar humor. O sinal
<span style="font-variant:small-caps;">entender</span> feito na barriga
pode mostrar que, embora a cabeça não entenda que não possa comer mais,
a barriga entende.

A mudança da locação também pode fazer parte das brincadeiras de
exagero. Por exemplo, uma aluna diz deve ir para uma sessão de
orientação com seu orientador. O sinal <span
style="font-variant:small-caps;">orientação</span> usa a borda fina da
mão aberta com os dedos retos (configuração de mão chamada “B”) na mão
passiva. Um amigo brinca com ela e observa que a aluna recebe muita
orientação (talvez mais do que seja o normal). Uma maneira não
humorística de expressar isso seria aumentar o tamanho e o número dos
movimentos da mão ativa. O humorista, no entanto, usa a mesma
configuração de mão e movimento, mas usa todo o antebraço como base para
fazer um sinal que significa <span
style="font-variant:small-caps;">muita-orientação</span>. A aluna
responde que, uma vez esclarecida sobre como fazer seu trabalho,
precisará de pouca orientação, agora usando apenas o mindinho como
articulador de base. Nesse exemplo, o tamanho do articulador e o tamanho
do movimento ao longo da mão base criam a brincadeira com os sinais, e a
expressão facial exagerada de “muito” ou “um pouco” aumenta o humor
(Figura 9).

![Orientação (citação e exagerado maior e menor)](/images/p3-c19-orientacao.png)

<div class="legenda"></div>

Figura 9: Orientação (citação e exagerado maior e menor)

### 19.7. Humor bilíngue

Traduções por empréstimo podem fazer parte de jogos linguísticos
bilíngues. O objetivo é transpor a palavra em português para o novo
contexto, mantendo o significado superficial de partes do vocábulo, mas
perdendo o significado certo durante a tradução. O espírito está na
criação de uma palavra ridícula e absurda, também no prazer e na
satisfação de resolver o enigma. Podemos dividir a palavra “salmão” em
duas palavras “sal” e “mão”, mesmo que não façam sentido no vocábulo. A
tradução para os sinais <span
style="font-variant:small-caps;">sal</span> e <span
style="font-variant:small-caps;">mão</span> é absurda no contexto da
nova língua, e o público vai rir porque os sinais e as palavras têm uma
ambiguidade que não combina com o original. “Já pão” sinalizado como
<span style="font-variant:small-caps;">já</span> e <span
style="font-variant:small-caps;">pão</span> pode significar que a pessoa
já fez alguma coisa com pão, mas podemos retraduzir os sinais para criar
a palavra “Japão”, que não é o sinal original <span
style="font-variant:small-caps;">japão</span>.

A palavra “absurdo” pode ser sinalizada com as letras “A” e “B” como
sinal de <span style="font-variant:small-caps;">surdo</span>. Podemos
criar uma sobreposição das configurações de mão em uma nova locação no
sinal “surdo”, com A na orelha e B no queixo.

As brincadeiras até podem entrar na Libras como vocabulário
convencional. Viracopos (a cidade no estado de São Paulo) gera um sinal
complexo no conceito de tradução, em que o sinalizante literalmente pode
virar o sinal <span style="font-variant:small-caps;">copo</span>.

<div class="float"></div>

![resumo](/images/resumo-parte3.png)

### 19.8. Resumo

A pesquisa sobre o humor em Libras é ainda incipiente, mas o uso dos
recursos humorísticos na comunidade surda já é bem estabelecido. Vimos
que o humor surge ao se *“transpor a expressão natural de uma ideia para
outra tonalidade”.* A seleção de uma imagem desconhecida de algo já
conhecido e familiar é o início de muito humor. Sobre o humor em relação
ao antropomorfismo, Bergson afirma que “*Rimo-nos sempre que uma pessoa
nos dê a impressão de ser uma coisa”*. O exagero é uma parte importante
do humor; ao selecionarmos uma característica de uma pessoa ou coisa e
distorcê-la, invertemos o mundo que conhecemos para que o pequeno vire
grande e o grande vire pequeno. Vimos esses princípios básicos nas
principais fontes de humor em Libras que destacamos aqui – as imitações,
o exagero, os jogos linguísticos e o humor bilíngue.

<div class="float"></div>

![atividade](/images/atividade-parte3.png)

### 19.9. Atividade

Assista aos vídeos:

-   *[Passeio no Rio de Janeiro](#vimeoModal-203292043),* de
    Leonardo Adonis de Almeida e Marcelo William da Silva.

-   *[A Pedra Rolante](#ytModal-kPXWu5UCTzk),* de Sandro
    Pereira.

* [*Golf Ball*](#ytModal-Gl3vqLeOyEE), de Stefan Goldschmidt.

* *[Um Morcego Surdo No Busão](#vimeoModal-356028141),*
de Marina Teles.

Nesses vídeos, onde está o humor nos sinais?
