---
capitulo: 8
parte: 2
titulo: "Gêneros: definidos pela origem, pelo conteúdo e pelo público"
pdf: "http://files.literaturaemlibras.com/CP08_Generos_definidos_pela_origem_pelo_conteudo_e_pelo_publico.pdf"
video: XwFL3MEG22I
---

## 8. Gêneros: definidos pela origem, pelo conteúdo e pelo público

<div class="float"></div>

![objetivo](/images/objetivo-parte2.png)

### 8.1. Objetivo

No capítulo anterior, refletimos sobre a divisão de gêneros com base na
ideia de ficção e não ficção e com base na forma da literatura. Neste
capítulo, vamos considerar mais alguns exemplos de literatura em Libras
e ver outras maneiras de categorizar a sua linguagem estética.

<div class="float"></div>

![lista de videos](/images/videos-parte2.png)

<div class="lista-de-videos"></div>


Neste capítulo falaremos sobre os seguintes vídeos:

* *[A Pedra Rolante](#ytModal-kPXWu5UCTzk),* de Sandro Pereira.

* *[Cinco Sentidos](#ytModal-AyDUTifxCzg),* de Nelson Pimenta.

* [Coleção de narrativas didáticas curtas](https://vimeo.com/showcase/6241328), de Marina Teles.

* *[Five Senses](#ytModal--qLcuxfdoYY),* de Paul Scott.

* *[Hino Nacional](#ytModal-jHfBUvskvbw),* de Bruno Ramos.

* [Coleção de Mãos Aventureiras](https://www.ufrgs.br/maosaventureiras/),
de Carolina Hessel.

* *[O símbolo do Olodum](#ytModal-_jhW0K4NN0A),* de Priscila
Leonnor.

* *[Vagalume](#ytModal-JG8xrh1az_g),* de Tom Min
Alves.

### 8.2. Gênero definido pela origem

Ao considerar as categorias de uma forma de arte de Libras fortemente
ligada à cultura surda, precisamos nos perguntar: de onde veio essa
literatura? Sua origem está na communidade e na cultura dos ouvintes ou
no mundo surdo? Surgiu no Brasil ou em outro país? Foi criada por um
autor conhecido ou faz parte da literatura em Libras que a comunidade vê
como parte de seu próprio folclore?

#### 8.2.1. Folclore

Folclore é o termo coletivo dado às tradições, aos costumes, aos rituais
e às formas de expressar a experiência de um grupo particular. Esses
costumes são passados às novas gerações por meio de histórias, poemas,
piadas ou outros usos de linguagem; normalmente, não são escritos e não
têm um autor conhecido (DUNDES, 1965). A palavra portuguesa “folclore”
vem da inglesa “folklore”. “Folk” significa “povo” e esse povo inclui
qualquer pessoa de uma comunidade com uma cultura própria. O “lore” está
relacionado ao conhecimento da comunidade. Então, o folclore é
caracterizado em termos de origem (não sabemos quem o criou), forma
(geralmente tem muita repetição e rimas simples, por exemplo),
transmissão (é principalmente não escrito) e função (normalmente é usado
para educar e entreter, também para fortalecer a identidade da
comunidade).

As piadas em Libras são um ótimo exemplo de folclore porque não sabemos
quem as criou. Essas narrativas curtas com um desfecho engraçado
simplesmente “surgem” sem autor, têm uma estrutura convencional, muitas
vezes com repetição, são transmitidas sem registro formal e funcionam
para entreter, mas muitas vezes também para educar a comunidade sobre
regras de comportamento. Também, utilizam muitos elementos linguísticos
que vemos em outros gêneros de literatura surda como poesia e narrativas
não humorísticas. Vamos falar mais de piadas surdas no capítulo 12, mas,
por enquanto, podemos destacar uma das piadas mais conhecidas da
comunidade surda, que trata da árvore surda.

Um lenhador corta uma árvore. Ele grita: “madeira!”, e a árvore cai. Um
outro dia ele corta mais uma árvore e grita: “madeira!”, mas ela não
cai. Então ele chama um médico, que faz alguns exames, relata que a
árvore é surda e orienta o lenhador a aprender a língua de sinais. O
lenhador vai à associação de surdos e aprende Libras. Depois, ele volta
à floresta e soletra M-A-D-E-I-R-A e a árvore cai.

Em muitas culturas é impossível separar os conceitos “folclore” e
“literatura”, porque os textos que chamamos de literatura contém
elementos oriundos dos textos que chamamos de folclore. Talvez essa seja
uma divisão meramente artificial e ligada ao poder social, imaginando-se
que a literatura é para as pessoas cultas e o folclore é para o restante
do povo. Essa não é uma distinção que achamos útil no caso da literatura
em Libras.

O folclore linguístico inclui jogos da língua e piadas, gestos
convencionais, comentários frente a atos de excreção do corpo (ex:
arrotos, gases e espirros), mitos, lendas, narrativas populares,
provérbios, insultos, provocações, trava-línguas, formas de saudação e
despedida, nomes (de lugares e de pessoas, incluindo apelidos) e poesia
popular (por exemplo, poesias tradicionais para crianças e canções
infantis)[18](#tooltip). Tudo isso é visto em Libras, mas faltam ainda pesquisas
sistemáticas sobre esse tipo de folclore em língua brasileira de sinais.
Certamente, a cultura surda tem muitos exemplos de folclore linguístico
em Libras, sendo uma mistura rica da cultura brasileira com a cultura
surda. Por isso, podemos dizer que o folclore da Libras faz parte da
literatura surda baseada na sua origem, ou seja, de dentro da comunidade
surda brasileira.

#### 8.2.2. Histórias traduzidas

Embora muito da literatura em Libras tenha sua origem no folclore da
comunidade surda brasileira, veremos no capítulo 22 que grande parte
dela vem de fora e foi trazida através da tradução. Assim, podemos
descrever um gênero de “Literatura em Tradução”.

As traduções de livros de outras línguas para o português são muito
comuns no Brasil, por isso existem gêneros reconhecidos como
“literatura francesa em português” ou “literatura inglesa em
português”. No entanto, a tradução da literatura de outras línguas de
sinais para Libras ainda é incomum, embora existam alguns exemplos
importantes. O conto de Libras de Nelson Pimenta chamado [*O
Passarinho
Diferente*](#ytModal-P_DR60BJuFI)
(aliás, notável por ser atipicamente longo – com quase meia hora de
duração) é uma tradução de uma história da ASL, *Bird of a Different
Feather*, atribuída a Ben Bahan. Da mesma forma, seu poema [*Cinco
Sentidos*](#ytModal-AyDUTifxCzg) é traduzido de um poema em
BSL, [Five Senses](#ytModal--qLcuxfdoYY), de Paul Scott. Sandro
Pereira traduziu *Ball Story*, de Ben Bahan, para Libras como [*A
Pedra Rolante*](#ytModal-kPXWu5UCTzk). Quanto mais crescer o
contato e a troca de ideias entre artistas de língua de sinais em
diferentes comunidades surdas, mais essas traduções aumentarão.\
\
Muitos textos traduzidos na literatura em Libras vêm de textos
escritos em português. São frequentemente voltados para a educação e
traduções de clássicos de autores como Machado de Assis e Carlos
Drummond de Andrade. Exemplos incluem, ainda, *Iracema* (de José de
Alencar, 2002), *O velho da horta* (de Gil Vicente, 2004), *O
Alienista* (de Machado de Assis, 2004), *O Caso da Vara* (de Machado
de Assis, 2005)[19](#tooltip). São feitas para dar acesso ao conteúdo dos
textos escritos, muito importante para o aluno surdo (SPOONER, 2016),
porém poucos surdos adultos procuram espontaneamente essas traduções
por prazer e diversão.

Existem histórias infantis que fazem parte do folclore da sociedade
ouvinte, que são **adaptadas** como parte do processo de tradução com
o objetivo de incluir personagens surdos e a Libras. As adaptações são
recentes e, no passado, algumas pessoas não achavam certo alterar
textos, preferindo respeitar a forma original deles. No século XXI, no
entanto, algumas traduções em Libras acrescentaram adaptações. Por
exemplo, Silveira, Karnopp e Rosa (2003) criaram a *Rapunzel Surda*,
que aprende Libras com um príncipe surdo; e a *Cinderela Surda*, que
perde sua luva (as luvas são ligadas às mãos e assim são uma marca do
sujeito surdo) em vez de seu sapato. Betty Lopes de Andrade (2015)
descreve uma versão dos "*Três Porquinhos*" em Libras em que o lobo
sinaliza tão rápido que o vento gerado por suas mãos destrói as duas
primeiras casas.


Uma terceira origem da literatura sinalizada
que está fora da comunidade surda é da tradução de filmes. Bahan (2006)
propôs uma categoria de histórias de ASL baseadas em filmes e essas
histórias também são populares em Libras. São traduções dos filmes, mas
ao invés de serem realizadas entre duas línguas, elas partem de um
sistema visual (a “gramática” do filme) para outro sistema visual (a
língua - Libras). O conteúdo das histórias cinematográficas vem do filme
(de modo que raramente descrevem a experiência específica do mundo
surdo, embora os surdos possam compartilhar essa experiência), mas quem
conta em Libras escolhe aqueles de ação que possibilitam a melhor
recontagem visual, porque há pouco diálogo e muito movimento. A
habilidade de tradução nessas histórias reside na maneira de reproduzir
o impacto visual do filme em Libras por meio de classificadores e
incorporação, por exemplo.

### 8.3. Definido pela origem e pela forma: canções 

O gênero músicas em Libras é polêmico, já que não se originam dentro da
cultura surda. Músicas são caracterizadas por elementos baseados mais no
som, principalmente na relação entre a linguagem das letras (traduzível,
até um certo ponto, de uma língua para a outra) e a música, que é
basicamente uma arte sonora. As músicas sinalizadas são quase sempre
traduzidas. Ao contrário de outros gêneros que se originaram fora da
comunidade surda, mas que podem refletir as experiências surdas, como o
cinema, o gênero musical não é naturalmente parte da experiência surda
devido à estreita relação entre a letra e a música. Ter acesso à letra
sem ter acesso à música pode ser frustrante para alguns surdos. Contudo,
as pessoas ouvintes que conhecem Libras tendem a gostar muito da
experiência de ver as letras das canções em Libras e ouvi-las
simultaneamente.

Cabe ressaltar que algumas pessoas surdas podem ouvir música,
especialmente com a ajuda de aparelhos de amplificação sonora ou sentir
as vibrações que vêm do som. Podem também ter a memória de alguma época
em que podiam ouvir mais. Na internet, ainda, é possível localizar
vídeos de canções traduzidas para Libras postados por pessoas surdas.
Além disso, Jonatas Medeiros, tradutor e pesquisador da Universidade
Federal do Paraná – UFPR –, descreveu maneiras de traduzir a música e as
letras para o público surdo, traduzindo a emoção gerada pelo sentido da
canção não apenas pelo movimento rítmico do corpo do tradutor, mas
através das luzes, vibrações, do movimento e da presença da multidão em
um show.


Apesar das polêmicas geradas por traduções que oferecem pouca satisfação
às pessoas surdas, algumas comunidades surdas têm tradições relacionadas
à arte da linguagem que incluem traduções de músicas. Cynthia Peters
(2000) observou que as línguas de sinais têm ritmos visuais e uma
musicalidade visual que sempre estiveram associados ao corpo. Todas as
pessoas batem palmas, batem os pés e dançam. Esses elementos corpóreos,
frequentemente associados à música, são facilmente transferidos para a
Libras. Priscila Leonnor, no poema [*O símbolo do Olodum*](#ytModal-_jhW0K4NN0A), mescla a poesia em Libras com a
dança do samba e os ritmos do samba-reggae com o acompanhamento sonoro e
de vibrações de uma bateria. É uma produção original em Libras que
mostra a identidade de uma brasileira negra e surda da Bahia, que segue
muitos dos elementos utilizados nas canções traduzidas.

Uma tradução importante de música que faz parte da vida da comunidade
surda brasileira é a do *Hino Nacional*. Algumas versões dele são
traduções aproximadas das palavras que ele contém, enquanto outras são
adaptações ou interpretações mais livres das ideias expressas nele. A
versão do [Hino Nacional](#ytModal-jHfBUvskvbw) de Bruno Ramos é
mais próxima de uma releitura e combina a apresentação da letra com
técnicas de produção de vídeo, com mudanças de cores da camiseta do
artista (que refletem as cores da bandeira brasileira) e diferentes
imagens do patrimônio natural e histórico do Brasil.

Outras produções são feitas a partir de hinos religiosos, que são atos
de adoração. Eles são traduzidos para Libras, muitas vezes, por coros e
destinados para aqueles que participam de eventos religiosos, de modo
que possam acessá-los, sentindo as emoções que geram, na sua própria
língua. Quando os hinos são traduzidos por pessoas que conhecem os
princípios poéticos da Libras, eles se tornam mais
literários.


As traduções de outras músicas, particularmente da música contemporânea,
são cada vez mais comuns e vídeos como esses são frequentemente
publicados na internet (RIGO, 2013, 2020). Às vezes, esse último tipo de
tradução é feito por surdos, mas é predominantemente realizado por
ouvintes. Estes, geralmente, são aprendizes de línguas que gostam de
brincar com sua nova língua e para quem a experiência de ouvir a música
e ver os sinais é particularmente gratificante. Porém, também, há
traduções apreciadas por muitas pessoas, surdas e ouvintes, no que diz
respeito às formas poéticas da Libras e por suas técnicas de produção.
Um exemplo valorizado é a tradução de 
[Vagalume](#ytModal-JG8xrh1az_g), de Tom Min Alves (vale anotar como ele troca chapéu
e camisa para refletir as mudanças entre a música mais melódica e o funk
mais rítmico).

### 8.4. Gênero definido pelo conteúdo

A pesquisadora Janaina Peixoto (2016) criou categorias temáticas para 70
poemas em Libras em seu corpus de análise e observou as seguintes
temáticas definidas pelo conteúdo: Mundo Surdo (26), Religião (13),
Datas Comemorativas (12), Amor (7), Terra Natal (4), Natureza (3) e
Outras (5). As narrativas de experiência pessoal fazem parte do que
Peixoto chamou de Mundo Surdo.

#### 8.4.1. Narrativas da experiência surda

Nessas histórias, as pessoas surdas narram os eventos que aconteceram
nas suas vidas. Embora possamos dizer que as narrativas da experiência
surda sejam literatura surda de não ficção, original ou folclore
(definidas conforme a sua origem), sua principal característica é o
conteúdo sobre a “experiência surda”. As narrativas podem ter a forma de
histórias em prosa, poemas ou teatro, porque a forma em que elas são
contadas é menos importante do que o tópico que apresentam.

É importante que ao serem contadas pela pessoa sobre si ou sobre outra
pessoa, tenham um protagonista surdo e que o que aconteça na história só
possa acontecer a essa pessoa porque ela é surda, ou seja, a história
não aconteceria com um ouvinte. Os tópicos incluem, entre outros, a
infância, as experiências do trabalho e com viagens (especialmente sobre
os encontros com outras pessoas surdas nessas viagens). As narrativas
sobre os encontros com pessoas ouvintes frequentemente informam o
público surdo sobre os desafios da vida sofridos pelas pessoas surdas e
as maneiras de superá-los. Isso transforma as histórias de experiências
particulares de uma pessoa em “Experiência Surda” de modo geral, por ser
mesma realidade vivenciada por outros surdos também. Nelas, o
protagonista pode ser qualquer surdo, com o qual o contador de histórias
espera que o público possa se relacionar. O ponto importante é que os
surdos contam essas histórias para que outras pessoas surdas possam se
identificar com a experiência de vida de "alguém como eu".

### 8.5. Gênero definido pelo público-alvo 

O público-alvo pode designar o gênero. Por exemplo, podemos categorizar
poemas adequados para jovens ou crianças. Outros são mais literários e
elaborados para um público com mais experiência. Alguns são feitos para
diversos níveis de público – do iniciante àquele com muita habilidade.
Por isso, veremos que os gêneros literários em Libras são para todos,
não apenas para uma elite de pessoas bem escolarizadas.

Obras em Libras de diferentes origens têm conteúdo e formas diferentes
de acordo com o seu público-alvo. Alguns tipos de literatura em Libras
são adequados para aprendizes de Libras – sendo eles adultos ou
crianças, surdos ou ouvintes. Alguns são bons para pessoas com pouca
escolarização e que sabem pouco sobre a literatura surda, como as
apresentações de teatro em que as pessoas participam. Também temos a
literatura infantil e a literatura feminina em Libras, que são parecidas
de alguma maneira, destinadas aos públicos que têm características
semelhantes, embora o gênero de literatura feminina seja destinado às
mulheres e o de literatura infantil seja destinado às crianças.

#### 8.5.1. Literatura feminina

Nodelman (1988) destacou que a literatura infantil e a literatura
feminina geralmente são parecidas. A literatura infantil se concentra na
vida de pessoas (ou animais) sem poder, crianças, homens ou mulheres,
que precisam lidar com uma hierarquia que os coloca no fundo. Além
disso, livros infantis caracteristicamente revelam o poder dos fracos:
agressores poderosos, definidos como vilões, geralmente perdem para
pequenos pacifistas.

Sendo assim, Nodelman (1988, p33) cita Rachel Blau DuPlessis:

> *O que aqui chamamos de (a) estética feminina acaba sendo um nome
> especializado para quaisquer práticas disponíveis para esses grupos -
> nações, gêneros, sexualidades, raças, classes - todas as práticas
> sociais que desejam criticar, diferenciar, derrubar as formas
> dominantes de conhecer e entender com as quais estão saturadas.
> NODELMAN, 1988, P.33 (Tradução nossa)*

Esses temas não exclusivos da literatura feminina, destinada
principalmente a um público de mulheres, que é também caracterizada por
histórias em que o protagonista é feminino e a maioria dos personagens é
feminina, incluindo a família e os amigos. Geralmente se concentram numa
protagonista feminina que enfrenta uma situação difícil que afeta a vida
das mulheres. No entanto, a análise das pesquisadoras surdas americanas
Christie & Wilkins (2007) sobre os conteúdos de literatura surda mostra
que as mulheres falam de autobiografia, mas não apenas sobre “ser filha,
ser esposa, ser mãe” (ver capítulo 21).

#### 8.5.2. Literatura infantil

A literatura infantil é um gênero definido pela faixa etária do
público-alvo, destinado a crianças pequenas e crianças mais velhas. A
literatura em Libras destinada aos jovens adultos é outra categoria. A
idade do público-alvo determina o conteúdo e a forma da linguagem.

O gênero de literatura infantil, escrita para crianças ouvintes, tem uma
longa história, com diversos contos de fadas destinados às crianças
registrados no século XVII e que se expandiu principalmente na Europa do
século XIX. O gênero de literatura infantil brasileira, escrita em
português, começou a se consolidar apenas no início do século XX.

Desde o início do século XXI, um número crescente de histórias em Libras
foi filmado e disponibilizado para alunos surdos. Como a tecnologia de
vídeo se torna cada vez mais sofisticada e ao mesmo tempo acessível, há
mais opções para se usar uma mistura de sinais e imagens no meio visual
de contar histórias para crianças pequenas. A maioria dessas histórias é
baseada em livros infantis escritos e são traduções ou recontos.

No site 
[Mãos Aventureiras](https://www.ufrgs.br/maosaventureiras/), por exemplo,
Carolina Hessel conta histórias em Libras de livros ilustrados ao redor
do mundo. As histórias são apresentadas em Libras de uma forma clara e
imaginativa, adequada para os mais jovens, intercaladas com ilustrações
dos livros para que as crianças possam associar os sinais às imagens. O
foco é na Libras, e não nas palavras escritas. Os livros escolhidos para
esse site são escritos em diferentes idiomas, mas isso não é um problema
para qualquer livro traduzido em Libras, se o foco for em Libras.

Para muitos ouvintes, histórias são fortemente ligadas ao objeto *livro*
e muitos professores entendem que os livros são a maneira de introduzir
as crianças surdas à literatura, mesmo em Libras. Quando se lê histórias
de livros para crianças surdas, contando-as em Libras, isso é sempre uma
tradução. Mas o uso da Libras para contar a história de um livro busca
incentivar as crianças surdas a ler, que é importante na educação
bilíngue. Além disso, a experiência ensina as crianças sobre a sociedade
e a cultura que elas compartilham com as pessoas ouvintes.

Alguns livros de histórias são escritos especificamente para crianças
surdas, com personagens surdos (KARNOPP, 2008). Esses livros fornecem
imagens positivas para elas, especialmente quando os autores são surdos.
Exemplos incluem *A Cigarra Surda e as Formigas*, de Carmen Elisabete de
Oliveira e Jaqueline Boldo (2003), *Tibi e Joca*, de Cláudia Bisol
(2001) e *O Patinho Surdo*, de Rosa e Karnopp (2005). No entanto, a
maioria dos livros de histórias que uma criança surda lê ou examina
contém histórias escritas para crianças ouvintes e não contém
personagens surdos ou se refere à experiência específica de uma pessoa
surda.

Quando as crianças pequenas são introduzidas à leitura, a esperança é de
que elas descubram os prazeres e a satisfação da leitura de modo que
esta se torne algo que a criança desfrute. Desejamos que as crianças
aprendam a amar a leitura e as histórias contadas nos livros. No
entanto, é importante que o desejo de ensinar um aluno surdo não destrua
o prazer da leitura. Os adultos podem usar a leitura como uma
oportunidade educacional, ajudando as crianças a aprenderem mais sobre
sua língua e seu mundo, mas essa normalmente não é a perspectiva da
criança. Muitos livros e muitas histórias para crianças surdas têm o
objetivo explícito de ensiná-las a linguagem escrita. Precisamos que
histórias em Libras direcionadas às crianças sejam vistas em textos
bilíngues, onde a Libras dirija a história e que haja uma versão em
português ao invés de esperar que a Libras siga o texto em português.

Infelizmente, as histórias de ficção originais criadas por surdos e
contadas em Libras, sobretudo para crianças mais novas, são poucas. Para
incentivar esses pequenos a desenvolverem sua própria criatividade em
Libras, precisamos de mais histórias originais para complementarem as
traduções. Sabemos que é difícil para muitos adultos surdos criarem
essas histórias (mesmo aqueles adultos que contam histórias tradicionais
em Libras fluente, muito visual e atraente às crianças surdas) quando
estes também cresceram com apenas livros de autores ouvintes como seus
modelos de literatura.

As crianças mais novas precisam de histórias originais em Libras com
sinais simples para acompanharem a literatura. Elas gostam de magia,
antropomorfismo de animais e gostam de rir de situações inesperadas ou
incongruentes.[20](#tooltip) Uma 
[Coleção de narrativas didáticas curtas](https://vimeo.com/showcase/6241328) feita por Marina Teles mostra essas características. É
importante para a criança surda ver personagens surdos nas narrativas
originais em Libras, isso cria empatia com contextos com os quais elas
podem se identificar. Elas também gostam de ver histórias com desafios a
serem superados pelo personagem surdo.

As crianças maiores, os adolescentes e adultos já não acham mais
interessante a magia e os animais na Libras. Histórias de ação
(especialmente para os meninos) e situações mais "reais" são preferidas,
mas ainda nos contextos de locais com os quais o público surdo pode se
identificar. A forma da Libras já é mais complexa e criativa porque esse
público já domina a língua, então o artista pode apresentar alternativas
para essa forma que eles conhecem, tais como novos sinais,
classificadores criativos ou elementos cinematográficos. Porém, essas
pessoas mais velhas ainda gostam de ver personagens surdos na literatura
para poderem experienciar a empatia e ver os desafios que o surdo define
e supera.

<div class="float"></div>

![resumo](/images/resumo-parte2.png)

### 8.6. Resumo

Nesse capítulo, focamos em mais gêneros de literatura em Libras. Vimos
que existem tipos de literatura com origem na comunidade surda
brasileira que têm muito a ver com o folclore. Também vimos que a
literatura vem de fora da comunidade. Pode vir por tradução e adaptação
de outras comunidades surdas mundiais, da literatura escrita em
português ou até de filmes e programas de televisão. Destacamos as
músicas em Libras como um gênero polêmico, que de certa forma é alheio
às tradições culturais da comunidade surda, mas que, por outro lado, foi
adaptado para fazer parte da vida dos surdos. Definindo os gêneros por
conteúdo, focamos nas narrativas de experiência surda, que podem ter
qualquer forma (história, poema ou teatro), mas que sempre falam do
protagonista surdo. Pensando no público, vimos que a literatura
destinada às crianças surdas tem muitas formas, origens e diferentes
conteúdos, mas é apresentada com objetivo de ensinar e entreter.

<div class="float"></div>

![atividade](/images/atividade-parte2.png)

### 8.7. Atividade

Assista à versão do Hino Nacional mencionada nesse capítulo. Apesar de
ser exemplo de literatura traduzida, qual o conteúdo e quem é o seu
público-alvo?

Assista a uma narrativa de experiência de pessoas surdas. O que os
membros da comunidade podem aprender ao assistirem a essa narrativa? O
que os ouvintes aprendizes de Libras podem aprender ao assistirem a essa
narrativa?

<span class="tooltip-texto" id="tooltip-18">
A lista veio de Dundes, 1965.
</span>

<span class="tooltip-texto" id="tooltip-19">
<a href="https://www.editora-arara-azul.com.br" target="_blank">www.editora-arara-azul.com.br</a>
</span>

<span class="tooltip-texto" id="tooltip-20">
Incongruente significa sem lógica ou o que não combina, como por
    exemplo uma vaca de salto alto ou uma pessoa botando uma meia na
    mão.
</span>
