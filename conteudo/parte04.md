---
capitulo: 4
parte: 1
titulo: Como criamos o “visual” nas línguas de sinais
pdf: "http://files.literaturaemlibras.com/CP04_Como_criamos_o_visual_nas_linguas_de_sinais.pdf"
video: AcK2VfUNtyg
---

## 4 Como criamos o “visual” nas línguas de sinais

<div class="float"></div>

![lista de videos](/images/objetivo-parte1.png)

### 4.1. Objetivo

Neste capítulo pensamos sobre como o artista surdo pode usar todos os
recursos estéticos da língua para apresentar imagens visuais e para
gerar emoções por meio da visão. Um dos principais objetivos da
literatura em Libras é criar imagens fortemente visuais. Muitos sinais
da língua são icônicos, indicando que há uma relação visual entre a
forma do sinal e o objeto ao qual se refere. Mas o objetivo desses
sinais é simplesmente comunicar ou dizer alguma coisa e não há uma
**intenção ilustrativa** quando são utilizados (CUXAC; SALLANDRE, 2008).
Por outro lado, existem sinais que são diretamente icônicos e as pessoas
os criam especialmente com essa intenção ilustrativa (veja também
CAMPELLO, 2007). É como o artista atinge a intenção ilustrativa que é o
foco deste capítulo.

<div class="float"></div>

![lista de videos](/images/videos-parte1.png)

<div class="lista-de-videos"></div>

Antes de ler mais sobre o assunto, vamos assistir a alguns exemplos de
literatura em Libras. Como sempre, vale a pena assistir a cada vídeo
pelo menos duas vezes. Os poemas geram efeitos visuais agradáveis por
meio das imagens visuais criadas na apresentação dos sinais, mas usam
diferentes recursos para isso.

* [*Lei de Libras*](#vimeoModal-267274663), de Anna Luiza Maciel e Sara Theisen Amorim.

* [*O Modelo do Professor Surdo*](#ytModal-rverroKm8Bg) de Wilson Santos Silva.

* [Saci](#ytModal-4UBwn9242gA), de Fernanda Machado, baseado no poema original do poeta britânico Paul Scott *[Tree](#ytModal-Lf92PlzMAXo)* (*Árvore*).

### 4.2. Intenção ilustrativa: Contar, mostrar ou se tornar?

Há três principais opções, no que se refere à intenção, ao se comunicar
em Libras:

**Contar** – falar sobre as coisas por meio de vocabulário (como vimos
no poema [*Lei de Libras*](#vimeoModal-267274663)).

**Mostrar** – mostrar a forma e o movimento das coisas com
classificadores (por exemplo, no poema
[*Saci*](#ytModal-4UBwn9242gA)).

**Se tornar** – mostrar a forma e o comportamento das coisas através da
incorporação (como vimos em [*O Modelo do Professor Surdo*](#ytModal-rverroKm8Bg)).

Diante disso, ao estudar a estética da linguagem literária em Libras,
precisamos pensar sobre o uso dessas três ações.

Todas as línguas têm elementos icônicos, mesmo as línguas faladas, na
modalidade oral e na sua escrita. Gestos, frequentemente, acompanham a
fala, mas as pessoas geralmente acham que os gestos são uma maneira
inferior de comunicar e que eles apenas dão suporte à mensagem falada.
As pessoas que acreditam que as línguas de sinais são “apenas gestos”
interpretam erroneamente tanto a natureza das línguas de sinais quanto a
natureza dos gestos. Precisamos enfatizar que, apesar de a Libras ser
uma língua de base visual, ela é uma língua “real”.

Todas as línguas têm maneiras de identificar referentes. O referente é a
coisa da qual se fala. Um objeto (por exemplo, a lua), uma ação (por
exemplo, sentar-se), uma qualidade (por exemplo, vermelho, redondo ou
novo) ou uma relação (por exemplo, no espaço, como em cima ou atrás ou,
ainda, no tempo como antes ou depois), todos são referentes. Outros são
emoções (como orgulho, alegria ou raiva) ou ideias abstratas (como
importância, economia ou herança). Cada língua tem seu próprio jeito de
categorizar e expressar esses referentes.

A comunicação baseada na visão usa a experiência visual e as
características visuais dos referentes sempre que possível, enquanto a
comunicação baseada no som usa qualquer som relacionado aos referentes.
A maioria das pessoas, surdas ou ouvintes, pode ver as coisas no mundo.
Os referentes têm forma, tamanho e outras qualidades, bem como uma
localização no espaço em que podem se movimentar. Os referentes são
todos visíveis, mas poucos deles produzem som. As línguas de sinais são
baseadas no raciocínio visual em todos os níveis porque são produzidas
por um meio visual. Seria estranho (e não faria sentido) se os sinais
visuais *não* apresentassem as relações visuais entre o sinal e o
referente. A ideia de que deve haver uma relação arbitrária entre
palavras e seus referentes vem das línguas orais, que são baseadas no
som. Isso não ocorre nas línguas de sinais porque existe algum problema
com elas, mas sim porque as línguas orais têm menos opções para usar os
sons de seus referentes do que as línguas de sinais têm para usar a
visão.

Sarah Taub (2012, p. 400) observou que a “\[i\]conicidade motiva, mas
não determina a forma dos sinais icônicos”. Podemos ver isso nas
variações regionais de sinais para os mesmos referentes em Libras. Os
diferentes sinais para referir à cor branca são todos icônicos, de
alguma forma, mas têm formas diferentes dependendo da imagem que é
escolhida para representar o sinal.

Para a literatura em Libras isso é importante, pois é a característica
essencial básica da sinalização estética. Isso dá aos sinalizantes
criativos a oportunidade de escolher qual a representação icônica a ser
usada para determinado referente. Se houvesse apenas um jeito de
produzir um sinal visual para cada referente, a literatura de língua de
sinais criativa não existiria assim como a conhecemos.

### 4.3. Sem intenção de ilustrar: sinais do vocabulário lexical e convencional 

Alguns sinais são criados, principalmente, para contar ou nomear o
referente, ao invés de mostrar, e os sinalizantes talvez nem percebam a
sua forma visual. Apesar de a maioria dos sinais em Libras serem até
certo ponto icônicos[11](#tooltip), os sinalizantes não usam cada sinal com a
intenção de produzir uma imagem visual. Muitos sinais que fazem parte do
vocabulário da Libras são, de certa forma, icônicos, mas a motivação
visual por trás dos sinais tem diminuído ao longo do tempo. Hoje, os
sinalizantes não têm necessariamente a intenção específica de ilustrar
quando usam esses sinais, mas apenas de identificar sobre o que estão
falando. O pesquisador francês Christian Cuxac (2000) chama isso de
“iconicidade degenerada”.

A forma desses sinais escolhe uma característica visual única do
referente para tornar o sinal útil para aludir a referentes genéricos
usando a linguagem visual, mas nem o sinalizante nem a pessoa com quem
ele fala estão particularmente cientes das imagens visuais fortes. Por
exemplo, o sinal em Libras CACHORRO é baseado no focinho
arredondado de um cachorro estereotipado e genérico, mesmo que exista
muito mais num cachorro do que o seu focinho e que este possa ter formas
diferentes. É por isso que os linguistas entendem que os sinais, no
vocabulário da língua de sinais, têm um significado geral e são usados
para identificar referentes. Mas esses sinais não mostram coisas
especificamente visuais, sendo assim, podemos dizer que eles, os sinais
de vocabulário estabelecidos, não têm intenção ilustrativa deliberada
(TAUB, 2001).

Se observarmos os sinais usados em *[Lei de
Libras](#vimeoModal-267274663),* veremos que a maioria
deles é de sinais de vocabulário, como é o caso de LIBRAS, LEI, ADQUIRIR, PRODUÇÃO,
SINALIZAR, COMUNICAR, EXPRESSÃO-FACIAL, VALORIZAR, POESIA, HUMOR,
TEATRO, VERNÁCULO-VISUAL, É, SEMPRE, NÓS Um estrangeiro que não
conheça a Libras não será capaz de adivinhar a maioria desses sinais,
mesmo que os usuários de Libras sintam que são icônicos. Isso ocorre
porque a iconicidade é reduzida e não há intenção de ilustrar. O prazer
estético que sentimos ao ver esses sinais no poema não se deve aos
sinais em si, mas em como eles são apresentados em um dueto, com os dois
sinalizantes ajustando o tempo de seus sinais, fazendo-os
simultaneamente e juntos construindo as imagens visuais com seus sinais.

No entanto, alguns sinais em *[Lei de
Libras](#vimeoModal-267274663)* são de tipos diferentes
e eles são criados para o contexto do poema para serem deliberadamente
visuais. Agora vamos falar sobre esses outros tipos de sinais que são
usados com **intenção deliberadamente ilustrativa**.

### 4.4. Intenção de ilustrar 

Os sinalizantes usam certos sinais para mostrar algo visualmente e assim
eles “dizem ao mostrar” ou “mostram enquanto contam”. Isso é importante
para nossa discussão sobre a literatura em Libras e a estética da
sinalização. Os sinais que têm a intenção de ilustrar têm sido chamados
por Cuxac de **estruturas altamente icônicas**. As estruturas altamente
icônicas ocorrem em muitos tipos de discursos e são especialmente
importantes para a literatura porque o foco da sinalização estética
geralmente é criar imagens visuais.

Às vezes, os artistas de Libras partem de um sinal de vocabulário com
iconicidade degenerada ou reduzida para torná-lo novamente icônico e
possivelmente fazer com que ele fique tão visual quanto era
originalmente. Dessa forma, os artistas alertam as pessoas sobre o
imaginário que está por trás do sinal (CUXAC; SALLANDRE, 2008; CAMPELLO,
2007).

Podemos dizer que as estruturas altamente icônicas são criadas quando os
sinalizantes transferem imagens da sua experiência visual do mundo real
diretamente para a língua de sinais. Estas são chamadas transferências
de pessoa (no Brasil geralmente chamadas de “incorporação”),
transferências de forma e transferências de tamanho (no Brasil,
geralmente chamadas de “classificadores”). A Libras criativa usa esses
tipos de transferência.

#### 4.5. Transferência de Pessoa - Incorporação

Na **transferência de pessoa**, o referente foi transferido para o
sinalizante de forma que o público entende que o sinalizante é a
pessoa, o personagem ou o referente. As mãos do sinalizante são
entendidas como as mãos do referente, os olhos do sinalizante significam
os olhos do referente e assim por diante. Se as mãos atuam como se
estivessem manipulando um objeto como, por exemplo, carregando e depois
abaixando uma maleta, pode-se entender que o personagem que atua como o
referente é quem está manipulando a maleta. Isso vale também para
elementos não manuais. Então, se o sinalizante olha para cima,
entendemos que o personagem está olhando para cima. Quando os
sinalizantes criam esse tipo de imagem visual, elas são apresentadas
especialmente no plano vertical, como em uma tela de cinema, e o
sinalizante está em seu centro.

Quando vemos o poema [*O Modelo do Professor
Surdo*](#ytModal-rverroKm8Bg), de Wilson Santos Silva,
observamos que o efeito visual se dá através do uso de incorporação, ou
transferência de pessoa. A mão fechada de Wilson representa a mão
fechada de um professor segurando a sua maleta, mas ele não sinaliza
PROFESSOR nem MALETA.

Sabemos que se trata de um professor apenas por causa do título e porque
o nosso conhecimento de mundo nos diz que é um professor movendo algo
naquela direção e que, provavelmente, está segurando uma maleta. A mão
aberta do sinalizante faz um movimento carinhoso que interpretamos, a
partir de nosso conhecimento sobre a cultura brasileira, como sendo a
mão de um professor acariciando a cabeça dos seus alunos[12](#tooltip), ainda que
ele não sinalize ALUNOS.
Seu rosto mostra uma expressão de preocupação quando seus olhos miram os
alunos, mas ele não sinaliza PREOCUPADO Seu corpo e sua
cabeça se movem de forma que parecem pertencer a alguém mais baixo do
que o professor e a expressão facial é a de falta de entendimento,
quando as mãos se movem aleatoriamente para que o público entenda que o
poeta muda de papel nesse momento e se torna o estudante que não sabe a
língua de sinais. O poeta não nos revela isso usando o vocabulário de
sinais, mas ele *mostra* isso. Quando o professor se lembra da sua
própria infância e do seu deleite com a compreensão da Libras, ele não
usa sinais como, LEMBRAR, EU, APRENDER, ENTENDER mas se mostra lembrando e depois mostra uma
versão mais jovem de si mesmo enquanto essas coisas acontecem. Ao longo
do poema, o poeta nunca *identifica* nada, apenas *mostra*, através de
ações de outros. De certa forma, o sinalizante se torna um ator
representando as partes de outros personagens, embora ele siga ainda as
regras gramaticais e espaciais da Libras.

#### 4.5.1. Transferências de forma

Sinais que mostram transferências de forma geralmente são descritos na
pesquisa linguística como aqueles que utilizam **classificadores de
entidades**. Descrevem o referente através de diferentes configurações
de mãos que refletem a sua forma ou o seu tamanho geral, como por
exemplo, ao se mostrarem planas, longas e finas, redondas, sólidas ou
tendo pernas e outras partes que se projetam a partir de um ponto
central. Cada língua de sinais tem seu próprio conjunto de configurações
de mãos convencionais, embora todos sejam visualmente motivados. Por
exemplo, a configuração de mão classificadora para uma bicicleta em
movimento em Libras usa a configuração de mão “X” (dedo indicador
curvado). Já em ASL, usa-se a configuração de mão de polegar “3” (como
no sinal CAVALO em Libras,
por exemplo) e em BSL se usa a configuração de mão plana “B”.

Estas configurações de mãos convencionais são posicionadas e se movem no
espaço de sinalização de modo que refletem o espaço e o movimento do
mundo real para mostrar onde está o referente e como ele se move. Por
exemplo, se a bicicleta se move da esquerda para a direita no mundo real
(ou, ao menos, no mundo real da história), o sinalizante move o
classificador de entidades que representa a bicicleta da esquerda para a
direita.

Quando os sinalizantes criam esse tipo de imagem visual, eles a
apresentam num espaço em três dimensões, em frente ao corpo; e o
sinalizante está situado atrás da imagem (ao invés de em seu centro,
como ocorre na incorporação). O público pode sentir que a imagem está
situada no plano horizontal, ao invés do vertical, como se fosse um
palco plano no qual os personagens se movem e atuam (ao invés de uma
tela de cinema vertical, como na incorporação). Ao invés de atuarem como
atores, os sinalizantes são narradores contando e mostrando ao público
onde estão as coisas, como ou onde elas se moveram. Eles também podem se
juntar ao público para assistir à ação se desenrolar no palco que
criaram.

Vimos, anteriormente, que em transferências de pessoa, ou seja, na
incorporação, os articuladores do sinalizante são entendidos como
articuladores do referente – a mão é a mão, a boca é a boca e assim por
diante. Em transferências de formas, no entanto, entende-se que o
articulador se refere à entidade. Um único dedo apontado para cima em
Libras (e em muitas outras línguas de sinais) é entendido como algo que
se *mostra* e, assim, *significa* uma pessoa de pé, provavelmente pronta
para se mover (na transferência de pessoa, ou incorporação, o mesmo dedo
apontando para cima significaria uma pessoa com um dedo apontando para
cima).

O poema [*Saci*](#ytModal-4UBwn9242gA), de Fernanda Machado, é
baseado no poema *[Tree](#ytModal-Lf92PlzMAXo)* (Árvore), do
poeta britânico Paul Scott. Nele a mão em forma de “O” faz círculos para
mostrar o formato e o movimento do sol nascendo e se pondo. Sua mão se
torna o sol. Ela não usa o sinal SOL ou SOL-NASCENTE. É esperado que o
público entenda isso na medida em que o poema progride. A mão em forma
de “Y” se move da direita para a esquerda. Essa mão se torna o Saci.
Sabemos que no folclore brasileiro o Saci tem uma perna só, então o dedo
indicador para cima, normalmente usado em Libras para identificar uma
pessoa se movendo, não seria entendido como uma criatura que só tem uma
perna. Por isso, a configuração de mão inovadora e criativa que usa o
minguinho para “ficar sobre” corresponde melhor à forma do Saci. O Saci
cava um buraco com sua perna para plantar a semente da árvore enquanto o
minguinho se movimenta para tapar a terra. Depois, a mesma configuração
de mão muda de orientação e vira um regador (usado pelo Saci, que vemos
incorporado na atriz Fernanda). Antes, o minguinho era a perna e o
polegar era a cabeça do Saci, mas agora o minguinho é o cano e o polegar
é a alça do regador. Em seguida, o Saci (apresentado por meio de
incorporação) dá uma baforada no cachimbo. A mesma configuração de mão
se tornou o cachimbo - o polegar faz a piteira do cachimbo e o minguinho
faz a cabeça deste. Fernanda nunca sinaliza SACI, REGADOR ou CACHIMBO, mas as suas mãos se
tornam esses referentes.

O dedo indicador então passa a significar o broto de uma árvore - sua
mão toda aberta e virada para baixo representa as raízes da árvore; o
antebraço e a mão se tornam o tronco e os galhos de uma árvore madura
quando a mão está virada para cima. Sendo assim, o movimento, a
orientação e a localização da mão também têm significado, pois estes se
somam à configuração de mão para mostrar como o referente se parece,
onde está e como se move. As configurações de mãos são altamente
criativas e fortemente visuais, criando uma história estética.

Ao longo do mesmo poema, também vemos a incorporação como pano de fundo.
Os classificadores são a principal maneira de mostrar informação,
enquanto a mão se torna todo o referente (por exemplo, o Saci ou a
árvore) mas, além disso, o corpo de Fernanda, sua cabeça e seus olhos se
tornam o corpo, a cabeça e os olhos do referente (do Saci e da árvore).
Quando o Saci se encosta na árvore, a mão que representa o Saci se
inclina para o lado e o corpo e a cabeça de Fernanda se inclinam na
mesma direção com o mesmo tempo de movimento. Assim, ela está
simultaneamente dizendo “este é o Saci se encostando na árvore” e “eu
sou o Saci e estou me encostando na árvore”.

Esse uso de múltiplas perspectivas é comum na literatura de língua de
sinais e é uma parte importante da sinalização visual. Nós vemos o uso
desse recurso repetidamente no poema quando suas mãos se tornam o
papagaio, a onça e o Saci velho e quando seu corpo incorpora os dois ao
mesmo tempo. Às vezes o corpo incorpora um personagem assistindo o que
outro personagem exibido com a mão está fazendo. O Saci (incorporado)
olha para cima para ver o sol viajando pelo céu (articulado pelo
classificador) e a árvore (incorporada), frequentemente, olha para
outras criaturas que passam (articulados pelos classificadores).

<div class="float"></div>

![lista de videos](/images/resumo-parte1.png)

### 4.6. Resumo

Vimos que há muitas opções na Libras para criar imagens visuais na
imaginação do público e algumas são apresentadas de forma mais visual
que outras. Usamos três obras de literatura em Libras para ilustrar
esses aspectos.

O poema [*Lei de Libras*](#vimeoModal-267274663) usa o
vocabulário da Libras. Uma pessoa que não sabe Libras vai entender pouco
sobre o conteúdo do poema. É um trabalho muito estético em sua maneira
de apresentar o poema através de um dueto que gera um efeito visual
agradável, mas cada sinal não cria uma “imagem no ar” (em inglês
“Pictures in the air”, STEPHEN BALDWIN, 2009).

O poema [*O Modelo do Professor Surdo*](#ytModal-rverroKm8Bg) é
feito totalmente por incorporação dos personagens. Não é mímica, mas sim
uma forma literária de Libras mais conhecida como Vernáculo Visual (que
vamos explorar mais profundamente no Capítulo 7) - mas o importante
agora é entender que a língua estética cria imagens no próprio corpo.

O poema [*Saci*](#ytModal-4UBwn9242gA) usa apenas
classificadores. Embora a poeta incorpore os personagens, todos os
sinais manuais são classificadores. Os sinais apresentam uma imagem
mental criada pelos sinais visuais estéticos. É como se ela estivesse
criando imagens no espaço em frente ao seu corpo por meio dos sinais.

Anna Luiza e Sara, no poema *[Lei de Libras](#vimeoModal-267274663),*
dizem: “Nomeamos e
identificamos essas ideias numa forma estética e visual”. Wilson, no
poema *[O Modelo do Professor Surdo](#ytModal-rverroKm8Bg),*
diz: “Sou os personagens”. E Fernanda, no poema
[*Saci*](#ytModal-4UBwn9242gA), diz: “Aqui estão os
personagens”. 

<div class="float"></div>

![lista de videos](/images/atividade-parte1.png)

### 4.7. Atividade

Assista novamente aos três poemas citados nesse capítulo.

Escolha:


<div class="lista-com-letras"></div>

1) Cinco sinais que “contam” ou “nomeiam” (isto é, que são sinais do
vocabulário da Libras)

1) Cinco sinais que “mostram” (isto é, sinais em que a mão é o
referente)

1) Cinco sinais em que o artista “se tornou o referente”

**Se preferir, você pode usar outro poema ou conto para fazer a mesma
atividade.**

<span class="tooltip-texto" id="tooltip-11">
Uma exceção importante é o grupo de sinais com base no alfabeto
    manual.
</span>

<span class="tooltip-texto" id="tooltip-12">
Devemos observar que isso faz parte da cultura brasileira, mas
    não acontece em outros países onde professores raramente tocam em
    seus alunos. Tal fato nos mostra que a poesia em Libras é
    verdadeiramente brasileira, bem como surda.
</span>