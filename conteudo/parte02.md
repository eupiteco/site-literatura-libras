---
capitulo: 2
parte: 1
titulo: A análise da literatura em Libras
pdf: "http://files.literaturaemlibras.com/CP02_A_analise_da_literatura_em_Libras.pdf"
video: Z2g6xLOIVI4
---

## 2. A análise da literatura em libras 

<div class="float"></div>

![objetivo](/images/objetivo-parte1.png)

### 2.1. Objetivo

Neste capítulo, vamos conhecer uma parte mais prática do estudo da
literatura em libras. Um dos objetivos de se pensar sobre essa
literatura é simplesmente conhecê-la e apreciar a sua beleza. Mas,
para pensar numa forma mais crítica sobre a literatura, cada vez que
assistirmos a um vídeo, vamos fazer um tipo de análise prática. Às
vezes, a análise poderá ser simples, outras vezes, talvez, façamos uma
análise detalhada, mas podemos seguir os mesmos passos. Não há uma
maneira “certa” de analisar a literatura em libras, porém aqui
oferecemos algumas dicas que achamos úteis.

A análise ajuda a descobrir e identificar os elementos que fazem parte
de um todo. Há muitas teorias sobre a “melhor” maneira de estudar e
analisar a literatura. Algumas exigem que as pessoas foquem na
linguagem, outras no contexto social, outras, ainda, que se observe as
diversas perspectivas políticas sobre o contexto da literatura. Quando
sabemos quais são os elementos que nos interessam, podemos tentar
entender a relação entre eles para que possamos compreender melhor o
todo. Porém, seja qual for o objetivo da análise, precisamos aprender
a **observar, descrever** e **explicar** o que estamos vendo.

A observação não é fácil. Cada vez que assistimos a um vídeo de uma
boa obra de literatura em libras, vemos sempre mais coisas que não
tínhamos percebido antes. É importantíssimo realmente *ver* o texto e
não simplesmente *olhar* ele. O que vamos descrever e explicar depende
do nosso interesse. Por exemplo, se temos interesse no uso da língua,
que é o foco deste livro, na estrutura das narrativas e dos poemas ou
nos movimentos físicos do artista, podemos prestar atenção nisso.
Normalmente, não queremos descrever tudo dentro de um texto de
literatura em libras (que será até quase impossível, porque há tantas
coisas para estudar), mas sim escolher algumas partes ou determinados
aspectos como, por exemplo, o espaço, a expressão não manual, o uso de
classificadores ou a incorporação dos personagens. Ou, se temos
interesse nos temas e na representação dos personagens, procuramos
elementos que possam ajudar a estudar isso.

### 2.2. Passo a passo para o estudo de vídeos de literatura em libras

Vamos supor que a obra literária esteja gravada e estamos estudando o
texto através de um vídeo. Hoje em dia existem muitos textos
literários em libras disponíveis por meio desse registro, os quais
podemos estudar. É muito bom participar de eventos literários de
libras ao vivo, mas não é possível estudar e analisar as obras em
tempo real. Alguns pesquisadores afirmam que os estudos da literatura
em línguas de sinais começaram apenas a partir do desenvolvimento do
vídeo (KRENTZ, 2006; ROSE, 1992), porque antes disso não era possível.
Talvez a gravação da literatura em libras tenha criado alguns
elementos que não existiam antes nas versões ao vivo. Talvez seja até
isso o que queremos estudar.

O primeiro passo de uma análise é assistir ao vídeo da produção do
texto. Ao vê-lo pela primeira vez, simplesmente observe a
apresentação. Depois, você pode refletir e anotar o que sentiu. Esse
momento de reflexão inicial é muito importante. Foi interessante? O
que achou interessante? Se não achou interessante, por quê? Gostou ou
não? Se gostou, por quê? Se não gostou, por que não? O que você achou
de “bom” na obra? Você percebeu se não entendeu algumas coisas?
(talvez você pense que entendeu bem e depois vê que se enganou, mas
isso não importa agora).

Assista ao vídeo novamente. Você vai começar a observar coisas que não
percebeu na primeira vez – talvez elementos do conteúdo, sinais ou
expressões faciais. Quando você já conhece uma história, por exemplo,
sabe o que acontece no final e quem são os personagens. A partir daí
você pode observar mais aspectos que contribuíram à obra[10](#tooltip). Por
exemplo, o final da narrativa poética
[*party*](#ytModal-s8bly1nm7qq) (em português *a festa*) de
johanna mesch, pode deixar o espectador confuso. Quem a assistiu deve
ver de novo e, já sabendo sobre o final, entender por que aconteceu
assim.

Uma pessoa que é aprendiz de libras pode observar os sinais que não
conhece ou o que não pegou de primeira. Não importando o nível de
fluência na língua, é interessante ver algumas partes do vídeo em até
0,25% de velocidade. Isso vai possibilitar uma nova perspectiva sobre
o texto.

Depois de ver o vídeo três vezes, talvez você ainda não tenha
entendido tudo. Isso não é um problema, porque estamos lidando com a
literatura e sabemos que há muitas razões pelas quais não
compreendemos um texto literário, embora o conheçamos.

### 2.3. O contexto de uma obra literária em libras

É bom começar o estudo de cada poema, narrativa ou outra obra
literária em libras com as seguintes perguntas:

<div class="identado"></div>

1.  **Onde e quando a obra foi apresentada?**

<div class="identado"></div>

Foi gravada num lugar com público ao vivo? Aconteceu num festival, em
uma festa, manifestação ou em outro evento? Em um estúdio? Em uma sala
de aula? É contada junto com outras imagens ou atividades?

<div class="identado"></div>

2.  **Quem a apresenta?**

<div class="identado"></div>

Não é apenas o nome da pessoa que importa, mas o “perfil” dela. É
surda ou ouvinte (às vezes, não sabemos)? É professor/a, intérprete,
aluno/a ou artista reconhecido/a? Quantos anos mais ou menos, ele/a,
tem? É homem ou mulher? Talvez a raça/etnia, a religião ou a região
geográfica da pessoa importe. Podemos observar muitos elementos de seu
perfil, enquanto outros dados são encontrados nas informações que
acompanham o vídeo ou podem, ainda, ser pesquisados em outras fontes.

<div class="identado"></div>

3.  **Por que foi apresentada?**

<div class="identado"></div>

A obra é para entreter (a quem conta ou a quem vê)? É para o ensino,
tem objetivo didático? É para celebrar as conquistas de uma pessoa ou
um evento? É para estudar a linguagem literária ou o conteúdo? É para
dar acessibilidade aos surdos a uma obra já conhecida na sociedade
brasileira dos ouvintes?

<div class="identado"></div>

4.  **Qual a origem e contexto?**

<div class="identado"></div>

É uma tradução de um texto (de um livro ou outro tipo, como um filme)?
É um “reconto” de uma história (por exemplo: uma piada, fábula, lenda
ou um conto de fadas)? Tem adaptações para se tornar mais agradável ao
público surdo? É um texto original do autor? Vem da experiência
pessoal de quem conta? É ficção ou fato?

<div class="identado"></div>

5.  **Qual é o seu público?**

<div class="identado"></div>

O texto é destinado a um público de que idade? Às crianças (de qual
idade)? Jovens? Adultos? Surdos fluentes em Libras? Ouvintes que
estudam Libras? Amigos do artista? Outros artistas surdos? Um público
grande e desconhecido?

<div class="identado"></div>

6.  **Qual o grau e o tipo de participação do público?**

<div class="identado"></div>

É formal, por exemplo um vídeo preparado e gravado sem público? É uma
performance, por exemplo do Hino Nacional ou de uma oração na igreja?
Esse tipo de formalidade tem regras e convenções específicas sobre a
participação dos outros? O artista conversa diretamente com o público
(por exemplo, cumprimenta-o antes de começar ou explica para eles o
contexto da obra)?

Cada pergunta vai nos ajudar a entender melhor a obra para fazer uma
análise melhor sobre ela. Não é preciso entrar em detalhes para
responder a cada pergunta (talvez não saibamos, pois a resposta não é
fácil de encontrar), mas é útil pelo menos refletir um pouco sobre o
seu contexto.

Podemos dividir a análise de um texto literário em três partes
principais (mas interligadas). Você pode focar na parte que achar mais
interessante:

- Na performance (isto é, como a pessoa apresenta ou faz o trabalho e
o contexto em que acontece)

- No conteúdo (isto é, o que tem dentro da obra. Qual o tópico, o
tema? Quem são os personagens? O que acontece? É uma metáfora?)

- Na forma da linguagem usada (por exemplo, tem muito vocabulário ou
não? Tem muitos classificadores? Usa muitas expressões faciais ou
espaço simétrico?)

### 2.4. Sugestões para análise da forma de um texto literário em Libras

A respeito da forma, existe um sistema para a análise da literatura em
qualquer língua de sinais criado pelo grupo ECHO, coordenado por Onno
Crasborn, na Holanda (publicado por Nonhebel, Crasborn e van der
Kooij, 2003). O grupo sugere que podemos incluir os seguintes
elementos linguísticos em uma análise e para cada sinal ou trecho de
sinais podemos marcar assim:

<div class="identado"></div>

1.  **A glosa do sinal**

<div class="identado"></div>

Glosa é uma tradução básica de cada sinal usando palavras em língua
portuguesa (no caso do Brasil). O registro em forma escrita não
precisa ser uma tradução em português e podemos, também, fazer uma
transcrição em SignWriting - mas o português facilita a busca feita no
computador. Usamos *versalete* para glosar (letras maiúsculas do
tamanho de minúsculas). Assim, por exemplo, os sinais que correspondem
aos referentes “pássaro” e “olhar” são grafados como <span
style="font-variant:small-caps;">pássaro</span> e <span
style="font-variant:small-caps;">olhar</span>. Esse recurso ajuda na
análise quando se juntam elementos com um sinal que equivale ao sinal
no vídeo. Podem-se anotar diversos elementos da forma pela glosa:

<div class="identado"></div>

- Quando há soletração (às vezes com o símbolo \# na frente da glosa)

- Apontando com o dedo indicador (às vezes marcado como “In.” ou
“Ix.”)

- Se faz um sinal com uma mão só quando deve haver duas mãos ou, ao
contrário, com duas mãos quando deve haver uma mão

- Se o sinal é um classificador, por exemplo, <span
style="font-variant:small-caps;">CL-PÁSSAROS-SE-ENTRE-OLHAM</span>
Veremos, nesse exemplo, que, onde um sinal precisa mais de uma palavra
para ser traduzido, juntamos as palavras com traços ou hifens (isso é
muito comum nos sinais com classificadores e incorporação)

- Se o sinal fica parado por muito tempo

- Se o sinal é mais um gesto (por exemplo, encolher os ombros em vez
de usar o sinal <span
style="font-variant:small-caps;">não-sei</span>)

<div class="identado"></div>

Podemos ter uma glosa para a mão dominante (normalmente a mão direita)
e uma para a mão não dominante (geralmente a mão esquerda).

<div class="identado"></div>

2.  **Repetições do sinal**

<div class="identado"></div>

O sinal pode ser repetido três ou até quatro vezes. Não precisamos
escrever a glosa quatro vezes, mas apenas anotar que foi repetido
quatro vezes. Por exemplo: <span
style="font-variant:small-caps;">BATA-AS-ASAS</span> x4.

<div class="identado"></div>

3.  **Direção de movimento de locação do sinal**

<div class="identado"></div>

Marcamos isso apenas se o lugar não for esperado. A maioria dos sinais
do vocabulário da Libras especifica o local do sinal, especialmente os
sinais que têm contato com o corpo. Mas, às vezes, os sinais do
vocabulário estão colocados em novos lugares, então devemos marcar
onde foi feito o novo sinal. Normalmente, no entanto, precisamos
marcar as locações dos sinais que são classificadores ou que apontam
com o dedo indicador, porque os sinais que indicam lugares e os
classificadores não têm um lugar específico. Os artistas de Libras
irão colocar esses sinais em diversas áreas do espaço de sinalização e
nossa análise pode mostrar onde estão.

<div class="identado"></div>

Para a mão direita:

<div class="identado"></div>

d-90 = significa que a mão está ao lado direito, a 90° do eixo central
do espaço (bem no limite da extensão do braço para o lado direito do
corpo)

<div class="identado"></div>

d = significa que a mão está ao lado direito, 45° do eixo central do
espaço (que é o local confortável para localizar um sinal “à direita”)

<div class="identado"></div>

(“e-90” e “e” são iguais para o lado esquerdo)

<div class="identado"></div>

Normalmente, o espaço neutro de sinalizar está na frente da pessoa, a
uma altura média entre a cintura e o peito. Se não está lá, podemos
colocar:

<div class="identado"></div>

F=mais em frente do que o normal

<div class="identado"></div>

P=mais perto do corpo do que o normal

<div class="identado"></div>

C=mais acima do que o normal

<div class="identado"></div>

B=mais abaixo do que o normal

<div class="identado"></div>

4.  **Movimento da cabeça**

<div class="identado"></div>

Podemos marcar:

<div class="identado"></div>

Acenar, sacudir, inclinar (e quantas vezes ocorrem – por exemplo:
acenar x3).

<div class="identado"></div>

5.  **Posição das sobrancelhas**

<div class="identado"></div>

Levantadas, franzidas.

<div class="identado"></div>

6.  **Abertura dos olhos**

<div class="identado"></div>

Piscando, bem abertos, franzidos, fechados.

<div class="identado"></div>

7.  **Direção de olhar**

<div class="identado"></div>

Igual à posição das mãos (por exemplo “d” ou “d-90”). Também observar
as mãos.

<div class="identado"></div>

8.  **Movimentos da boca**

<div class="identado"></div>

O movimento da boca vem da língua portuguesa ou não? Segue o movimento
das mãos? Mostra emoção ou ação da boca numa incorporação?

<div class="identado"></div>

9.  **Papel do ator**

<div class="identado"></div>

O ator é narrador ou personagem? Podemos anotar cada papel e cada
mudança de papel.

<div class="identado"></div>

10.  **Observações e anotações**

<div class="identado"></div>

11.  **Tradução**


Além desses aspectos, podemos marcar simetria, velocidade, contato
entre as mãos, pausas, rimas, o que quisermos. Porém, é importante
lembrarmos que vale a pena anotar em detalhes apenas os elementos que
vamos usar na nossa análise. Se tivermos interesse pelo uso da boca,
não precisa fazer descrição detalhada do uso de espaço, mas, se
tivermos interesse nos sinais classificadores ou no uso do espaço,
também, poderemos anotar ambos, os olhos e a boca.

No passo a passo para preparar uma análise,
conforme Nonhebel, Crasborn e van der Kooij, sugerimos o uso do software
ELAN, que permite uma análise escrita junto ao vídeo da história ou do
poema. Não é nossa intenção descrever como funciona o ELAN, mas quem
tiver interesse em usá-lo para fazer uma análise do texto literário pode
encontrar mais informações no site do ELAN
[aqui](https://tla.mpi.nl/tools/tla-tools/elan/) e no [Manual de
Transcrição do Corpus de
Libras](http://www.corpuslibras.ufsc.br/informacoesdoprojeto?lang=ptbr).

<div class="float"></div>

![resumo](/images/resumo-parte1.png)

### 2.5. Resumo

Nesse capítulo, fizemos uma introdução aos passos para análise de
literatura em Libras. Embora os objetivos de cada análise sejam
diferentes, seguimos sempre os passos de **observar**, **descrever** e
**explicar**. Independentemente de o nosso interesse estar mais
centrado no conteúdo das obras, na estrutura delas ou na linguagem
produzida, é recomendado considerar o contexto sociocultural da
produção antes de começar a fazer a análise. As dicas do grupo
holandês ECHO ajudam a quem quiser analisar os traços linguísticos das
produções literárias, como faremos na maior parte deste livro.


<div class="float"></div>

![resumo](/images/atividade-parte1.png)

### 2.6. Atividade

Escolha um trecho de uma obra de literatura em Libras (por exemplo: [Como Veio Alimentação](#ytModal-nMOTYprbYoY), 
de Fernanda Machado, ou outra que você quiser). Siga os
passos sugeridos por Nonhebel, Crasborn e van der Kooij para fazer a
análise dos sinais.

* Os passos ajudaram você a realmente observar a obra?

* Quais as dificuldades que você enfrentou?

* Você pensou como esses passos podem ajudar você na sua análise de uma
obra de literatura em Libras?



<span class="tooltip-texto" id="tooltip-10">
Uma vez assisti a uma narrativa que contava sobre o cachorro do
    narrador. Quase no final, o cachorro deu luz a seis filhotes. Era
    fêmea! Precisei ver a história mais de uma vez para entender tudo de
    uma nova maneira sabendo desse fato fundamental.
</span>