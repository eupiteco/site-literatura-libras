---
capitulo: 24
parte: 4
titulo: Levando a Literatura em Libras para o Futuro
pdf: "http://files.literaturaemlibras.com/CP24_Levando_a_literatura_em_Libras_para_o_futuro.pdf"
video: 80OhgaFdsQM
---

## 24. Levando a literatura em Libras para o futuro

<div class="float"></div>

![objetivo](/images/objetivo-parte4.png)

### 24.1. Objetivo

Neste capítulo final, pensaremos sobre a importância do ensino da
literatura em Libras às pessoas surdas e ouvintes de diversas idades e
para diversos fins. O ensino e a aprendizagem da literatura em Libras
são fundamentais tanto para alunos surdos quanto para adultos surdos,
sobretudo porque muitos destes não estudaram sobre literatura em Libras
quando eram crianças. Além disso, podemos ensinar esse conteúdo também
para os ouvintes aprendizes de Libras como L2.

Em relação a sua distribuição e seu status, a literatura em Libras é uma
forma de arte ainda precária. Com pouquíssimas exceções, ainda não é
amplamente ensinada nas escolas e raramente no ensino superior.
Costumava ser compartilhada informalmente por meio de reuniões de surdos
em escolas e nas associações de surdos. Porém, ambas as instituições
estão ameaçadas, algumas encontram-se até mesmo em ponto de extinção.

É sempre importante gravar as performances de literatura em Libras e
armazená-las a fim de preservá-las como material para o futuro. Por
outro lado, artistas surdos estão desenvolvendo novas formas para esses
registros, compartilhando seus trabalhos com outros surdos, muitos deles
pela internet; alguns professores têm ensinado a literatura para seus
alunos, pesquisadores e outras pessoas com interesse no assunto estão
cada vez mais reconhecendo a sua importância. Todos eles trabalham para
promovê-la e tentar “estimular um movimento literário” (ORMSBY, 1995, p.
169, tradução nossa). Qualquer interesse acadêmico na literatura em
Libras lhe dá mais credibilidade; ela é mais segura quando vive no
cotidiano de sua comunidade. Se a próxima geração de poetas surdos e de
públicos de surdos ou de ouvintes tiver acesso à literatura, aprender
com ela, adaptá-la e passá-la para as gerações subsequentes, seu futuro
estará assegurado.

<div class="float"></div>

![lista de videos](/images/videos-parte4.png)

<div class="lista-de-videos"></div>

Aqui, falaremos de alguns vídeos mais destinados aos jovens:

* [O Armário](#vimeoModal-348652466), de Juliana Lohn.

* [O Pássaro](#vimeoModal-348080802) (números) e
* [Animais](#vimeoModal-348189766) (números), de
Juliana Lohn e Jaqueline Boldo.

* [A Vaca Surda de Salto Alto](#vimeoModal-356033857),
* [Um Morcego Surdo no Busão](#vimeoModal-356028141),
* [Macaco Surdo Fazendo Música](#vimeoModal-355992442),
* [A Formiga Indígena Surda](#vimeoModal-355984518) e
* [O Churrasco da Banana Surda](#vimeoModal-355978488), de Marina Teles.

### 24.2. A literatura em Libras e o ensino de Libras como primeira língua

Sabe-se que algumas pessoas surdas têm um jeito natural para criar
Libras estética. Essas pessoas são artistas da língua de sinais: poetas,
contadores de histórias e atores em suas comunidades surdas. No entanto,
a aptidão natural não é suficiente para criar e desenvolver a literatura
em Libras e, nesse sentido, pensaremos sobre o que mais é necessário
para que os artistas possam desenvolver e melhorar o seu trabalho
criativo.

#### 24.2.1. Por que é importante a um aluno surdo aprender literatura surda em Libras?

Sabemos que o principal objetivo de se contar histórias é entreter e
divertir. Essa deve ser uma razão para um professor usar a literatura
com seus alunos, por exemplo, quando os mais novos precisam descansar
depois de um trabalho intenso. Um aluno que se sente animado vai
aprender melhor e a contação de histórias (de forma apropriada, e usando
histórias ou poemas bem escolhidos) vai criar um ambiente mais agradável
e propício para a aprendizagem de quase qualquer assunto. Mas se o
professor só conta uma história, sem ter um objetivo didático, isso pode
acarretar na perda de uma oportunidade de ensino e aprendizagem.

Sabemos, também, que a literatura surda é uma atividade e um processo.
Não é apenas um artefato ou uma “coisa”. Assim, quando os alunos
participam da criação e da apresentação da literatura em Libras, ela
pode se tornar ainda mais útil como ferramenta de ensino. Além de uma
pessoa narrar uma história, o ambiente de teatro em grupo pode exigir
menos demandas linguísticas e estimular comportamentos sociais, o que é
bom para alunos mais tímidos.

O conteúdo das histórias é educativo. As que contam fatos são boas
ferramentas para o ensino porque os estudantes aprendem melhor quando as
informações são apresentadas num contexto mais divertido, e a contação
pode justamente ajudar a despertar o interesse em um assunto. Em
qualquer situação, aliás, a contação pode estimular a imaginação.

Além disso, literatura serve para aculturar. Um aluno surdo vai se
aculturar como uma pessoa surda brasileira através da literatura em
Libras, adquirindo o conhecimento da comunidade surda e suas regras de
comportamento. Assim, a literatura surda serve para estimular
habilidades sociais. Para alunos surdos, a contação de histórias e de
poemas em Libras pode até contribuir para a criação e manutenção de suas
identidades como pessoas surdas brasileiras.

A linguagem literária das histórias e dos poemas em Libras ensina a
língua de sinais. Ela permite aos alunos explorarem e desenvolverem a
sua Libras, testando seus limites e os limites da própria língua. Além
de tudo isso, a exposição dos alunos à literatura surda em Libras pode
ensiná-los a contarem histórias sinalizadas (HEINZELMAN, 2014).

#### 24.2.2. Como o aluno aprende a contar histórias em Libras

Cada nova geração deve saber como contar uma história na comunidade para
aprender a literatura. Quando contamos histórias às crianças, esperamos
que elas aprendam a fazer isso. Se contamos aos alunos surdos apenas em
português e do jeito da sociedade ouvinte, eles não aprenderão a contar
histórias em Libras do jeito dos surdos. A principal maneira de aprender
a contação do jeito surdo deve ser pela observação de contos que os
alunos surdos veem quando o professor conta histórias. O ideal seria ter
um professor surdo, com habilidades de contar, mas docentes ouvintes
também podem aprender a fazer isso, e caso o professor não tenha essas
habilidades (não importa se ele ou ela sejam ouvintes ou surdos), podem
usar recursos gravados de outros contadores conhecidos.

A observação é importante para a aprendizagem, mas não basta para o
ensino. Depois de ver a história apresentada pelo professor, o aluno
pode copiá-la, porque o ato de contar é importante em si só. Mais tarde,
com mais habilidade, o professor incentiva a história de um aluno e os
outros alunos o assistem, apoiam e comentam. Assim, os estudantes podem
construir suas próprias histórias coletivamente com o apoio do
professor. Este pode dar sugestões para os alunos, mas também deve
ensinar explicitamente as técnicas de se contar histórias. Por isso, o
professor deve saber bem sobre essas técnicas. Mesmo os docentes com dom
criativo que podem contar histórias e poemas maravilhosos para encantar
os alunos não podem ensinar a literatura em Libras sem saber como
funciona a literatura e quais as normas literárias da Libras.

#### 24.2.3. Tipos de narrativas que os professores podem contar aos alunos

O professor pode usar literatura em Libras para ensinar o conteúdo
das histórias e a linguagem literária. Muitas vezes, os docentes
surdos comentam que nas escolas contam narrativas de experiência
pessoal, utilizam suas próprias experiências como pessoas surdas
para serem apresentadas aos seus alunos, também surdos.
Frequentemente, as histórias ajudam os alunos a aprenderem sobre o
orgulho de ser surdo, as vitórias e os sucessos ou soluções de
problemas por ser uma pessoa surda vivendo numa sociedade onde a
maioria das pessoas é de ouvintes. *[O Armário](#vimeoModal-348652466)*, de Juliana Lohn, é uma versão em Libras de uma narrativa
contada originalmente em BSL pela surda escocesa Rita McDade. Embora
a história seja de uma pessoa surda de outro país, fala de um
assunto que toca as emoções de todos os surdos. Trata do momento em
que a mãe ouvinte de filhas surdas ouviu pela primeira vez as duas
rindo e felizes porque estavam usando a língua de sinais. Por isso,
a mãe as deixou se comunicarem por meio de sinais e parou de
obrigá-las a usarem apenas a oralização.

Essas narrativas de experiência pessoal de professores, ou de outras
pessoas, surdos, são uma parte importante da pedagogia surda em que
os docentes incentivam os alunos a terem uma identidade forte e que
fiquem contentes por serem surdos (GONZALES, 2017).

Histórias fundamentadas em elementos da Libras são criadas para alunos
de diversas idades. As narrativas curtas e humorísticas podem focar nas
configurações de mão dos números (por exemplo *[O Pássaro](#vimeoModal-348080802)*
e *[OsAnimais](#vimeoModal-348189766),* criadas por Juliana
Lohn e Jaqueline Boldo, apresentadas por Juliana Lohn) com objetivo de
estimular o conhecimento numérico, dos sinais para os animais, ou o uso
de incorporação. As narrativas antropomórficas de Marina Teles que usam
apenas uma configuração de mão, também usam expressões não manuais muito
fortes e os personagens são surdos (veja *[A Vaca Surda de Salto
Alto](#vimeoModal-356033857), [Um Morcego Surdo no
Busão](#vimeoModal-356028141), [Macaco Surdo Fazendo
Música](#vimeoModal-355992442), [A Formiga Indígena
Surda](#vimeoModal-355984518)* e [*O Churrasco da
Banana Surda*](#vimeoModal-360265158)\).

É importante que os alunos recebam traduções em Libras de literatura
brasileira, histórias tradicionais, lendas e contos de fadas que venham
da comunidade ouvinte, como vimos nos capítulos anteriores. Essas
produções fazem parte das diversas culturas mundiais e a contação, assim
como o reconto, pode existir em qualquer língua, inclusive em Libras.

Adaptações de histórias tradicionais e lendas podem deixar os alunos
mais envolvidos se houver características que se aproximem mais da sua
experiência. O professor pode contar uma história sem adaptação e
convidar os alunos a sugerirem adaptações. Assim, estimula-se a
imaginação e a participação ativa dos estudantes, o que gera ainda mais
um sentimento de pertencimento (SCOTT, 2011). As adaptações de histórias
existentes, a contação de narrativas pessoais e o estímulo da imaginação
são fundamentais para que os alunos mais velhos posam criar suas
próprias histórias criativas originais.

Vimos no capítulo 7 que as histórias de autoria surda são ainda escassas
em Libras, porque a maioria dos alunos surdos na escola vê apenas
histórias originais de autoria ouvinte. Quando o professor conta e
mostra histórias criativas e originais em Libras, o aluno aprende a
criar seus próprios exemplos (SUTTON-SPENCE; RAMSEY, 2010).

### 2.4.3. A literatura em Libras e o ensino de Libras como segunda língua

Talvez seja estranho falar para alunos ouvintes da importância de
aprenderem sobre a literatura em Libras. Mas quando um público não surdo
entende o valor e a beleza da literatura, ele fortalece a sua
aprendizagem e o próprio status da literatura. Geralmente, a
aprendizagem da literatura ajuda qualquer estudante de uma língua
estrangeira (PARAN, 2008). Os professores de adultos ouvintes que
estudam Libras como uma segunda língua (L2) podem aproveitar a
literatura em Libras, mas infelizmente muitos cursos ainda não ensinam
nada nessa perspectiva.

Acima de tudo, o professor de Libras pode usar a literatura para ensinar
sobre uma boa forma de língua brasileira de sinais que é fortemente
visual. A linguagem das obras literárias é uma forma de Libras mais
valorizada; com ela se criam imagens visuais ou sinais originais e
criativos que chamam atenção. A literatura pode ensinar sobre a cultura
surda, que faz parte do ensino de Libras, porque o conteúdo das
narrativas originais em língua brasileira de sinais e dos poemas, das
piadas e das peças de teatro surdo mostra o que um autor surdo e o seu
público acham importante para representar a visão da sua cultura surda.

Os textos literários também oferecem exemplos para o ensino de
tradução/interpretação. Os formandos já são fluentes em Libras, mas os
textos mostram uma forma da Libras mais visual e menos parecida à língua
portuguesa. Isso ajuda a aperfeiçoar a língua dos intérpretes e pode
estimular novas estratégias de tradução. Por fim, podemos utilizar a
literatura em Libras para ensinar sobre a própria literatura, mostrando
as possibilidades e novos limites da literatura visual para os
estudiosos dessa área.

#### 24.3.1 Por que há pouca literatura nas aulas de Libras como L2?

Visto que é tão importante incluir literatura em Libras nos cursos para
os alunos de L2, podemos perguntar por que ela não é mais utilizada. Às
vezes, simplesmente ninguém a colocou no currículo e basta que este seja
atualizado e considere a inclusão de elementos mais literários. Mas,
além disso, se os professores não se formaram com disciplinas sobre
literatura em Libras em seus cursos, eles não saberão como utilizá-la no
ensino, tampouco que a literatura tão visual é uma ferramenta muito útil
para melhorar a experiência de aprendizagem dos alunos.

A falta de recursos é outro problema. Precisamos de antologias e
coleções de literatura em Libras disponíveis para todos os professores,
mesmo que eles tenham um dom para contar histórias ou poemas. Como vimos
no capítulo 20, os professores precisam de recursos como antologias de
ensino com exemplos de literatura em Libras e materiais didáticos para
serem utilizados com os vídeos. Isso se torna ainda mais importante se o
professor não tiver a aptidão de contar histórias ou outros tipos de
literatura em Libras (SUTTON-SPENCE, 2020).

Faltam ainda recursos adequados para todas as idades de alunos de L2,
porque a maioria dos recursos é destinada para crianças, alunos de
Libras como L1. Embora os contos de fadas sejam levemente agradáveis
para os adultos, nem sempre têm um nível adequado para os mais velhos.
Alunos que são pais ouvintes de filhos surdos ou professores de alunos
surdos podem querer assistir à *Chapeuzinho Vermelho* em Libras para que
possam contar esse conto a um novo surdo. Mas talvez *Chapeuzinho
Vermelho* não seja uma escolha adequada para alunos de graduação nas
universidades federais, por exemplo. Por isso, precisamos de recursos
para todos os perfis de alunos do ensino superior, do ensino médio,
fundamental, até o jardim de infância.

### 24.4.  A divulgação da literatura em Libras para adultos surdos 

Sabemos que, numa escola bilíngue, os alunos surdos convivem com outros
alunos surdos e, normalmente, têm pelo menos alguns professores surdos.
Nesses contextos, a literatura surda e a Libras criativa florescem e
podem passar dos surdos mais velhos aos mais novos. No entanto, a
realidade da educação das pessoas surdas hoje em dia está nas escolas
inclusivas, onde não se aprende nada sobre a literatura surda e não se
encontra a linguagem literária em Libras. Por isso, o ensino de
literatura em Libras para adultos surdos é ainda mais relevante.

#### 24.4.1. O ensino de literatura nos cursos de Letras Libras

As instituições de educação superior podem contribuir para assegurar e
promover a literatura em Libras. No nível da graduação, ela é ensinada
de diversas formas nos cursos de Letras Libras (ROSA, 2017) e é
importante para estudantes de Letras Libras (Licenciatura) que querem se
tornar professores. Apenas com o estudo de literatura nesse nível eles
terão acesso ao conhecimento explícito sobre a estrutura e a função da
literatura de língua de sinais, bem como o conhecimento do cânone
existente em sua língua, para passá-lo às próximas gerações. É também
fundamental para estudantes que querem trabalhar como
tradutores-intérpretes de Libras-português. O conhecimento dessa
dimensão da linguagem literária e da cultura surda é muito importante
para quem vai atuar especialmente como intérprete em sala de aula, onde
há o ensino de literatura, seja ele em Libras ou em língua portuguesa.

O ensino de literatura em Libras como parte das atividades de extensão
universitária oportuniza o seu acesso por pessoas que não têm o desejo,
o tempo, as qualificações ou os recursos para estudar em uma
universidade, mas que ainda estão interessadas em aprender sobre como
fazer literatura em Libras (SUTTON-SPENCE; MACHADO, 2020).

#### 24.4.2. Os festivais de literatura e folclore em Libras

Os festivais de literatura (ou folclore) de Libras são espaços
importantes para divulgar e ensinar a literatura em língua de sinais
para adultos. Neles, os membros da comunidade surda assistem a
performances literárias (como peças de teatro, contos, narrativas,
piadas e poemas) de artistas surdos reconhecidos. O principal objetivo é
entretenimento, e sabemos que muitas pessoas conhecem a literatura em
Libras pela primeira vez nesses eventos. Embora as pessoas apenas
assistam às performances, podem voltar para suas casas inspiradas e
motivadas pelos artistas para criarem seus próprios trabalhos. Há também
festivais de folclore e literatura surda realizados em diversos
países[50](#tooltip) que incluem nas suas programações aulas, oficinas e
workshops com o intuito de encorajar as pessoas surdas a participarem da
literatura e do folclore surdo e a falarem a respeito disso. Nesses
eventos, elas aprendem a criar suas próprias obras literárias, ao invés
de serem meras espectadoras (SUTTON-SPENCE et al., 2016).

Kenny Lerner descreveu a conferência de literatura de língua de sinais
no NTID em 1988. Lá foi decidido que:

> Deve haver performance, mas também discussões, painéis e palestras. A
> ideia era que, quando as pessoas fossem embora, elas voltassem às suas
> escolas de ensino médio e faculdades e criassem programas de
> literatura de Língua de Sinais Americana. Era um grande encontro
> \[...\] e as pessoas realmente saíam falando sobre a literatura de
> língua de sinais americana. (LERNER em NATHAN LERNER; FEIGEL, 2009), tradução no vídeo.

Os Festivais de Literatura e Folclore surdos realizados no Brasil seguem
as sugestões feitas por Lerner. Os participantes vêm com a intenção de
aprender sobre literatura e criar suas próprias produções em Libras com
o apoio dos artistas surdos mais experientes. O formato geral dos
festivais inclui convidar artistas surdos com diversas habilidades, como
as de contar histórias ou piadas, criar peças de teatro ou fazer poemas.
Muitas vezes, os artistas apresentam um show, mostrando ao vivo as suas
habilidades apenas por diversão. Eles podem criar um debate com o
público sobre as performances, falando sobre as intenções literárias e
criativas nas obras que se apresentam no show. Também podem oferecer
oficinas práticas em que os participantes aprendem como criar seus
trabalhos. Essas oficinas fazem parte da tradição coletiva de educação
do povo surdo, em que aqueles que já sabem como fazer alguma coisa
ajudam os outros surdos a realizarem os trabalhos com o sucesso que
nunca poderiam alcançar sozinhos.

A visita de artistas literários surdos internacionais pode criar uma
experiência de aprendizagem ainda mais rica. A realidade da cultura e da
literatura surdas é que muitos artistas estrangeiros conseguem se
comunicar facilmente com o povo surdo brasileiro por meio de sinais
fortemente visuais e de sinais internacionais. Sabemos que a literatura
surda é disseminada mundialmente e influencia muito a literatura surda
brasileira, como ocorreu nas viagens dos artistas surdos brasileiros
Carlos Góes e Nelson Pimenta aos EUA, no século XX. Além disso, os
artistas brasileiros viajam para compartilhar os conhecimentos
literários e as habilidades criativas com os surdos em outros países na
América Latina e Europa.

Todas essas formas de repassar novas ideias e habilidades criarão
oportunidades para a promoção e o desenvolvimento da literatura em
Libras. Nem todos os participantes desses festivais irão se tornar
artistas consagrados, mas todos têm a oportunidade de aprender sobre a
literatura surda e criar sua própria expressão criativa em Libras.

<div class="float"></div>

![resumo](/images/resumo-parte4.png)


### 24.5.  Resumo - Mantendo a chama acesa 

A literatura em Libras pode sobreviver apenas quando for ensinada às
próximas gerações. Precisamos educar os novos artistas e os novos
públicos, sendo eles surdos ou ouvintes. Nesse capítulo, mostramos a
importância da escola no ensino da literatura em Libras. Pensamos sobre
alguns dos desafios e maneiras de se resolver as dificuldades dos alunos
surdos nas escolas inclusivas em acessar a literatura. Vimos também os
benefícios de se ensinar a literatura em Libras para alunos de Libras
como L2. Além da educação formal, observamos que os festivais, com duplo
foco na aprendizagem e no entretenimento, podem divulgar e estimular
sempre mais o desenvolvimento da literatura em Libras no país inteiro.

Com isso, mostramos que podemos criar os próximos artistas e públicos de
literatura em Libras. O contato entre pessoas surdas em contextos que
permitem o desenvolvimento da literatura em Libras é muito importante.
Nenhum artista de Libras surge de repente como artista completamente
formado. Suas experiências na escola e na universidade, sua exposição ao
teatro surdo e a outras literaturas e, em todos os casos, seus encontros
com outros artistas de literatura surda, fazem com que se tornem o que
são hoje. Da mesma forma, os públicos devem se acostumar à literatura
surda para valorizar e promover essa parte da cultura surda.

Até certo ponto, a gravação de vídeos, as coleções e o arquivamento de
materiais resguardam a literatura para o futuro. No entanto, sem as
pessoas, a mera proteção não serve para manter a arte viva. Peter Cook,
em uma entrevista em *[The Heart of the Hydrogen Jukebox](#ytModal-aJ0Y-luT5_w),* se dirigiu ao
público atrás da câmera, quando falou: “não é a nossa hora agora. É a
sua vez! Vamos, vá lá!” (tradução nossa). Em outra entrevista, o
professor surdo Robert Panara disse: “Mantenha a chama acesa!”.

As pessoas surdas têm mantido a chama acesa por gerações. Com os
esforços da comunidade surda e os aliados ouvintes que amam e valorizam
a literatura em Libras, o fogo se espalhará. Esperamos que este livro
faça a sua contribuição.

<span class="tooltip-texto" id="tooltip-50">
Por exemplo, nos EUA, na França, no Reino Unido, na África do
    Sul, no Chile e no Brasil.
</span>

