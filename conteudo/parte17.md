---
capitulo: 17
parte: 3
titulo: Antropomorfismo
pdf: "http://files.literaturaemlibras.com/CP17_Antropomorfismo.pdf"
video: JagoAYH76QE
---

## 17. Antropomorfismo

<div class="float"></div>

![objetivo](/images/objetivo-parte3.png)

### 17.1. Objetivo

Neste capítulo, investigaremos o jeito como a literatura em Libras
apresenta seres e objetos não humanos como se tivessem características
humanas. Veremos os tipos de não humanos que ocorrem na literatura e por
quê. Pensaremos sobre os diferentes níveis das características humanas
dadas a animais ou objetos.

<div class="float"></div>

![lista de videos](/images/videos-parte3.png)

<div class="lista-de-videos"></div>

Antes de prosseguir, assista a alguns vídeos de literatura em Libras (e
a alguns em outras línguas de sinais) que têm animais e objetos
inanimados.

<div class="lista-de-videos" style="border: 0"></div>

Será que você já assistiu a estes vídeos sobre animais? Se não, pode
assistir agora:

* *[Jaguadarte](#ytModal-DSSXTh0wriU)*,
de Aulio Nóbrega – um monstro.

* *[Leoa Guerreira](#ytModal-rfnKoCXmSg4),* de Vanessa Lima  – uma leoa e outros leões.

* [*Príncipe Procurando Amor*](#ytModal-D4hPR6DCcDA) (em inglês, *Prince Looking for Love*), de
Richard Carter – dois sapos[44](#tooltip).

* [*A Rainha das Abelhas*](#ytModal-nXI4aO2_G3E),
de Mariá de Rezende Araújo – uma formiga, um pato e uma abelha.

* [*O Sapo e o Boi*](#ytModal-23HQFq0A4AY),
de Nelson Pimenta – um sapo, outros sapos e um boi.

* *[Voo sobre Rio](#ytModal-YaAy0cbjU8o),* de Fernanda Machado – dois pássaros.

<div class="lista-de-videos" style="border: 0"></div>

Será que você já assistiu aos seguintes vídeos sobre coisas ou objetos
inanimados? Se não, pode assistir agora:

* [*Bolinha de Ping-pong*](#ytModal-VhGCEznqljo), de
Rimar Segala - uma bolinha de pingue-pongue.

* [*O Espelho*](#ytModal-p0B8ztiVI4s) (em inglês Mirror), de Richard Carter – um espelho.

* [*Golf Ball*](#ytModal-Gl3vqLeOyEE), de Stefan Goldschmidt  – uma bola de golfe.

* [*Saci*](#ytModal-4UBwn9242gA), de Fernanda Machado – uma
árvore.

* *[Tinder](#vimeoModal-267275098)*, de Anna Luiza Maciel – um celular.

### 17.2. O que é antropomorfismo?

Em todos as obras citadas acima, o corpo do artista se transforma pelo
processo de incorporação e, portanto, passa a *ser* o animal ou o
objeto. Esse recurso literário é conhecido como **antropomorfismo** ou
**personificação**. É “um conceito filosófico que está associado às
formas humanas, ou seja, ele atribui características físicas,
sentimentos, emoções, pensamentos, ações ou comportamentos humanos aos
objetos inanimados ou aos seres irracionais”[45](#tooltip). A palavra
antropomorfismo significa “dar uma forma humana a uma coisa não humana,
dar características ou comportamento humano” (Bloomsbury) ou “atribuir
forma humana ou personalidade às coisas” (Merriam-Webster).

O antropomorfismo é muito comum nas sociedades humanas. Personificar ou
antropomorfizar é um tipo de metáfora que apresenta um conceito como se
fosse outro e, nesse caso, portanto, trata um não humano como se fosse
um humano. Uma área de estudo sobre antropomorfismo e personificação
foca na religião e nas formas concretas dos espíritos divinos, mas não
vamos tratar disso neste livro porque o nosso foco está na literatura e
na criatividade. Sabemos que, na maioria das vezes, o antropomorfismo é
um tipo de fingimento, ou é uma coisa meramente da nossa imaginação,
porque sabemos que não existem não humanos com desejos, comportamentos e
línguas humanas. No entanto, essa forma de “brincadeira” faz parte da
vida de todas as sociedades já conhecidas.

Os contos de fadas e as fábulas de Esopo utilizam o antropomorfismo.
Nessas histórias, os animais (e às vezes outras coisas, como árvores ou
um espelho) falam e pensam como se fossem humanos - apesar de geralmente
manterem sua própria forma física. Nos contos de fada, vemos animais
falantes como em *Os Três Porquinhos* e *O Gato de Botas*. Em *Branca
Neve,* o espelho fala. Nas fábulas de Esopo, os animais conversam na
língua humana, como em *A Lebre e a Tartaruga*, [*O Sapo e o
Boi*](#ytModal-23HQFq0A4AY) e *A Raposa e
a Cegonha*.

O antropomorfismo acontece também em livros infantis escritos por
autores conhecidos e que não fazem parte das histórias mais
tradicionais. Entre muitos exemplos, podemos destacar o coelho branco,
criado por Lewis Carrol em *Alice no País das Maravilhas*; Visconde de
Sabugosa, o sabugo de milho do *Sítio do Pica-Pau Amarelo*, de Monteiro
Lobato; ou as cores falantes de *Flicts*, de Ziraldo. Também existem
muitos exemplos entre os filmes de animação, dentre eles destacamos
*Shrek, Rio, Procurando Nemo* e *Carros* da Pixar e Disney. Vemos esse
fenômeno até em livros para adultos, por exemplo em *A Revolução dos
Bichos*, publicado em 1945 por George Orwell. Apesar de ser considerado
hoje um clássico moderno e abordar um assunto sociopolítico complexo, *A
Revolução dos Bichos* foi rejeitado inicialmente por não ser “sério”,
porque tratava de animais que falam e atuam como humanos, o que foi
considerado infantil. Antropomorfismo em Libras não é sempre considerado
infantil.

### 17.3. Por que antropomorfizar?

Existem muitas razões para se antropomorfizar, mas, para nossos estudos
de literatura em Libras, um dos principais motivos é porque isso é bem
divertido. O filósofo francês Henri Bergson afirmou (em 1900) que os
humanos não riem das coisas totalmente não humanas, mas que o riso é
gerado quando percebemos traços de humano no não humano.

> Não há comicidade fora do que é propriamente humano. Uma paisagem
> poderá ser bela, graciosa, sublime, insignificante ou feia, porém
> jamais risível. Riremos de um animal, mas porque teremos surpreendido
> nele uma atitude de homem ou certa expressão humana. Riremos de um
> chapéu, mas, no caso, o cômico não será um pedaço de feltro ou palha,
> senão a forma que alguém lhe deu, o molde da fantasia humana que ele
> assumiu (tradução em português, 1983, p. 07).

Em Libras, o ato de incorporar um não humano no corpo humano do
sinalizante apresenta “uma atitude de homem ou certa expressão humana”,
e isso por si já é divertido.

Outro objetivo com o uso do antropomorfismo é o de mostrar uma
perspectiva diferente porque, ao assumirmos lugares não humanos, podemos
entender uma situação melhor através de outro olhar. Nas fábulas, isso é
importante para o ensino da moral.

### 17.4. Como antropomorfizar? Conteúdo ou forma?

Os objetos e animais antropomorfizados na literatura e nos filmes muitas
vezes tomam a forma humana usando roupas, falando a língua humana e
participando em diversas atividades humanas. As características humanas
que damos aos não humanos são de dois tipos principais: comportamento e
forma. No comportamento, atribuímos os desejos humanos aos não humanos
para explicar ou para entender o motivo de um evento. Por exemplo,
digamos que o mar está brabo, com ondas altas, o sol brilha cruelmente,
o carro se recusa a ligar, o cachorro abana o rabo de felicidade e dois
passarinhos namoram. Em nenhum desses exemplos podemos verificar emoções
na entidade não humana. Mas quando se dá forma humana aos não humanos,
vemos os traços humanos (especialmente o rosto) em diversos objetos, por
exemplo, nas nuvens, nas pedras, ou até num pão torrado (ANDRADE, 2015).

Nas línguas orais, é fácil *falar* *sobre* os não humanos como se fossem
indivíduos, usando as palavras designadas para ações humanas. Além
disso, vemos os antropomorfismos nas imagens, especialmente em desenhos
animados e ilustrações. Porém, não é fácil *mostrar* por meio da língua
de que maneira, por exemplo, uma geladeira se comporta, fala ou usa a
voz. E um espelho? Um tomate? Um pássaro, um sapo ou uma leoa?

Por outro lado, um sinalizante consegue mostrar diretamente a forma, o
comportamento e até mesmo a fala dos não humanos em razão da modalidade
visual das línguas de sinais. Em Libras, apresentamos o que podemos
mostrar sobre as formas humanas através do nosso corpo. Quando não
conseguimos mostrar diretamente, usamos as mãos para representar. Não é
fácil fazer isso de uma maneira imaginativa, interessante, suave ou
engraçada em Libras, mas quem consegue fazer um texto muito criativo com
êxito é muito valorizado na comunidade surda.

Os não humanos incluem, entre outros, animais (por exemplo, um gato ou
um caramujo), plantas (uma árvore), elementos naturais (como uma
montanha), objetos feitos por humanos (por exemplo, um carro), lugares
(uma cidade) e qualidades e conceitos abstratos (como, por exemplo, a
consciência ou a inveja). A maneira de incorporar esses diversos tipos
de não humanos em Libras será diferente dependendo da similaridade entre
o corpo do não humano e do humano. A forma do gato, por exemplo, é mais
parecida com a de um humano do que a do caramujo.

Podemos falar de um animal com características
humanas usando a língua cotidiana. É assim que vemos frequentemente na
literatura escrita em português. Também temos isso em Libras, inclusive
nas narrativas recontadas de histórias em língua portuguesa. Na fábula [*O Sapo e o Boi*](#ytModal-23HQFq0A4AY),
Nelson Pimenta usa os sinais <span
style="font-variant:small-caps;">imponente</span> (para o boi) e <span
style="font-variant:small-caps;">inveja</span> (para o sapo), dando
qualidades e emoções humanas aos não humanos. O sapo e seus amigos
conversam entre si de uma maneira totalmente humana. Também no conto [*A
Rainha das Abelhas*](#ytModal-nXI4aO2_G3E),
traduzido para Libras por Mariá de Rezende Araújo, a formiga, o pato e a
abelhas conversam com o humano normalmente em Libras, como se fossem
como ele. Entendemos que os animais ainda são animais, com forma,
habilidades e tamanhos próprios de cada espécie, mas o antropomorfismo
está totalmente presente no conceito e não na forma linguística da
apresentação.

Na literatura em Libras, especificamente, também temos a opção de dar
forma humana ao não humano por meio da incorporação. Com esse recurso,
em Libras, normalmente incorporamos outra pessoa. Vemos um mapeamento
simples entre o corpo do sinalizante e o do personagem. A cabeça do
narrador é a cabeça do personagem; o seu tronco é o tronco do
personagem; os olhos são os do personagem; as mãos são as mãos do
personagem, os membros são os membros do personagem e assim por diante.
A expressão facial do artista mostra as emoções atrás da mesma expressão
facial do personagem.

Diferentemente, quando o artista incorpora um animal, sua cabeça se
torna a cabeça do personagem animal; seu tronco é o tronco do animal;
seus olhos são os do deste. Mas, os membros específicos do humano não
são os membros específicos do animal (estes podem representar ações
humanas ou animais). A expressão facial do artista não mostra as emoções
por trás da expressão facial do personagem não humano, porque sabemos
que as emoções dele não são iguais às do humano.

Quando o sinalizante se torna um objeto inanimado, a relação entre as
formas do humano e o objeto é ainda mais reduzida porque as formas e os
comportamentos dos objetos e dos seres humanos são muito diferentes.
Aceitamos o fato de ver uma expressão facial e olhos em uma árvore, num
carro ou numa montanha, apesar deles não possuírem rostos e olhos,
porque o artista não pode desligar as partes do corpo desnecessárias. Se
o objeto se comporta com características humanas, a expressão facial e
os olhos podem mostrá-las.

As partes físicas análogas dos não humanos são tratadas como membros ou
outras partes do corpo em relação à forma, como acontece quando usamos
nossos braços para significar asas, ou os dedos para mostras galhos. Em
*[Tinder](#vimeoModal-267275098),* de Anna Luiza
Maciel, o rosto e o tronco dela representam a tela do celular, delineada
pelas mãos. O rosto e a cabeça são a tela quando ela a desliza, mas
também o corpo inteiro se torna o celular quando ela digita um número no
teclado. Vemos o mesmo na história [*O
Espelho*](#ytModal-p0B8ztiVI4s) (*Mirror*), de Richard Carter.

Alguns animais que podem sinalizar usam sinais baseados na forma de seus
membros, modificando os parâmetros do sinal conforme as possibilidades
físicas. As diferenças podem estar na configuração da mão, na locação ou
no movimento, mas, principalmente, na forma das mãos. Aranhas e polvos,
por exemplo, podem soletrar porque têm oito pernas que se transformam em
oito dedos. Cobras não têm membros para atuar como uma mão no
antropomorfismo, mas é possível usar sinais com a configuração de mão de
um dedo, porque podem sinalizar com o rabo. Os gatos, ursos e leões
sinalizam com as patas, facilmente criadas no corpo humano com as mãos
abertas em garras. Já vimos um ótimo exemplo disso em [*Leoa
Guerreira*](#ytModal-rfnKoCXmSg4), de Vanessa Lima (no capítulo
05), no qual a leoa produz quase todos os sinais com essa configuração
de mão.

Os objetos inanimados sinalizam pouco em Libras porque eles não têm a
forma física que permite a produção dos sinais. As plantas podem
sinalizar com as folhas, as árvores com os galhos e os aviões com as
asas, mas a maioria dos objetos não tem forma para sinalizar. Por
exemplo, um tomate, uma montanha e uma geladeira não têm uma parte
física que se aproxime à forma das mãos. Ou eles sinalizam de uma forma
totalmente humana e entendemos que o antropomorfismo é completo, ou eles
se comunicam apenas não manualmente.

A narrativa [*O Passarinho
Diferente*](#ytModal-P_DR60BJuFI),
contada em Libras por Nelson Pimenta, é uma história que contém muito
antropomorfismo. Fala de uma família de águias na qual nasce um filhote
pequeno com um bico curto e reto. Apesar de falar das aves, essa fábula
é claramente uma história humana em que elas leem jornais, consultam
médicos e frequentam igrejas e escolas, onde leem e escrevem. A
narrativa é contada com muita incorporação dos personagens não humanos.
Essa incorporação às vezes mostra como se as aves tivessem um corpo
humano e, em outras vezes, Nelson incorpora o corpo da ave. Em alguns
momentos, as águias têm asas, em outras vezes, parecem ter braços. Nada
disso atrapalha a história e Nelson mescla os dois mundos suavemente
para criar um mundo narrativo em que acreditamos que as aves estão mesmo
tendo experiências humanas. Elas têm asas para voar, as águias caçam
coelhos e o passarinho canta e come uvas, mas em paralelo a isso, a vida
deles é como a dos humanos e todos pensam e conversam de uma forma
totalmente humana.

### 17.5. Níveis de antropomorfismo

O antropomorfismo em Libras não é sempre igual. Em uma pesquisa
anterior, descrevemos diferentes níveis de antropomorfismo: descritivo,
pré-linguístico e linguístico (SUTTON-SPENCE e KANEKO, 2016). Vamos
falar sobre esses três.

#### 17.5.1. O nível descritivo

Nesse nível, o antropomorfismo ocorre puramente na forma e não no
conceito. O sinalizante descreve o não humano com o corpo, mas não
mostra outras características humanas. A incorporação do não humano
basta para antropomorfizar, ainda que possamos perceber características
humanas na apresentação do não humano.

Na história [*Eu x Rato*](#ytModal-UmsAxQB5NQA),
Rodrigo Custódio da Silva incorpora o rato. O rosto dele é o rosto do
rato, os dedos dele representam o bigode, o dedo indicador é o rabo e as
mãos são as patas. O rato é totalmente descrito como um rato, sem
desejos ou comportamentos humanos. De forma similar, Aulio Nóbrega
descreve detalhadamente o monstro em *[Jaguadarte](#ytModal-DSSXTh0wriU)*. O corpo dele é o corpo do monstro.
As mãos mostram o que falta no corpo humano, por exemplo o rabo, as
garras e os dentes enormes. Essa relação entre os dois seres é muito
divertida. Salientamos, no entanto, que o monstro é completamente
monstro, e não que tem traço humano além de ter sido recriado e
representado pelo corpo humano.

No poema [*Saci*](#ytModal-4UBwn9242gA), Fernanda Machado
descreve a onça e o papagaio em Libras usando o seu corpo humano. As
mãos mostram o movimento do papagaio enquanto o corpo, os olhos e a
cabeça da poeta incorporam o papagaio repousando na árvore. A mão mostra
o movimento felino da onça por meio de um classificador e a boca o
rugido. A boca de Fernanda também apresenta o rugido porque é a
representação da boca da onça. Porém, os animais se comportam como
animais e não têm desejos, emoções nem falam línguas humanas.

Na narrativa *[Príncipe Procurando Amor](#ytModal-D4hPR6DCcDA),*
de Richard Carter, o sapo feio não tem comportamento humano. Já vimos,
no capítulo 10, que Richard transforma seu corpo no corpo do sapo. As
mãos humanas são os pés abertos do sapo. Os pequenos olhos humanos são
aumentados pelas mãos para virarem os grandes olhos do sapo e, o papo do
sapo, que os humanos não têm, é criado com as mãos. No momento em que o
sapo pega a mosca, a língua humana sai da boca humana, como uma
representação do sapo, mas também os dedos se abrem e se deslocam na
direção da mosca para revelar a língua extensa do sapo.

#### 17.5.2. O nível pré-linguístico

Neste, atribuímos emoções, intenções ou outros sentimentos e
comportamentos aos não humanos, mas eles não se comunicam através da
língua humana. Frequentemente, essas caraterísticas são simplesmente
ditas ou contadas, como vimos anteriormente com o boi imponente ou o
sapo invejoso. Mas, em Libras artística, a forma de mostrar essas
atribuições gera prazer.

No poema [*Voo Sobre Rio*](#ytModal-YaAy0cbjU8o), os dois
pássaros namoram com uma mistura de comportamento humano e não humano,
gerando um prazer especial no público. Embora eles comam e façam
carinhos nas cabeças com os bicos, algumas atividades sugerem que os
pássaros têm uma forma mais humana. O macho mexe com as sobrancelhas e a
fêmea arruma os cabelos (lembramos que os pássaros, na verdade, não têm
sobrancelhas nem cabelos), mas a “mão” que se move é o classificador que
mostra a cabeça com o bico e o corpo do pássaro. O macho faz “quá-quá”
(forma natural de comunicar de uma ave), mas quando a fêmea não responde
ele fica feliz (que é uma emoção humana) porque ela é surda. As
expressões não manuais das aves mostram as emoções humanas de dúvida,
prazer, indignação, desculpas e amor.

Na história [*Príncipe Procurando Amor*](#ytModal-D4hPR6DCcDA), de Richard Carter, o sapo bonito chega ao nível
pré-linguístico. Ele sente orgulho e mostra a intenção de receber o
beijo do humano, mas não usa a língua humana. Virando um humano, depois
de receber o beijo, ele usa os sinais do vocabulário de BSL como humano
(“encontrei o amor eterno”), mas quando ele olha com desdém ao outro
sapo no final, vemos aqui um exemplo de nível pré-linguístico.

Normalmente, os olhos, a cabeça e outros elementos não manuais expressam
emoções e desejos humanos dos não animais. Vemos isso no comportamento
da árvore no poema , de Fernanda Machado, que mostra cautela,
felicidade, carinho, medo e alívio por meio dos olhos, da expressão
facial e de outros elementos não manuais. Normalmente, os não animais
não sinalizam. Às vezes, as árvores sinalizam porque o classificador de
uma árvore tem a mesma forma de uma mão, e a incorporação da árvore
mostra a forma parecida entre um tronco humano e um tronco de árvore,
sendo os braços do humano os galhos da árvore. No poema , as árvores
acenam para dizer “olá”, mas elas comunicam pouco manualmente.

Nos exemplos selecionados no início do capítulo, [*Bolinha de
Ping-pong*](#ytModal-VhGCEznqljo), [*Golf
Ball*](#ytModal-Gl3vqLeOyEE), e , os objetos mostram emoções e
desejos de forma não manual. Eles comunicam apenas por esse meio, e não
sinalizam porque não têm mãos, sendo esféricos ou quadrados. Alguns
objetos inanimados usam o corpo para representar ações humanas. Por
exemplo, os espelhos na narrativa de Richard Carter estufam o peito para
atrair a atenção humana. O pesquisador surdo Frank Bechter (2008)
defende que os objetos mostrem a experiência dos surdos, sendo “mudos”
por falta de mãos e incapazes de comunicar e entender o mundo ao redor
deles. Em muitas narrativas em que os objetos não podem comunicar, eles
sofrem opressão. Os objetos que conseguem comunicar estão liberados e
não sofrem mais[46](#tooltip).

#### 17.5.3. O nível linguístico 

No nível linguístico, atribuímos a língua humana ao não humano. Se essa
atribuição for totalmente conceitual, os não humanos sinalizam de forma
exatamente igual aos humanos, como vimos nas fábulas de Esopo e nos
contos de fadas como [*A Rainha das Abelhas*](#ytModal-nXI4aO2_G3E). Sabemos que não são humanos, mas aceitamos que
eles dominem a Libras.

Podemos mostrar o conceito de humano também ao se manter a forma de uma
parte animal. A *[Leoa Guerreira](#ytModal-rfnKoCXmSg4)*, de Vanessa Lima, sinaliza em Libras, mas com a forma
metade animal e metade humana. É essa mescla de humano e não humano que
achamos mais engraçada em Libras, como observou Bergson (citado no
início do capítulo).

O caso do pássaro em *[Voo sobre Rio](#ytModal-YaAy0cbjU8o)* é um exemplo raro, mas importante, que mostra outra
possibilidade. No encontro com a fêmea, o macho faz “quá-quá” como um
não humano, mas, depois disso, ele usa os sinais da Libras <span
style="font-variant:small-caps;">surdo</span> e <span
style="font-variant:small-caps;">igual</span>. A locação e o movimento
dos sinais estão certos, mas a configuração de mão não é de Libras
natural, nem um classificador de asas na incorporação. O sinal é feito
com o classificador que mostra a cabeça e o tronco do bicho. Isso não
faz nenhum sentido linguístico, mas aceitamos a sua ocorrência no poema
porque a manutenção do classificador mostra a identidade do pássaro e a
artista incorporou o corpo e a expressão facial dele – tudo, além da
mão.

<div class="float"></div>

![resumo](/images/resumo-parte3.png)

###  17.6. Resumo

Nesse capítulo, exploramos o conceito de antropomorfismo. Entendemos que
a ideia de se atribuir emoções, comportamentos e formas humanas aos não
humanos é comum na literatura em Libras. As línguas faladas podem falar
sobre o conceito, e as imagens e os desenhos podem mostrar os
antropomorfismos. Mas vimos que os artistas criativos de Libras podem
usar a língua e as técnicas gestuais para mostrar o antropomorfismo, de
tipo basicamente descritivo, pré-linguístico ou linguístico. No nível
linguístico, especialmente, a possibilidade de se mapear a forma do
corpo humano que sinaliza a forma dos não humanos permite a alguns
animais e objetos comunicar em Libras do jeito do seu corpo. Mas, se não
há a opção física para permitir ao não humano sinalizar do seu jeito,
ele deve comunicar totalmente de forma humana. Tudo isso gera um grande
encanto no espectador.

<div class="float"></div>

![atividade](/images/atividade-parte3.png)

### 17.7. Atividade

Vamos assistir à *[A Rainha das Abelhas](#ytModal-nXI4aO2_G3E)*, de Mariá de Rezende Araújo, em Libras, *[Príncipe Procurando Amor](#ytModal-D4hPR6DCcDA)*, de
Richard Carter, em BSL e [*Golf Ball*](#ytModal-Gl3vqLeOyEE), de Stefan Goldschmidt, obras que envolvem
personagens não humanos.

* Liste os personagens não humanos que aparecem.

* Como o narrador representa esses personagens?

* Tente categorizá-los nos níveis: descritivo, pré-linguístico e
linguístico. Lembre-se que eles podem não se encaixar perfeitamente
nessas categorias.

* Você acha que esses personagens são surdos? Explique por quê.

<span class="tooltip-texto" id="tooltip-44">
Esse texto é em BSL, mas tudo é feito através de classificadores,
    com exceção dos últimos sinais, que significam “Encontrei meu amor
    eterno”.
</span>

<span class="tooltip-texto" id="tooltip-45">
<a href="https://www.todamateria.com.br/o-que-e-antropomorfismo" target="_blank">
    https://www.todamateria.com.br/o-que-e-antropomorfismo</a>
</span>

<span class="tooltip-texto" id="tooltip-46">
No final da história da Bolinha de pingue-pongue, a bolinha pede
    ajuda com um sinal humano da Libras, apesar de não ter mãos. Isso
    não incomoda o público, que entende bem que é apenas um sinal. É
    nesse momento de comunicar em Libras que a bola é resgatada.
</span>
