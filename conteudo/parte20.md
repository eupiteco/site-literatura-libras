---
capitulo: 20
parte: 4
titulo: Cânone e antologias de literatura em Libras
pdf: "http://files.literaturaemlibras.com/CP20_Canone_e_antologias_de_literatura_em_Libras.pdf"
video: qWo4ZscL8hQ
---

## 20. Cânone e antologias de literatura em Libras

<div class="float"></div>

![objetivo](/images/objetivo-parte4.png)

### 20.1. Objetivo

Este capítulo vai analisar os conceitos de cânone e de antologia,
respondendo à pergunta: “o que faz parte de uma
antologia de literatura em Libras?”. Ao longo deste livro, falamos de
literatura surda usando exemplos de poemas e narrativas em Libras. Já
descrevemos a forma, a origem e a função da literatura. Neste capítulo,
perguntamos o que estudamos, como escolhemos os exemplos e por quê. Como
se cria uma coleção de textos em Libras para ser estudada e apreciada e
como avaliamos as obras literárias para dizer que são boas? Quem as
escolhe?

Nos últimos anos, a literatura em Libras se tornou um tema de estudo de
nível universitário, surgindo a necessidade de acesso a alguns exemplos
para o ensino. Embora atualmente poucas escolas de surdos ensinem
literatura em Libras, as que a ensinam também precisam de recursos.
Temos pesquisas acadêmicas e estudos sobre literatura em Libras,
incluímos conteúdos de literatura nas escolas e nas instituições de
ensino superior, mas devemos perguntar: o que estudamos e por quê?

Podemos dizer que uma antologia de literatura surda em Libras deve
incluir histórias ou outras performances que são parte do “cânone
sinalizado”, como narrativas originais de autores específicos,
adaptações de narrativas tradicionais para se adequarem à realidade dos
surdos e traduções de narrativas tradicionais ou de outras narrativas de
línguas escritas (MOURÃO, 2011; 2016).

###  21.2 Antologia ou cânone?

Um cânone é um conjunto teórico das produções
literárias que os líderes de uma comunidade e os seus membros reconhecem
como “principal” ou “melhor”. Os exemplos que representam o cânone de
literatura em Libras são aqueles que os surdos apontam como de
literatura surda no Brasil, e que são discutidos por pesquisadores e
professores de maneira ampla. O cânone não
existe fisicamente, de uma forma concreta. É a *ideia* do conjunto de
todos os textos e produções literárias. O cânone de literatura em Libras
é a base da criação de qualquer antologia de literatura nessa língua.
Uma antologia, sim, tem forma concreta.

O cânone de literatura surda no Brasil começou com a criação do curso de
Letras Libras na modalidade a distância em 2006, na Universidade Federal
de Santa Catarina - UFSC. Os alunos desse curso (tanto os de 2006 quanto
os de 2008) estudaram as disciplinas de *Literatura visual* e
*Literatura surda*, com foco específico na literatura infantil. Os
textos literários escritos e em sinais estudados fundamentaram as ideias
do cânone que continuam até hoje. Os mesmos estudantes passaram a ser
nossos professores e intérpretes de Libras, desse modo eles também
perpetuam esse cânone em suas aulas e pesquisas. Porém, nenhum cânone é
fixo, o crescimento da literatura em Libras permanece. Sendo assim,
precisamos entender que ele já ampliou e mudou.

Di Leo (2004, p. 03) define uma antologia como "uma coleção de escritos
conectados ou inter-relacionados que se centram em torno de um tópico."
(Tradução nossa). Há antologias de literatura feitas a partir de uma
seleção de todos os gêneros ou (por exemplo) apenas de poemas, de
narrativas ou de contos. Imaginamos uma seleção de poemas em Libras
sobre ser surdo e ser brasileiro ou, ainda, de contos e lendas de surdos
de uma região do país. Uma antologia geralmente apresenta textos de
vários autores. Um organizador (que não é o autor) seleciona os textos e
pode fazer comentários adicionais sobre o que escolheu.

Algumas antologias incluem apenas textos já publicados, mas ainda é cedo
para exigir essa condição da literatura em Libras. Entendemos que uma
antologia de literatura em Libras pode ser feita com trabalhos novos
e/ou antigos. O importante é que os textos sejam selecionados de acordo
com os critérios do organizador (SUTTON-SPENCE; MACHADO, 2018).

As antologias tradicionalmente promoveram e divulgaram textos literários
que não estavam facilmente disponíveis, mas sabemos que hoje existem
muitos poemas e narrativas na internet, que permite o acesso fácil às
produções. Apesar disso, antologias de literatura surda ainda são
imprescindíveis, pois esta é pouco conhecida no Brasil. Muitas pessoas
surdas quase que desconhecem sobre a riqueza do seu próprio patrimônio
linguístico e são muito poucos os ouvintes que a conhecem. Uma antologia
pode mostrar a diversidade da literatura atual. As antologias são
fundamentais especialmente para o ensino, para ajudar pessoas
(principalmente os professores) a encontrarem diferentes tipos de
literatura em Libras valorizados pela comunidade surda, com orientações
sobre como usar os textos para estudo, ensino e apreciação
(SUTTON-SPENCE; MACHADO, 2018; WILLIAMS, 2004).

###  20.3. Antologias de literatura surda e literatura em línguas de sinais

Existem antologias de literatura surda escritas por surdos que
apresentam trabalhos selecionados por vários autores. A antologia
literária “O som das palavras”, publicada em 2003, mostra o trabalho
escrito em português por surdos.

Também existem antologias de poemas escritos por poetas surdos. Essa
ainda é uma área a ser desenvolvida no Brasil, embora o livro “Os Meus
Sentimentos em Folhas”, de Ronise Oliveira (2005), seja uma coleção de
uma pessoa surda publicada no Brasil em português escrito.

As coleções e antologias de poemas em Libras de um único autor também
existem para documentar, preservar, promover e tornar acessível o
trabalho de poetas surdos. Essas antologias foram produzidas em filmes,
fitas de videocassetes, DVDs e agora estão na internet - nas plataformas
de vídeos e nas redes sociais - em números crescentes em todo o mundo.
Em Libras, o vídeo *Literatura em LSB* (NELSON PIMENTA, 1999) foi o
primeiro a ser publicado fisicamente, e isso contribuiu para que ele
fizesse parte do cânone da poesia em Libras; mas, hoje, a tecnologia
também tem permitido a divulgação de outras obras.

#### 20.3.1. Antologias de literatura em Libras

A tecnologia da internet e as plataformas que armazenam e exibem vídeos
on-line permitiram o desenvolvimento de antologias nesse mesmo sistema
de vários tamanhos. Aqui no Brasil, existem diversas coleções
importantes, tais como [culturasurda.net](https://culturasurda.net/) e
[Librando](https://librando.paginas.ufsc.br/), que fornecem links para
arte surda e produções culturais em Libras e outras línguas de sinais.
As coleções da [TV INES](http://tvines.org.br)
e [Mãos Aventureiras](https://www.ufrgs.br/maosaventureiras/)
disponibilizam traduções para Libras de livros infantis escritos em
português, mas que não incluem comentários sobre os textos ou a respeito
da seleção deles.

[A antologia de Libras](https://antologiaslibras.wordpress.com)
(organizada por Fernanda Machado) compreende poemas selecionados de
fontes diferentes (como YouTube, DVDs comerciais, festivais e cursos de
ensino de poesia em Libras), todos com metadados e notas introdutórias
que trazem os perfis dos poetas (nomes, sinais pessoais e outros dados
como idade e profissão), o contexto de apresentação do poema (local,
data e extensão), a forma e o conteúdo dos poemas e seus públicos-alvo
(como crianças, estudantes e membros adultos da comunidade surda).

[A antologia de literatura em Libras](https://libras.ufsc.br/) do Portal
Libras da UFSC contém poesia, contos e narrativas de diversos gêneros
(poemas, ficção original de autoria surda, contos infantis, lendas e
narrativas pessoais) e foi criada para ser um recurso para professores e
alunos de Libras (como primeira língua - L1- ou como segunda língua
-L2), pais de surdos, pesquisadores e membros da comunidade surda. Os
critérios para inclusão de obras nessa antologia incluíram a diversidade
de gêneros literários e de perfil demográfico dos artistas surdos
brasileiros. Além disso, outros critérios importantes delimitados
englobaram a estrutura, a clareza dos sinais, o uso de técnicas
literárias com incorporação e ritmo e a duração (de 1 até 4 minutos).
Acima de tudo, todos os textos deveriam ser inesquecíveis. Cada produção
conta com anotações detalhadas sobre o autor e o texto para orientar os
seus usuários. Se essa antologia não existisse, o livro que você lê
agora também não existiria.

### 20.4. O público das antologias de literatura em Libras

No capítulo 06, vimos a importância do público de literatura em Libras.
As antologias sempre têm um público em mente e consideram as demandas
dele. Podemos esperar que os membros da comunidade surda sejam
considerados o público-alvo de uma antologia de literatura em Libras
para que eles possam apreciar as produções da sua cultura. Todo dia,
vemos exemplos individuais compartilhados nas redes sociais, porém
precisamos de uma coleção que dure, para que os surdos brasileiros
possam ter uma coleção de obras para assumirem como “sua”.

As antologias também são feitas para os poetas e contadores de histórias
de Libras, que, assim, podem acessar possibilidades de fazer novas
criações baseadas em textos dessas coleções. Pesquisadores de literatura
em Libras também precisam de uma antologia. Antigamente, nessas
pesquisas, eram utilizados textos isolados, ou uma pequena seleção
deles, simplesmente por falta de acesso a outros materiais. O tamanho
maior das novas antologias de Libras permite melhores análises
literárias, linguísticas e culturais. Professores e alunos precisam com
urgência de antologias de literatura em Libras. Precisamos utilizá-las
na escola para promover o orgulho linguístico e cultural e para criar a
próxima geração de poetas de línguas de sinais.

As antologias são importantes, também, para os aprendizes de Libras como
segunda língua. Stephen Ryan (1993) argumentou que os iniciantes no
aprendizado de ASL aprendem sobre a língua e sua cultura através do
estudo da literatura em ASL, mas para isso, eles precisam ter acesso ao
material certo. Os poemas publicados atualmente no YouTube, ou em outros
sites da rede, podem complementar obras canônicas, mas os professores e
alunos precisam de um *corpus* básico de poemas para estudarem,
especialmente aqueles referidos em publicações de pesquisa.

As notas introdutórias de uma antologia podem ajudar a todos os usuários
que tiveram menos oportunidades de estudar literatura em Libras em
profundidade. As informações biográficas sobre o artista, os dados
históricos das obras, sua relação com outras ou as sugestões de
interpretação, tudo isso ajuda na compreensão da literatura.

### 20.5. O papel do organizador

As antologias acadêmicas e de ensino atual das literaturas escritas,
como a literatura brasileira da língua portuguesa, geralmente são feitas
por pessoas reconhecidas dentro de suas comunidades literárias. Alguns
exemplos são a seleção de textos de *História Concisa da Literatura
Brasileira*, de Alfredo Bosi (lançado em 1970, mas até hoje um livro
canônico na área) e a coleção *Os cem melhores poemas/contos/crônicas*
da editora Objetiva (lançados na primeira década dos anos 2000 com
diferentes organizadores).

Os editores de antologias de ensino frequentemente também são acadêmicos
e professores. As antologias de literatura em Libras devem ser criadas
por equipes de surdos e ouvintes, professores, poetas, artistas e
pesquisadores. No fim das contas, todas as decisões dos editores não
passam de preferências pessoais, mas o julgamento editorial sobre o que
deve ser incluído em uma antologia de poemas em Libras será influenciado
pela experiência de poetas, pesquisadores acadêmicos e membros da
comunidade surda. Um organizador que conheça bem a comunidade surda pode
selecionar os textos, negociar com os poetas na comunidade, escolher
títulos, caso seja necessário, e expor suas observações.

#### 20.5.1. Seleção de conteúdo

Uma antologia é uma *seleção* feita dentre muitos textos. Essa
compilação tem várias implicações importantes, porque as antologias não
são somente exemplos concretos de cânones abstratos, mas também criam o
cânone (DI LEO, 2004). Em Libras, por exemplo, talvez o que esteja numa
antologia de um gênero de literatura defina aquele gênero.

Muitos editores e usuários acreditam que o conteúdo deve representar a
melhor literatura disponível, contudo os julgamentos sobre o que é
“melhor” variam. Um público hoje pode achar que as obras literárias mais
antigas pareçam fora de moda, e algumas pessoas hoje podem até sentir
que certas obras sequer sejam consideradas literatura em Libras (por
exemplo, poemas traduzidos do português).

O editor deve criar uma antologia que represente poetas de diferentes
contextos sociais, com uma seleção deliberada. Na escolha de obras para
uma antologia de ensino, seria bom tentar mostrar as experiências da
mais ampla visão dos surdos quanto possível. Por exemplo, apesar de o
nosso foco ser a literatura em Libras, é bom lembrar que a literatura
surda e sinalizada é, muitas vezes, acessível em outras línguas de
sinais. Uma antologia de poesia escrita para os leitores brasileiros
dificilmente incluiria poemas em alemão ou em inglês, porque os idiomas
não são acessíveis a quem não domina essas línguas. Mas uma antologia de
ensino em Libras pode incluir exemplos de literatura surda sinalizada
internacional, mesmo para uma turma brasileira. Além de falar de Libras,
podemos mostrar exemplos de obras de surdos da Alemanha, Suécia,
Holanda, Inglaterra e EUA porque, com uma explicação mínima, o público
brasileiro pode entender e aprender bastante sobre essa literatura.

Dentro do Brasil, uma antologia deve representar os surdos do país
inteiro. Por exemplo, contos e poemas de surdos gaúchos, nordestinos e
cariocas mostram a diversidade e a riqueza da experiência do povo surdo
do Brasil. Seria bom escolher obras que representem igualmente artistas
homens e mulheres. Por diversas razões, os artistas masculinos de Libras
(e das línguas de sinais de outros países) são mais presentes nos
espaços públicos. É importante, portanto, incluir os trabalhos femininos
para promover e incentivar tanto essas artistas quanto os públicos a
participarem na criação e divulgação das suas próprias obras.

Apesar de o povo brasileiro e da comunidade surda brasileira pertencerem
a um único povo, existem heranças familiares étnicas que fazem parte da
sua riqueza multicultural. Sabemos que as tradições indígenas,
africanas, japonesas e europeias, por exemplo, são diferentes e é
importante que uma antologia de literatura em Libras mostre essa
riqueza. Podemos incluir e celebrar a literatura de todas as heranças
étnicas dentro da comunidade surda.

A idade dos artistas ou contistas numa antologia de Libras também pode
torná-la mais representativa. Os tópicos das obras e os estilos de
apresentação serão diferentes dependendo se a pessoa é criança,
adolescente, adulta ou idosa. Para representar a diversidade da
literatura e promover a inclusão social para um público de todas idades,
uma antologia deve incluir exemplos de Libras literária de todas as
faixas etárias.

Embora a Libras seja uma língua antiga (sabemos que já era usada nos
anos 1860, e com certeza antes disso), o registro literário é muito mais
recente e não temos muitos exemplos de gravações de Libras artística
antes do final do século XX. Apesar disso, uma antologia pode incluir
recursos que mostrem as diferentes formas de poesia e narrativas em
diferentes épocas.

#### 20.5.2. Gênero literário

Vimos nos capítulos 07 e 08, que os gêneros de literatura são
caracterizados conforme a sua forma, origem, seu conteúdo e o
público-alvo. O organizador pode escolher obras de literatura de um
gênero específico (fábulas, narrativas, poemas líricos e teatrais,
duetos e performances grupais, haicais e renga, duetos, poemas de
homenagem etc.).

Os gêneros literários de Libras numa antologia, apesar de variados,
devem ser apresentados por homens, mulheres e por pessoas de diferentes
idades e etnias, além de também incluir conteúdo destinado para esses
diversos públicos.

<div class="float"></div>

![resumo](/images/resumo-parte4.png)

### 20.6. Resumo

Pesquisas e criação de antologias de literatura em línguas escritas
podem ajudar na criação de antologias de Libras. Questões sobre a
responsabilidade do editor e a seleção de textos são parecidas, e é
importante para todas as antologias identificar o seu público. No
entanto, as diferenças sociais, culturais e históricas entre as
comunidades surdas e ouvintes, bem como as principais diferenças entre
os poemas escritos/impressos e sinalizados/gravados em vídeo, exigem que
os editores façam escolhas adicionais e encontrem soluções para
problemas específicos. Como a literatura em Libras é pouco conhecida
(mesmo dentro da comunidade surda), há poucos guias acadêmicos ou
modelos para se tomar como embasamento. É um desafio garantir que todos
os usuários concordem a respeito da "qualidade" das obras sobre ela. A
distinção aparentemente simples entre coleções impressas e gravadas em
vídeo tem implicações em todas as etapas. Embora tenhamos a necessidade
de proteger qualquer antologia contra mudanças tecnológicas, a
existência do gênero *antologia* cria uma ferramenta fundamental para se
preservar, promover e disseminar a literatura para membros da comunidade
surda e aprendizes de Libras.

<div class="float"></div>

![atividade](/images/atividade-parte4.png)

### 20.7. Atividade

1. Escolha uma antologia de literatura surda escrita ou literatura em
Libras e responda:

   * Quais são os gêneros literários presentes nessa antologia?

   * O que você acha da função da antologia?

   * Quem é o público-alvo dessa antologia?

   * Quantas obras estão na antologia?

O que você pode falar sobre o perfil dos artistas ou autores? (São
homens ou mulheres? Há informações sobre a idade, a região ou outras
características?)

2. Crie sua antologia fictícia de literatura em Libras!

Escolha 10 obras para a inclusão e justifique por que você selecionou
esses exemplos.

* Quais são os gêneros literários que você selecionou?

* O que você acha da função da antologia que você criou?

* Quem é o público-alvo da sua antologia?

