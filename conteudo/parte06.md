---
capitulo: 6
parte: 1
titulo: Literatura surda brasileira 
pdf: "http://files.literaturaemlibras.com/CP06_Literatura_surda_brasileira.pdf"
video: mpREfZRmw3g
---

## 6. Literatura surda brasileira 

<div class="float"></div>

![lista de videos](/images/objetivo-parte1.png)


### 6.1. Objetivo

Neste capítulo, vamos pensar sobre a criação e a apresentação da
literatura em Libras, com foco na ideia de literatura surda. Lembramos,
do capítulo 03, que a literatura surda coloca o foco no conteúdo, nos
autores e no público. Perguntamos: quem conta a literatura em Libras e
para quem conta? Onde encontramos essa literatura?

<div class="float"></div>

![lista de videos](/images/videos-parte1.png)

<div class="lista-de-videos"></div>

Como preparação para ler este capítulo, você pode assistir aos seguintes
vídeos:

* [A Pedra Rolante](#ytModal-kPXWu5UCTzk), de Sandro Pereira.

* [Eu x Rato](#ytModal-UmsAxQB5NQA), de Rodrigo Custódio da Silva.

* *[Fazenda: Vaca](#ytModal-NtN98y67ukM),* de Rimar
Segala.

### 6.2. Literatura oral ou a "tradição face a face" 

Costumamos dizer que uma língua que não tem um modo escrito é uma língua
“oral”. A maioria das línguas no mundo não usa sistemas escritos no dia
a dia e são línguas orais. Muitos falantes de línguas minoritárias sabem
falar uma língua oral, mas também falam e são letrados numa outra
língua, que é muitas vezes uma língua de origem europeia, em função da
história da colonização dos continentes pelos europeus. A Libras é,
dessa forma, uma língua "oral", porque sua cultura se baseia nos sinais
“face a face”, de modo presencial, com interação, além de ter sua
tradição literária ativa através do folclore e de espetáculos. Porém,
vale destacar, que há uma conexão entre o “oral” e o “alfabetizado”.
Muitos surdos são alfabetizados na língua da sociedade dominante (que no
Brasil geralmente é o português) e interagem diariamente com os
ouvintes. Nesse sentido, o narrador e pesquisador surdo de literatura
surda nos EUA, Ben Bahan (2006), usa o termo "tradição face a face"
quando fala da apresentação e da divulgação da literatura surda, ao
invés de “tradição oral”.

### 6.3. Quem conta as histórias?

A questão “quem pode contar histórias em língua de sinais?” é uma
polêmica. De fato, todos podem contar histórias, essa é uma necessidade
vital para todos. Sem sinalizar, os surdos ficam privados desse direito
humano. Conforme Stephen Ryan, contador de histórias e pesquisador surdo
americano, “a contação de histórias em ASL \[...\] fornece uma estrutura
para a perspectiva das pessoas surdas: sem ar, nossas células morrem;
sem contar histórias em ASL, nossos eus (surdos) morrem” (1993, p. 145,
tradução nossa).

Os principais contadores de histórias em Libras são pessoas surdas. Elas
têm a língua, a cultura e as habilidades para produzir formas de arte e
querem compartilhar suas experiências com outros membros da comunidade.
Algumas pessoas ouvintes com pais ou irmãos surdos que dominam bem a
Libras[14](#tooltip) contam histórias em Libras porque participam da comunidade
surda, apesar de não serem surdos. Aprendizes ouvintes podem aprender
técnicas de contação de histórias ao contá-las, o que vai ajudar nos
seus estudos de elementos específicos da língua. No entanto, em sua
maioria, os principais contadores são surdos. Susan Rutherford, uma
pesquisadora americana de folclore surdo, descobriu nos anos 1980 que as
pessoas surdas geralmente acham que apenas os surdos devem contar as
histórias tradicionais da ASL (RUTHERFORD, 1993).

Ben Bahan (2006) criou o termo “Smooth signers”, ou seja, “sinalizantes
suaves”. Essas pessoas são artistas da língua e sua comunidade reconhece
que eles podem explicar ideias complicadas de uma maneira bonita e que
pareça simples. Elas têm as habilidades, o jeito, a capacidade ou o dom
de narrar. Talvez tenham nascido com um dom, mas também devem treinar
para desenvolvê-lo. Nesse sentido, Reilly e Reilly (2005) usam o termo
“sinalizantes mestres” (em inglês “master signers”). Devemos lembrar que
os “mestres” são homens e mulheres, mas no Brasil (e em muitas outras
comunidades surdas mundiais) vemos que quem faz as performances públicas
em frente a grupos, atualmente, são, na maioria das vezes, homens. Nas
escolas, as mulheres (como professoras) contam as histórias, mas ainda
falta uma presença notável delas nos palcos e nas redes sociais (ver
capítulo 21).

### 6.4. Como se aprende a contar?

Ninguém nasceu sabendo como contar histórias e o processo de aprender
essa arte é importante para a continuação da literatura surda. Os filhos
de pais surdos muitas vezes aprendem com os pais e com outros parentes
ou amigos dos pais. Por terem nascido dentro da comunidade surda, essas
pessoas têm mais acesso às tradições literárias da comunidade. No
entanto, os filhos de pais ouvintes aprendem de outras maneiras. Eles
assistem aos adultos ou às crianças mais velhas, que são “sinalizantes
suaves”, e interagem com os “mestres”. Muitos deles estudam vídeos na
internet, contam as histórias para os outros e acrescentam o seu próprio
estilo. Mesmo as histórias tradicionais têm a sua “marca” quando
recontadas em Libras.

### 6.5. O público e os elementos importantes para se contar histórias ao público

Quem conta uma história trabalha junto com o público para criar uma
experiência artística de literatura compartilhada. A influência do
público sobre o artista e sobre o texto varia. Tradicionalmente, o
público da literatura surda costumava ser apenas a comunidade surda. Os
surdos nas escolas e nas associações contavam histórias para os amigos e
as pessoas procuravam os que tinham o dom de narrar. Os espectadores
podem determinar a escolha da história ou a sua forma. Se um grupo pedir
ao surdo para que conte histórias, pode também solicitar o tipo dela e
até qual história dentre as várias que já viram antes e que querem
rever.

O público, hoje, contém mais ouvintes porque mais ouvintes sabem Libras
e existem mais opções e espaços para eles acessarem a literatura
(KRENTZ, 2006). Isso decorre especialmente dos vídeos na internet e das
redes sociais; também é uma consequência dos eventos públicos de
performance literária como shows, espetáculos e festivais. Bahan (2006)
questionou se o artista surdo com um público de pessoas ouvintes vai
mudar seu jeito de sinalizar ou as histórias que escolhe. Por exemplo,
contar as histórias dos ouvintes que fazem coisas bobas ou que oprimem
os surdos constituíam uma parte da tradição da literatura em ASL que os
surdos contavam entre si (BAHAN, 2006)? Krentz (2006) sugeriu que o
desejo de vender vídeos para os ouvintes no final do século XX e no
início do século XXI poderia influenciar a forma das narrativas para
agradar os ouvintes e assim diminuiria a contação das histórias que
zombam dessas pessoas. Porém, parece que isso não aconteceu, os ouvintes
que querem se aliar à comunidade surda assistem às mesmas narrativas e
aos mesmos teatros que os surdos.

Atualmente, há mais material em vídeo no YouTube e nas redes sociais
gratuitas e o público influencia menos nas histórias porque falta uma
interação dinâmica. O artista simplesmente grava o que ele acha que o
público vai gostar e lança na internet.

Sem um público, não vale a pena apresentar literatura surda. Entretanto,
este não é “um” público homogêneo; os artistas surdos têm um público
muito variável – sendo composto de surdos com diversas experiências,
ouvintes ou uma mistura de ambos. Os artistas sabem como entreter e
apresentar as obras que irão agradar e encantar a comunidade surda.
Muitas vezes, eles mudam as apresentações de acordo com o tipo de
público e por isso eles escolhem:

1. Os **elementos da língua** (sinais, elementos não manuais e forma
gramatical), mais adequados para um público específico – por exemplo,
poucas pessoas ou uma sala grande; bons amigos ou pessoas desconhecidas;

2. A **maneira de contar a história** (ritmo, velocidade, elementos não
manuais) para gerar as emoções desejadas (muitas vezes risos, mas também
suspense, respeito, indignação ou um sentimento de pertencer no grupo);

3. O **tema ou assunto** do conto, da piada ou do poema para atender às
expectativas do público;

4. O **conteúdo** de acordo com o público, considerando se este reage da
maneira esperada (por exemplo, se o público surdo não entende o artista
ou se os intérpretes não conseguem passar para português o que é
necessário).

Em todos esses pontos, vimos a importância do público. Com certeza,
qualquer artista deve agradar ao seu até certo ponto, mas, na relação
entre o artista surdo que apresenta seu trabalho ao vivo e um público
surdo, é preciso pensar mais sobre os aspectos que não sejam tão
importantes nos textos escritos.

As dicas a seguir vêm de Stephen Ryan (RYAN, 1993), sobre uma boa
relação entre o contador e o público. Mostram a importância da afinidade
entre os dois na comunidade surda.

#### 6.5.1. Usar contato visual e cumprimentar o público

Essa primeira dica de Ryan é para quando as narrativas são apresentadas
ao público ao vivo. Este deve se sentir como parte do evento e, por
isso, o contador deve criar um bom contato com os espectadores. Ainda
hoje, vemos muitas performances de narrativas e teatro ao vivo, mas com
as mudanças tecnológicas das últimas décadas há muito mais narrativas
gravadas em vídeo sem um público presente.

Mesmo num vídeo que não conte com a presença de espectadores, é
importante prender a atenção do público desde o início. Uma maneira de
chamar atenção à narrativa é saudar o público diretamente. Na história
*[Fazenda: Vaca](#ytModal-NtN98y67ukM),* de Rimar
Segala, por exemplo, o artista começa com “olá, tudo bem?” e oferece uma
introdução, dando o contexto da história. Rodrigo Custódio da Silva, ao
contar [*Eu x Rato*](#ytModal-UmsAxQB5NQA),
começa com uma saudação ao público e explica que vai contar uma história
que aconteceu de verdade. Sandro Pereira, em seu papel de narrador da
narrativa *[A Pedra Rolante](#ytModal-kPXWu5UCTzk),* gravada em
vídeo, finge uma interação com um público imaginário. Ele começa mexendo
com as unhas e se volta para a câmera como se tivesse acabado de notar a
presença de seus espectadores, pedindo desculpas por não tê-los visto.
Ele prepara as suas unhas, sobrancelhas e os cabelos e sinaliza “tudo
bem?”, seguido por “vou começar”. Explica o tópico da história e a
introdução termina com uma fala direta ao público: “você verá quando eu
começar a contar agora”. Alguns vídeos destinados ao público infantil
também começam com saudação para prender a atenção das crianças.

Mas talvez a tradição de saudar o público esteja mudando, porque muitas
narrativas em vídeos gravados para distribuição nas redes sociais ou no
YouTube começam sem cumprimentos ao público.

#### 6.5.2. Considerar a idade e o interesse do público 

Primeiro, o artista deve escolher os itens que ele espera que o público
goste. Muitos surdos, de todas as idades, gostam de histórias que se
relacionam comCidade Nova Heliópolis, São Paulo - SPlíngua de sinais que não tenha personagens surdos.

O nível de Libras usado precisa ser apropriado ao público. Por exemplo,
se os seus integrantes são de uma comunidade surda rural que tem pouco
contato com a Libras mais avançada e não conhecem a literatura surda, as
escolhas serão diferentes daquelas feitas no caso de um público urbano
com uma grande comunidade fluente em Libras onde se tem muito acesso à
Libras literária.

#### 6.5.3. Criar fortes imagens visuais

Já vimos que o objetivo principal de muitas histórias literárias em
Libras é criar imagens visuais fortes. Em alguns casos, é suficiente que
os fatos sejam contados em Libras para que o conteúdo fique claro. No
entanto, em muitos textos literários de Libras, o público surdo acha
mais divertidas as imagens fortes criadas por incorporação,
classificadores e expressões não manuais exageradas.

#### 6.5.4. Usar gestos com sinais, mas variar gestos com sinais

A dica de Ryan de misturar gestos com sinais significa que uma história
contada apenas com vocabulário é de iconicidade reduzida e a sua
intenção não é ilustrativa. A incorporação e os classificadores tornam
as narrativas em Libras mais divertidas, porque são estruturas altamente
icônicas (ver capítulo 04) e criam imagens visuais mais fortes. Usar
técnicas mais teatrais, no entanto, vai além da Libras, porque não
utilizam convenções linguísticas específicas, mas se baseiam em
“gestos”. Ryan sugeriu usar esses gestos fortes como parte da narrativa,
mas também que os bons contadores de histórias (os “mestres” ou
artistas) deveriam usar uma mistura de ambos. Veremos nos capítulos 07 e
10 que um gênero específico de narrativa, o “Vernáculo Visual”, usa
apenas gestos da técnica teatral, mas a maioria das histórias em Libras
mistura os dois.

#### 6.5.5. Usar pausas para permitir “cair a ficha”

O ritmo de uma narrativa precisa de preparação e planejamento cuidadoso.
Contar uma história com muita pressa deixa o público sem tempo para
apreciar e saborear as piadas, os sinais estéticos e criativos ou as
reviravoltas. O ritmo de apresentação é importante e depende da idade e
das habilidades linguísticas do público, bem como do tipo de história. É
bom que o artista faça uma pausa em momentos específicos e verifique se
o público captou a história e entendeu o que acabou de ser apresentado
ou contado. Se a história é engraçada ou assustadora ou se pretende
gerar qualquer outra emoção forte, o narrador deve dar tempo para a
emoção se acumular no público. Nos vídeos de Libras literária na
internet, que permitem múltiplas visualizações, alguns aspectos do ritmo
são menos importantes, porque se os espectadores não entenderam a obra,
podem simplesmente assisti-la novamente. No entanto, para criar um
efeito estético e aproveitar o ritmo da performance, as pausas são
essenciais.

#### 6.5.6. Celebrar o patrimônio surdo

Em toda boa literatura surda, o contador celebrará o patrimônio surdo,
seja por conteúdo da história relativa aos surdos, seja pela forma da
Libras que celebra a língua de sinais como a parte mais valorizada da
herança surda. O narrador deve ser entusiasta ao contar a história,
reunindo o público para celebrar as coisas boas da vida dos surdos
através da literatura em Libras.

### 6.6. Onde os surdos aprendem a literatura sinalizada?

Tradicionalmente, os artistas de Libras aprendem a arte de contar
narrativas nas escolas de surdos. As crianças passam histórias adiante
entre si e os sinalizantes transmitem suas habilidades aos mais novos na
escola. Nas bilíngues, hoje, podemos esperar que os professores, e não
mais apenas os estudantes, ensinem às crianças como contar histórias. No
entanto, a política de integração na educação cria a falta de grupos de
pares surdos que possam interagir e aprender. Além disso, a maioria
deles estuda apenas a literatura em português, às vezes traduzida para
uma forma de Libras (por exemplo, os contos de Machado de Assis em
Libras) que mostra o conteúdo da história. Dar acesso a essas narrativas
clássicas é muito importante para os alunos surdos (SPOONER, 2016), mas
não gera as mesmas emoções neles como gera nos alunos que têm o
português como primeira língua.

Alunos surdos (e ouvintes) ainda podem aprender as narrativas em Libras
através de coleções gravadas e disponíveis na internet ou por outros
meios de divulgação. Mas simplesmente assistir a uma história não é
suficiente para aprender a riqueza da literatura surda, pois isso não
ensina explicitamente sobre como criá-la e como se expressar. Além
disso, é importante para os surdos aprender a literatura surda em Libras
em eventos culturais como os que acontecem, por exemplo, nas
associações.

Muitas pessoas, hoje em dia, aprendem narrativas e piadas, poemas e
peças de teatro por meio da internet, especialmente pelas redes sociais,
uma oportunidade imensa para a divulgação da literatura em Libras. Por
outro lado, ficar em casa assistindo à literatura numa tela não é igual
a participar de um evento literário presencial, onde os surdos se
encontram para compartilhar a cultura surda na íntegra - e não apenas
uma parte da literatura. Tirar a literatura em Libras da comunidade
surda é como cortar flores para um buquê: por uns dias elas permanecerão
lindas, mas vão murchar e morrer em pouco tempo por falta das suas
raízes.

<div class="float"></div>

![resumo](/images/resumo-parte1.png)

### 6.7. Resumo

Nesse capítulo, vimos que a Literatura em Libras é fortemente ligada à
cultura surda e à comunidade surda brasileiras. As questões de “quem” e
“onde (se)” produz literatura em Libras estão relacionadas ao contexto
social e cultural da literatura nessa língua. Em grande parte deste
livro focaremos na produção de **artistas** e no uso linguístico dos
seus produtos, mas, especificamente, esse capítulo enfatizou o papel
crucial do **público** - o qual temos que ter sempre em mente.

<div class="float"></div>

![atividade](/images/atividade-parte1.png)

### 6.8. Atividade

1. Escolha duas histórias ou dois poemas que você considera que sejam
destinados a diferentes públicos (por exemplo, um para as crianças mais
jovens e outro para alunos de Libras ouvintes ou destinado a adultos
surdos fluentes em Libras) e pense sobre:

    * Quem é o público? (Lembramos que pode haver uma variedade de
públicos). Por que você acha isso?

    * Como é que o texto indica que o público-alvo é a comunidade de surdos?
(Será que eles sinalizam isso?)

    * Quem está contando a história?

    * Onde a história está sendo contada?

    * Por que a história está sendo contada?

2. Escolha uma história em Libras e identifique quem é o público-alvo.
Pense num público diferente. Como você mudaria a história se fosse
destinada a esse outro público? Por quê?

<span class="tooltip-texto" id="tooltip-14">
Denominados CODAs (Children of Deaf Adults – que significa os
    “filhos de pais surdos”) ou SODAs (Siblings of Deaf Adults – que
    significa os irmãos de surdos).
</span>
