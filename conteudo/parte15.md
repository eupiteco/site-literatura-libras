---
capitulo: 15
parte: 3
titulo: A estrutura espacial na Literatura em Libras
pdf: "http://files.literaturaemlibras.com/CP15_A_estrutura_espacial_na_literatura_em_Libras.pdf"
video: aX3AZN1Sozs
---

## 15. A estrutura espacial na Literatura em Libras

<div class="float"></div>

![objetivo](/images/objetivo-parte3.png)

### 15.1. Objetivo

Neste capítulo, investigaremos o uso do espaço nas histórias e nos
poemas em Libras, focando especialmente nos poemas. Artistas podem usar
o espaço para criar imagens fortemente visuais ou para gerar sentidos
específicos através de metáforas. Normalmente, nos poemas, esperamos o
uso do espaço de maneira mais metafórica, mas ambos os gêneros de prosa
ou poesia usam-no para criarem imagens visuais agradáveis ao público.

<div class="float"></div>

![lista de vídeos](/images/videos-parte3.png)

<div class="lista-de-videos"></div>

Para descrever os usos do espaço, vamos usar os seguintes poemas:

* *[As Brasileiras](#vimeoModal-242326425),* de Anna
Luiza Maciel e Klícia Campos.

* *[A Economia](#vimeoModal-267272909),* de Sara Theisen Amorim e
Ângela Eiko Okumura.

* *[O Farol da Barra](#ytModal-VXcKgO-jD9A),* de Maurício Barreto.

* *[Lei de Libras](#vimeoModal-267274663),* de Anna Luiza
Maciel e Sara Theisen Amorim.

* *[Meu ser é nordestino](#ytModal-t4SLooMDTiw),*
de Klícia Campos.

* *[Voo sobre Rio](#ytModal-YaAy0cbjU8o),* de Fernanda Machado.

### 15.2. Introdução ao uso do espaço em línguas de sinais

A Libras é uma língua viso-espacial. Isso significa que os sinais são
produzidos, localizados e movidos no espaço físico ao redor do
sinalizante. Os sinais ocupam o “espaço neutro” em frente ao sinalizante
ou são articulados no corpo. Os estudos linguísticos de línguas de
sinais apontam que existem representações diversas de espaço, apesar de
as mãos ocuparem o mesmo espaço físico.

Nos sinais convencionais e estabelecidos pelo vocabulário da Libras (ver
capítulo 04), os parâmetros fonético-fonológicos dos sinais necessitam
que cada sinal seja articulado em pelo menos um lugar. Além disso, o
parâmetro de movimento pode criar o deslocamento da mão de um lugar para
outro. Por exemplo, no poema *[O Farol da Barra](#ytModal-VXcKgO-jD9A)*, de Maurício Barreto, vemos os sinais <span
style="font-variant:small-caps;">pedra</span> e <span
style="font-variant:small-caps;">Bahia</span>. A locação da mão ativa no
sinal <span style="font-variant:small-caps;">pedra</span> ocorre nas
costas da mão passiva; no sinal <span
style="font-variant:small-caps;">Bahia</span>, a mão começa no peito e o
movimento desloca o sinal até o espaço neutro. Muitas vezes, o lugar de
articulação é motivado pela iconicidade.

Alguns sinais lexicais são fixados no corpo e outros são colocados no
espaço neutro. Sinais fixados no corpo são feitos em contato com locais
nele, por exemplo <span style="font-variant:small-caps;">comer</span>
(na boca) <span style="font-variant:small-caps;">entender</span> (na
têmpora), <span style="font-variant:small-caps;">aluno</span> (no braço)
e <span style="font-variant:small-caps;">saúde</span> (no peito). A
locação da maioria dos sinais ancorados no corpo acontece de alguma
forma iconicamente motivada. O sinal <span
style="font-variant:small-caps;">comer</span> está localizado na boca
porque comemos colocando comida em nossas bocas, então localizar o sinal
em qualquer outro lugar seria ilógico. A locação do sinal <span
style="font-variant:small-caps;">entender,</span> na têmpora, é motivada
pelo fato de que processos mentais ocorrem no cérebro, que está
localizado fisicamente na cabeça. A escala crescente de alunos durante a
vida escolar de 0 a 12 é mapeada nos braços e ombros do sinalizante, com
a locação no corpo ficando cada vez mais alta para cada série e, por
associação, um sinal comum em Libras <span
style="font-variant:small-caps;">aluno</span> está localizado na mesma
área geral do braço. Metonimicamente (no capítulo 16 falamos mais sobre
metonímia), o sinal <span style="font-variant:small-caps;">saúde</span>
baseia-se na associação com o movimento do estetoscópio de um médico de
um lado do peito ao outro para avaliar a saúde de um paciente. A locação
no corpo é especificada dentro da estrutura de todos esses sinais por
causa do sentido, e qualquer alteração na locação do sinal mudará o seu
significado.

Os sinais articulados no espaço neutro são um pouco diferentes dos
sinais fixados no corpo. O espaço neutro é o espaço em frente ao corpo
do sinalizante, geralmente se estendendo horizontalmente da esquerda
para a direita, com os braços em um ângulo de 45° em relação ao corpo;
verticalmente da altura da cintura até a altura da cabeça e avançando
até onde os cotovelos podem se estender confortavelmente, pelos 70°.
Exemplos de sinais que são feitos em espaço neutro incluem <span
style="font-variant:small-caps;">trabalhar, brincar, professor</span> e
<span style="font-variant:small-caps;">agricultor</span>. A locação
deles geralmente não é iconicamente motivada, são colocados na frente do
sinalizante - simplesmente num lugar confortável para articulação com
menor esforço. Mas eles são colocados em diferentes partes do espaço
neutro, quando o contexto do discurso exige. Por exemplo, se quisermos
comparar o trabalho de professores e agricultores, podemos colocar o
sinal <span style="font-variant:small-caps;">professor</span> à esquerda
do espaço e <span style="font-variant:small-caps;">agricultor</span> à
direita, localizando as ideias dos dois trabalhos nos espaços
contrastantes para que cada lugar se torne o espaço de referência para
essas ideias. Entendemos que o professor e o agricultor não estão
realmente nesses dois espaços, mas sim *as ideias* de um professor e de
um agricultor. Quando colocamos os sinais no espaço neutro, seu
significado fundamental não muda. Essa distinção entre os sinais
ancorados no corpo e aqueles feitos no espaço neutro é importante quando
pensamos sobre o uso do espaço na literatura em Libras.

A visão tradicional do uso do espaço nas línguas de sinais
(especialmente do espaço neutro) é a de que ele pode ser arbitrário ou
motivado. No **espaço arbitrário**, a colocação e o movimento dos sinais
são aleatórios (por exemplo, colocar os professores à esquerda ou à
direita dos agricultores é geralmente uma escolha do sinalizante) ou
controlados por regras gramaticais como 1ª, 2ª e 3ª pessoa. Liddell
(1990; 2003) refere-se a esse uso abstrato do espaço como o “espaço
simbólico”. O **espaço motivado** tem sido descrito como “topográfico”,
como um mapa (PADDEN, 1990), ou “substituto”[39](#tooltip) , porque o espaço dos
sinais substitui o espaço real (LIDDELL, 2003). Aqui, a locação dos
sinais no espaço neutro tem um significado específico. O layout espacial
criado nesse tipo de espaço reflete o layout real de um evento. No
topográfico, podemos colocar e mover classificadores para mostrar o
posicionamento e o movimento de objetos no mundo real. Podemos, também,
incorporar uma pessoa que está localizada dentro do espaço topográfico
apresentado. Na incorporação, a locação e o movimento das mãos do
sinalizante representam a locação e o movimento das mãos do personagem.

### 15.3. O espaço literal e topográfico (não metafórico) na literatura

O espaço físico literal, também chamado de espaço topográfico, mostra o
mundo de fora do sinalizante dentro do espaço dos sinais.

No poema [*Meu ser é
nordestino*](#ytModal-t4SLooMDTiw), a artista
Klícia Campos cria uma imagem do sertão nordestino, colocando e movendo
as mãos em classificadores no espaço literal e topográfico. No primeiro
sinal do poema, ela nos diz que se trata de uma memória. Os sinais
falados dessa memória são feitos perto da testa, pois a memória é um
processo mental localizado no cérebro. Seria ilógico colocar os sinais
em qualquer outro lugar. Depois, entramos na sua memória e ela mostra
uma ave que voa do céu para a terra, de cima para baixo. As mãos mostram
esse movimento dentro do espaço literal e topográfico por meio do uso de
um classificador que mostra as asas da ave batendo e descendo de cima
para baixo. Os rios descem das montanhas para o vale. As mãos novamente
fazem classificadores representando a largura dos rios, que se movem de
cima para baixo, até que chegam no vale largo, aberto e plano, onde se
movem sinuosamente no plano horizontal, do corpo para frente, criando a
imagem do rio sinuoso fluindo. Os classificadores que mostram a terra da
beira do rio traçam o espaço nos dois lados dele. Essa imagem traz um
cenário “a distância” parecido com as aberturas de um filme, que mostra
uma paisagem (lembra-nos dos filmes de Glauber Rocha, por exemplo)[40](#tooltip).

Depois, vemos uma pessoa dentro dessa paisagem, na seca, com imagens
mais próximas, mas sempre mantendo o espaço topográfico. A nordestina
(incorporada no corpo da poeta) monta a cavalo e a sua mão esquerda,
localizada no lado esquerdo, perto do rosto, mostra uma árvore torta
passando perto da face da cavaleira (a mão realmente passa perto do
rosto da poeta). A mão direita, na mesma configuração, chega a tocar o
rosto (da cavaleira), mostrando que a árvore o arranha (e a expressão
facial na incorporação mostra que ela não se importa). Novamente, vemos
a representação fortemente visual da imagem de um lugar no mundo, criada
no espaço de sinalização de uma forma topográfica e literal.

Poemas e histórias em Libras usam o espaço para criar imagens de máxima
força visual. Eles podem descrever uma cena ou uma vista estática,
construindo a imagem como se fosse um filme. Para criar uma cena, o
narrador normalmente apresenta primeiro os objetos maiores, imóveis e
inanimados e depois os objetos menores, móveis ou animados. Isso quer
dizer que, normalmente, o narrador mostra a cena antes de mostrar o
personagem. Vemos isso no poema [*Meu ser é
nordestino*](#ytModal-t4SLooMDTiw). E a mesma
coisa acontece no poema [*Voo sobre Rio*](#ytModal-YaAy0cbjU8o),
de Fernanda Machado, em que a artista apresenta primeiro o planeta e
depois a ave.

A apresentação suave de sinais numa escala graduada descreve as coisas
para mostrar o crescimento do mais distante ao mais próximo (ou ao
contrário). O artista pode aumentar o tamanho do sinal abrindo os dedos,
substituindo um classificador por outro ou utilizando a incorporação.
Pode, também, alterar a posição das mãos no espaço neutro em relação ao
corpo. Em [*Voo sobre Rio*](#ytModal-YaAy0cbjU8o), o primeiro
sinal falando do planeta mostra uma perspectiva muito longe por meio
apenas da ponta do dedo, e cada sinal cresce, aumentando o tamanho do
planeta: da mão com os dedos fechados em forma de “O”, depois as duas
mãos com os dedos curvados juntos para criar uma forma esférica, até as
duas mãos separadas de dedos quase retos, que criam uma esfera ainda
maior. O primeiro sinal da estátua do Cristo Redentor é visto de longe -
a poeta usa um classificador para uma cruz com apenas os dedos
indicadores cruzados. As mãos se aproximam do corpo para mostrar o
tamanho crescente da estátua, até que a poeta a incorpora em seu próprio
corpo e mostra o Cristo Redentor de uma perspectiva grande e de perto. A
representação do Pão de Açúcar também começa a distância, com
classificadores de dois punhos (as mãos fechadas) para duas coisas
esféricas pequenas, cada vez crescendo mais com os dedos se abrindo e as
mãos se aproximando do corpo da poeta para mostrar a ave se aproximando
e passando perto das duas rochas grandes.

Normalmente, o espectador entende que o sinal mais próximo ao corpo do
artista significa que o tamanho aumentou, porque vemos a cena da
perspectiva do sinalizante, mas, o artista pode também levar os sinais
ao espectador. Em algumas gravações em vídeo, os sinais podem se
aproximar da câmera, chegando quase até a lente. Embora não seja um
poema, vimos isso na história *[Galinha Fantasma](#ytModal-isyT8-mgCDM)*,
 de Bruno Ramos, na descrição das cabeças fantasmas das
galinhas, que usa classificadores na forma do bico da ave para mostrar
as cabeças circulando no quarto. Mas, para aumentar o terror, os
classificadores ficam cada vez mais perto da câmera – e assim mais perto
do público. No último sinal da história, a mão e o rosto do narrador
preenchem a tela inteira.

Numa obra literária em Libras, cada objeto pode
ser localizado para criar movimentos suaves no espaço. Isso cria o
**morfismo**, em que o local final de um sinal é o ponto inicial do
próximo sinal. Assim, minimiza-se o movimento de transição entre os
sinais e cria-se uma experiência suave e estética para o espectador.
Maurício Barreto usa muito o morfismo no poema [*O Farol da
Barra*](#ytModal-VXcKgO-jD9A). O primeiro sinal, <span
style="font-variant:small-caps;">deus,</span> termina num ponto alto na
esquerda da tela. O seguinte, <span
style="font-variant:small-caps;">graça-divina,</span> começa nesse ponto
do sinal anterior e vai até o rosto do poeta. A mão cai lentamente até o
nível do peito e se torna o sinal <span
style="font-variant:small-caps;">ondas-do-mar</span>. A mão direita,
sinalizando a onda, muda para o classificador, mostrando uma <span
style="font-variant:small-caps;">pedra</span> no mesmo lugar. A mão vira
e se levanta para se tornar a <span
style="font-variant:small-caps;">lua</span>. Não há movimento
desnecessário na criação da cena, e isso gera um efeito estético muito
forte, mesmo que também represente uma imagem no espaço topográfico.

### 15.4. O espaço simbólico (metafórico)

Nos poemas, o uso do espaço não é apenas estético, mas pode gerar outros
sentidos metafóricos. Veremos no capítulo 16 que a teoria da metáfora
cognitiva (partindo principalmente do trabalho de Lakoff e Johnson, 1980)
fala da metáfora orientacional, na qual entendemos que o espaço
real e concreto tem sentido metafórico nos conceitos abstratos. Por
exemplo, o que está mais alto significa metaforicamente uma coisa mais
positiva ou poderosa e o que está mais baixo significa algo mais
negativo ou sem poder. De frente para trás, o espaço mais perto e mais
longe do corpo pode significar, numa escala metafórica, futuro ou
passado, valorizado ou não, ou algo que está visível (e assim mais perto
do espectador) ou escondido (portanto, mais próximo ao corpo). Vamos
pensar mais sobre isso agora observando os poemas.

#### 15.4.1. Alto-baixo

Os poemas [*As Brasileiras*](#vimeoModal-242326425), de
Anna Luiza Maciel e Klícia Campos, e [*Meu ser é
nordestino*](#ytModal-t4SLooMDTiw), de Klícia
Campos, usam a mesma ideia do que está no alto caracteriza o que é mais
poderoso. No primeiro poema, os sinais da Paulistana rica – numa cidade
chuvosa onde a vida é mais fácil com água - são articulados acima dos
sinais da Nordestina pobre - onde a seca torna a vida mais difícil. No
segundo, o sofrimento e a opressão são apresentados com sinais mais
baixos e com o corpo inteiro abaixado. Quando a personagem no poema
resiste à opressão e reconhece o orgulho de ser nordestina, seu corpo
fica mais ereto e os sinais voltam a ser mais altos.

#### 15.4.2. Lado a lado

Os sinais apresentados em lados opostos podem mostrar a ideia de
oposição ou concordância de conceitos. Num dueto, podemos criar essas
metáforas com as duas pessoas lado a lado no espaço real. No poema [*As
Brasileiras*](#vimeoModal-242326425), as duas artistas
ficam dispostas espacialmente desse modo. No início, elas ficam perto
uma da outra, com os corpos em contato, sinalizam com as mãos juntas e
até utilizam, cada uma, a mão para criar os sinais no corpo da outra.
Essa proximidade extrema (lembramos que não é normal sinalizar dessa
maneira em Libras) mostra que as duas são brasileiras, portanto iguais.
Depois, elas se afastam para mostrar o contraste entre suas vidas
diferentes. Mas o último sinal, em que a paulistana joga a água para o
alto em São Paulo e a nordestina pega seu copo quase nos joelhos, no
Nordeste, cria um sentido muito forte de contraste por elas estarem lado
a lado. No final do poema, no entanto, as duas se aproximam novamente,
com suas mochilas em contato. Apesar das diferenças, elas são
brasileiras.

No poema dueto [*Lei de
Libras*](#vimeoModal-267274663), de Anna Luiza Maciel e
Sara Theisen Amorim, vemos a ideia de concordância e apoio no espaço
lado a lado. Elas sinalizam os mesmos sinais simultaneamente, no mesmo
espaço, e até usam a mão passiva de uma e a mão ativa de outra para
criarem juntas os sinais <span
style="font-variant:small-caps;">lei</span> e <span
style="font-variant:small-caps;">libras</span>. Isso mostra,
metaforicamente no espaço, o conceito central do poema que é a unidade
da comunidade surda perante a Lei de Libras. Elas dizem, no final, que a
Lei de Libras “é sempre nós”, mas a ideia de ser “nós” é também criada
através do uso do espaço.

#### 15.4.3. Em frente e atrás

A ideia de que o que está em frente é conhecido e atrás desconhecido é a
metáfora usada no poema dueto *[A
Economia](#vimeoModal-267272909),* de Sara Theisen Amorim e Ângela
Eiko Okumura. Nele, uma poeta, Ângela, fica por trás da outra poeta,
Sara. Esta incorpora a trabalhadora humana que tem emoções. Ângela
incorpora o dinheiro e a economia, que não são humanos, e não têm
emoções. São as partes escondidas e mal-entendidas da nossa vida, que
funcionam sem termos pleno conhecimento e controle sobre elas. Sara, na
frente, sinaliza <span style="font-variant:small-caps;">trabalhar</span>
para mostrar a pessoa trabalhando. Vemos as emoções dela no rosto. No
final do mês, a mão de uma pessoa desconhecida vem por trás e coloca o
salário no bolso dela. Isso mostra as forças impessoais da economia.
Sara, na frente, sinaliza as necessidades de pagar o aluguel, a internet
e o mercado e as mãos de Ângela, que representam a economia que tira o
dinheiro dela, sinaliza <span
style="font-variant:small-caps;">gastar</span>. Sara sinaliza <span
style="font-variant:small-caps;">trabalhar</span> novamente e a mão de
Ângela, escondida, ao invés de colocar o salário mensal no bolso, tira-o
e o leva dali. Finalmente, a economia se revela, e a trabalhadora pode
ver a economia real quando é demitida (sinalizado pela personagem por
trás). Quando a trabalhadora reclama, vemos enfim a economia não humana
que está atrás do nosso trabalho encolher os ombros com expressão de
desdém. Por fim, demasiadamente tarde, a realidade da economia é
revelada à trabalhadora. Essa metáfora pode ser representada de diversas
maneiras, mas apresentá-la em forma de dueto, onde a pessoa por trás se
esconde e se revela, cria um resultado fortemente visual em Libras.

### 15.5. Simetria

As divisões de espaço entre alto e baixo, esquerdo e direito ou em
frente e atrás são exemplos de divisões espaciais com simetria. Vemos
muita simetria nas poesias em Libras. Segundo o matemático Hermann Weyl,

> *por um lado, simétrico significa algo do tipo proporcional, bem
> equilibrado \[...\] A beleza está ligada à simetria \[...\] A imagem
> do equilíbrio proporciona uma ligação natural com o segundo sentido em
> que a palavra simetria é usada nos tempos modernos: a simetria
> bilateral, a simetria entre a esquerda e a direita, a qual está tão em
> evidência... no corpo humano. (WEYL, 1952, p. 1, tradução nossa.)*

Dentro da ideia de simetria, pensamos na dualidade, na oposição e no
equilíbrio. A dualidade está relacionada à oposição, por exemplo, entre
o bem e o mal, o rico e o pobre, o velho e o jovem; em que conceitos que
parecem opostos estão equilibrados, há simetria e harmonia. Às vezes, a
simetria na poesia em Libras é meramente estética, gerando imagens
equilibradas e agradáveis, mas outras vezes, é metafórica (KLAMT,
MACHADO e QUADROS, 2014).

#### 15.5.1. Simetria geométrica 

Existem diversos tipos de simetria no espaço, muitas delas sendo criadas
no espaço dos sinais em Libras. Na descrição dos tipos de simetria,
vamos focar principalmente no poema *[Voo sobre
Rio](#ytModal-YaAy0cbjU8o),* de Fernanda Machado.

##### 15.5.1.1. Reflexo

O tipo de simetria mais conhecido, e o que normalmente encontramos, é o
“reflexo”, que é a simetria como num espelho. Muitos poemas usam as duas
mãos com a mesma configuração de mão, localizadas no mesmo espaço
(através do plano de simetria – por exemplo, a mão esquerda no ombro
esquerdo e a mão direita no direito como no sinal <span
style="font-variant:small-caps;">passear</span>) e com o mesmo
movimento. Às vezes, as mãos articulam a simetria simultaneamente (isso
é, no mesmo momento) e, outras vezes, sinalizam em sequência,
primeiramente de um lado e depois do outro lado, criando uma simetria de
reflexo temporal. Todos os poemas que descrevemos nesse capítulo têm
muitos exemplos disso. Vale a pena assistir a todos eles e contar
quantos sinais *não* são dessa forma.

O reflexo pode ter diferentes planos e eixos de simetria, e vemos todos
esses no poema *[Voo sobre Rio](#ytModal-YaAy0cbjU8o),* de
Fernanda Machado.

###### Direita e esquerda (simetria vertical) 

O poema tem muitos exemplos desse tipo de simetria. É o mais comum na
poesia em Libras porque o nosso corpo é simétrico nos lados direito e
esquerdo, com o plano de simetria central vertical. O olho, a orelha, o
braço, a mão e a perna no lado esquerdo são (quase) iguais a essas
mesmas partes do corpo no lado direito. Por isso, os locais e os
movimentos dos sinais podem facilmente ser simétricos nesse plano.

Quando Fernanda incorpora a ave, ela move os braços para mostrar o bater
das asas, com um sinal simétrico em que os braços têm a mesma
configuração e se movem da mesma maneira nos dois lados do plano de
simetria vertical, que está localizada no corpo da artista (e que, nesse
contexto, é o corpo da ave). No classificador representando a ave, as
duas mãos têm a mesma forma e o mesmo movimento, divididas no plano de
simetria vertical que está localizado nos polegares em contato. O
classificador mostrando o Cristo Redentor com a forma de uma cruz é
feito pelas duas mãos com a mesma configuração, criando uma imagem
simétrica com o plano vertical localizado no dedo indicador. O sinal
mais conhecido do poema, em que as duas aves se beijam, é simétrico
nesse plano vertical. É um sinal destacadamente especial porque dois
sinais de um classificador representando as aves se juntam para criar um
sinal simétrico, o do coração.

###### Acima e abaixo (simetria horizontal) 

Esse tipo de simetria com o plano horizontal é menos comum porque nosso
corpo não tem esse plano de simetria fisicamente. A parte de cima do
corpo é muito diferente da parte de baixo. Porém, podemos criar uma
simetria com esse plano porque, com um pouco de esforço, podemos pôr uma
mão e até um braço acima do outro.

Em [*Voo sobre Rio*](#ytModal-YaAy0cbjU8o), o sinal <span
style="font-variant:small-caps;">planeta</span> é feito com uma mão por
cima da outra.

###### Em frente e atrás

Essa simetria é a menos comum, porque é difícil criá-la (pois exige mais
esforço na produção do sinal) e percebê-la (pois exige mais esforço por
parte do espectador). Apesar disso, vemos exemplos lindos no poema *[Voo
sobre Rio](#ytModal-YaAy0cbjU8o)* quando as aves se abraçam e
uma fica olhando para trás e a outra para frente.

Além desses exemplos, vemos que qualquer plano de simetria pode ser
também diagonal.

##### 15.5.1.2. Outros tipos de simetria geométrica

Na **rotação** (girar) vemos os sinais que têm os mesmos parâmetros e
estão no mesmo plano, mas que se movem em volta de um eixo central.
Quando o planeta gira, as mãos giram, num exemplo de simetria de
rotação. No primeiro encontro entre as aves, elas se olham e depois
olham para o lado. A única diferença entre a ave olhando para o lado
esquerdo ou direito, para frente ou para trás, é a rotação do sinal no
espaço. Nas **translações**, as mãos deslizam, mas sabemos que, se
estivessem juntas novamente, iam voltar a ficar simétricas. Os sinais
das pessoas passeando na praia têm essa simetria, o movimento do planeta
orbitando mostra translação; a cruz do Cristo Redentor deslizando para
ficar mais perto do corpo também é uma translação. Nas **dilatações**,
alteramos o tamanho de um sinal em relação a outro, mas todas as outras
dimensões ficam iguais. Quando o Pão de Açúcar se aproxima, o tamanho
das duas mãos (e assim, das rochas) aumenta com a mesma forma, mas uma
mão é menor do que a outra. No mesmo sinal também vemos uma translação
porque as duas mãos mantêm a relação entre elas, mas se deslocam para
outro espaço.

##### 15.5.1.3. E se não for simétrico? 

Nem todos os poemas mostram simetria nem todos os sinais dentro de um
poema simétrico são simétricos. A não simetria acontece quando não há
uma tentativa de simetria, porém usamos o termo **assimétrico** para
falar de uma rejeição intencional desta. A assimetria é tão
significativa quanto a simetria na poesia. Quando a ave finalmente chega
na terra, uma mão cai e ficamos apenas com a outra mão, mostrando uma
ave. É um momento de constrangimento para o espectador. Até aqui tivemos
movimento e simetria. De repente, tudo para e temos apenas uma mão
articulando. A ave está sozinha. Com isso, perguntamos: o que
acontecerá? Estamos preparados para o próximo evento, a chegada da
segunda ave, e estamos aliviados em ver a volta da simetria. Daqui para
frente, tudo é simétrico, até os últimos sinais assimétricos, quando o
planeta diminui até virar um pontinho e nos despedimos das aves.

#### 15.5.2. Simetria temática

A simetria temática fala do conteúdo do poema, e não necessariamente da
sua forma. Essa simetria cria um equilíbrio no texto. O pesquisador de
folclore Axel Olrik (1965) propôs a “Lei do contraste”, em que duas
qualidades são apresentadas para criar desequilíbrio e equilíbrio dentro
da narrativa ou do poema. Olrik observou que os contos folclóricos
incluem contrastes como jovens e velhos, grandes e pequenos, homem e
monstro, o bem e o mal para gerar equilíbrio no texto. Em Libras,
podemos ver simetria temática entre surdos e ouvintes, Libras e
português, sinalizar ou falar, fazer parte da comunidade surda ou se
isolar, por exemplo. Nos poemas que estudamos nesse capítulo, vemos o
contraste entre rico/pobre e seca/chuva ([*As
Brasileiras*](#vimeoModal-242326425)),
sofrimento/opressão e resistência/orgulho ([*Meu ser é
nordestino*](#ytModal-t4SLooMDTiw)), vontades
humanas/poder de Deus ([*O Farol da
Barra*](#ytModal-VXcKgO-jD9A)) e vida emocional do
povo/indiferença da economia ([*A
Economia*](#vimeoModal-267272909)).

#### 15.5.3. Simetria temporal

Na simetria temporal, falamos de estruturas que são apresentadas durante
um período que podemos imaginar de uma forma espacial. Assim, elementos
como sinais, eventos ou ações podem acontecer no início e no final do
espaço de tempo. O poema *[Voo sobre Rio](#ytModal-YaAy0cbjU8o)*
mostra a simetria temporal de reflexão, em que os sinais e eventos do
início são repetidos no final. Vemos as quatro perspectivas do planeta
crescendo da ordem 1, 2, 3 e 4 no início, e no final, reduzindo, como 4,
3, 2 e 1 (decrescendo). Na primeira parte do poema, vemos a estátua do
Cristo Redentor e o Pão de Açúcar crescendo e na segunda parte eles
diminuem. No início, o bonde sobe, e, no final, ele desce. Na primeira
parte do poema, a ave voa para baixo e na segunda parte, as asas batem
para cima. Com essa simetria temporal, o espectador fica satisfeito com
a estrutura do poema e com o prazer das imagens visuais criadas.

<div class="float"></div>

![resumo](/images/resumo-parte3.png)

### 15.6. Resumo

Nesse capítulo, vimos que o espaço dos sinais cria muitas oportunidades
para os poetas produzirem imagens estéticas agradáveis e metafóricas. O
uso de espaço gera contraste e equilíbrio e o estudo da estrutura
espacial pode mostrar, novamente, a riqueza dos poemas em Libras.

<div class="float"></div>

![atividade](/images/atividade-parte3.png)

### 15.7. Atividade

Analise um poema onde vemos a simetria. Você pode observar alguns
exemplos de:

-   Reflexão (como num espelho)
    -   Vertical: esquerda-direita (você deve encontrar muitos)
    -   em frente e atrás
    -   horizontal: acima-abaixo

-   Rotação (girar)

-   Translações (deslizar)

-   Dilatações (alterar o tamanho)

-   Você consegue encontrar exemplos de conceitos simétricos de
    temática?

-   Você consegue encontrar exemplos de conceitos simétricos temporais?

-   Observe como a simetria é criada e depois quebrada. Qual o efeito
    que a assimetria acrescenta?

<span class="tooltip-texto" id="tooltip-39">
Em inglês “surrogate”, às vezes traduzido em português como
    “sub-rogado”.
</span>

<span class="tooltip-texto" id="tooltip-40">
Já falamos dessa ideia no capítulo 10.
</span>
