---
capitulo: 5
parte: 1
titulo: Libras estética
pdf: "http://files.literaturaemlibras.com/CP05_Libras_estetica.pdf"
video: VRWRxdTaiSw
---

## 5. Libras estética

<div class="float"></div>

![objetivo](/images/objetivo-parte1.png)

### 5.1 Objetivo

Neste capítulo, veremos alguns exemplos da linguagem literária em Libras
e em outras línguas de sinais que criam um efeito estético para o
público e que não têm correspondência fácil com o português ou com
outras línguas escritas.

<div class="float"></div>

![lista de videos](/images/videos-parte1.png)

<div class="lista-de-videos"></div>

Trazemos muitos exemplos de obras literárias para esta discussão. Chegou
a hora de conhecermos um pouco mais sobre a literatura em Libras (e em
outras línguas de sinais). Você pode assistir aos vídeos agora e depois
revê-los durante a sua leitura.

* *[Árvore](#vimeoModal-267272296),* de André Luiz
Conceição.

* *[A Pedra Rolante](#ytModal-kPXWu5UCTzk),* de Sandro Pereira.

* *[Bolinha de Ping-Pong](#ytModal-VhGCEznqljo),* de Rimar Segala.

* *[Como Veio Alimentação](#ytModal-nMOTYprbYoY),* de Fernanda Machado.

* *[Eu x Rato](#ytModal-UmsAxQB5NQA),* de Rodrigo Custódio da Silva.

* *[Farol de Barra](#ytModal-VXcKgO-jD9A),* de Maurício Barreto.

* *[Golf Ball](#ytModal-Gl3vqLeOyEE),* de Stefan Goldschmidt.

* *[Jaguadarte](#ytModal-DSSXTh0wriU),*
de Aulio Nóbrega.

* *[Leoa Guerreira](#ytModal-rfnKoCXmSg4),* de Vanessa Lima.

* *[Meu ser é Nordestino](#ytModal-t4SLooMDTiw),*
de Klícia Campos.

* *[A Rainha das Abelhas](#ytModal-nXI4aO2_G3E),*
de Mariá de Rezende Araújo.

* [*Tinder*](#vimeoModal-267275098) de Anna Luiza Maciel.

* [*Tree*](#ytModal-Lf92PlzMAXo) (em português *Árvore*), de Paul
Scott.

Com esses exemplos em mente, vamos destacar alguns elementos da
literatura em Libras que surgem diretamente do fato dela ser uma forma
de arte de uma língua visual.

### 5.2 Linguagem estética em Libras

A experiência corporal das pessoas surdas é, na maioria, de visão e de
tato ao invés de som, e a linguagem estética da literatura destaca isso.
Já falamos que a Libras artística e literária nos poemas, nas
narrativas, no teatro e até nas piadas, centra-se na linguagem estética
visual. A linguagem estética apela aos sentidos e por meio dela o
artista surdo busca criar uma **experiência** para o seu público, em vez
de apenas afirmar algo ou dar uma informação.

No capítulo anterior, vimos três principais opções para a criação de
sinais visuais (vocabulário, incorporação e classificadores), mas há
outros elementos da literatura em Libras que contribuem para gerar
emoção no público. A Libras criativa é uma forma de arte linguística que
compartilha elementos em forma de arte visual e arte visual em
movimento. Frequentemente, a literatura vai além do vocabulário da
Libras para criar algo muito mais visual. Às vezes, a literatura em
Libras é mais parecida com a pintura, a dança, o filme e o cinema e tudo
isso compõe um elemento estético (ROSE, 1992; CASTRO, 2012).

Na literatura, brincamos principalmente com a língua para criar efeitos
estéticos. A teoria linguística lida com uma descrição de “unidades”
delimitadas da língua, descrevendo os fonemas e morfemas, os sinais, os
itens do vocabulário e a sintaxe das sentenças, mas a língua artística
vai além dos limites dessas unidades fundamentais da Libras. As
brincadeiras estéticas mesclam os sinais até que não existam mais
“unidades”, quebram as regras fonológicas, geram morfemas esquisitos e
criam novas experiências visuais e comunicativas fora dos padrões da
Libras cotidiana. Os elementos na literatura sinalizada chamam atenção
ao “visual” com movimento no espaço e por isso são diferentes dos
elementos literários na literatura escrita, especialmente na literatura
escrita das línguas orais. Pensaremos agora sobre alguns elementos
estéticos em Libras.

#### 5.2.1 Velocidade

Um parâmetro fundamental dos sinais é o movimento e em todo movimento há
uma velocidade. No ritmo da língua, que é “normal” e sem intenção de ser
estética, a velocidade do sinal não é destacada. O movimento de sinais
do vocabulário se encaixa no ritmo normal de uso da língua. A velocidade
dos sinais classificadores, quando não se tem intenção estética,
representa a velocidade do referente. Se uma bicicleta anda lentamente,
o movimento do sinal classificador será lento; se a velocidade da
bicicleta aumentar, a velocidade do sinal classificador também aumenta.
Isso acontece igualmente com sinais de incorporação. As ações que foram
feitas em uma dada velocidade são recriadas dentro do corpo do
sinalizante na mesma velocidade.

Esses ritmos e movimentos dos sinais são usados na literatura também,
mas na linguagem estética podemos brincar com a velocidade para gerar
emoções no público. Um recurso utilizado, e valorizado, nas narrativas é
o da “câmera lenta”. Já sabemos que nos filmes esse efeito especial
mostra eventos em uma velocidade reduzida para que se veja melhor os
detalhes da ação. Os artistas surdos podem recriar esse efeito e
sinalizar com movimentos prolongados e lentos para aumentar as emoções
no público com imagens mais fortes.

Assista agora ao vídeo da narrativa metafórica de Rimar Segala, [*Bolinha de Ping-pong*](#ytModal-VhGCEznqljo). Nesse vídeo,
Rimar descreve um jogo de pingue-pongue. O jogo começa lento e se torna
mais rápido. De repente (aos 00:02:40), todos os movimentos diminuem a
velocidade e entramos em “câmera lenta”, vendo as ações dos jogadores e
da bola destacadas. Lembramos que sabemos que os movimentos dos
referentes não diminuíram “na realidade”, mas apenas os movimentos dos
sinais. As emoções ficam mais intensas por causa desse uso lento dos
movimentos.

Não é apenas com sinais de incorporação que a velocidade muda, mas
também com o movimento dos sinais classificadores. Na história de
*[Eu x Rato](#ytModal-UmsAxQB5NQA)* de Rodrigo Custódio da Silva, vemos
o uso de câmera lenta quando o rato pula perto do rosto do rapaz. No
exemplo, as incorporações do narrador e do rato têm movimentos lentos e
o sinal classificador mostra o rato que pula.

Essa forma de linguagem estética é construída apenas nas línguas de
sinais. Nas línguas orais se pode articular uma palavra mais lentamente,
mas o efeito disso não é igual, porque meramente prolonga a palavra e
não a imagem visual da ação. Numa forma escrita, podemos aumentar o
espaço na página entre as palavras, mas isso também não gera as mesmas
emoções nem os efeitos estéticos visuais.

#### 5.2.2 Espaço e simetria

Sabemos que em Libras se pode colocar os sinais em diversos lugares para
criar sentidos adicionais. No poema *[Como Veio Alimentação](#ytModal-nMOTYprbYoY)*, Fernanda Machado usa o espaço de uma forma estética para
criar sentidos adicionais.

Colocar dois sinais em lugares opostos do espaço pode gerar o sentido de
que dois referentes se opõem. Nesse poema, as mãos são localizadas e
movidas em lados contrários no espaço de sinalização, mostrando os
mundos separados, o do trabalhador rural pobre (do lado direito) e o do
rico habitante da cidade que não pensa sobre como surgiu a alimentação
(à esquerda). A mão representando o trabalhador rural é sempre mais
baixa e a mão representando o morador da cidade é sempre mais alta, como
uma metáfora para as pessoas “inferiores” ou oprimidas e as que estão em
posições “mais altas” da sociedade.

A simetria é outra maneira de criar efeitos de linguagem estética por
meio da criação de uma sensação de equilíbrio. Vamos falar mais sobre a
simetria no capítulo 15, mas, por enquanto, podemos ver que muitos
exemplos de poesia sinalizada usam as duas mãos ao mesmo tempo, muitas
vezes com a mesma configuração de mão (MACHADO, 2013).

#### 5.2.3 Mesmas configurações de mãos: estética e metafórica

Os primórdios da poesia em língua de sinais, em particular a
desenvolvida nos EUA nos anos de 1960 e de 1970, utilizavam
configurações de mãos repetitivas nos sinais para criar um sentido de
“rima”. Esse método foi usado, de forma pioneira, por Dorothy Miles
(descrito pelos pesquisadores americanos Klima e Bellugi, 1979) e por
Clayton Valli (1993) na língua de sinais americana (ASL), possivelmente
por ambos terem sido fortemente influenciados pela poesia na forma
escrita que estudaram. Naquele tempo, muitos acreditavam que a rima (ou
outras partes repetidas de palavras, que criam aliteração ou assonância)
era fundamental para a poesia.

Esteticamente, a visualização repetida de uma mesma configuração de mão
é muito agradável de se ver. O público pode apreciar a sagacidade da
artista que cria sinais significativos ao usar repetidamente uma mesma
forma. A história *[Leoa Guerreira](#ytModal-rfnKoCXmSg4),* de
Vanessa Lima, usa a mesma configuração de mão em forma de garra da leoa,
seja o que for que sinalize, e é divertida e espirituosa por conta de
tal recurso, que sempre nos lembra que ela é a leoa. Tudo é feito pela
incorporação das leoas. Os sinais LUTAR, DESISTIR, NÃO, CANSA, FLORESTA, SOL, CALOR, LIMPA-O-SUOR-DA-TESTA, OLHAR-NA-DISTÂNCIA, TRABALHAR, DIFÍCIL, VAMOS, GRUPO têm a configuração de mão de pata em
garras. Quando Vanessa sinaliza com patas, os sinais do vocabulário que
já usam cinco dedos são fáceis de alterar. Por exemplo, a coroa de Miss
que a leoa ganha já tem a configuração certa de cinco dedos curvados. Os
sinais que normalmente fecham a mão se fecham um pouco para manter
melhor a forma da pata. PROCURAR parece ser meio sinal
normal e meio sinal das patas, porque dois dedos do sinal são esticados,
mas os outros são em garras. O sinal OUVINTE não fecha completamente
(como esperamos no sinal normal) e FELIZ dobra o dedo indicador,
mas a configuração da mão não é igual à letra manual “F”. O sinal MISS é quase normal na primeira
vez, depois se parece mais com garras.

Algumas configurações de mão são mais fáceis de serem repetidas por
serem mais comuns em línguas de sinais e classificadores. Por exemplo, é
menos desafiador repetir a configuração de mão aberta (porque muitos
sinais a utilizam) do que usar as configurações “X”, utilizadas para
poucos sinais.

Outros poemas podem usar uma configuração de mão
repetida para ir além do meramente estético, a fim de acrescentar
significação metafórica, como em [*Meu Ser é
Nordestino*](#ytModal-t4SLooMDTiw), de Klícia
Campos. Esse poema repete a configuração “mão aberta” em sinais que se
referem a aspectos positivos da vida nordestina, ao passo que a forma
“mão aberta em garra” e mais tensa em sinais reflete a dificuldade com o
calor e a seca da região, enquanto o punho cerrado repetido em sinais se
refere à força e determinação dos habitantes da região.

#### 5.2.4 Morfismo: mudando as configurações da mão

Os pesquisadores pioneiros da poesia em língua de sinais, Klima e
Bellugi (1979), perceberam que a poesia em língua de sinais pode reduzir
o movimento entre sinais através da fusão de um sinal no seguinte. Um
sinal com uma configuração manual em um local pode tomar novo movimento
ou nova locação e adquirir um novo sentido.

Assista ao poema *[O Farol da Barra](#ytModal-VXcKgO-jD9A),* de
Maurício Barreto. Nesse poema, a luz brilhante da graça de Deus é
mostrada pela configuração de mão aberta virada de lado e
movimentando-se em direção ao rosto do poeta. A mesma configuração de
mão muda de orientação e o seu movimento passa a ser o de flutuar para
baixo para se tornar o mar. Desse modo, os sinais GRAÇA-DIVINA ou LUZ-DIVINA-NO-ROSTO e 
MAR-EM-MOVIMENTO unem as duas
ideias ao se metamorfosearem um no outro. A configuração de mão em “O”,
representando a rocha no oceano, muda de orientação e começa a se
erguer, tornando-se a lua crescente. Assim, embora nada ligue de maneira
óbvia as ideias de rocha e lua, o poema fala do naufrágio que aconteceu
durante a noite depois do barco bater na rocha, então os dois sinais
estão conectados por suas formas visuais quando um se funde no outro. A
transição fluida entre os sinais cria uma serenidade no poema que não
poderia ser obtida se houvesse transições mais longas entre cada sinal
(JESUS, 2019).

#### 5.2.5 Mostrar humanos (por incorporação)

No lugar de contar ao público acerca dos personagens literários, a
Libras frequentemente os apresenta através do recurso da incorporação, e
um aspecto da sinalização estética altamente valorizado é a habilidade
de imitar pessoas (MORGADO, 2011). Tal recurso é particularmente
agradável quando a pessoa é caricaturada através do exagero de sua
aparência, seja de suas características físicas ou de seus movimentos.
Muitos contadores de história têm o cuidado de descrever seus
personagens fisicamente através da incorporação e continuam a enfatizar
esses aspectos enquanto contam a história. Mostrar os personagens é algo
visualmente muito satisfatório para a plateia, que por vezes se sente
como se estivesse assistindo a personagens de um filme.

Em *[Bolinha de Ping-pong](#ytModal-VhGCEznqljo),* Rimar Segala
descreve cuidadosamente a aparência física dos dois competidores,
deixando claro o quão diferentes ambos são fisicamente, antes de
incorporar cada um sucessivamente durante a partida de pingue-pongue. A
descrição dos dois é exagerada pela ampla movimentação nos sinais, pelas
expressões faciais e pelos movimentos corporais particularmente fortes,
criando caricaturas de uma mulher excessivamente empertigada e feminina
e de um homem vaidoso, machista e de um atleta agressivo. Como veremos
no capítulo 19, o exagero é uma parte importante na sinalização
humorística e aumenta o impacto da imagem visual.

#### 5.2.6 Mostrar animais, plantas e objetos inanimados

A imitação estética de seres humanos estende-se à imitação de não
humanos, sejam eles animais, plantas ou objetos inanimados. Esta utiliza
o importante recurso literário do antropomorfismo, no qual o sinalizante
retrata o personagem não humano como se este fosse humano. Estudaremos
tal recurso com maior profundidade no capítulo 17, mas por ora podemos
examinar alguns exemplos. Na literatura em Libras, de modo geral, os
animais antropomorfizados tendem a sinalizar, ao passo que objetos
antropomorfizados raramente fazem isso (ANDRADE, 2015).

Como vimos, Vanessa Lima assume o papel da leoa
na história [*Leoa Guerreira*](#ytModal-rfnKoCXmSg4). A leoa tem
habilidades e desejos humanos (por exemplo, quer ser uma Miss), utiliza
língua de sinais (o uso da linguagem é considerado uma habilidade
exclusivamente humana), mas seus sinais são apresentados usando patas de
leão no lugar de mãos humanas, lembrando-nos de que ela não é humana.

O antropomorfismo de objetos está presente em *[Bolinha de
Ping-pong](#ytModal-VhGCEznqljo),* em que vemos os
sentimentos, desejos e movimentos da bola de pingue-pongue ao sofrer
raquetadas de um lado para o outro durante a competição. Normalmente,
não esperaríamos que uma bola tivesse sentimentos e o modo como estes
são mostrados no corpo humano através da Libras é divertido de ver. A
peça [*Golf Ball*](#ytModal-Gl3vqLeOyEE), de Stefan Goldschmidt,
é outro exemplo de uma bola antropomorfizada. Aqui vemos novamente seus
sentimentos e suas reações ao vivenciar o mundo como uma bola de golfe,
mas, assim como a bola de pingue-pongue, a de golfe não sinaliza.

#### 5.2.7 Classificadores (e novos classificadores) 

Vimos no capítulo 04 que a Libras utiliza classificadores, em que as
configurações das mãos são escolhidas a partir de um conjunto
convencional da Libras, posicionadas e movidas no espaço a fim de
mostrar como os personagens e os objetos se movem e se relacionam uns
com os outros. Espera-se que qualquer boa história em Libras as utilize
como um meio de criar um texto que seja visualmente divertido. [*Eu x
Rato*](#ytModal-UmsAxQB5NQA), de Rodrigo Custódio
da Silva, é um bom exemplo. Nessa obra, ele conta uma história que
mostra uma versão mais jovem de si mesmo caçando um rato na sala de
ferramentas. Mas, ao invés de simplesmente nos contar o que fez, ele
também mostra. O autor utiliza parcialmente o recurso da incorporação,
mas a história também é notável por seu amplo uso de sinais
classificadores que mostram o rato, seu rabo e o bastão, todos em uma
disposição espacial complexa, mas de grande clareza.

Alguns sinalizantes, entretanto, vão além dos limites das regras da
linguagem e criam configurações de mão classificadoras que não são
convencionais. Como exemplo, veja o poema [*Tree*](#ytModal-Lf92PlzMAXo), de Paul Scott. Embora tenha sido composto em BSL
(British Sign Language, a língua de sinais britânica), é facilmente
compreensível por sinalizantes de Libras. A obra mostra novos
classificadores para o gato, o cão e o cego andando com sua bengala.
Existem classificadores convencionais que o autor poderia ter escolhido,
mas ele criou novas configurações de mão divertidas e visualmente
atraentes.

Alguns sinalizantes de Libras adaptaram
[*Tree*](#ytModal-Lf92PlzMAXo) para compor seus próprios poemas
que refletem sua experiência nacional. A adaptação
*[Saci](#ytModal-4UBwn9242gA),* de Fernanda Machado, mostra uma
onça que caminha em direção à árvore, fazendo uso de um classificador
que é parte de todo o animal e sua bocarra aberta. Nem o classificador
manual nem seu movimento estão nos padrões dos classificadores da
Libras. Assista também ao poema-história
[*Árvore*](#vimeoModal-267272296), de André Luiz
Conceição (outra adaptação de [*Tree*](#ytModal-Lf92PlzMAXo)). A
configuração de mão classificadora que representa os pais e a criancinha
caminhando juntos não existe em Libras, mas pode-se compreendê-lo
facilmente, bem como a configuração de mão classificadora que representa
o grupo de crianças que se dirige à casa na árvore no fim da história.

Os classificadores também descrevem a aparência de um personagem.
[*Jaguadarte*](#ytModal-DSSXTh0wriU),
de Aulio Nóbrega, mostra descrições detalhadas da aparência do guerreiro
e do monstro. Como o monstro só existe no mundo ficcional dessa história
(felizmente), não há classificadores prontos para descrevê-lo e por isso
o artista utiliza novas formas de mão para criar imagens visuais com
significados que somente podem ser imaginados.

#### 5.2.8 Elementos não manuais

Embora com frequência se diga que a Libras é uma língua manual e que sua
literatura é uma forma de arte produzida por mãos literárias (MOURÃO,
2016), os elementos não manuais são muito importantes, especialmente
quando se tem o objetivo de acrescentar impacto estético.

O movimento da boca em Libras pode derivar de palavras da língua
portuguesa ou ser motivado por formas visuais que não têm nenhuma
relação com a língua oral. Em geral, os movimentos da boca derivados do
português são associados com o vocabulário (sinais não ilustrativos, ver
capítulo 04) e os padrões bucais visuais se associam a classificadores e
à incorporação (estruturas altamente icônicas, ver capítulo 04). Na
sinalização cotidiana, os dois tipos de movimento de boca ocorrem em
conjunto, em proporções variadas, mas na sinalização estética com
frequência há menos padrões bucais derivados do português, em parte
devido a uma frequência maior de estruturas altamente icônicas. Quanto
mais visual a peça, e quanto menos sinais de vocabulário de Libras
houver, menos padrões de boca derivados do português há. Nesse sentido,
e como exemplo, a obra
*[Jaguadarte](#ytModal-DSSXTh0wriU),*
de Aulio Nóbrega, não tem traços de português na boca. Histórias
infantis educativas traduzidas do português tendem a ter mais
vocabulário sinalizado e mais padrões de boca derivados do português.
Isso não significa que a tradução não é boa, nem que não segue as regras
da Libras. Uma boa tradução pode manter o vocabulário de Libras
(articulada com movimentos de boca em português), misturado com as
estruturas de classificadores e incorporação. Como exemplo, a tradução
de Mariá de Rezende Araújo para o conto de fadas [*A Rainha das
Abelhas*](#ytModal-nXI4aO2_G3E), dos irmãos
Grimm, que contém padrões de boca do português quando usa vocabulário,
ao passo que não tem traços de português na boca nas outras estruturas
altamente icônicas sinalizadas.

Movimentos de cabeça, abertura do olhar e o posicionamento deste são
utilizados em diversas maneiras para engajar o público na performance do
texto estético e são uma parte muito importante da sinalização estética.
A abertura do olhar frequentemente mostra emoção e a direção dele pode
mostrar movimento e espaço. Por ser o exagero um importante elemento de
entretenimento na sinalização estética, os elementos não manuais são com
frequência exagerados. Poética e cheia de humor, a narrativa *[A Pedra
Rolante](#ytModal-kPXWu5UCTzk),* de Sandro Pereira, utiliza
essas características manuais de maneira extensiva. Às vezes os
articuladores não manuais são os principais articuladores, ou até mesmo
os únicos. Por dez segundos[13](#tooltip), a história é sinalizada totalmente com
os olhos, a boca e a cabeça. *[Golf
Ball](#ytModal-Gl3vqLeOyEE),* de Stefan Goldschmidt, também faz
uso extensivo desses elementos não manuais, enquanto ele mostra como a
bola de golfe se sente e se move sem nenhum sinal manual.

#### 5.2.9 Perspectivas múltiplas

Veremos no capítulo 10 que a sinalização estética frequentemente tem
características similares às das técnicas cinematográficas, incluindo o
recurso de mostrar diferentes perspectivas. Há dois tipos: o sinalizante
pode produzir sinais que representam dois pontos de vista sobre o mesmo
personagem, com um *close* ou um plano distante cinematográfico, ou pode
mostrar a perspectiva de dois personagens, com o observador e o
observado por uso de espaço dividido. Os dois tipos também podem ocorrer
na mesma peça. Em [*Tinder*](#vimeoModal-267275098), de
Anna Luiza Maciel, vemos os mesmos eventos pela perspectiva do telefone
móvel e do usuário, com a mulher usando o teclado, deslizando o dedo
para a esquerda e para a direita, para cima e para baixo e finalmente
capturando a tela. Cada ação é mostrada do ponto de vista da mulher e,
consequentemente, da perspectiva do aparelho. A artista utiliza seu
corpo inteiro para se tornar o aparelho de telefone através do recurso
da incorporação, que faz o telefone parecer grande, como em um *close*.
Quando a mulher segura o telefone, este aparenta ser pequeno, como em um
plano distante.

Textos com sinais estéticos também podem combinar sinais para dois
diferentes objetos ou personagens, o que permite a interação entre duas
entidades. Paul Dudis (2004) chama isso de partição do corpo (veja
também BASÍLIO, 2017). Em
[*Tinder*](#vimeoModal-267275098), vemos o dedo da
mulher teclando o número enquanto vemos o celular recebendo a digitação.
Configurações de mãos classificadoras podem ser usadas, também, com a
incorporação de um personagem. Em *[Golf
Ball](#ytModal-Gl3vqLeOyEE),* de Stefan Goldschmit, quando o
taco de golfe bate na bola, a cabeça do sinalizante é a bola de golfe,
mostrada por incorporação, e o taco de golfe é mostrado através de um
classificador. O sinalizante coloca a sua mão, mostrando o taco, ao lado
da sua cabeça, mostrando a bola. Dessa forma, pode misturar os papéis de
narrador e personagem: a bola é o personagem e o fato de o taco de golfe
estar ao lado da bola é contado de alguma forma pelo narrador
(MULROONEY, 2009).

<div class="float"></div>

![objetivo](/images/resumo-parte1.png)

### 5.3 Resumo

Esse capítulo revisou uma ampla gama de modos como os sinalizantes podem
produzir em Libras formas que gerem emoções em seu público. Entender a
importância desses recursos visuais em uma linguagem visual nos permite
compreender como a literatura em Libras é tão diferente da literatura em
português escrito. O português simplesmente não tem esses recursos.

Peter Cook, um poeta famoso dos EUA, falou: “os poetas modernos ficam
fascinados quando veem a poesia em ASL, porque eles se esforçam para
transformar imagens em linguagem, em deixar as palavras virarem imagens.
É isso que fazemos em poesia em ASL” (NATHAN LERNER; FEIGEL, 2009.
Tradução nossa).

<div class="float"></div>

![objetivo](/images/atividade-parte1.png)

### 5.4 Atividade

* Escolha duas obras literárias sugeridas nesse capítulo. Procure estes
exemplos de sinalização estética em cada uma:

* Velocidade

* Espaço e simetria

* Mesmas configurações de mão

* Morfismo

* Mostrando humanos por incorporação

* Mostrando não humanos (animais e objetos inanimados) por incorporação

* Classificadores (e novos classificadores)

* Elementos não manuais

* Perspectivas múltiplas

<span class="tooltip-texto" id="tooltip-13">
Inicia aos 3 minutos.
</span>


