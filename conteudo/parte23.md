---
capitulo: 23
parte: 4
titulo: Tecnologia e textos híbridos em Libras 
pdf: "http://files.literaturaemlibras.com/CP23_Tecnologia_e_textos_hibridos_em_Libras.pdf"
video: c_VBY--HCFI
---

## 23. Tecnologia e textos híbridos em Libras 

<div class="float"></div>

![objetivo](/images/objetivo-parte4.png)


### 23.1. Objetivo

Vimos neste livro que a literatura surda está sempre mudando e se
desenvolvendo por razões sociais e culturais e por avanços tecnológicos
que criam oportunidades para artistas de Libras. Neste capítulo, vamos
investigar mais uma dessas oportunidades: o uso de imagens não verbais
junto à linguagem literária de Libras por meio da edição e de outras
técnicas de vídeo.

Há muitos gêneros em que textos verbais e visuais são combinados, tanto
na língua portuguesa quanto em Libras. Em português, por exemplo, vemos
as combinações nas ilustrações de textos de diversos tipos (das
reportagens de jornal aos contos infantis), nas novelas, nos cartazes e
nos quadrinhos. Na literatura infantil, as imagens são fundamentais para
a compreensão e o prazer dos leitores jovens. O conjunto dos dois é tão
comum que hoje em dia quase não percebemos o efeito da junção dos
sistemas visuais e linguísticos.

A linguagem estética e literária da Libras é caracterizada pelas imagens
fortes que os sinais criam, e esse efeito visual é ainda maior quando
apresentado com imagens não verbais. O conjunto de informações verbais e
não verbais é denominado “expressão intersemiótica”, porque é uma
construção de significado feita com dois sistemas “semióticos”, ou
signos com significado. A tecnologia de edição de vídeo torna cada vez
mais fácil a mistura dos sinais do sistema semiótico linguístico da
Libras com as imagens do sistema semiótico visual de fotos ou filmes.

<div class="float"></div>

![lista de videos](/images/videos-parte4.png)

<div class="lista-de-videos"></div>

Já assistimos à maioria dos exemplos em outros contextos, mas podemos
estudar os vídeos novamente, e temos alguns que ainda não conhecemos
neste livro.

* *[Anjo Caído](#vimeoModal-209842983),* de Fernanda Machado.

* [*Chapeuzinho Vermelho*](#ytModal-JuCVU9rGUa8), de Heloise
Gripp.

* *[O Curupira](#vimeoModal-292526263),* de Fábio de Sá.

* [*O Farol da Barra*](#ytModal-VXcKgO-jD9A), de Maurício Barreto.

* *[Homenagem Santa Maria/RS](#ytModal-9LtOP-LLx0Y),* de Alan
Henry Godinho.

* *[Jaguadarte](#ytModal-DSSXTh0wriU),*
de Aulio Nóbrega.

* [*O Lobinho Bom*](#ytModal-HXD1YszZdp8), de
Carolina Hessel.

* *[O Pássaro (números)](#vimeoModal-348080802),* de
Juliana Lohn.

* [*Slow Motion Portrait*](#ytModal-rWowtWJSd4E) (em português
“Retrato em câmera lenta”), de Tony Bloem.

* *[X-Men Apocalipse Mercúrio](#ytModal-Yh_qXzHYkfs),* de Cézar
Pedroso de Oliveira.

### 23.2. Tecnologia e literatura em Libras

A tecnologia tem um papel muito importante em toda a literatura, seja
escrita, em línguas orais ou sinalizadas como a Libras. Com a invenção
da máquina de impressão, criou-se a possibilidade de se produzir
múltiplas cópias de um livro. Todavia, a tecnologia precisava mais do
que a imprensa para criar uma literatura tão difundida e abrangente como
a da sociedade atual. Junto ao crescimento da alfabetização, surgiram os
avanços da tecnologia na fabricação de papel e tinta, além dos meios de
transporte para os novos livros (BENEDICT, 2001). Tudo isso contribuiu à
forma de literatura escrita que conhecemos hoje no Brasil (e no mundo).

A literatura surda também sofreu mudanças tecnológicas e sociais. Krentz
(2006) propôs uma analogia da câmera com a máquina de impressão para
literatura surda. Talvez seja mais correto dizer que a câmera com seu
filme é a tecnologia análoga ao papel e à caneta[48](#tooltip), porque os dois
permitem o registro permanente das palavras ditas de modo que podemos
reproduzi-las sempre da mesma forma, sem a necessidade da presença do
artista literário. Já vimos, no capítulo 14, que uma performance
literária em ASL foi gravada em filme em 1913.

Porém, fazer múltiplas cópias de um filme não era simples naquela época,
então nos primeiros tempos das filmagens de línguas de sinais, não havia
muita distribuição. Até o final da década de 1970, uma associação de
surdos que quisesse, por exemplo, mostrar os filmes dos poemas em ASL
“Gestures” (em português, “Gestos”), da poeta surda Dorothy Miles, tinha
que encomendar o filme, projetá-lo no salão de encontros e devolver a
cópia para que outra associação pudesse recebê-la. O que mudou, para
criar uma situação parecida com a máquina de impressão, foi o
videocassete, que permitia múltiplas cópias, por um bom preço, e que
podiam ser assistidas por muitas vezes. Krentz (2006) mostrou claramente
que a invenção do vídeo teve um grande impacto na literatura em ASL. Com
a sua ampla disponibilidade, os artistas tornaram-se mais conhecidos, os
seus trabalhos puderam ficar mais fixos em uma forma, e o público das
obras literárias aumentou muito. Heidi Rose (1992) também observou que o
desenvolvimento do vídeo permitiu uma forma de literatura mais complexa,
que exigiu do público assisti-lo múltiplas vezes. Antes disso, numa
apresentação ao vivo, era difícil pedir ao artista para refazer um poema
– estava feito e acabou. Por isso os trabalhos literários eram mais
simples. O controle do vídeo para reproduzir, parar, pausar, avançar e
retroceder rapidamente criou a oportunidade de se fazer pesquisas em
literatura de línguas de sinais, primeiramente em ASL e, no início do
século XXI, em Libras, começando com a fita VHS “Literatura em LSB”, de
Nelson Pimenta, em 1999.

Os DVDs, com seus aparelhos de reprodução, gradualmente substituíram os
videocassetes, e outras obras em Libras foram distribuídas nesse formato
de mídia. Apesar disso, o principal conceito, o de se criar cópias
físicas e distribui-las (por meio de venda ou gratuitamente), continuou.

O uso da internet para arquivar e reproduzir vídeos foi mais uma mudança
enorme para a literatura surda. Inicialmente, os vídeos estavam
disponíveis apenas nos sites especializados, mas rapidamente os surdos
mundialmente entenderam as possibilidades de postarem vídeos literários
no YouTube e, mais recentemente, em redes sociais como Facebook e
Instagram. Pela internet, a literatura em línguas de sinais estourou e
hoje qualquer pessoa pode postar um vídeo para ser compartilhado ou
assistido (SCHALLENBERGER, 2010). Com essa facilidade, tornou-se quase
uma necessidade gravar a literatura em Libras para que os textos e as
performances sejam distribuídos.

#### 23.2.1. Edição de vídeo das imagens do próprio artista

A tecnologia de vídeo mudou não apenas a distribuição da literatura
surda, mas a sua própria forma. Quando a literatura em Libras não era
gravada, a pessoa simplesmente apresentava o trabalho ao vivo. O público
via o artista inteiro, e apenas isso, na obra. Não havia a opção de dar
um zoom para mostrar apenas uma parte do corpo (por exemplo, o rosto ou
as mãos) e tudo era apresentado em velocidade normal. Com certeza, os
artistas imitavam os efeitos tecnológicos dos filmes, recriando os
*zooms* e a câmera lenta do cinema, mas isso era feito com o corpo e
apenas com os sinais.

A edição agora oferece muito mais opções para se criar literatura com
efeitos. Podemos, com isso, alterar as imagens do próprio artista
durante a apresentação do poema. No vídeo
*[Homenagem Santa Maria/RS](#ytModal-9LtOP-LLx0Y),* de Alan
Henry Godinho, a câmera mostra apenas o peito nos sinais <span
style="font-variant:small-caps;">abraço</span> e <span
style="font-variant:small-caps;">apoio</span>, o que exige que o público
foque apenas nessas produções, para enfatizar a importância dos sinais.
Não há como criar o mesmo efeito em uma performance ao vivo e sem edição
de vídeo. [*Slow Motion Portrait*](#ytModal-rWowtWJSd4E) (em português
“Retrato em câmera lenta”), do poeta holandês Tony Bloem, foi gravado em
alta velocidade num iPhone e editado em velocidade normal. O efeito das
imagens físicas do corpo (como o movimento das bochechas e da pele nas
mãos) é impossível de ser criado ao vivo. Outro exemplo de brincadeira
com a edição foi feita no vídeo do poema [*Anjo Caído*](#vimeoModal-209842983), de Fernanda Machado. Nele, vemos as opções de edição feitas pelo
cineasta Martin Haswell. Há quatro performances do mesmo poema,
simultaneamente no vídeo, cada uma sendo um pouquinho diferente da
outra, gravadas de diferentes ângulos e distâncias. A repetição de
imagens parecidas no mesmo poema gera um efeito visual e estético mais
intenso, mas sem causar perda de compreensão do poema. Fernanda Machado
publicou [*um artigo científico em Libras sobre o assunto*](#vimeoModal-305384383) (2019), em vídeo, na revista
on-line [*Altre Modernità*](https://riviste.unimi.it/index.php/AMonline/article/view/12409/11686).
Essa publicação em vídeo é mais um exemplo das possibilidades que a
tecnologia oferece à literatura surda.

Além de alterar as imagens do próprio artista, a edição de vídeo
oportuniza formas de inserção de imagens não verbais na performance
literária, criando efeitos que vão além da Libras, mas sempre
fundamentados na língua. Falaremos disso agora.

### 23.3. Imagens não verbais na literatura em Libras

O papel das imagens na literatura em Libras ainda não foi muito
pesquisado, mas sabemos que a literatura intersemiótica vai além da
língua para criar experiências visuais com o objetivo de gerar novas
sensações agradáveis no público.

#### 23.3.1. Relação entre o texto em Libras e as imagens em vídeo

Leo Hoek (2006) fala dos tipos de relações
entre texto e imagem. Do ponto de vista da produção, ele observou que o
texto pode existir antes da imagem (assim, esta é uma “ilustração” do
texto) ou a imagem antes do texto (e o texto que segue da imagem é
denominado texto “ecfrástico”). Claus Clüver (2006) explica que a
ilustração de um livro pressupõe a existência de um texto verbal. Vemos
exemplos disso em muitos contos infantis em Libras. O contador conta a
história em Libras e a imagem desenhada mostra o que foi dito. Por
exemplo, nos contos de *[Anjo Caído](#vimeoModal-209842983)*, de
Carolina Hessel, depois de contar um trecho da história, ela mostra a
imagem desenhada (veja um exemplo no conto
[*O Lobinho Bom*](#ytModal-HXD1YszZdp8)). Lembramos que isso fala da
*produção* do texto, e talvez na recepção o leitor ou espectador olhe
primeiro as ilustrações para entender as informações visuais e depois
leia o texto. Por outro lado, o tipo de texto denominado “ecfrástico”, é
baseado numa obra visual.
*[Jaguadarte](#ytModal-DSSXTh0wriU),*
de Aulio Nóbrega, foi criado a partir de uma imagem de um monstro. Outro
tipo de Libras “literário-ecfrástico” é o reconto de filmes baseado nas
imagens contidas nele. O trecho de *[X-Men Apocalipse Mercúrio](#ytModal-Yh_qXzHYkfs),* do filme X-Men Apocalipse, em
que Cézar Pedroso Oliveira mostra os sinais das imagens no filme, é um
exemplo em Libras. Quando a interdependência entre texto e imagem é
total, não é que um venha antes do outro, nem que um tenha predominância
sobre o outro. Isolados, nem texto nem imagem tem sentido coerente, mas
juntos eles criam significado, como ocorre, por exemplo, nos gibis.

A partir da perspectiva da recepção, Hoek afirma que o texto pode estar
situado em uma imagem, a imagem situada em um texto, o texto ficar
próximo a uma imagem ou uma imagem ficar próxima a um texto. A relação
entre os textos e as imagens não é sempre igual. Nos contextos descritos
acima, em que o texto gera uma imagem (como ilustração) ou a imagem cria
o texto (na ecfrásis), a imagem e o texto são facilmente separados. Os
dois nem precisam ser apresentados no mesmo momento, e na separação cada
um mantém seu sentido (esse tipo e relação é chamado **transposição**).
Em outros contextos, vemos a imagem e o texto juntos ao mesmo tempo e
cada um faz referência ao outro, mas podem existir separadamente e ainda
fazerem sentido (chamado **justaposição**). Na **combinação**, os
sistemas verbais e visuais juntos formam uma única obra e os dois são
necessários para criar o sentido. É possível separar os dois
fisicamente, mas depois, independentes, perdem o sentido. Finalmente, na
**fusão**, não é possível separar o verbal do visual, nem fisicamente,
nem de forma a manter o sentido.

Depois de contar uma história em Libras (por
exemplo, *[O Curupira](#vimeoModal-292526263),* de
Fábio de Sá, ou [*O Pássaro*](#vimeoModal-348080802) de
números, por Juliana Lohn), a professora pode pedir aos alunos para
fazerem um desenho baseado na história. Isso é um exemplo de
**transposição**, em que um texto em Libras se transforma em uma imagem.
A professora também pode mostrar uma imagem aos alunos e pedir a eles
para criarem uma história baseada no desenho. Isso também é
transposição.

A narrativa [*Chapeuzinho Vermelho*](#ytModal-JuCVU9rGUa8), contada por
Heloise Gripp, é um exemplo de **justaposição**, em que ela sinaliza a
história e no fundo há diversas ilustrações do cenário inseridas pela
edição de vídeo. Quando a contadora fala do passeio de Chapeuzinho
Vermelho na floresta, vemos a imagem de uma floresta ao fundo, atrás de
Heloise. A ilustração contextualiza esse trecho da narrativa na hora da
contação, mas não precisamos das imagens para entender os sinais da
história nem para compreender que a imagem é de uma floresta.

O poema de Maurício
Barreto, *[O Farol da Barra](#ytModal-VXcKgO-jD9A),* mostra um
bom exemplo de **combinação** entre Libras linguística e imagens não
verbais. Nele, o poeta apresenta um poema sinalizado e na edição do
vídeo as imagens são apresentadas atrás dele. O poema usa muitos
classificadores e cada um apresenta um significado incerto, mas com as
imagens, o sentido se torna muito claro. É possível separar os sinais
das imagens (porque o poema foi criado sem elas e existem gravações dele
sem as imagens ao fundo), mas assim será mais difícil entender o seu
significado. Por outro lado, as imagens podem ser separadas dos sinais,
mas assim perdemos as informações de movimento que os sinais mostram.

Nos primeiros 40 segundos do poema, o poeta apresenta os sinais <span
style="font-variant:small-caps;">luz-brilhando, mar, ondas-do-mar,
cl-pedra</span>[49](#tooltip)<span style="font-variant:small-caps;">,
cl-ondas-batem-pedra, cl-lua</span> e **farol**, e cada um é acompanhado
por uma imagem do respectivo referente ao fundo. Cada classificador pode
representar qualquer coisa dessa forma ou daquele tamanho. Com a poesia,
muitas vezes deixamos algumas informações “no ar” para que o espectador
possa entrar na brincadeira e procurar o sentido dos sinais. Mas, nesse
caso, o poeta cria uma oportunidade para o espectador ver a relação
entre os classificadores que têm sentidos ambíguos ou não exatos (por
exemplo, um classificador representa qualquer coisa esférica, e não
sabemos do que o poeta fala sem um sinal para clarificar). Há diversas
opções para um poema tradicional apresentado ao vivo: o poeta pode
oferecer o sinal do vocabulário para esclarecer; o contexto pode
fornecer as informações de que precisamos para entender; ou podemos
ficar sem saber com certeza. Mas, num vídeo, temos mais opções,
inclusive a de mostrar as imagens dos objetos de que o poeta fala. O
classificador junto à imagem de uma rocha no mar esclarece que se trata
da fala de uma rocha; e quando vemos uma imagem da lua cheia, entendemos
que o classificador agora se refere à lua cheia.

Outro exemplo de **combinação** é visto no
vídeo [*X-Men Apocalipse - Mercúrio*](#ytModal-Yh_qXzHYkfs), de
Cezar Pedrosa de Oliveira, do qual já falamos. No vídeo *[O Farol da
Barra](#ytModal-VXcKgO-jD9A),* de Maurício Barreto, o poema em
Libras é o principal, vindo primeiro; as imagens ilustram e clarificam
os sinais do poeta, e elas estão paradas e atrás dele. No vídeo [*X-Men
Apocalipse- Mercúrio*](#ytModal-Yh_qXzHYkfs), o filme vem
primeiro e os sinais destacam elementos das imagens. A tela é dividida
em duas: na parte superior é apresentado o filme e na parte de baixo a
transposição do mesmo para a Libras. É possível separar o texto da
imagem (lembramos que o filme foi criado sem se pensar em uma tradução
para Libras), e entendemos o filme sem os sinais, mas será difícil
manter a coerência da sinalização sem as imagens do filme.

Lembramos, da seção acima, que o vídeo permite ver apenas partes do
artista. No vídeo de Cézar Pedrosa de Oliveira, isso também é usado para
focar nas mãos quando não falamos de Mercúrio. Barros e Vieira (2020)
observam que o resto corpo do poeta é mostrado apenas quando se mostra
Mercúrio (ou, pelo menos, a parte do corpo do artista que sinaliza – não
vemos o corpo abaixo da cintura, que não participa da produção em
Libras).

Embora não vejamos um exemplo de **fusão** nos vídeos, Barros e Vieira
(2020) falam de um exemplo de fusão por meio de outra tecnologia – o
SignWriting. No poema visual *Comunidade*, de Kácio Evangelista (que já
vimos no capítulo 3) os sinais e a imagem são inseparáveis, porque os
sinais criam a imagem – esta é feita pelos sinais. O círculo dos sinais
escritos <span style="font-variant:small-caps;">surdo</span> com os
semicírculos pequenos dos sinais escritos <span
style="font-variant:small-caps;">sinalizar</span> e o semicírculo maior
dos sinais <span style="font-variant:small-caps;">surdo</span> e <span
style="font-variant:small-caps;">ouvinte</span> criam a imagem de um
rosto sorridente, significando a felicidade de uma comunidade surda em
que surdos e ouvintes usam Libras. Não podemos tirar nem os sinais nem a
disposição deles da página e continuar mantendo o sentido.

#### 23.3.2. Imagens, ilustração e narrativas infantis

Vamos terminar com uma mirada específica para o caso especial das
narrativas infantis em Libras e suas ilustrações. Já sabemos que estas
são parte fundamental da literatura infantil. Faria (2016) nos lembra de
outra relação entre imagem e texto que é circular. O leitor de livros de
imagem apresenta as imagens das ilustrações à criança, depois conta a
parte da história relevante e em seguida mostra as imagens novamente.
Assim, as imagens e o texto constroem juntos o sentido. As crianças
surdas também gostam de ver os livros ilustrados, mas é importante
incluir a Libras, não somente as imagens. Nos vídeos de contos em Libras
destinados às crianças, a edição permite a inclusão de imagens para
tornar as histórias mais interessantes e ainda mais visuais (veja, por
exemplo, [*Coleção de narrativas didáticas curtas*](https://vimeo.com/showcase/6241328), de Marina Teles, e 
[*O Pássaro (números)*](#vimeoModal-348080802) e
[*Animais (números)*](#vimeoModal-348189766) de
Juliana Lohn).

Albres (2014) observa que a tradução de literatura infantil de português
escrito para a Libras é fortemente influenciada pelas ilustrações do
livro impresso. Nesse sentido, podemos tratar a tradução como um
processo em que o texto de Libras é parcialmente motivado pelas
ilustrações, como um trabalho ecfrástico. Na pesquisa de Albres, Costa e
Adams (2018), os tradutores-pesquisadores destacam que às vezes é
necessário traduzir para Libras as informações do livro que não estão no
texto em português, mas sim nas ilustrações. O novo leitor, ao assistir
ao vídeo da tradução “Vira-lata”, de Michael King (2004), não precisa
separar o texto das imagens na sua construção mental da história. Também
na contação da história em Libras, tudo que é visual ajuda a criar a
história. A menina pega o vira-lata no colo e ele a lambe, enquanto ela
pergunta ao cachorro “O que é que eu vou fazer com você?”. Isso é claro
na ilustração, apesar de não ser mencionado no texto em português. Em
Libras, o tradutor decidiu sinalizar o vira-lata lambendo, mesmo que
isso não faça parte do texto e sim da imagem. Com isso, vemos a relação
íntima entre o texto em português, as imagens e o texto em Libras. Não
podemos facilmente separar os três da tradução e da experiência do
leitor surdo.

### 23.4. Resumo

As novas tecnologias de vídeo e de SignWriting apresentam oportunidades
significativas para artistas de literatura em Libras. Vimos que a edição
do vídeo permite novas perspectivas do artista e do uso de seu corpo.
Além disso, a edição possibilita a inserção de imagens no texto
literário. A relação entre o texto em Libras e as imagens é variável e
depende da separabilidade dos dois, mas, em todos os casos, cria opções
para artistas e seus públicos, especialmente poetas e contadores de
histórias infantis.

### 23.5. Atividade

Escolha três vídeos dentre os que já conhecemos neste livro em que a sua
edição cria um efeito que vai além da apresentação do poema ou da
narrativa apresentada ao vivo.

1. O que foi adicionado pela edição?

2. Qual o efeito dessa edição?

3. Você acha que a relação entre os sinais e as imagens é de
Transposição, Justaposição, Combinação ou Fusão?




<span class="tooltip-texto" id="tooltip-48">
Que é também uma tecnologia maravilhosa, apesar de quase não
    reconhecermos mais isso hoje em dia por ser tão comum.
</span>

<span class="tooltip-texto" id="tooltip-49">
CL- mostra que o sinal tem uma configuração de mão
    classificadora, representando a forma do objeto. Por exemplo: o
    sinal <span style="font-variant:small-caps;">cl-pedra</span> não é o
    sinal <span style="font-variant:small-caps;">pedra,</span> mas sim
    um sinal mostrando o tamanho, lugar e a forma da pedra.
</span>
