---
capitulo: 1
parte: 1
titulo: Literatura em libras no contexto brasileiro
pdf: "http://files.literaturaemlibras.com/CP01_Literatura_em_Libras_no_contexto_brasileiro.pdf"
video: xIZ3Xsmv1ew
---


## 1. Literatura em Libras no contexto brasileiro

Literatura em Libras? Como assim? Será que isso existe?

Existe, sim. Neste livro, vamos conhecer a literatura em Libras, a
literatura em língua de sinais da comunidade surda brasileira. Vamos
conhecer uma seleção de obras de literatura em Libras e pensar sobre
algumas maneiras de analisá-las.

A literatura em Libras é feita principalmente para divertir. A maioria
das pessoas que assistem às performances não têm interesse em fazer uma
análise da obra, basta somente que sintam prazer em ver a literatura,
para dar um *like* ou aplaudi-la. Mas, para estudar essa literatura,
precisamos refletir sobre os textos literários e fazer uma análise que
ajude a perceber por que uma obra literária é boa e entender como ela
foi feita.

### 1.1. Sabemos o que é literatura?

Você já sabe o que é literatura. Com certeza, seja a Libras ou o
português a sua primeira língua, você já estudou poesia ou narrativas na
língua portuguesa da literatura brasileira na escola. Seus professores
provavelmente mostraram para você alguns exemplos de contos, poemas,
crônicas ou romances de autores brasileiros julgados como o melhor da
literatura brasileira, que apresentaram a riqueza da língua, a
complexidade e diversidade da vida e da cultura do Brasil. Talvez você
tenha se perguntado: mas quem foi que fez esse julgamento?

Muitas pessoas pensam que a literatura é apenas um tipo de criação
linguística culta e de alta cultura, frequentemente inacessível às
pessoas comuns ou simples. Achamos que a literatura brasileira é feita
de obras de autores clássicos (e mortos) como Casimiro de Abreu, José de
Alencar, Vinícius de Morais, Castro Alves, Jorge Amado, Machado de
Assis, Clarice Lispector e Rachel de Queiroz. E essas obras fazem parte
dela, sim, porém defendemos que a literatura não é apenas culta para
estudo, mas também popular e informal para entretenimento. Os livros de
Paulo Coelho, livros de contos de fadas ou outras narrativas de
literatura infantil (como por exemplo o *Sítio do Pica-Pau Amarelo*)*,*
os poemas de Cordel contemporâneos (como os apresentados por Bráulio
Bessa na televisão no programa da Fátima Bernardes), as letras das
canções de samba, rap e funk, os quadrinhos da Turma da Mônica, até as
discussões sobre novelas da TV nas redes sociais, são todos formas de
literatura brasileira. Talvez você tenha achado estranho pensar nesses
exemplos como “literatura”, mas cada gênero[1](#tooltip)
foi julgado por determinados grupos de brasileiros como uma forma válida
de se usar a linguagem criativa para gerar emoções no público.

Tratamos a literatura como um artefato ou
evento linguístico não cotidiano,[2](#tooltip) que vai além de simplesmente
comunicar. O importante na literatura é o uso da linguagem com o
principal objetivo de proporcionar prazer, de maneira que a sua
estrutura se destaque. Seguindo Sutton-Spence e Kaneko (2016, p. 24),
dizemos que “a literatura é qualquer corpo de produções baseado na
linguagem que é considerado socialmente, historicamente, religiosamente,
culturalmente ou linguisticamente importante para a comunidade”
(tradução nossa).

Muitas pessoas entendem literatura como uma arte estética da linguagem
escrita que se centra no texto escrito, com foco nas atividades de ler e
escrever. Essa definição, todavia, é muito limitada e exclui já muitos
exemplos de uso da língua estética, mesmo em português, porque estão na
forma falada. A literatura dos surdos criada em Libras é raramente
escrita, mas é uma forma de literatura. Defendemos, nos próximos
capítulos que, se a literatura em Libras não se encaixa numa definição
de “literatura” preexistente, esta deveria mudar para que possa incluir
as produções em língua de sinais.

A definição de literatura não é fixa. O poeta Romano Ovídio disse que o
objetivo da literatura era “ensinar por encantar” (*docere delectando*
em Latim). A partir do século XVI, surgiu a noção de que, entre todos os
gêneros de texto, a literatura é que tem o prazer como o principal
objetivo (embora o ensino possa ser também outro objetivo com seu uso).
Hoje, as noções de literatura e prazer são intimamente ligadas, mas há
uma certa ironia quando a maneira de estudar a literatura se desvincula
do prazer.

Um conceito fundamental para a literatura é o de “estética”. Isso quer
dizer, o foco na qualidade que percebemos, especialmente, a beleza.
Notamos a linguagem estética quando reconhecemos alguma coisa como bela
ou prazerosa na forma das palavras ou no jeito como é apresentada. Por
isso, uma característica da literatura é a percepção da linguagem estética – bela, aprazível, agradável e
divertida. Geralmente, a literatura se centra na língua estética que tem
características fora do comum, trata com a perspectiva não cotidiana,
apresenta-se de uma forma diferente da vida no seu dia a dia. Na Europa,
nos séculos XVII e XVIII, a literatura valorizada usava a linguagem
antiga, complexa e erudita. Um texto prazeroso escrito na língua do
cotidiano não era considerado literatura. Hoje, a forma da língua chama
a atenção para a literatura (e vice-versa), mas não é preciso saber um
vocabulário e uma gramática especial para acompanhá-la.

Vemos que outras ideias sobre o que “a literatura deve ser” mudaram. A
partir do século XIX, surgiu a noção de que a literatura deveria
apresentar novas perspectivas sobre os assuntos tratados, questionando o
que é que achamos que já sabemos. A originalidade e a perspectiva
individual começaram a ser valorizadas. Por muitos anos, a metáfora e as
maneiras indiretas de expressar uma ideia foram estimadas, mas no século
XX surgiu o objetivo de expressar pensamentos do modo mais direto
possível. Também, no mesmo século, surgiu a ideia de que a literatura
deve desconstruir a estrutura da língua para mostrar a inconfiabilidade
das palavras e dos sentidos. Desde o Modernismo, alguns poetas inovaram
muito o modo de fazer poesia, associando elementos fortemente visuais,
utilizando recursos das linguagens dos meios de comunicação de massa e
buscando abolir a utilização do verso tradicional, por exemplo nos
poemas concretos.

Esses são apenas alguns exemplos para mostrar como o conceito de
literatura variou e mudou durante a história[3](#tooltip), portanto não podemos
dizer que a literatura “é assim” ou “deve ser assim”. Mas todas as
ideias sobre a sua natureza durante a história têm elementos que podemos
usar para entender a literatura em Libras hoje.

Principalmente, defendemos que a literatura nos permite brincar com a
língua para gerar prazer, tanto em português quanto em língua brasileira
de sinais.

### 1.2. O que é literatura em Libras? 

O termo “literatura em Libras” pode se referir
a poemas, contos, piadas, jogos e outras formas de arte criativas feitas
em Libras que são culturalmente valorizadas. A literatura produzida em
Libras é uma forma linguística de celebrar a vida surda e a língua de
sinais. Embora tenha as suas origens na língua de sinais cotidiana, essa
língua mudou e se destaca por ser “diferente”. Conforme afirma a
pesquisadora norte-americana Heidi Rose (2006), a literatura em qualquer
língua de sinais mescla a língua, as imagens visuais e a dança, sendo
uma mistura de sinais e gestos, uma literatura do corpo e uma literatura
de performance.

A literatura em Libras é um **artefato**[4](#tooltip)
importante da cultura surda e também é um **processo** (MOURÃO, 2011),
visto que as pessoas surdas participam da literatura e assim ela está
constantemente mudando. A forma dessa arte é uma troca social na qual os
artistas e seu público a constroem juntos.

A literatura surda original em Libras, ou seja, a que não foi traduzida
da literatura das línguas orais para língua de sinais, é especialmente
valorizada na comunidade surda, porque ela mostra as experiências das
vidas dos surdos. Algumas dessas experiências vivenciadas são iguais às
das pessoas ouvintes, mas outras são particulares de pessoas surdas
(como a resistência à opressão pela sociedade dos ouvintes, os problemas
de educação dos surdos, as alegrias de conhecer a Libras, a experiência
visual do mundo dos surdos e os sucessos da comunidade surda). Seja qual
for o assunto, a literatura mostra a
perspectiva visual de uma pessoa surda através da língua de sinais.

A literatura em Libras é uma oportunidade de brincar com a língua.
Libras não é uma mera “linguagem” que permite que os surdos tenham
acesso à sociedade dos ouvintes e à língua portuguesa. Ela é uma língua
completa e deve ser usada para todas as funções de uma língua, inclusive
a lúdica[5](#tooltip). Assim como ocorre com a literatura brasileira escrita em
português, a literatura em Libras se concentra na forma estética da Libras, que
tem características fora do comum, trata do conteúdo com perspectiva não
cotidiana e se apresenta de uma maneira que seria diferente da vida
comum. Em resumo, a literatura em Libras é bonita, espirituosa,
brincalhona e frequentemente muito agradável.

#### 1.2.1. A Relação entre a Literatura em Libras e a Literatura Brasileira 

Todos os brasileiros, sejam eles ouvintes ou surdos, podem ter visto
alguns exemplos de literatura brasileira em língua portuguesa. Mas
sabemos que a literatura brasileira não é feita apenas em português e
deve incluir também a literatura do povo brasileiro surdo, que é feita
em Libras.

O povo surdo brasileiro cria a literatura dentro de seu contexto
nacional. Embora os surdos componham literatura em Libras, são todos
bilíngues que sabem a língua portuguesa (ainda que esta seja uma segunda
língua) e participam da vida cultural dos brasileiros. A experiência dos
surdos brasileiros faz parte da vida brasileira: a comida, as roupas e
as tradições culturais (como as festas e as crenças folclóricas); a
natureza, a geografia e a história do país; a vida política, social,
econômica e técnica, tudo isso faz parte da literatura em Libras. Por
isso, ainda que se trate de uma literatura em língua de sinais feita por
pessoas surdas, a literatura em Libras faz parte da literatura
brasileira. Além da importância da cultura do país, também tem
influência da literatura em língua portuguesa nos assuntos abordados, na
estrutura e na sua forma de apresentação.

Por outro lado, a literatura em Libras é principalmente a literatura de
uma comunidade surda, do “povo do olho” (em inglês "People of the
Eye"[6](#tooltip)). Por isso tem características compartilhadas com outras
comunidades surdas mundiais. Por exemplo, a experiência de se perceber o
mundo principalmente através da visão - e não através dos sons - gera
literaturas surdas com foco principal nas imagens visuais em qualquer
país. A experiência compartilhada pelos surdos como um grupo minoritário
no contexto da sociedade dos ouvintes cria muitos tópicos parecidos nas
literaturas mundiais dos surdos.

Em todas as literaturas de línguas de sinais já pesquisadas vemos a
importância da criação de imagens visuais por meio de sinais. A
gramática das línguas de sinais, sendo baseada no raciocínio visual,
gera línguas de sinais com elementos linguísticos muito parecidos e
assim podemos ver que a língua estética nas literaturas em outras
línguas de sinais como ASL,[7](#tooltip) DGS[8](#tooltip) ou BSL[9](#tooltip) tem muito em comum
com a literatura em Libras, mesmo que as culturas nacionais sejam
diferentes.

Devido a essa similaridade, os artistas surdos também podem influenciar
a literatura em outros países. Veremos, por exemplo, que alguns poetas
de ASL nos Estados Unidos influenciaram outros artistas surdos no Brasil
e que os artistas brasileiros já compartilharam ideias que influenciaram
poetas como os chilenos e irlandeses. Graças à tecnologia de vídeo e da
internet, os artistas hoje têm muitos recursos para estudar a literatura
surda internacional.

Assim, entendemos que a literatura em Libras faz parte da literatura
surda mundial e da literatura brasileira.

### 1.3. Literatura e Letras Libras

Apesar de os surdos no mundo inteiro terem muitas características
semelhantes, a comunidade surda brasileira é diferente das comunidades
de outros países pela existência da *Lei de Libras.* As experiências
políticas e educacionais – e até literárias – acontecem todas em
respeito à Lei 10436/2002 e ao Decreto 5626/2005, que estabelece o
direito dos surdos de ter acesso a informações em Libras. Com isso,
estamos vivendo um momento importantíssimo do país, com professores
surdos em muitas universidades – federais, estaduais e privadas– nos
cursos de Letras Libras e/ou ensinando Libras, pesquisando sobre a
língua e a comunidade surda e divulgando os conhecimentos através de
publicações e ações de extensão. Hoje, em universidades de todo o
Brasil, temos disciplinas que estudam a literatura surda.

A palavra "literatura" é derivada da mesma palavra latina que nos traz o
vocábulo "letra" e tem sido associada com “escrita”. Antigamente,
relacionava-se à ideia da educação obtida por meio de leitura,
referindo-se às habilidades técnicas de leitura e de escrita. Nos
estudos de Letras, como Letras Português, Letras Inglês ou Letras
Espanhol, esperamos estudar as línguas e suas respectivas literaturas.
Nos cursos de Letras Libras também é importante estudarmos a língua,
Libras, e a sua literatura. Temos cada vez mais publicações sobre a
língua brasileira de sinais que impulsionam os seus estudos. E os
currículos dos cursos incluem diversas disciplinas sobre a estrutura, o
uso e o ensino da língua. Desde o início dos cursos de Letras Libras, em
2006, sempre foi incluída pelo menos uma disciplina sobre literatura
surda ao currículo (cabe aqui nossa homenagem à professora Lodenir
Karnopp da Universidade Federal do Rio Grande do Sul – UFRGS – pelo
trabalho pioneiro nessa área).

Infelizmente, raramente pessoas surdas (e ouvintes também) estudam a
literatura em Libras na escola. Por isso, há poucos artistas literários
de Libras formados na área e seu público não tem muita experiência ou
conhecimento sobre o assunto. No entanto, os artistas da comunidade
surda e o seu público criam e se divertem com a literatura surda.
Estudar esse conteúdo nas disciplinas de cursos de Letras pode servir
para ajudar a aumentar o número de artistas e para que estes aprimorem
suas criações, além de criar e incentivar um público surdo com
conhecimentos para entender e estimar a literatura surda. Podemos
divulgar informações sobre essa forma de arte para os futuros
professores de Libras, para professores de alunos surdos e para
intérpretes de Libras/português, a fim de que a literatura cresça cada
vez mais. Através do estudo da literatura em Libras se pode entender
progressivamente a cultura e a identidade surdas, a essência do ser
surdo e, assim, melhor a Libras. E, esperamos, ter um enorme prazer em
conhecer essa literatura.


<div class="float"></div>

![objetivo](/images/resumo-parte1.png)

### 1.4. Resumo

Nessa introdução à literatura em Libras vimos que a literatura feita em
língua de sinais pela comunidade surda brasileira tem muitas
características semelhantes à literatura escrita em português e a
literaturas em outras línguas de sinais. Todas as literaturas se centram
na linguagem estética, têm características fora do comum, tratam de
conteúdo de perspectiva não cotidiana e se apresentam de uma forma
diferente da vida no seu dia a dia. A literatura em Libras (ou em outras
línguas de sinais) e a literatura em português nos permitem brincar com
as línguas e nos mostram a riqueza dos idiomas.

<div class="float"></div>

![objetivo](/images/atividade-parte1.png)

### 1.5. Atividade

<div class="identado"></div>

1. Vamos refletir um pouco sobre a sua experiência com a literatura
brasileira.

    * Você pode pensar em exemplos de literatura em português que você conhece
ou gosta (poemas, narrativas, contos, piadas, peças de teatro, tanto de
literatura “culta” quanto “popular”)?

    * Por que você gosta dessas obras?

    * Você já leu ou viu literatura em outros idiomas? Tentou assistir a um
filme em inglês ou espanhol (com ou sem legendas em português)? Leu
algum livro escrito em outro idioma traduzido para o português (por
exemplo: *Harry Potter* ou *O Código da Vinci*)?

    * Você já participou de um evento de literatura em português?

    * Você gosta de criar textos literários em português? Você já escreveu
poemas (embora nunca tenha mostrado eles para ninguém)? Escreveu algum
tipo de narrativa curta ou contou histórias da sua juventude para os
seus sobrinhos?

##### Agora:

Você consegue pensar em exemplos de literatura (poemas, narrativas,
contos, piadas ou peças de teatro) em Libras que você conhece ou gosta
(de literatura “culta” ou “popular”)?

* Por que você gosta dessas obras?

* Você já viu literatura em outras línguas de sinais?

* Você já participou de algum evento de literatura em Libras como, por
exemplo, um festival?

* Você gosta de criar textos literários em Libras? Você já criou um poema,
traduziu uma música ou uma piada de português para Libras ou contou
histórias em Libras?

* Talvez você tenha respondido às questões na atividade acima sobre o
conhecimento de literatura em Libras afirmativamente porque já conhece a
literatura surda. Mas, se você ainda não tem experiência com a
literatura surda, vamos conhecer muitos exemplos dela nos próximos
capítulos. Começaremos aqui, na próxima atividade.

<div class="identado"></div>

2. Dedique algum tempo para assistir aos vídeos a seguir. Vale a pena
investir seu tempo. Você vai levar 17 minutos para assistir a todos. No
entanto, pedimos que você assista a cada vídeo no mínimo duas vezes,
porque eles mostram diversos exemplos de literatura em Libras. Na
primeira vez, você pode assistir ao vídeo apenas para conhecer (e se
divertir) e depois pode ver de novo para apreciar mais profundamente.


<div class="float"></div>

![lista de videos](/images/videos-parte1.png)

<div class="lista-de-videos"></div>

* Poesia estética – [*Como veio alimentação*](#ytModal-nMOTYprbYoY), de Fernanda Machado (duração 00:37).
* Poesia narrativa – [*As Brasileiras*](#vimeoModal-242326425), de Klícia de Araújo Campos e Anna Luiza Maciel (duração 02:42).
* Narrativa original - *[Leoa Guerreira](#ytModal-rfnKoCXmSg4),* de Vanessa Vidal (duração 03:33).
* Poesia Cinemática ou Vernacular Visual - [*Unexpected moment*](#ytModal-hD48RQLQurg) (“Momento inesperado”), de Amina Ouahid e Jamila Ouahid (duração 01:31). (Obs: Essa poesia foi feita por pessoas suecas, mas devido a sua forma visual não é preciso dominar língua sueca de sinais para entendê-la).
* Narrativa de lenda tradicional – [*O Negrinho do Pastoreio*](#vimeoModal-306082977), de Roger Prestes (duração 03:11).
* Poesia traduzida – [*As Borboletas*](#ytModal-mi8bnByv4S4),  de Vinicius de Moraes, traduzido por Wilson Santos (duração 01:15).
* O [*Hino Nacional Brasileiro*](#ytModal-STrLJipI18Q), adaptado por Bruno Ramos (duração 04:15).

<div class="identado"></div>

3. Nesses exemplos de literatura sinalizada, vimos a Libras estética não
cotidiana.

<div class="identado"></div>

-   O que você achou da forma da língua utilizada? É normal e igual à
 língua cotidiana?

-   O que você achou dos tópicos apresentados? Falam de assuntos que
encontramos no dia a dia?

-   O que você achou do estilo da apresentação? Uma pessoa normalmente


<span class="tooltip-texto" id="tooltip-1">
Uma classificação de um conjunto de textos com características
    semelhantes. Falaremos mais sobre gêneros de literatura nos
    capítulos 07, 08 e 14.
</span>

<span class="tooltip-texto" id="tooltip-2">
Que não acontece todos os dias, ou seja, extraordinário.
</span>

<span class="tooltip-texto" id="tooltip-3">
Esse resumo baseia-se nas ideias de Peter Barry, 2017.
</span>

<span class="tooltip-texto" id="tooltip-4">
Um objeto feito pelo ser humano.
</span>

<span class="tooltip-texto" id="tooltip-5">
Divertida ou como uma brincadeira.
</span>

<span class="tooltip-texto" id="tooltip-6">
Atribuído ao surdo George Veditz, 1912. Veja, por exemplo, Lane,
    Pillard e Hedberg (2010).
</span>

<span class="tooltip-texto" id="tooltip-7">
American Sign Language - Língua de Sinais Americana.
</span>

<span class="tooltip-texto" id="tooltip-8">
Deutsche Gebärdensprache – Língua de Sinais Alemã.
</span>

<span class="tooltip-texto" id="tooltip-9">
British Sign Language - Língua de Sinais Britânica.
</span>
