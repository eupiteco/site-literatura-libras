---
capitulo: 13
parte: 3
titulo: Poema ou prosa?
pdf: "http://files.literaturaemlibras.com/CP13_Poema_ou_prosa.pdf"
video: 0rvwjqFIsdg
---

## 13. Poema ou prosa?


<div class="float"></div>

![lista de videos](/images/videos-parte3.png)

<div class="lista-de-videos"></div>

Neste capítulo, vamos falar dos seguintes textos em Libras:

* *[Anjo Caído](#vimeoModal-209842983),* de Fernanda Machado.

* *[Árvore](#vimeoModal-267272296),* de André Luiz
Conceição.

* *[Associação](#ytModal-yd1VVhdsT3Q),*
de Fernanda Machado.

* *[Ave 1 x 0 Minhoca](#vimeoModal-267277312),* de Marcos
Marquioto.

* *[A Pedra Rolante](#ytModal-kPXWu5UCTzk),* de Sandro Pereira.

* *[Como Veio Alimentação](#ytModal-nMOTYprbYoY),* de Fernanda
Machado.

* *[A Economia](#vimeoModal-267272909),* de Sara Theisen Amorim e Ângela
Eiko Okumura.

* *[Eu x Rato](#ytModal-UmsAxQB5NQA),* de Rodrigo Custódio da Silva.

* *[Galinha
Fantasma](#ytModal-isyT8-mgCDM),*
de Bruno Ramos.

* *[Homenagem Santa Maria](#ytModal-9LtOP-LLx0Y)/RS,* de Alan
Henry Godinho.

* *[Meu Ser é Nordestino](#ytModal-t4SLooMDTiw),*
de Klícia Campos.

* *[Paraná](#vimeoModal-267276733),* de Marcos Marquioto.

* *[Peixe](#ytModal-LEDC479z_vo),* de Renato Nunes.

* *[Saci](#ytModal-4UBwn9242gA),* de Fernanda Machado.

* [*O Símbolo do Olodum*](#ytModal-_jhW0K4NN0A), de Priscila Leonnor.

* *[Slow Motion Portrait](#ytModal-rWowtWJSd4E),* (em português
“Retrato em câmera lenta”), de Tony Bloem.

* *[Tree](#ytModal-Lf92PlzMAXo),* de Paul Scott.

* *[Voo Sobre Rio](#ytModal-YaAy0cbjU8o),* de Fernanda Machado.

<div class="float"></div>

![objetivo](/images/objetivo-parte3.png)

### 13.1. Objetivo

Vamos falar sobre poemas em Libras para que possamos entender ainda mais
sobre esse gênero. Através da análise de alguns poemas em Libras
aprendemos mais sobre a forma da poesia na língua e, assim, podemos
compreender melhor a linguagem estética da língua de sinais. Mas, antes
de analisar um poema, talvez seja útil saber se um texto é um poema ou
não. Esse é o objetivo deste capítulo[33](#tooltip).

Uma definição simples do que é um poema em língua de sinais é: “a forma
mais elevada de linguagem estética” em que a linguagem é tão importante
quanto (ou ainda mais importante que) a mensagem. A linguagem poética em
geral se desvia da cotidiana para que a própria língua se destaque no
primeiro plano, aumentando seu poder comunicativo além do simples
significado proposicional (LEECH, 1969). Infelizmente, essa definição
nem sempre serve para identificar poemas, já que ela também serve para a
literatura como um todo.

### 13.2. As dificuldades de separar poema e prosa

Uma das principais divisões entre os gêneros literários é aquela entre a
poesia e a prosa. Mas devemos questionar se existe realmente uma
diferença entre elas. Uma definição de prosa a identifica como a forma
de uso natural da língua, com uma linguagem que não usa ritmo especial e
que tem elementos lexicais e gramaticais cotidianos e comuns. Dito
assim, parece que a prosa é qualquer tipo de linguagem que não é um
poema. Mas, para essa definição ser útil, precisamos saber o que é um
poema.

Cada sociedade tem sua própria ideia do que significa um poema e essa
concepção pode variar. Podemos definir a prosa como o “não poema” e a
poesia como a “não prosa” e, assim, entramos num círculo vicioso de
definição. Ultimamente, não podemos separar os dois porque a distinção é
artificial, mas vale a pena considerar a questão porque, ao definirmos
seus conceitos, começamos a ver com mais clareza os elementos da poesia
e da prosa em Libras.

Lembramos que o conceito de gênero é social, baseado nas convenções de
uma comunidade. Os conceitos dos gêneros poesia e prosa não devem ser
iguais nas línguas orais e escritas (como português ou inglês) e nas
línguas de sinais (como Libras ou ASL). Além disso, um poema em ASL
também não deve ser igual a um poema em Libras. No entanto, podemos usar
ideias já desenvolvidas na literatura escrita e na análise de poemas em
ASL para investigar o conceito de poesia em Libras.

As comunidades surdas têm uma longa tradição de narrativa em prosa, mas
os textos que hoje chamamos de poemas só passaram a existir nos anos 70
ou 80 do século XX nos EUA (ROSE, 1992). A tradição de poesia em Libras
é nova e provavelmente começou apenas no final dos anos 90. Porém, não
importa que essa tradição seja recente no Brasil. A poesia em Libras
está se espalhando e se desenvolvendo cada vez mais para se tornar um
gênero literário importante no mundo.

Na falta de uma definição, talvez possamos oferecer exemplos de poemas
até que se revele o que eles sejam de fato. Em um argumento um tanto
circular, podemos até afirmar que um poema é o que está incluído numa
antologia de poemas. Por exemplo, na Inglaterra, os livros *Golden
Treasury* de Palgrave (publicado pela primeira vez em 1861) e *The
Oxford Book of English Verse* (primeira edição de 1900) tinham tanta
importância que seus conteúdos definiram o conceito de poesia por meio
de exemplos (KORTE, 2000; HOPKINS, 2008). Para muitos leitores da
literatura inglesa na primeira parte do século XX, os textos que
constavam nesses dois livros eram poemas e os que não estavam neles
incluídos não eram. A própria criação de uma antologia de poesia em
Libras pode ajudar a definir o que são poemas, de modo que o que é
valorizado em antologias de poemas em Libras é entendido como poesia em
Libras (vamos falar mais sobre antologias no capítulo 20).

O poeta inglês TS Eliot afirmou, em 1958: "Não acredito que nenhuma
distinção entre prosa e poesia seja significativa" (tradução nossa),
embora ele também acreditasse que a maioria das pessoas reconheceria um
poema na sua própria língua, mesmo sem ter uma definição. Não podemos
separar a prosa e a poesia como duas categorias exclusivas, mas podemos,
pelo menos, falar sobre as qualidades que normalmente são associadas a
cada uma delas. Talvez seja possível afirmar que existe um *continuum*
(ou uma escala) com elementos fortemente poéticos em uma extremidade e
elementos fortemente prosaicos na outra ponta.

O poeta surdo Clayton Valli (1993) fez um estudo linguístico pioneiro
sobre poemas em ASL. Ele observou que a distinção entre poemas e não
poemas é uma questão de tendência, conforme uma obra tenha mais ou menos
características poéticas. A falta de uma definição clara do que é um
poema em línguas de sinais não impediu os pesquisadores de analisarem e
discutirem sobre uma ampla gama de aspectos contidos na forma de arte
sinalizada. Talvez uma definição provisória possa ser a de que poemas
são aquilo que os pesquisadores e/ou as pessoas que criaram essas formas
de arte em Libras consideram como poemas. Estudos que analisaram vários
textos ou apresentações artísticas ou poéticas em Libras incluem:
Machado (2013), Klamt, Machado e Quadros (2014), Barros (2014), Peixoto
(2016) e Campos (2017). Em cada um desses estudos, há um entendimento
claro de que os textos e as performances têm características artísticas
e que, portanto, significam que podem ser denominados poemas.

### 13.3. Critérios para ver a diferença entre poema e prosa

Os critérios destacados por Michiko Kaneko para diferenciar poesia e
prosa nas línguas de sinais são: a) comprimento; b) segmentação de
verso; c) fim, função ou objetivo; d) colocação da linguagem no primeiro
plano e “desfamiliarização”; e) (in)flexibilidade do texto; f)
vocabulário; g) ritmo e velocidade; h) enredo e personagens; e i)
regras. Nas próximas seções, veremos que alguns poemas seguem alguns
desses critérios, mas nem todos os poemas seguem todos os critérios por
completo.

#### 13.3.1. Comprimento

Poemas geralmente são mais curtos do que histórias. É raro contar uma
história em prosa com poucas palavras, mas num poema isso é possível.
Geralmente, os poemas são curtos e têm menos de dois minutos. O poema
[*Peixe*](#ytModal-LEDC479z_vo), de Renato Nunes, dura apenas 14
segundos. [*Como Veio Alimentação*](#ytModal-nMOTYprbYoY), de
Fernanda Machado, dura 36 segundos. *[A
Economia](#vimeoModal-267272909),* de Sara Theisen Amorim e Ângela
Eiko Okumura, dura apenas 30 segundos.

De acordo com esse critério, um poema é uma forma comprimida de
linguagem e, portanto, requer menos palavras. Mas muitas piadas são
curtas também. As propagandas da televisão são breves. Nas redes
sociais, vemos muitas peças mais ou menos teatrais de no máximo 90
segundos. Por outro lado, alguns textos relativamente longos ainda são
poemas e, geralmente, os textos em prosa em Libras são mais curtos do
que os escritos. [*O Voo Sobre Rio*](#ytModal-YaAy0cbjU8o)
(normalmente considerado um poema) dura 00:03:46, mas [*Eu x
Rato*](#ytModal-UmsAxQB5NQA), de Rodrigo Custódio
da Silva (uma narrativa), dura 00:04:03.

[*O
Jaguadarte*](#ytModal-DSSXTh0wriU),
de Aulio Nóbrega, é difícil de ser categorizado. Podemos dizer que é um
poema, mas também chamá-lo de peça de teatro, ou mesmo narrativa VV.
Certamente não é uma prosa comum, uma forma de narrativa com um
narrador. Mas a obra leva 00:02:20 para ser apresentada e, com outras
considerações que veremos, a duração pode ser uma razão para a
considerarmos um poema.

Vemos então que, embora os poemas tendam a ser curtos, o comprimento por
si só não distingue a poesia da prosa.

#### 13.3.2. Segmentação de verso

A noção de “verso" é comumente utilizada para se distinguir a poesia da
prosa na literatura escrita em muitas culturas, tanto nas tradições
literárias ocidentais quanto nas não ocidentais. Na prosa, as quebras de
linha ocorrem arbitrariamente. Na poesia, o poeta escolhe romper linhas
e usar as quebras de linha e a pontuação incomuns para manter a
estrutura rítmica do poema e os padrões de som (tais como rima e
aliteração) ou para enfatizar certas palavras. A forma visual dos versos
numa página muitas vezes sinaliza aos leitores de poesia em português
que o texto é um poema. Em algumas situações, basta quebrar as linhas de
um texto em prosa para se criar a impressão de um poema.

A existência de versos, no entanto, não é sempre importante na poesia,
nem mesmo nos poemas escritos. Tipos diferentes de poema definem verso
de formas diferentes. Existe uma categoria de poemas nomeada **verso
livre**, em que os versos não têm rima nem ritmo métrico. Os **poemas
concretos**, com objetivo de criar imagens visuais com palavras, podem
colocar letras e palavras em qualquer espaço e, assim, fogem do verso
linear. Existem, também, **poemas em prosa,** que usam linguagem
fortemente estética e poética de maneiras diversas, mas não têm versos
e, de tal modo, não se parecem visualmente como poemas numa página.

Dado que os versos vêm da forma escrita, o maior problema dos **poemas
de performance** e de outros **poemas orais** e falados (e que não são
escritos) é não haver a opção de se dividir o texto em versos.

Além disso, em Libras, a grande maioria dos poemas não está escrita, o
que dificulta a ideia de se ter versos. Às vezes parece que esses eles
existem, pois historicamente os poemas apresentados em línguas de sinais
eram traduções de poemas escritos de uma língua oral. Essa influência
dos poemas escritos também acontece nas traduções de poemas originais em
Libras. Podemos traduzir um poema em Libras para português escrito e
assim ver alguns exemplos de verso na página de um texto. Por exemplo, o
poema em Libras *[Homenagem Santa
Maria](#ytModal-9LtOP-LLx0Y)/RS,* do poeta Alan Henry Godinho,
foi traduzido por Markus Weininger (2013) com o objetivo de manter
diversos elementos poéticos da língua de sinais em uma forma poética
entendida pelos leitores de poesia em português.

##### HOMENAGEM SANTA MARIA - Alan Henry Godinho 

<div class="legenda"></div>

 *Tradução: Markus J. Weininger (2013)*

<div class="poesia"></div>

>  ... meu olhar a pesar em silêncio ...
>  
>  *Vejam,* como o mundo todo sofre,
>  
>  Sofre, sofre, sofre...
>  
>  Toda a nossa nação de Norte a Sul
>  
>  Nos seus estados despedaçados
>  
>  No Rio Grande do Sul
>  
>  S-A-N-T-A M-A-R-I-A ... Santa Maria
>  
>  *Vejam,* as flores a brotar em todo jardim
>  
>  Imaginem as estrelas a brilhar no céu sem fim
>  
>  Vejam o que a morte nos ceifou
>  
>  Amigos, familiares, reunidos
>  
>  união
>  
>  união, adentrando o coração
>  
>  *Vejam,* no céu, eclodiram diversas estrelas
>  
>  Desabrocham no jardim as flores da eternidade
>  
>  Vislumbrem seu vôo sublime
>  
>  voar sem limites nem fronteiras, em
>  
>  paz que um dia também em nós irá
>  
>  irradiar
>  
>  *Vejam,* o consolo nos braços,
>  
>  abraço
>  
>  meu abraço profundo
>  
>  meu amparo
>  
>  amparo
>  
>  o amparo silencioso do mundo

O uso do verso nesse poema é resultado da tradução e não uma parte
natural da obra em Libras.

Em sua pesquisa, Clayton Valli (1993) afirmou que podemos encontrar
versos em poemas de ASL e que esses versos estão ligados aos padrões de
rima nos sinais. A divisão de linhas em versos na poesia sinalizada pode
mostrar a intenção do poeta e fazer parte do poema (como nos exemplos de
Valli), mas muitas vezes uma divisão em versos é artificial e essa não
era a intenção do artista. Embora vejamos certamente divisões internas
em poemas definidas pelas ideias e pelas construções linguísticas, essas
divisões são apenas *parecidas* com versos, mas não iguais a eles.
Marion Blondel e Chris Miller (2000) sugeriram que cada verso num poema
sinalizado trata de uma nova ideia. Nenhuma pesquisa até agora
identificou um meio de segmentação de textos literários em Libras que
diferencie poesia de prosa.

#### 13.3.3. Finalidade, função e objetivo

Normalmente, o objetivo de se usar uma língua é ser compreendido. As
histórias em prosa, por exemplo, geralmente contam uma narrativa
coerente que descreve uma sequência de eventos. O leitor (ou o
espectador) deve compreender os acontecimentos contados e por isso a
comunicação bem-sucedida é o principal objetivo de uma narrativa em
prosa. Muitas vezes, um contador de história espera que o público
assista à história uma vez, mas o poeta pode esperar que as pessoas
assistam a uma gravação do poema múltiplas vezes. Isso acontece porque
um poema nem sempre espera uma compreensão imediata. O público pode
assistir a um poema várias vezes até que entenda a mensagem; essa é uma
estratégia gerada pelo uso da tecnologia de vídeo. Anteriormente, quando
uma pessoa fazia uma performance em Libras, o público a assistia ao
vivo, uma vez só, não havia a opção de rever a apresentação. Com o
vídeo, um poeta pode criar textos em Libras sempre mais complexos e
menos claros. O poema
[*Associação*](#ytModal-yd1VVhdsT3Q),
de Fernanda Machado, não é fácil de entender à primeira vista, embora
seja muito bem apresentado. Ele exige que seja visto várias vezes antes
de se entender e apreciar sua riqueza. Podemos dizer o mesmo de
[*Peixe*](#ytModal-LEDC479z_vo), de Renato Nunes.

Mas, não é apenas a poesia que exige foco na linguagem. Existem
narrativas do gênero VV em que o público precisa prestar muita atenção
para entender o significado dos classificadores apresentados.

#### 13.3.4. Colocar a linguagem no primeiro plano e “desfamiliarizá-la”

Um poema tem o objetivo de explorar a linguagem artística. O poeta de
Libras pode romper os limites linguísticos e descobrir expressões novas,
ousadas e originais para transmitir seus pensamentos. Um poema muitas
vezes quebra as regras da Libras e usa linguagem incomum, estranha e
distorcida para criar algo diferente. Isso chama atenção à linguagem.
Por outro lado, a prosa usa a linguagem mais cotidiana ("normal"), um
vocabulário mais estabelecido e a gramática padrão para garantir que o
conteúdo seja compreendido. A linguagem da prosa, mesmo que usada de
forma prazerosa, não chama a atenção e é menos importante do que o
conteúdo.

A **desfamiliarização** é uma prática literária
de apresentar coisas comuns e familiares de forma desconhecida, para
obrigar os leitores a prestarem atenção às coisas que normalmente passam
despercebidas. O poeta pode colocar a linguagem no primeiro plano e
desfamiliarizá-la. Colocar a linguagem no primeiro plano é uma tentativa
de usar uma palavra (ou uma imagem) de modo que atraia a atenção.

A alteração de perspectiva nos poemas de perspectiva múltipla, a criação
de animais que sinalizam e os duetos em que duas pessoas criam um sinal
juntas - todos exigem que o público foque na linguagem. No poema
[*Ave 1 x 0 Minhoca*](#vimeoModal-267277312),
de Marcos Marquioto, o poeta alterna entre a perspectiva da minhoca e a
da ave, cada vez se aproximando mais para que os classificadores mostrem
a minhoca e a ave maiores. Isso traz novas perspectivas para o público,
que tem a oportunidade de ver a caça de uma minhoca por uma ave a partir
da visão dos bichos. O uso de classificadores criativos, portanto,
desfamiliariza a linguagem e o conceito.

#### 13.3.5. (In)flexibilidade do texto

Um poema, geralmente, tem um texto fixo, mas as histórias sinalizadas em
prosa podem ser adaptadas de acordo com o público ou a situação. Isso
acontece apenas nas performances orais - sabemos que prosa literária
escrita também tem um texto fixo. Um poema é quase sempre preparado para
gerar um texto completo antes da performance e o poeta prepara seu
trabalho cuidadosamente antes de mostrá-lo ao público. Cada vez que
apresenta o poema, esperamos que seja o mesmo poema apresentado da mesma
maneira. Ao contrário disso, as histórias em prosa contadas ao vivo não
seguem tanta rigidez, são mais espontâneas. Por exemplo, cada vez que
contamos uma narrativa de experiência pessoal ou um conto de fadas,
sempre apresentamos os principais fatos, mas temos flexibilidade para
contar as histórias.

Por outro lado, em muitas culturas, um poeta que cria um poema de forma
espontânea é altamente valorizado, e nos vídeos de hoje em dia, as
histórias em Libras são vistas em formas mais fixas.

#### 13.3.6. Vocabulário

As histórias em prosa utilizam vocabulário para desenvolver a sequência
de eventos e têm uma maior frequência de sinais vocabulares para
garantir que todos entendam. Já os poemas usam sinais mais produtivos,
criativos ou mesmo novos. Os dois gêneros também usam diferentemente os
elementos não manuais. Nas histórias em prosa, os componentes manuais
são indispensáveis porque as informações são importantes para contar os
fatos. Um poema depende muito menos de sinais manuais, e mais de
elementos não manuais - expressões faciais, olhar, movimento do corpo,
espaço, velocidade, ritmo – para criar mais sensações e imagens e menos
fatos. *[Anjo Caído](#vimeoModal-209842983),* de Fernanda Machado, é um bom
exemplo de um poema que usa sinais produtivos e muitos elementos não
manuais. No entanto, [*A Pedra Rolante*](#ytModal-kPXWu5UCTzk),
de Sandro Pereira, usa não somente esses elementos, característicos dos
poemas, mas também outros que caracterizam produções narrativas em
prosa.

#### 13.3.7. Ritmo e velocidade 

As funções de ritmo e velocidade são diferentes em um poema ou numa
história em prosa. As histórias normalmente variam de ritmo e de
velocidade para destacar ações ou emoções, mas os poemas podem variar de
ritmo e velocidade de maneira mais deliberada e estética para chamar
atenção à própria linguagem, e não ao conteúdo. *[Como Veio
Alimentação](#ytModal-nMOTYprbYoY),* de Fernanda Machado, e [*O
símbolo do Olodum*](#ytModal-_jhW0K4NN0A), de Priscila Leonnor,
usam ritmos destacados. No primeiro, o ritmo lento e tenso dos sinais do
trabalhador rural contrasta com o ritmo mais rápido e solto da pessoa
urbana. No segundo, o ritmo dos sinais é motivado pelo ritmo da bateria
de samba. Num poema, sinalizar em câmera lenta destaca cada movimento do
corpo e a expressão do poeta. Mas também vemos
na prosa que a velocidade do movimento do sinal não reflete a velocidade
real, é "extradiegética" (algo fora da história). 
*[Slow Motion Portrait](#ytModal-rWowtWJSd4E),* (em português “Retrato em câmera lenta”),
de Tony Bloem, e [*Bolinha de
Ping-pong*](#ytModal-VhGCEznqljo), de Rimar
Segala, são narrativas em que a velocidade dos sinais não reflete a
velocidade das ações descritas.

#### 13.3.8. Enredo e personagens

Histórias em prosa normalmente têm um enredo, ou seja, uma sequência em
que a história se desenrola. Mas os poemas nem sempre têm enredo. Eles
podem focar mais na autoexpressão do poeta, mostrando suas opiniões ou
suas perspectivas sobre um assunto. Além disso, podem focar na criação
de uma imagem sem ação. O objetivo dos poemas japoneses de haicai, por
exemplo, é criar uma imagem por meio de palavras. Em Libras, a
finalidade de muitos poemas é a mesma que vemos no haicai – criar uma
imagem forte, construída pelos sinais. O poema
[*Paraná*](#vimeoModal-267276733), de Marcos Marquioto,
apresenta uma série de imagens (muitas delas não totalmente
especificadas) sem uma trama. O poema não conta uma história com
eventos, mas sim apresenta uma montagem de imagens. O sinal do título é
<span style="font-variant:small-caps;">paraná,</span> o que ajuda o
espectador a entender os sinais. No poema, no entanto, vemos que há uma
igreja, mas não sabemos que ela é a catedral da cidade de Maringá (a
mais alta da América Latina); vemos uma ave, mas não sabemos que é a
gralha azul, ave símbolo do estado do Paraná; e vemos uma construção em
formato esférico, mas não sabemos que é a famosa estufa da cidade de
Curitiba.

Nas narrativas escritas, os personagens geralmente são claramente
apresentados, mas em alguns poemas desconhecemos quem é o personagem.
Sabemos que uma pessoa atua ou tem uma experiência, mas não sabemos nada
sobre ela. Em Libras, essa diferença não é tão clara, possivelmente por
causa das origens da poesia na língua - que é fundamentada nas
narrativas tradicionais da comunidade surda. É sabido que muitos poemas,
até mesmo poemas curtos, têm pelo menos uma trama curta.

#### 13.3.9. Regras

Poemas são de alguma forma mais "disciplinados" do que histórias. É
fundamental ter regras num poema. Estas são estabelecidas em tradições
literárias ou selecionadas pelo próprio poeta. Se não houver restrições,
não podemos compor um poema. Poemas frequentemente seguem regras
específicas.

Há uma escolha obrigatória dos tipos específicos de palavras (por
exemplo, o haicai tradicional deve incluir uma palavra que indica uma
estação do ano). Em poemas que são homenagens a outros poemas, a
estrutura segue alguns elementos do original e acrescenta uma nova
perspectiva. Por exemplo, os poemas
[*Saci*](#ytModal-4UBwn9242gA), de Fernanda Machado, e
[*Árvore*](#vimeoModal-267272296), de André Luiz
Conceição, incluem elementos do poema
[*Tree*](#ytModal-Lf92PlzMAXo), de Paul Scott. O poema
[*Paraná*](#vimeoModal-267276733), citado
anteriormente, usa o passeio de uma ave e o crescimento das imagens na
sua aproximação, em homenagem ao poema [*Voo Sobre
Rio*](#ytModal-YaAy0cbjU8o), de Fernanda Machado.

Alguns poemas em português exigem esquemas de rima estritos, obrigando o
uso de palavras que terminem com o mesmo som na composição final dos
versos. Por exemplo, a forma dos poemas de Cordel escrito exige que as
estrofes de seis versos tenham palavras que rimem no padrão A, B, C, B,
D, B[34](#tooltip). Os sonetos exigem outros diversos padrões, mas, depois de
escolher um padrão de rima, o poeta deve seguir as suas regras. Já os
poetas de Libras podem criar um esquema com as mesmas configurações de
mão ou os mesmos movimentos, e as exigências geram um prazer maior no
espectador quando são cumpridas. Por exemplo, o poema
[*Peixe*](#ytModal-LEDC479zvo), de Renato Nunes, tem apenas uma
configuração de mão. No poema, [*Meu Ser é
Nordestino*](#ytModal-t4SLooMDTiw), de Klícia
Campos, uma configuração de mão dos sinais é repetida em cada estrofe e
muda dependendo do sentimento a ser apresentado.

As estruturas métricas são importantes nos poemas escritos em português,
mas não são exigidas nos poemas em Libras, que usam outros tipos de
ritmo gerados pelo tipo, tamanho, pela velocidade e direção de movimento
dos sinais.

Por outro lado, apesar de as histórias em prosa seguirem as regras de
criação de um texto visual, elas tendem a ser muito mais livres e mais
espontâneas, e a sua composição é menos controlada por regras
específicas. Apesar disso, no texto *[A Pedra
Rolante](#ytModal-kPXWu5UCTzk),* de Sandro Pereira, o ritmo é
seguido cuidadosamente.

<div class="float"></div>

![resumo](/images/resumo-parte3.png)

### 13.4. Resumo

Nesse capítulo, apresentamos os critérios que apontam as diferenças
entre poesia e prosa. Vimos que alguns poemas contêm esses elementos,
mas nem todas as produções mostram todos os elementos e, às vezes, vemos
características de poemas nas prosas. Vimos também que os elementos que
definem poemas em português nem sempre definem poemas em Libras.

Mas será que existem regras estabelecidas para a forma de poesia em
línguas de sinais? É isso que veremos no próximo capítulo.

<div class="float"></div>

![atividade](/images/atividade-parte3.png)

### 13.5. Atividade 

Assista a cinco obras artísticas em Libras:

1. *[Galinha
Fantasma](#ytModal-isyT8-mgCDM),* de Bruno Ramos.

2.  *[Ave 1 x 0 Minhoca](#vimeoModal-267277312),* de
    Marcos Marquioto.

3.  *[Meu ser é
    nordestino](#ytModal-t4SLooMDTiw),* de Klícia
    Campos.

4.  *[A Economia](#vimeoModal-267272909),* de Sara Theisen Amorim
    e Ângela Eiko Okumura.

5.  *[Eu x Rato](#ytModal-UmsAxQB5NQA),* de
    Rodrigo Custódio da Silva.

**Use a lista de critérios estudada nesse capítulo. O que fez você pensar
que este é mais um poema ou uma narrativa (história)? Coloque-os ao
longo da escala “prosa-poesia”.**



<span class="tooltip-texto" id="tooltip-33">
A estrutura do capítulo segue as ideias da pesquisadora Michiko
    Kaneko. (Veja SUTTON-SPENCE; KANEKO, 2016)
</span>

<span class="tooltip-texto" id="tooltip-34">
Por exemplo, no poema Antônio Silvino por Leandro Gomes de
    Barros, as palavras *acertadamente, presente* e *contente* rimam no
    final dos 2º, 4º e 6º versos

    Mas mestre padre entendeu (A)

    Que ia acertadamente (B)

    Em pegar meus cangaceiros (C)

    E fazer deles presente, (B)

    Quem tiver pena que chore (D)

    Quem gostar fique contente. (B)
</span>
