---
parte: 0
capitulo: 1
titulo: Pré textos
pdf: http://files.literaturaemlibras.com/Literatura_em_Libras_Pre_textos.pdf
---

## Pré textos

### Literatura em Libras

A literatura surda em Libras é fundamental para a expressão dos surdos na sua própria língua. Compreender melhor a arte feita na sua língua é um direito de todos os surdos no Brasil. O objetivo deste livro é conhecer as obras literárias feitas em Libras, comentar sobre elas e tentar entender melhor como é feita a literatura nessa língua. O livro está dividido em 4 seções: os conceitos fundamentais da literatura em Libras; a produção de narrativas e contos em Libras; os elementos da linguagem estética de Libras; e a inter-relação entre a sociedade e a literatura em Libras.

Este livro será útil para qualquer leitor com interesse em Libras e/ou em literatura, alunos, professores e tradutores-intérpretes. Esperamos que nossos leitores sejam bilíngues em Libras e português (em diversos níveis nas duas línguas), embora acreditemos que quem não domina Libras possa também acompanhar as discussões e os exemplos que serão apresentados. 

O livro que apresentamos a vocês está disponível na internet por meio digital. É um livro bilíngue, com versão em português e versão em Libras. Pode ser lido em computadores, tablets ou celulares e possui hiperlinks para se assistir aos vídeos indicados - todos com livre acesso. O leitor pode baixá-lo e imprimir o texto em português, se preferir. Todos os capítulos em Libras se encontram no site. 

O livro é gratuito para acessar e baixar. As informações aqui são fundadas nos conhecimentos e nas experiências da comunidade surda brasileira – que não têm preço.

Rachel Sutton-Spence é professora no Departamento de Libras na Universidade Federal de Santa Catarina.

### Dedicatória

> À comunidade surda brasileira.<br/>Agradeço por vocês me acolherem.<br/>
>
> E ao Martin Haswell.<br/>You made this happen.<br/>Rite, Mart?

### Agradecimentos

Agradeço aos artistas de Literatura em Libras – surdos e ouvintes – pela permissão dada para incluir os links de suas obras. Sem vocês, este livro não existiria.

Alan Henry Godinho; André Luiz Conceição; Ângela Eiko Okumura; Anna Luiza Maciel; Aulio Nóbrega; Bruno Ramos; Cézar Pedrosa de Oliveira; Clóvis Albuquerque dos Santos; Daltro Roque Carvalho da Silva Junior; Luciano Canesso Dyniewicz; Fábio de Sá; Fernanda Machado; Juliana Lohn; Kácio Evangelista; Klícia Campos; Leonardo Adonis de Almeida; Marcelo William da Silva; Marcos Marquioto; Mariá de Rezende Araújo; Marina Teles; Maurício Barreto; Nelson Pimenta; Priscila Leonnor; Rafael Lopes dos Santos (Rafael Lelo); Renato Nunes; Rimar Segala; Rodrigo Custódio da Silva; Roger Prestes; Rosana Grasse; Sandro Pereira; Sara Theisen Amorim; Tom Min Alves; Vanessa Lima; Victória Hidalgo Pedroni; e Wilson Santos Silva.

O português é minha segunda língua. Camila Pasquetti trabalhou como revisora, professora de português e guia cultural. Thank you, Camila!

Gisele Iandra Pessini Matos fez a revisão final do texto inteiro.

Martin Haswell gravou e editou vários vídeos com exemplos das obras literárias incluídas aqui. Ele fez a tabela de configurações de mão. Ele criou o site do nosso livro, planejou e editou os vídeos do texto em Libras e forneceu todo tipo de apoio - especialmente técnico - durante todo o processo de elaboração deste livro.

Gustavo Gusmão traduziu e fez a versão do texto em Libras.

O livro foi escrito como parte do projeto financiado pelo CNPq Documentação de Libras, coordenado pela Professora Ronice Müller de Quadros da UFSC.

Laís Benedetto e Anderson Rodrigues Alves posaram as mãos para a lista de configurações de mão. Jaqueline Boldo demonstra os exemplos no capítulo 19.

Por fim, gostaria de agradecer a todos os alunos, artistas de Libras, colegas do Departamento de Libras da UFSC e membros da comunidade surda, especialmente Fernanda Machado, por suas conversas e contribuições ao longo dos anos.

### Convenções

Neste texto falamos de muitos vídeos de produções literárias em Libras (e alguns em outras línguas de sinais) e de alguns sites de interesse na internet. Cada vez que mencionamos no texto um vídeo ou outro recurso acessível pela internet, há um link. Ao ler o texto num dispositivo com conexão à internet, basta clicar nas palavras sublinhadas em azul para abri-lo. Quem ler o texto impresso pode usar o QR code.

#### Glosas

Glosa é uma tradução básica de cada sinal usando palavras em língua portuguesa. Usamos versalete para glosar (letras maiúsculas do tamanho de minúsculas). Assim, por exemplo, os sinais traduzidos em português como “lei” e “pedra” são grafados como lei e pedra). Onde os sinais precisam mais de uma palavra para serem traduzidos, juntamos as palavras com traços ou hifens. Assim “muita orientação” será grafada como muita-orientação.

#### Siglas

* **ASL** American Sign Language - Língua de Sinais Americana
* **DGS** Deutsche Gebärdensprache – Língua de Sinais Alemã
* **BSL** - British Sign Language - Língua de Sinais Britânica
* **INES** - Instituto Nacional de Educação dos Surdos
* **L1** - Primeira língua, língua de conforto ou língua materna
* **L2** - Segunda língua, língua adquirida depois de adquirir L1
* **LSB** - Língua de Sinais Brasileira, hoje em dia um termo menos usado comparado com o termo Libras
* **VV** - Vernáculo Visual, uma técnica de contação de história cinematográfica
* **UFPR** - Universidade Federal de Paraná
* **UFSC** - Universidade Federal de Santa Catarina
 
#### Configurações de mão

Existem diversas maneiras de representar e descrever a forma de configurações de mão incluindo por português escrito, sistemas de notação e sistemas de escrita. Em algumas partes deste livro no texto escrito em português, descrevemos as configurações de mão usando palavras e em outras nomeamos com símbolos baseados em letras do alfabeto manual ou números de Libras. Também descrevemos variações nas configurações de letra e número, especialmente naquelas em que os dedos são curvos ou "em garras". Para leitores familiarizados com o alfabeto manual de Libras, essas configurações de mão são facilmente compreendidas. Para quem não sabe o alfabeto manual, fornecemos aqui fotos das configurações de mão mencionadas no texto com seus nomes.

![Imagem com fotos dos sinais alfabéticos](/images/p0-c0.png)

<div class="legenda"></div>

Imagem com fotos dos sinais alfabéticos

<div class="grid">
<div>A</div>
<div>B</div>
<div>C</div>
<div>D</div>
<div>E</div>
<div>F</div>
<div>G</div>
<div>H</div>
<div>I</div>
<div>L</div>
<div>M</div>
<div>N</div>
<div>O</div>
<div>S</div>
<div>T</div>
<div>U</div>
<div>V</div>
<div>V em garras</div>
<div>X</div>
<div>Y</div>
<div>1</div>
<div>3</div>
<div>Mão aberta</div>
<div>Mão aberta em garras</div>
<div>ILY</div>
</div>

### Prefácio

Este é um livro sobre o amor. O amor às línguas de sinais e às literaturas dessas línguas, mas especialmente o amor pela literatura em Libras.

Começo este livro com uma história, uma narrativa pessoal. Quando fiz minha faculdade de Psicologia e Linguística na Inglaterra em 1986, cursei uma disciplina chamada “Distúrbios em comunicação e linguagem”. O professor da disciplina me pediu para preparar uma apresentação sobre a pergunta: *A ‘língua’ dos surdos é mesmo uma língua?* Não sabia nada sobre o assunto, mas achei um livro na biblioteca sobre a língua de sinais americana (ASL) chamado *The Signs of Language*, de Klima e Bellugi, pesquisadores da Califórnia que trabalhavam na mesma época que o linguista americano William Stokoe. Fiquei encantada. Nunca tinha lido nada parecido com aquilo. O livro mostrava repetidamente a riqueza, a criatividade e a genialidade da língua de sinais. No final havia um capítulo sobre poemas em ASL em que descreviam poemas de origem japonesa, os chamados haicais (em português às vezes escrito haikai ou haiku), feitos em língua de sinais americana. Na conclusão da minha apresentação, ressaltei que uma língua em que podemos criar haicais é sem dúvida uma língua.

Nos anos 1970, época em que Klima e Bellugi pesquisaram e escreveram o referido livro, havia poucos poemas em ASL. Muitos destes eram traduções de poemas escritos em inglês, mas assim foi o início do desenvolvimento da poesia e da literatura em ASL. Naquele tempo, a parceria entre pesquisadores, professores e poetas surdos contribuiu para o crescimento de obras literárias nos EUA. Isso aconteceu também no Brasil. Nos últimos 20 anos aqui no país, a literatura em Libras tem crescido rapidamente, e acredito que chegou a hora de estudar, curtir e elogiar essa forma de arte – a riqueza, a criatividade e a genialidade da Libras – em um livro.

O principal objetivo do livro é conhecer as obras literárias de Libras. Nosso foco será no uso da língua para criar os efeitos literários valorizados pela comunidade surda brasileira. Por isso vamos assistir (várias vezes) a alguns exemplos de poemas e de histórias em língua brasileira de sinais. É claro que vamos analisar os exemplos, comentar sobre eles e tentar entender melhor como é feita a literatura em Libras. Mas é preciso conhecer a literatura em primeiro lugar para podermos falar dela. Antigamente, a tecnologia não facilitava o debate sobre a literatura em língua de sinais em livros escritos e impressos. Klima e Bellugi (1979) usaram desenhos feitos com canetas-tinteiro; Sutton-Spence e Woll (1998) usaram imagens de captura de tela de vídeos em VHS; Bauman, Nelson e Rose (2006) incluíram um DVD de trechos de ASL literária em um pequeno envelope na capa do seu livro; Sutton-Spence e Kaneko (2016) usaram capturas de tela junto a uma lista de vídeos indicados, muitos deles do YouTube, esperando que os leitores os procurassem na internet. 

Este livro que apresentamos a vocês está disponível na internet por meio digital, sem tantas restrições como em livros impressos. Pode ser lido em computadores, tablets ou celulares e possui hiperlinks para se assistir aos vídeos indicados - todos com livre acesso. Esperamos que nossos leitores assistam aos vídeos das obras artísticas e literárias em Libras, leiam os textos em português que falam sobre as obras e/ou assistam aos vídeos deste texto em Libras, voltem aos vídeos das obras, e assim por diante, buscando uma maior compreensão sobre o assunto.

Com acesso fácil aos exemplos, pode-se usar este livro simplesmente para ler e conhecer algumas informações. Mas ele também pode funcionar como um caderno de exercícios – um livro de trabalho e estudos. Esperamos que nossos leitores não apenas olhem os exemplos e leiam (ou assistam) o texto, mas sim reflitam um pouco sobre eles. Pedimos a você, leitor, para interagir com os poemas ou contos em Libras e as ideias divulgadas no texto. Faça as atividades sugeridas no final de cada capítulo. Tente aplicar o que você leu nas obras indicadas, ou procure outras obras e aplique as ideias nesses textos. Você consegue? Dá certo? Não dá certo? Tem outras dúvidas? Ótimo. Estamos no caminho de desenvolver cada vez mais reflexões sobre a literatura em Libras.

Estudar apenas um ou dois poemas ou contos não é suficiente para se conhecer a literatura em Libras. Conhecer apenas poemas, traduções de canções ou contos de fadas infantis também não basta. Para compreender a importância da literatura em Libras precisamos acessar e estudar muitas obras de diversos tipos. Hoje em dia, é impossível estudar todas as obras de literatura em Libras disponíveis, mas vamos ver uma seleção que acredito que seja representativa dos trabalhos de hoje (talvez você não acredite, mas é verdade que a primeira disciplina de literatura em língua de sinais britânica – BSL –que ministrei no ano 2000 incluía todos os poemas gravados disponíveis na língua naquela época). 

Os exemplos apresentados neste livro, no fim das contas, refletem escolhas pessoais. Selecionei apresentações de poemas e histórias que conheço, feitas por poetas e artistas de Libras que, acredito, fazem obras interessantes, importantes, relevantes e que me agradam. Tentei criar um equilíbrio na escolha de trabalhos que me parecem ser “bons”, que representam a diversidade da comunidade surda e obras de diversos artistas. Outro autor, de um livro que não este, com outras experiências de vida e outros interesses acadêmicos certamente tomaria decisões diferentes. O campo da literatura em Libras é grande e a cada ano vai se ampliando. Estamos no início do trabalho e quanto mais pessoas pesquisarem e publicarem sobre esse assunto, mais promoveremos a arte em Libras.

É por isso, também, que não adoto uma única perspectiva teórica, embora não seja possível (nem desejável) evitar totalmente a teoria. Minha formação linguística e meu interesse pessoal pela magia da língua significa que a minha abordagem tem mais foco na linguagem poética e estética da Libras. Outras perspectivas, por exemplo, a dos estudos culturais que focam mais no assunto da literatura surda e no sujeito surdo (MOURÃO, 2016; KARNOPP; SILVEIRA, 2016), são igualmente importantes, mas deixo esse foco aos outros – ou para o futuro. Onde acho que as teorias linguísticas ajudam a esclarecer algum ponto, falo delas. Também há muitas teorias na área de crítica literária e algumas delas podem nos ajudar a entender melhor um texto em Libras. Por outro lado, textos literários podem nos auxiliar a entender melhor alguns textos teóricos e críticas literárias. Teorias formalistas, estruturalistas, feministas, pós-coloniais, ambientalistas e outras mais, podem contribuir ao nosso entendimento sobre a literatura em Libras. É possível que a abordagem que mais usamos aqui seja a da “leitura detalhada” (em inglês “Close Reading”) com foco principal no texto, na linguagem do texto e nos efeitos do uso dessa linguagem.

Queremos que este livro seja útil para qualquer leitor com interesse em Libras e/ou em literatura. A literatura em Libras é fundamental para a expressão dos surdos na sua própria língua e, compreender melhor a arte feita na sua língua, é um direito de todos os surdos no Brasil. Além disso, esperamos que o livro ajude cada professor de Libras (sejam seus alunos surdos ou ouvintes) a ver e usar os textos de contos e poemas para fortalecer suas aulas e aprofundar o conhecimento da língua. 

É bem sabido que não há nada melhor para exigir a compreensão de um texto literário do que tentar traduzi-lo. Na hora de se buscar traduzir o significado de um poema em Libras para o português é que percebemos a verdadeira riqueza e a complexidade da linguagem do poema. Por outro lado, como podemos traduzir obras literárias do português para a Libras sem saber como é feita a literatura em Libras? Para os tradutores-intérpretes de Libras (especialmente para quem trabalha como intérprete educacional), o livro ajudará a conhecer a literatura em Libras para contribuir à compreensão da língua e ao aperfeiçoamento das habilidades linguísticas e tradutórias. Acima de tudo, queremos que o livro abra os olhos para as possibilidades de modos de expressão em Libras.

Apesar de ter o foco primário na linguagem literária em Libras, este é um livro bilíngue. É escrito em português, com texto paralelo em Libras, falando sobre uma forma de arte em Libras. Esperamos que os usuários do livro sejam (na maioria) bilíngues em Libras e português, embora acreditemos que aquele que não domina a língua brasileira de sinais possa acompanhar as discussões e os exemplos apresentados. Algumas pessoas têm Libras como a primeira língua (L1) e outras têm a língua portuguesa como L1, mas o objetivo é sempre elogiar e promover a Libras por meio das duas línguas.

Mas e a autora? Por acaso ela tem Libras ou português como L1? Não. Sou inglesa, sou ouvinte e não tenho parentes surdos. Aprendi a primeira língua de sinais com 21 anos. Nasci em Liverpool (a cidade dos Beatles) e tenho o inglês como minha língua materna. De repente, quando estava com quase 11 anos, meu pai informou à minha família que iríamos morar no Brasil. Por três anos muito felizes moramos na cidade de São Paulo/SP e lá aprendi (mais ou menos) o português. Infelizmente, o emprego do meu pai mudou e voltamos à Inglaterra, mas sempre tivemos saudades do Brasil.

Talvez tenha sido pela experiência de aprender português, mas por alguma razão descobri que amava as línguas – cada uma com seu jeito fascinante de expressar – e por isso estudei linguística na faculdade. Foi depois de ler o livro de Klima e Bellugi que resolvi aprender a língua de sinais britânica (BSL). Não sabia quanto tempo iria levar – uma semana? Duas semanas? Depois da primeira aula na associação dos surdos na minha cidade, entendi que iria levar a vida inteira para dominar BSL, mas valeria a pena. Fiz amizades com os surdos, trabalhei na Universidade de Bristol nos projetos linguísticos de pesquisa em BSL e fiz meu doutorado sobre datilologia em BSL. Trabalhei como professora de Estudos Surdos na Universidade de Bristol e em 1997 resolvi escrever um livro sobre a linguística da BSL com a minha orientadora Bencie Woll. O parecerista da proposta do livro gostou do trabalho, mas pediu mais dois capítulos – um sobre fonética e fonologia e outro sobre a literatura em BSL. Agradeço muito àquele parecerista anônimo! Me lembrei dos haicais no livro de Klima e Bellugi e pedi a Bencie para que me deixasse fazer o capítulo sobre literatura. Comecei a pesquisar sobre o assunto e nunca mais parei.

Em 2003, conheci Ronice Müller de Quadros num congresso de linguística. Eu disse a ela que há muito tempo eu tinha morado no Brasil e ela me convidou para dar um minicurso na Universidade Federal de Santa Catarina - UFSC - sobre a literatura em língua de sinais. Fizemos o primeiro curso em 2004, com poucos materiais em Libras, além do maravilhoso vídeo pioneiro Literatura em LSB, de Nelson Pimenta. Embora eu fosse fluente apenas em BSL (tinha esquecido muito do português da minha infância), os estudantes fossem fluentes em Libras e eu estivesse com uma gripe feia, passamos uma semana feliz estudando a literatura em BSL e Libras. Mantive as conexões com a professora Ronice e os participantes do curso até que voltei em 2013 como professora visitante da UFSC. Conheci muitos poetas e contadores de histórias surdos brasileiros, ministrei disciplinas sobre literatura (a maioria de BSL porque ainda conhecia pouco sobre as obras em Libras), orientei pesquisas e não quis mais voltar à Inglaterra. A literatura em Libras tinha conquistado meu coração. Ainda sou professora na UFSC. A cada dia que conheço mais a literatura em Libras e a cada aula, evento e pesquisa sobre ela, amo mais. Vamos celebrar essa forma de arte linguística!

#### Este livro está dividido em 4 partes. 

Na **primeira parte**, falaremos dos conceitos fundamentais da literatura em Libras. Pensaremos sobre esta como um tipo de literatura brasileira. Como queremos nos tornar leitores analíticos e críticos de literatura, veremos algumas dicas práticas para fazer análises da literatura em Libras. Investigaremos a relação entre a literatura surda (que trata com a experiência e vida do sujeito surdo) e a literatura em Libras (produzida na língua de sinais da comunidade surda brasileira) e outras maneiras de categorizar essa forma de arte linguística. Focando na linguagem, pensaremos sobre os recursos para se criar o “visual” em Libras e os elementos da Libras estética. Pensando na cultura da comunidade surda, vamos descrever alguns elementos importantes que surgem a partir de ser a literatura brasileira surda. 

Na **parte 2**, vamos estudar a produção de narrativas e contos em Libras. Numa tentativa de descrever a rica gama da literatura em Libras, vamos explorar os gêneros, definidos pelo grau de ficção, pela forma, origem, pelo conteúdo e público. Veremos como é feita a produção de narrativas visuais, estudando exemplos de literatura cinematográfica e o Vernáculo Visual (VV). Vamos ver também a estrutura das narrativas contadas em Libras, inclusive as piadas surdas. Finalmente, tentaremos separar contos e narrativas em prosa ou poesia de acordo com sua forma.

A **parte 3** enfoca os elementos da linguagem estética de Libras. Com os olhos voltados aos gêneros, tipos e às formas de poesia em Libras, veremos o uso da estrutura espacial, da metáfora, do antropomorfismo e de elementos de repetição, como ritmo e rima. Muitos desses elementos se encontram também na estética especial do humor em Libras.

A **parte 4** fala da inter-relação entre a sociedade e a literatura em Libras. Pensaremos sobre a realidade da criação de um cânone de literatura em Libras e o papel das antologias. Ao refletirmos sobre a inclusão nas antologias, consideraremos o papel das mulheres na literatura em Libras. Além disso, embora a tradução esteja presente em toda parte do livro, focaremos na questão da tradução literária e na tradução de literatura entre Libras e português. Com as mudanças sociais nos últimos anos, destacaremos a influência da tecnologia e a criação de textos híbridos e alguns fatores importantes para o ensino, a divulgação e a promoção da literatura em Libras. Depois disso, fecharemos o livro sabendo bem que este não é o fim das nossas pesquisas. Não sabemos onde o caminho vai terminar.

**Mas começaremos por aqui.**
