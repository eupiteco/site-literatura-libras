---
capitulo: 3
parte: 1
titulo: Como vemos esse espaço literário?
pdf: "http://files.literaturaemlibras.com/CP03_Como_vemos_esse_espaco_literario.pdf"
video: cgEBPB1nsCw
---

## 3. Como vemos esse espaço literário?

<div class="float"></div>

![objetivo](/images/objetivo-parte1.png)

### 3.1. Objetivo

Já falamos de literatura em geral e neste capítulo pensaremos sobre as
características e os tipos específicos (ou categorias) de literatura da
comunidade surda que vamos estudar. Há vários termos, em português e em
Libras, além de alguns conceitos parecidos que precisamos investigar,
principalmente “literatura surda”, mas também “literatura em língua de
sinais”, “literatura em Libras”, “literatura sinalizada” e “literatura
visual”. Lembramos que não é fácil definir as categorias concretamente
porque não existe uma simples definição satisfatória, mas podemos
delinear as áreas de interesse para nossa discussão.

Podemos dizer, de forma breve, que a literatura surda é da comunidade
surda e das pessoas surdas, já a literatura em língua de sinais é
produzida na língua das pessoas surdas, lembrando que a literatura surda
nem sempre está produzida em língua de sinais. Já a literatura em Libras
é feita na língua de sinais dos surdos brasileiros.

A partir da perspectiva dos autores ou produtores da literatura, dos
assuntos tratados nas produções e do público esperado, podemos pensar
nos autores surdos e nos públicos surdos que são fundamentais para
literatura surda. Não existe literatura em Libras sem a comunidade
surda.

<div class="float"></div>

![lista de videos](/images/videos-parte1.png)

<div class="lista-de-videos"></div>

Neste capítulo, para esclarecer esses conceitos, falaremos de alguns
vídeos, que você pode assistir agora:

* [*O Curupira*](#vimeoModal-292526263), de Fábio de Sá.

* [*O Negrinho do Pastoreio*](#vimeoModal-306082977), de
Roger Prestes.

* [Golf Ball](#ytModal-Gl3vqLeOyEE), de Stefan Goldschmidt.

* [A Pedra Rolante](#ytModal-kPXWu5UCTzk), de Sandro Pereira.

### 3.2. Literatura surda

Digamos que existe **literatura surda,**
caracterizada por pelo menos um de quatro critérios. Todavia, é
importante destacar que qualquer produção de literatura surda não tem a
obrigação de satisfazer todos esses quatro critérios simultaneamente.

Literatura surda é uma literatura **feita por surdos,** geralmente
membros da comunidade surda, semelhante ao conceito de “literatura
negra”, que é escrita principalmente pelos autores negros. Pode ser
criada e apresentada por surdos ou elaborada originalmente por não
surdos, mas adaptada e apresentada por pessoas surdas.

Tem objetivo principal de **atingir um público surdo.** Os ouvintes
também podem apreciar essa literatura, por exemplo os pais de crianças
surdas ou professores que trabalham com alunos surdos, semelhante à
literatura infantojuvenil, que espera ter como leitores crianças e
jovens, sabendo, porém, que os adultos podem ser os seus leitores
também. A maior parte da literatura surda em que o destinatário
imaginado é o público surdo é criada por surdos, mas não é preciso ser
assim. Autores ouvintes, ou autores surdos e ouvintes em parceria,
também criam literatura surda destinada aos surdos e que trata da
experiência ou do conhecimento dos surdos.

Nesse sentido, é sobre **assuntos que tratam da experiência de ser
surdo** e muitas vezes do conhecimento da cultura surda, semelhante à
literatura feminina que trata da experiência de mulheres, que se faz a
literatura surda. Existem muitos exemplos de literatura surda com a
temática do “sujeito surdo”. Um autor surdo pode escrever sobre a
experiência surda para atingir um público ouvinte. Por outro lado,
alguns livros infantis têm autores ouvintes, mas desde que fale de uma
pessoa surda e esteja destinado ao público surdo, digamos que é um tipo
de literatura surda.

Existe literatura surda na língua da sociedade ouvinte ou na língua de
sinais; no Brasil, isso significa dizer que pode ser em português ou em
Libras. Nessa categorização de “literatura surda”, focamos nas pessoas e
não no conteúdo, então a língua em que é produzida não se destaca. Há
vários livros escritos por surdos em português, muitos deles falando da
experiência das pessoas surdas. Livros autobiográficos descrevem a
experiência de ser surdo para explicar para os leitores (surdos ou não)
quais os desafios, os sofrimentos, os esforços, os prazeres e as
felicidades de ser surdo. A pesquisa de Müller e Karnopp (2015)
apresenta um levantamento de alguns livros de autobiografia, romances,
contos e poemas escritos na língua portuguesa por autores surdos, sobre
as experiências de ser surdo, e destinados ao público surdo ou ouvinte.
Outro gênero de literatura surda escrita em português é o infantil,
destinado às crianças surdas que também fala das experiências de
personagens surdos.

Há muitos exemplos de literatura surda criada e apresentada nas línguas
de sinais, que seguem as características: 1) **ser feita por surdos;** 2)
**tratar da experiência de ser surdo** **e do conhecimento da cultura
surda;** 3) ter o objetivo de **atingir um público surdo** e de 4) ser
apresentada **em Libras.** E esse quarto critério, literatura surda
**produzida em Libras,** é o foco de nossos estudos neste livro.

#### 3.2.1. Literatura surda em Libras

Literatura em qualquer língua de sinais é criada em uma língua de
modalidade gestual-visual-espacial. “Literatura em língua de sinais”
traz uma perspectiva diferente para essa forma de “literatura surda”, em
que o foco está na **língua**. O conceito de “língua de sinais” inclui
todas as línguas de sinais mundiais em comparação ao conceito de “língua
oral”, que engloba todas as línguas de modalidade oral-auditiva,
portanto as línguas orais. Essa literatura é caracterizada por ser
produzida em uma língua de sinais, como literatura em ASL (americana),
literatura em BSL (britânica), em DGS (alemã) e em Libras (brasileira).
Literatura em Libras, então, é um tipo de literatura em língua de
sinais, que faz parte da literatura surda brasileira.

Nessa perspectiva, não se destaca tanto a **origem da literatura**. Esta
pode ser de origem surda, criada e apresentada por um autor surdo. Mas
algumas produções de literatura em Libras são traduções ou
reapresentações da literatura brasileira (fora da comunidade surda), e,
assim, de origem não surda. A tradução pode ser mais ou menos fiel ao
texto original; as mais fiéis acontecem frequentemente na criação de
textos bilíngues educacionais com objetivo de proporcionar acesso à
literatura de uma língua oral, muitas vezes para o ensino, como, por
exemplo, uma tradução de português para Libras da história de
[O Pequeno Príncipe](#ytModal-foMiwFlVHCc) de Antoine de
Saint-Exupéry, ou de [O Alienista](http://acessibilidadeembibliotecas.culturadigital.br/2016/11/16/o-alienista-de-machado-de-assis-e-mais-uma-obra-disponivel-com-recursos-de-acessibilidade/),
de Machado de Assis. Nas adaptações apresentadas em Libras, a obra
original passa por alguns ajustes para mostrar a perspectiva dos surdos,
podendo, por exemplo, incluir alguma personagem surda que não se
encontrava na forma do original.

Algumas adaptações soam mais como um reconto, como o conto de fadas
*Cinderela Surda*, e não precisam seguir um texto escrito, mas são
baseadas nas histórias originais dos ouvintes. A história de Cinderela é
mundial, já as histórias do
[*Curupira*](#vimeoModal-292526263) ou do [*Negrinho do Pastoreio*](#vimeoModal-306082977)
pertencem à literatura folclórica brasileira, com origem na sociedade
dos ouvintes brasileiros, mas que foram contadas em Libras.
Observamos, também, que a origem da literatura
em língua de sinais não precisa ser necessariamente de textos escritos.
Há muitos exemplos de traduções de filmes, peças de teatro, desenhos
animados e cartuns.

Apesar de não ser de origem surda e de não tratar do assunto específico
das vidas dos surdos, muitas vezes, essa literatura em Libras traduzida
ou adaptada é apresentada por surdos, destinada a surdos, na língua
gestual-visual-espacial dos surdos e faz parte da literatura surda. Há
traduções feitas por ouvintes incluídas na literatura surda porque o que
importa dentro dessa perspectiva é a língua de apresentação e o
público-alvo.

#### 3.2.2. Modalidade de literatura em Libras: sinalizada e escrita

Assim como a língua portuguesa pode ser falada e escrita, também a
**Libras existe em duas modalidades,** sinalizada e escrita, embora a
Libras escrita ainda não seja muito comum.

A literatura surda em língua de sinais se realiza, normalmente, na
modalidade sinalizada e muitos elementos dessa forma de arte são
fundamentados no fato daquela ser uma literatura visual “de performance”
e “do corpo”, que existe apenas quando uma pessoa a apresenta. Assim, é
quase impossível separar o corpo do artista do texto da narrativa ou do
poema. Nas literaturas escritas, tais como a brasileira, escrita em
português, estamos acostumados à ideia de que o texto pode ser separado
do autor e quando o lemos recebemos pouca informação sobre o aspecto da
pessoa que o escreveu. Já, na literatura sinalizada em Libras, podemos
ver o artista como ator e vemos os poemas, as narrativas ou as piadas
pelo meio visual através do seu corpo.

Entretanto, a literatura surda em língua de sinais não se desenvolve
somente na modalidade sinalizada. Existem alguns exemplos de literatura
em **Libras escrita**, especialmente, e atualmente, em SignWriting. Esse
sistema já foi usado para as histórias infantojuvenis traduzidas ou
adaptadas por razões didáticas (MARQUEZI, 2019). O livro *Cinderela
Surda* (SILVEIRA; KARNOPP; ROSA, 2003) é um exemplo, bilíngue, escrito
em português e Libras.

Outros livros escritos em SignWriting são textos originais em Libras,
como as lendas indígenas *Onze histórias e um
segredo - Desvendando as lendas Amazônicas*, de Taísa Sales (2016).
Também há poemas em Libras, criados como literatura surda e escritos em
SignWriting, como os poemas dos artistas Maurício Barreto e Kácio
Evangelista. O poema abaixo, *Comunidade*, de Evangelista (2018) mostra
as possibilidades da literatura em Libras escrita. Podemos ver que os
sinais escritos SURDO, OUVINTE e SINAIS são organizados para
criar uma imagem língua (Figura 1).

![Poema Comunidade](/images/p1-c3-1.png)

<div class="legenda"></div>

Figura 1: *Comunidade*, de Evangelista (2018) Fonte: Evangelista (2018)

#### 3.2.3. Literatura Visual?

Até esse ponto, falamos de literatura produzida numa língua, em Libras
ou em português. A literatura visual é uma categoria de literatura que
dá prioridade às imagens visuais, especialmente às produções não
verbais. Assim, os teatros sem palavras e a mímica, os livros de imagem,
os gibis e as histórias em quadrinhos fazem parte também da literatura
visual. Muitas dessas formas de literatura visual produzidas por pessoas
ouvintes são acessíveis à comunidade surda por não usarem uma língua
baseada no som, apesar de não serem literatura surda (porque não
satisfazem nenhum dos nossos quatro critérios de literatura surda).
Também, há literatura visual não verbal que não utiliza a Libras (por
exemplo mímicas e histórias em quadrinhos), mas que faz parte da
literatura surda pelo fato de ser feita por surdos, por abordar assuntos
que destacam as experiências dos surdos e destinada ao público surdo.

Porém, lembramos que a **literatura em Libras é verbal,** embora as
línguas de sinais sejam gestuais-visuais-espaciais e a literatura surda
tenha o objetivo de criar imagens claras para o público.

Existe um *continuum* de linguagem com a intenção de criar imagens
visuais em forma de gestos. Os gestos vão além de uma língua de sinais
para criar literatura visual sinalizada, que não é a língua, mas
derivada da estrutura visual desta; essa é uma técnica chamada
**Vernáculo Visual** ou **VV.** Falaremos mais a respeito nos capítulos
7 e 10, mas precisamos ainda de muito mais pesquisa sobre essa arte para
entender o seu lugar na literatura surda. [*Golf
Ball*](#ytModal-Gl3vqLeOyEE), de Stefan Goldschmidt, um alemão
surdo, é um bom exemplo de VV. Esse é um conto sobre uma bola de golfe,
que apresenta imagens fortemente visuais, sem qualquer vocabulário de
nenhuma língua de sinais, mas que utiliza gramaticalmente o aspecto
viso-espacial das línguas de sinais. No Brasil,
[*Tinder*](#vimeoModal-267275098), por Anna Luiza
Maciel, é outro exemplo que recorre à técnica.

### 3.3. Diversas perspectivas sobre a categorização de literatura

Vimos que há diversos elementos que fazem parte da categorização da
literatura surda. É assim em qualquer tipo de literatura. Algumas
definições de tipos de literatura falam do material produzido durante um
determinado período (por exemplo: “literatura do século XIX”), outras
destacam literatura destinada a um grupo em particular (como a
“literatura infantil”) ou produzida por autores de um país ou de um
grupo particular (“literatura francesa” ou “literatura negra”). Um
conjunto de literatura pode ser aquilo que foi produzido sobre um
determinado tópico (“literatura de viagens” ou “literatura policial”) e
outra divisão possível é literatura escrita ou literatura oral.
Atualmente, a palavra "literatura" passou a incluir mais do que a
palavra escrita e pode significar uma forma especial de se trabalhar
criativamente com a língua em qualquer modalidade. Existem outras
categorias, mas essas servem para mostrar que podemos ver a literatura
surda a partir de diversas perspectivas.

Como categorizar o livro *O Quinze*, de Rachel de Queiroz? É
**literatura brasileira** (pensando no **país de origem** da autora, no
contexto da história e na nacionalidade dos leitores esperados)? É **um
romance** (por seu **gênero literário**)? É literatura escrita em
**português** (acessível por leitores em Portugal, Angola ou Moçambique
além do Brasil, por exemplo)? É **literatura feminina** (por ser
**escrita por uma mulher** e pelo fato de o assunto ter características
mais direcionadas às **leitoras femininas**)? É **literatura
modernista**, do século XX (o livro foi escrito em 1930)? (Figura 2
mostra um resumo).

![Livro O Quinze](/images/p1-c3-2.png)

<div class="legenda"></div>

Figura 2: Como categorizar o livro *O Quinze*, de Rachel de Queiroz?

Assim, vimos que um livro se encontra dentro de muitas categorias, todas
entrelaçadas. Do mesmo modo, podemos ver que há diversas formas de se
categorizar a literatura da comunidade surda. Podemos olhar para essa
literatura de várias maneiras, dependendo do foco, observando seus
produtores, seu público, o assunto, sua língua e se é sinalizada ou
escrita. Vale destacar que, as produções de
literatura surda não necessitam cumprir todos os critérios já destacados
nesse capítulo: 1) **ser feita por surdos;** 2) **tratar da experiência
de ser surdo** **e do conhecimento da cultura surda;** 3) ter o objetivo
de **atingir um público surdo** e 4) ser apresentada **em Libras.**

O poema *Lei de Libras,* de Anna Luiza Maciel e Sara Theisen Amorim,
contempla esses quatro critérios: foi criado e apresentado por autores
surdos, fala de um assunto da comunidade surda (o prazer de usar
Libras), é destinado principalmente ao público surdo e foi apresentado
em Libras. Porém, muitas produções cumprem apenas três ou até dois
critérios e mesmo assim fazem parte da literatura surda.

A narrativa *O Negrinho do Pastoreio,* de Roger Prestes, foi apresentada
em Libras por um autor surdo e é destinada principalmente ao público
surdo, mas tem origem não surda e não fala da experiência dos surdos.

O Livro *A Verdadeira Beleza*, de Vanessa Lima Vidal, foi escrito por
uma autora surda e fala da experiência de uma pessoa surda. Não está
apresentado em Libras, mas em português, e o público esperado é tanto de
ouvintes quanto de surdos.

A história *A Rainha das Abelhas*, contada por Mariá de Rezende Araújo,
foi apresentada por uma tradutora ouvinte e tem origem não surda (por
ser um conto de fadas dos irmãos Grimm), então não fala da experiência
dos surdos. Porém, está contada em Libras e é destinada ao público
surdo.

A narrativa *Golf Ball*, por Stefan Goldschmidt, é feita por um ator
surdo e destinada (principalmente) aos surdos, porém não fala da
experiência surda e não usa Libras, porque usa a técnica VV, que está na
fronteira da comunicação verbal-não verbal.

A seguinte tabela mostra essas possibilidades em resumo

![Livro O Quinze](/images/p1-c3-tabela.png)

Com essas ideias em mente, entendemos que a literatura surda pode ser
estudada de diversas abordagens e que a literatura em Libras faz parte
fundamental de tudo.

<div class="float"></div>

![objetivo](/images/atividade-parte1.png)

### 3.4. Atividade

**Assista novamente aos vídeos:**

* [*As Borboletas*](#ytModal-mi8bnByv4S4) de Vinicius de Moraes, traduzido por Wilson Santos.

* [*As Brasileiras*](#vimeoModal-242326425), de Klícia
Campos e Anna Luiza Maciel.

* [*Como veio alimentação*](#ytModal-nMOTYprbYoY) , de Fernanda Machado.

* [*Leoa Guerreira*](#ytModal-rfnKoCXmSg4), de Vanessa Vidal.

* [*Hino Nacional Brasileiro*](#ytModal-STrLJipI18Q), de Bruno
Ramos.

* [*O Negrinho do Pastoreio*](#vimeoModal-306082977), de
Roger Prestes.

* [*Unexpected moment*](#ytModal-hD48RQLQurg) (“Momento
inesperado”), de Amina Ouahid e Jamila Ouahid.

**Para cada um, decida:**

* É literatura surda?

* É literatura em língua de sinais?

* É literatura em Libras?

* É literatura sinalizada?

* É literatura em VV?

