---
capitulo: 18
parte: 3
titulo: Repetição, ritmo e rima
pdf: "http://files.literaturaemlibras.com/CP18_Repeticao_ritmo_e_rima.pdf"
video: zfOBLBnKfTo
---

## 18. Repetição, ritmo e rima

Neste capítulo, focaremos nos elementos que são repetidos na literatura
em Libras e nos efeitos literários dessa repetição, a exemplo do ritmo e
da rima.

Um dos principais efeitos da repetição na literatura em Libras é o de
destacar a linguagem, colocá-la no primeiro plano. No uso da língua
cotidiana, normalmente seguimos as regras do próprio idioma e, assim, a
forma da língua tem pouca coisa de inesperado. Por isso, não percebemos
a linguagem que usamos para comunicar e focamos principalmente na
mensagem comunicada. A língua pode se tornar “notável” ou se destacar
quando quebramos suas regras. Isso chama atenção a ela. Desse modo, na
literatura em Libras podemos criar um significado adicional usando
poucas palavras.

A ruptura de regras pode acontecer de duas formas. Primeiramente, quando
fazemos alguma coisa que a língua normalmente não faz, e a isso chamamos
de “irregularidade notável”. Podemos quebrar as regras do léxico na
criação de palavras ou sinais que ninguém jamais viu antes, ou podemos
mexer com as normas semânticas e usar sinais já existentes, mas com
novos sentidos metafóricos. A irregularidade notável em Libras acontece
cada vez que um artista literário cria um sinal agradável com
classificadores ou pela incorporação (ver capítulo 04), ou, também,
quando brinca com a estrutura dos sinais (como veremos no capítulo 19
sobre o humor em Libras) (QUADROS e SUTTON-SPENCE, 2006).

Outra forma de colocar a língua no primeiro plano é usando um elemento
já existente nela, que tenha frequência incomum, para realçar ou
destacar a linguagem utilizada no poema. Dessa forma, temos a
“regularidade notável”. O uso da repetição para criar uma frequência
incomum é o nosso foco neste capítulo.

<div class="float"></div>

![lista de videos](/images/videos-parte3.png)

<div class="lista-de-videos"></div>

Vamos falar dos seguintes vídeos na nossa exploração do assunto:

* *[Amar é também
silenciar](#ytModal-Oz5YqANdunU),* de Rafael
Lopes dos Santos (Lelo).

* *[Árvore](#vimeoModal-267272296),* de André Luiz
Conceição.

* *[As Brasileiras](#vimeoModal-242326425),* de Anna
Luiza Maciel e Klícia Campos.

* [*Ave 1 x 0 Minhoca*](#vimeoModal-267277312), de Marcos
Marquioto.

* *[A Pedra Rolante](#ytModal-kPXWu5UCTzk),* de Sandro Pereira.

* *[Cinco Sentidos](#ytModal-AyDUTifxCzg),* de Nelson Pimenta.

* *[Como Veio Alimentação](#ytModal-nMOTYprbYoY),* de Fernanda
Machado.

* *[O Jaguadarte](#ytModal-DSSXTh0wriU),* de Aulio Nóbrega.

* *[Lei de Libras](#vimeoModal-267274663),* de Sara
Theisen Amorim e Anna Luiza Maciel.

* [*Meu Ser é Nordestino*](#ytModal-t4SLooMDTiw),
de Klícia Campos.

* *[O Modelo do Professor Surdo](#ytModal-rverroKm8Bg),* de Wilson
Santos Silva.

* *[Peixe](#ytModal-LEDC479z_vo),* de Renato Nunes.

* *[Saci](#ytModal-4UBwn9242gA),* de Fernanda Machado.

* *[Tinder](#vimeoModal-267275098),* de Anna Luiza Maciel.

* *[Tree](#ytModal-Lf92PlzMAXo),* de Paul Scott.

* *[Voo sobre Rio](#ytModal-YaAy0cbjU8o),* de Fernanda Machado.

### 18.1. Efeitos da repetição

A repetição cria padrões que se destacam como incomuns. Acima de tudo,
em muitos poemas, ela cria um efeito estético, fazendo-os parecerem
elegantes ou divertidos. Admiramos a habilidade do poeta de manter
rigorosamente os padrões repetitivos. Normalmente, no entanto, repetir
um elemento por muito tempo pode gerar cansaço no público espectador,
então a repetição é usada cuidadosamente com a intenção de se criar
outros efeitos. Podemos destacar as relações inusitadas entre palavras e
ideias, criando um significado que vai além no poema.

A repetição pode acarretar tensão antes de uma resolução. Por exemplo,
no poema [*Ave 1 x 0 Minhoca*](#vimeoModal-267277312), de Marcos
Marquioto, a reprodução dos sinais que mostram os movimentos da ave e da
minhoca durante a caça aumenta a tensão entre elas, até que a ave pega a
minhoca.

É a repetição que cria a simetria na literatura em Libras, como
estudamos no capítulo 15. Sem ela, não temos simetria fora de uma mão,
porque a simetria faz parte de padrões e precisa de mais do que uma
unidade para criá-los.

Repetir sinais ou parâmetros de sinais também contribui à criação de
imagens visuais fortes e agradáveis. Ver os novos sinais estéticos ou
outros elementos irregulares múltiplas vezes em um poema serve para
destacar a criatividade. Por exemplo, no poema *[Voo sobre Rio](#ytModal-YaAy0cbjU8o)*, de
Fernanda Machado, o sinal que descreve o Pão
de Açúcar com o teleférico é enfatizado ao ser repetido pela segunda
vez.

### 18.2. Elementos repetidos

Veremos muita repetição em um poema (também em muitas narrativas, mas
principalmente nos poemas), e esse recurso pode gerar ritmo. Essas
repetições podem acontecer em todos os níveis: no início da frase, no
final dela ou, simplesmente, “às vezes”, ou seja, de forma quase
aleatória.

#### 18.2.1. A repetição de um sinal

No nível do léxico, o artista pode repetir um sinal. Por exemplo, no
poema *[Tree](#ytModal-Lf92PlzMAXo)*, de Paul Scott, e nos
poemas-homenagem a este como
[*Árvore*](#vimeoModal-267272296), de André Luiz
Conceição e , de Fernanda Machado, vemos repetido o sinal com movimento
circular <span
style="font-variant:small-caps;">sol-nasce-e-se-põe</span>. Cada vez que
vemos esse sinal, sabemos que o tempo passa antes de um novo evento na
vida da árvore, e isso dá um ritmo confortável ao poema. O mesmo sinal
aparece no poema [*Amar é também
silenciar*](#ytModal-Oz5YqANdunU), de Rafael
Lopes dos Santos (Lelo), para dividi-lo em seções sobre os diferentes
amores.

#### 18.2.2. A repetição de uma frase

No nível da frase, veremos repetição numa sequência de sinais. No poema
*[Cinco Sentidos](#ytModal-AyDUTifxCzg)*, contado em Libras por Nelson
Pimenta, o personagem pergunta a cada sentido (tato, paladar e olfato)
“Você, quem é?”, e o sentindo responde “Quem sou? Olhe para mim. Vamos
juntos”; depois cada um começa a descrever quem é, antes de falar “Você
me conhece agora”. Essa repetição tripla promove um sentimento de prazer
em se ver um padrão conhecido, mas também cria um momento inesperado
quando o quarto sentido não dá a mesma resposta que os demais.

A obra *[A Pedra Rolante](#ytModal-kPXWu5UCTzk)*, de Sandro Pereira,
mostra muita repetição, inclusive a da sequência dos sinais
classificadores dos personagens caçando a pedra. Vemos a mesma repetição
dos personagens já introduzidos e depois o mais recente. Por exemplo,
primeiro vemos a pedra passando, depois a pedra e o cavalo; na terceira
vez vemos a pedra, o cavalo e a mulher; na quarta vez, vemos a pedra, o
cavalo, a mulher, o idoso e o pássaro; depois observamos a pedra, o
cavalo, a mulher, o idoso, o pássaro e o gordo; e finalmente a pedra, o
cavalo, a mulher, o idoso, o pássaro, o gordo e os surdos. Com os
participantes na fila da caça, cada sinal se repete com o mesmo
movimento em diversas direções. Assim como no poema , a repetição produz
uma sensação prazerosa ao ver o padrão crescente e, também, provoca
admiração diante das habilidades do poeta em fazer tantas repetições sem
cometer um erro.

#### 18.2.3. Repetição gramatical de tipos de sinais

Já sabemos que a Libras tem diferentes tipos de sinais, como de
vocabulário, classificadores de diversas perspectivas e de incorporação.
O poeta pode criar um padrão de repetição ao repetir os tipos de sinais
desejados.

Na narrativa poética [*A Pedra Rolante*](#ytModal-kPXWu5UCTzk),
Sandro Pereira apresenta uma sequência de sinais manualmente e depois os
mesmos movimentos não manualmente. Sem ver a primeira sequência, o
público não vai entender o encadeamento não manual, mas as duas juntas
criam um efeito muito agradável e permitem que o público use a
imaginação para criar a imagem visual usando as sugestões dos olhos e a
memória dos sinais já apresentados.

O poeta também pode repetir o mesmo evento a partir de perspectivas
diferentes. Por exemplo, em *[Tinder](#vimeoModal-267275098)*, de Anna Luiza Maciel, cada ação da mulher mexendo no
celular é repetida da perspectiva do celular.

Podemos criar um padrão repetido de sequências escolhidas dos sinais de
vocabulário, de classificadores e de incorporação. O poeta escolhe o
encadeamento, por exemplo “sinal – incorporação – classificador”, “sinal
– incorporação”, “sinal – classificador”, ou “incorporação –
classificador”. No poema [*A Pedra
Rolante*](#ytModal-kPXWu5UCTzk), Sandro Pereira incorpora cada
novo personagem que entra na caça e depois mostra o classificador
correspondente a cada um deles.

Dentro dos classificadores, temos a opção de escolhê-los segundo
perspectivas diferentes. Por exemplo, no poema , de Marcos Marquioto,
cada apresentação da ave e da minhoca tem um classificador que mostra os
dois animais cada vez mais próximos.

Essas repetições, já descritas, de sinais individuais, de uma estrutura
sintática gramatical ou até mesmo de um discurso, podem simplesmente
provocar uma emoção de prazer no público, mas, também, criar a própria
estrutura do poema, delineando unidades parecidas com estrofes ou com o
conceito de verso nas línguas escritas (lembrando que os poemas
sinalizados em Libras não têm versos da mesma forma que os poemas
escritos em português porque não são escritos).

#### 18.2.4. A repetição dos elementos internos aos sinais

A poeta surda Dorothy Miles, nos anos 70, começou a criar poemas em ASL
com diferentes sinais de mesma configuração de mãos. Ela descreveu essa
repetição como sendo parecida com a rima nas línguas orais. Vemos a
repetição dos parâmetros dos sinais hoje em Libras especialmente nos
poemas mais líricos. Em [*Meu Ser é Nordestino*](#ytModal-t4SLooMDTiw),
de Klícia Campos, a mesma configuração de mão ocorre nos sinais <span
style="font-variant:small-caps;">ave-voando</span>, <span
style="font-variant:small-caps;">rio-descendo-das-montanhas</span> e
<span style="font-variant:small-caps;">rio</span>. Uma outra
configuração de mão acontece nos sinais <span
style="font-variant:small-caps;">terra-seca, árvore-torta</span> e <span
style="font-variant:small-caps;">sol-forte</span>. No haicai
[*Peixe*](#ytModal-LEDC479z_vo), de Renato Nunes, todos os
sinais usam a mesma configuração de mão. Podemos até manter esse
parâmetro para criar um sinal “errado” para sustentar o padrão. Em , de
Fernanda Machado, o pássaro macho sinaliza que a fêmea é surda como ele.
Os sinais <span style="font-variant:small-caps;">surdo</span> e <span
style="font-variant:small-caps;">igual</span> têm movimento e locação
natural, mas a configuração de mão dos dois é “errada” por ser a mesma
dos outros sinais naquele trecho que usa o classificador do pássaro com
bico.

Embora Dorothy Miles sugira que a repetição de um parâmetro estrutural
dos sinais possa criar um efeito parecido com a rima, tal repetição
também é semelhante à aliteração nos poemas escritos e orais. Na rima, a
parte final de diversas palavras tem as mesmas letras ou os mesmos sons.
Em português, as palavras “patas” e “latas” ou “pura” e “dura” criam o
efeito de rima porque a porção final das palavras é igual. Na
aliteração, a primeira parte de palavras diferentes é igual. Vemos
exemplos de rima e aliteração no poema *Violões que choram* (do poeta
simbolista João Cruz e Sousa, 1897).

<h5 style="text-align:center">Violões que choram</h5>

<div class="legenda"></div>

(do poeta simbolista João Cruz e Sousa, 1897).

<div class="poesia"></div>

> Vozes veladas. Veludosas vozes.\
> Volúpias dos violões, vozes veladas\
> Vagam nos velhos vórtices, velozes\
> Dos ventos, vivas, vãs, vulcanizadas.

Há rima em “vozes” e “velozes” e no par “veladas” e “vulcanizadas”; e
vemos aliteração em todas as palavras que começam com a letra “V”.

Em Libras, o uso de sinais com a mesma configuração de mão, locação ou
direção de movimento pode gerar um efeito parecido, mas não igual à rima
ou à aliteração. Esses efeitos, em português, são criados dentro da
estrutura temporal de uma palavra, mas em Libras, os parâmetros existem
simultaneamente. Dificilmente podemos dizer que a configuração de mão
vem antes ou depois do movimento num sinal, porque a mão que se move já
deve ter uma configuração.

A rima normalmente acontece no final do verso nos poemas em português,
mas em Libras não ocorre assim. A repetição pode gerar um tipo de
“estrofe” em que uma seção do poema usa vários sinais do mesmo parâmetro
e a seção seguinte usa outros sinais de outro parâmetro, mas,
principalmente, ser um recurso estético para se criar efeitos estéticos,
simbólicos e metafóricos.

### 18.3 Ritmo

Ritmo é o efeito que percebemos quando os padrões de repetição são
organizados no espaço ou no tempo. Em Libras, o ritmo vem do fluxo
visual dos sinais, e vemos padrões de tempo (por exemplo, variação
organizada na velocidade ou duração do movimento de sinais e pausas
entre eles), ou da ênfase do movimento (alteração entre movimento agudo
e suave, por exemplo). Qualquer tipo de padrão nos sinais e de variações
desse padrão pode criar um ritmo visual e temporal. Por exemplo, o
aumento ou a diminuição no número de dedos usados em uma forma de mão, a
alteração no tamanho de uma imagem ou o uso das diferentes articulações
(dedo, punho, cotovelo ou ombro).

Cada uma das repetições descritas pode gerar um ritmo, mas o tempo, a
qualidade e a duração dos movimentos dos sinais contribuem muito à
sensação de ritmo (KLAMT, MACHADO e QUADROS, 2014).

#### 18.3.1. Repetição do tempo 

A duração, a velocidade, o tipo de movimento de um sinal e a duração das
pausas criam um ritmo nos poemas. O poeta surdo Clayton Valli (1993)
afirmou que o ritmo da poesia sinalizada se encontra nas mudanças que
ocorrem dentro dos sinais ou na transição de um sinal para o próximo
(nos "movimentos") e em períodos sem mudança (nas "pausas"). Usando seus
próprios poemas em sua pesquisa, ele categorizou as maneiras pelas quais
podemos criar o ritmo de poesia em ASL. Podemos usar os mesmos critérios
para Libras.

Ele destacou quatro categorias de movimentos e pausas que poetas
manipulam para criar um ritmo poético:

1. Ênfase na suspensão (pausa longa, pausa sutil, pausa brusca)
1. Ênfase no movimento (longo, curto, alternado, repetido)
1. Tamanho do movimento (movimento de trajetória ampliada, movimento
reduzido, movimento de trajetória reduzida, movimento acelerado)
1. Duração do movimento (regular, lento ou rápido)

No poema *Lei de Libras*, de Sara Theisen
Amorim e Anna Luiza Maciel, o ritmo forte é criado parcialmente pela
duração dos movimentos dos sinais e pelas pausas. A velocidade mais
rápida e a ênfase no movimento criam uma energia nos sinais que gera uma
impressão de leveza e de alegria. Os sinais em , de Klícia Campos, por
outro lado, têm uma redução da velocidade, são mais lentos, os
movimentos mais longos com pausas destacadas, até que a protagonista
rejeita a opressão e o sofrimento. A partir desse ponto, os movimentos
são mais curtos e reduzidos, o início e o término dos sinais são mais
acentuados, dando a ideia de mais energia e firmeza.

Já que vimos algumas possibilidades para repetição de elementos e os
seus efeitos, passamos agora para o número de repetições e os padrões
criados.

### 18.4. Padrões de repetição

Podemos repetir elementos poéticos várias vezes.

#### 18.4.1. Duas vezes

Na literatura em Libras, é mais comum apresentar os elementos duas
vezes. Como vimos no capítulo 15, isso cria uma sensação de equilíbrio
muito satisfatória para o público. Como o corpo humano tem dois braços e
duas mãos, duas pernas e dois pés, temos uma estabilidade que não temos
com apenas um desses membros. Conceitualmente, também vimos que o fato
de se falar de duas coisas cria um contraste entre elas. Como temos duas
mãos e o espaço de sinalização é facilmente dividido nos lados esquerdo
e direito, fica fácil recorrer à quantidade de “dois” para os
referentes.

No poema , de Klícia Campos, vemos dois rios descendo de duas vertentes,
cada rio sinalizado por uma mão no lado esquerdo e outra no direito.
Vemos a vasta área da terra seca sinalizada por duas mãos. Vemos as
bandoleiras carregadas de balas nos dois lados do corpo, cada uma
sinalizada por uma mão.

Para falar de duas pessoas, temos a possibilidade do contraste, que cria
cooperação ou conflito entre elas. No poema *[O Modelo do Professor Surdo](#ytModal-rverroKm8Bg)*, de Wilson Santos Silva, vemos o padrão de
uma repetição, apresentado “duas vezes”, para dar a ideia de cooperação.
No primeiro contraste, temos o professor que sabe Libras e os alunos que
não sabem, que dão o contraste. No segundo, temos dois alunos. Em
seguida, há a repetição de uma pessoa em duas épocas: o professor como
adulto que sabe Libras e o professor na sua infância quando ele também
não sabia a língua. Finalmente, temos o atual professor e o seu próprio
professor. Essas comparações de dois tipos de personagens criam muito
interesse pelo poema. Os mesmos sinais são repetidos; por exemplo, vemos
primeiramente os alunos surdos fazendo gestos sem entender a vida, e
depois os mesmos sinais do professor fazendo gestos quando ele era
aluno, igual aos alunos dele. Vemos os sinais que o professor antigo
ensinou ao atual professor e como ele aprendeu e, na repetição no poema,
vemos o atual professor sinalizando da mesma maneira aos seus alunos.
Essa repetição de sinais dá ênfase à ideia da continuidade entre as
gerações. O adulto surdo ensina Libras à criança surda.

Em *[O Jaguadarte](#ytModal-DSSXTh0wriU)*, de Aulio Nóbrega, vemos a
possibilidade do conflito entre os personagens do herói e do monstro. No
poema de Wilson Santos Silva descrito anteriormente, os sinais são
repetidos quase que igualmente para os personagens para mostrar
cooperação e similaridade entre eles. Em *[O Jaguadarte](#ytModal-DSSXTh0wriU)*, a nobreza e a coragem do
herói sempre contrastam com o monstro terrível e feio. Com essa
contraposição na repetição dos mesmos sinais a partir de duas
perspectivas, podemos ver o ataque do monstro e como o herói se defende
dele.

No poema [*Como Veio Alimentação*](#ytModal-nMOTYprbYoY), de
Fernanda Machado, também temos um contraste entre o lado direito e o
lado esquerdo, entre a riqueza e a pobreza, entre a vida urbana e a vida
rural. A repetição não é do sinal, mas sim da seleção do dedo que cria o
sinal feito nos dois lados. A poeta usa o dedo indicador em dois espaços
contrastantes para falar de duas pessoas - a pobre que trabalha no campo
e a rica urbana que come as frutas desse trabalho. Em seguida, ela usa o
indicador e o dedo médio para a pessoa pobre e os mesmos dedos para
falar da rica. Embora os dedos para o trabalhador pobre estejam sempre
curvados e para o rico urbano estejam retos, essa apresentação do mesmo
número de dedos duas vezes cria um efeito estético e faz uma ligação
forte entre as duas vidas diferentes.

#### 18.4.2. Três vezes e três mais um 

A apresentação de três sinais, ou de um sinal três vezes, também é
comum. Apesar de o corpo humano não ter três mãos, temos três principais
articuladores em Libras: as duas mãos e o corpo central com a cabeça e o
tronco do sinalizante. O espaço também é facilmente dividido em três
partes – o lado esquerdo do corpo, o eixo central que é o corpo e o lado
direito. Leva um pouco mais de tempo apresentar um sinal três vezes do
que por duas vezes e, talvez, seja uma razão pela qual os poetas
articulam menos um sinal três vezes. No entanto, é comum se apresentar
três eventos, ou dividir o poema em três partes. Isso está relacionado à
divisão da estrutura narrativa que consideramos no capítulo 11, com
introdução, complicação e fechamento.

No poema *[Lei de Libras](#vimeoModal-267274663)*, de Sara Theisen
Amorim e Anna Luiza Maciel, vemos estruturas caraterizadas pelo número
três. Há três partes no dueto. Na primeira, as duas poetas apresentam os
mesmos sinais simultaneamente; na segunda, elas mostram as ideias em
turnos diferentes e fazem isso três vezes; na terceira, elas voltam a
sinalizar simultaneamente. Na primeira parte, elas apresentam o mesmo
sinal três vezes. Simultaneamente, cada poeta sinaliza <span
style="font-variant:small-caps;">libras</span> duas vezes – primeiro do
lado de fora e depois do lado de dentro, e, na terceira articulação,
elas criam o sinal juntas usando a mão ativa de uma e a mão passiva de
outra. Elas fazem isso também com o sinal <span
style="font-variant:small-caps;">lei</span>. O efeito estético é forte,
e cria um ritmo de 1, 2, 3.

O poema *[Voo sobre Rio](#ytModal-YaAy0cbjU8o)*, de Fernanda Machado,
não tem o ritmo interno de apresentação de sinais três vezes, mas é
dividido em três partes: a viagem de chegada da ave, o encontro com a
outra ave e a viagem de volta. [*Ave 1 x 0 Minhoca*](#vimeoModal-267277312),
de Marcos Marquioto, tem um ritmo de 1, 2, que mostra o
movimento da minhoca e o mesmo movimento da ave, mas é construído para
apresentar três perspectivas dos dois bichos: mais distante, mais ou
menos distante e mais perto – desde o momento em que a minhoca percebe a
ave e tenta fugir, até que a ave a pega.

#### 18.4.3. Quatro

É menos comum ver sinais apresentados quatro vezes ou em quatro lugares
específicos dentro do espaço de sinalização, porque isso leva mais
tempo, mas é possível de acontecer. O poema
[*As Brasileiras*](#vimeoModal-242326425), de Anna
Luiza Maciel e Klícia Campos, apresenta os sinais quatro vezes para
criar um ritmo em que as duas mulheres comparam as suas vidas. O sinal
classificador que mostra as pessoas no Nordeste é apresentado quatro
vezes, as quatro casas simples são colocadas nos dois lados da estrada;
em comparação, quatro grandes prédios são apresentados em São Paulo.
Porém, dividir um poema em quatro partes é mais comum. Nas quatro partes
desse poema, apresentadas em quatro locais, vemos que ele começa com a
introdução, que é o encontro das duas mulheres, depois, a parte central
do poema, segue com a descrição das duas paisagens, dos modos de
transporte diferentes, dos dois climas diferentes que deixam uma com
sede; e finaliza quando uma joga a água para a outra pegar.

#### 18.4.5. Muitas repetições

Até agora, o único conto poético bem conhecido em Libras com um número
alto de repetições é *[A Pedra Rolante](#ytModal-kPXWu5UCTzk),*
de Sandro Pereira (traduzida e adaptada da história “Ball Story”, do
poeta surdo Ben Bahan, em ASL). Além da pedra mágica que sai para
passear na rua, sete personagens saem para caçar (o cientista montado
num cavalo, o cachorro, a menina patinando, o idoso, o pássaro, a gorda
e os surdos), e eles passam por sete lugares. A repetição é muito
divertida, serve para criar uma sensação prazerosa e gera uma
expectativa no público com a previsão da próxima repetição. Ao se
introduzir cada novo personagem, também ocorre a repetição do uso de
classificadores e a incorporação. A repetição de elementos não manuais é
especialmente divertida nessa história. Podemos dizer que o
objetivo dela é mostrar essas repetições,
porque há tantas delas que resta pouco tempo para se contar mais sobre a
história além da caça.

<div class="float"></div>

![resumo](/images/resumo-parte3.png)

### 18.5. Resumo

Nesse capítulo, investigamos um elemento muito comum da literatura em
Libras, a repetição. Isso talvez seja fundamental para alguns tipos de
poemas em língua de sinais. Vimos que a repetição, e suas formas, sendo
de diversos tipos e níveis nas histórias e nos poemas, faz parte da
criação de um ritmo visual. Esse ritmo é divertido e estético, mas
também cria sentidos que vão além do conteúdo dos sinais. Mostramos,
também, exemplos com diferentes números de repetições influenciados pela
forma do corpo humano, articulando a literatura ao tempo necessário para
realizá-las.

<div class="float"></div>

![atividade](/images/atividade-parte3.png)

### 18.6. Atividade

Escolha um poema que tenha muitas repetições (nem todos têm, mas para
algumas definições de poesia deve haver alguma forma de repetição) e:

<div class="lista-com-letras"></div>

1. Identifique exemplos de repetição de elementos internos aos sinais
    (configuração de mão, locação, movimento, elementos não manuais);

1. Identifique exemplos de repetição do sinal; e

1. Identifique exemplos de ritmo.

**Esses elementos fazem parte da poesia? Que efeito eles geram?**

