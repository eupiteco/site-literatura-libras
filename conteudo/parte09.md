---
capitulo: 9
parte: 2
titulo: A produção de narrativas visuais 
pdf: "http://files.literaturaemlibras.com/CP09_A_producao_de_narrativas_visuais.pdf"
video: Xut7Q-T01Kg
---

## 9. A produção de narrativas visuais 

<div class="float"></div>

![objetivos](/images/objetivo-parte2.png)

### 9.1. Objetivo

Neste capítulo, vamos explorar as maneiras de produzir uma forma
fortemente visual na literatura em Libras. Como vimos no capítulo 04, os
artistas têm ferramentas diferentes para destacar a capacidade visual de
gerar emoções no público. Eles fazem isso pela apresentação de imagens
visuais e pelo uso do vocabulário, dos classificadores e da
incorporação.

Stephen Ryan foi um contador de histórias e pesquisador americano surdo.
No capítulo 06, vimos algumas dicas dele sobre a contação de histórias
em ASL. Ryan (1993) criou outra lista de sugestões para se criar
narrativas fortemente visuais em ASL. Neste capítulo, investigamos cada
uma dessas dicas. Veremos que elas também são relevantes para a
literatura em Libras.

<div class="float"></div>

![lista de videos](/images/videos-parte2.png)

<div class="lista-de-videos"></div>

Para ver alguns exemplos, vamos assistir a
alguns novos vídeos e voltar para outros já conhecidos:

* [Bolinha de Ping-pong](#ytModal-VhGCEznqljo), de Rimar Segala.

* *[O conto VV](#ytModal-0WfXyP4DoIg),* de
Lúcio Macedo.

* [*O Curupira*](#vimeoModal-292526263), de Fábio de Sá.

* *[Eu x Rato](#ytModal-UmsAxQB5NQA),* de Rodrigo
Custódio da Silva.

* [Galinha Fantasma](#ytModal-isyT8-mgCDM), de Bruno Ramos.

* [Fazenda: Vaca](#ytModal-NtN98y67ukM), de Rimar Segala.

* [A *Formiga Indígena*](#vimeoModal-355984518) *Surda,*
de Marina Teles.

* *[Leoa Guerreira](#ytModal-rfnKoCXmSg4),* de Vanessa Lima.

* *[Meu Ser é Nordestino](#ytModal-t4SLooMDTiw),*
de Klícia Campos.

* *[O Jaguadarte](#ytModal-DSSXTh0wriU),*
de Aulio Nóbrega.

* *[A Rainha das Abelhas](#ytModal-nXI4aO2_G3E),*
de Mariá de Rezende Araújo.

* *[Saci](#ytModal-4UBwn9242gA),* de Fernanda Machado.

* *[Tinder](#vimeoModal-267275098),* de Anna Luiza
Maciel.

### 9.2. Criar personagens descrevendo sua forma e seu comportamento 

A descrição da aparência física dos personagens em Libras é uma forma de
arte em si. Cria uma imagem agradável e clara destes, de modo que quando
o narrador os incorpora ao longo da história o público vai tendo uma
ideia mais concreta sobre o caráter deles.

O pesquisador de folclore Axel Olrik afirmou (em 1909) que existem
várias estratégias utilizadas nos contos folclóricos para se dar ênfase.
Isso é feito através da repetição, do tamanho e dos detalhes. Falaremos
sobre repetição e exagero no capítulo 19. Aqui, vamos focar na
importância da ênfase feita através do número de detalhes em uma
descrição. Olrik observou que nos contos folclóricos em línguas orais a
descrição detalhada não é comum (a ênfase nos contos folclóricos
geralmente ocorre através da repetição), mas em Libras ela acontece e é
recorrente.

Quando Rimar Segala apresenta os dois jogadores em [Bolinha de Ping-Pong](#ytModal-VhGCEznqljo), sabemos que a personagem feminina usa batom, um lenço
elegante e luvas. Depois disso, cada vez que Rimar incorpora a
personagem, ele apenas nos mostra seu comportamento, sem repetir os
outros detalhes. Ainda assim, podemos imaginá-la usando os elementos que
já haviam sido descritos. Isso é suficiente e produz uma imagem visual
satisfatória para o público.

Normalmente, um contista apresenta os sinais
que descrevem visualmente uma pessoa na direção de cima para baixo e,
nessa ordem, cria um movimento suave. Fábio de Sá, por exemplo,
apresenta primeiro o cocar do menino
[*Curupira*](#vimeoModal-292526263), depois os dentes,
em seguida o peito, a saia e finalmente os pés virados. Quando ele
introduz o caçador, os sinais também passam do chapéu na cabeça para o
bigode no rosto, a bandoleira carregada de balas no peito e as armas no
cinto. A história *[A Formiga Indígena
Surda](#vimeoModal-355984518),* de Marina
Teles, começa com o sinal <span
style="font-variant:small-caps;">formiga</span> na testa, depois vem a
descrição dos olhos, da boca e das penas frontais no peito, antes de
baixar um pouco as mãos para mostrar a formiga passeando na selva.

Mas nem todas as histórias fornecem essas descrições longas. Em
[Fazenda: Vaca](#ytModal-NtN98y67ukM), Rimar Segala dá apenas uma breve descrição do homem
pobre usando seu chapéu e mexendo seu queixo de uma maneira específica,
antes de dizer que ele era pobre - mas isso já é o suficiente. Quando
reencontramos o personagem novamente na história, lembramos dessa
imagem. Também é possível construir uma descrição do personagem à medida
que a história avança. Por exemplo, a descrição inicial do tio na
história [Galinha Fantasma](#ytModal-isyT8-mgCDM), de Bruno Ramos, é breve, mas aos poucos ela
se desenvolve quando o vemos comendo seu almoço e avisando o sobrinho
para não abrir os olhos se ele acordasse à noite.

Nas histórias traduzidas para Libras,
especialmente nos contos de fadas e nos contos folclóricos, podem faltar
descições visuais dos personagens porque a informação não está no texto
original. Isso se relaciona com a observação de Olrik de que os contos
tradicionais não têm tempo para descrições detalhadas porque criam
ênfase por meio da repetição. Na tradução para Libras de *[A Rainha das Abelhas](#ytModal-nXI4aO2_G3E),*
sabemos que havia três irmãos, mas a narradora descreve poucos
aspectos visuais dos personagens e dos lugares porque a tradução segue a
estrutura da história original. Essa diferença não é uma coisa ruim nem
boa, as duas tradições de contar histórias são simplesmente diferentes.
Mesmo assim, nas traduções de contos de fadas que não têm uma descição
dos personagens falta um elemento visual esperado nas narrativas
originais de Libras. Quando essas histórias são recontadas, pode-se
adicionar essa informação como parte de uma adaptação cultural surda. O
reconto de Nelson Pimenta da fábula de Esopo *[O Sapo e o
Boi](#ytModal-23HQFq0A4AY)*, por
exemplo, adiciona a descrição visual do boi e do sapo no começo da
história. Todavia, uma descrição excessiva pode criar uma história menos
atraente, porque a ação demora para acontecer. Devemos notar também que
a descrição cuidadosa é valorizada na narração de histórias, mas muitas
vezes é dispensada na poesia, para a qual a brevidade é importante.

### 9.3. Não utilizar muita soletração manual

As narrativas de Libras são especialmente valorizadas pela criação de
imagens altamente visuais que usam recursos como classificadores e
incorporação. A sequência das letras soletradas não cria uma imagem do
referente e essa é uma razão para não se ter tantas soletrações.

O ritmo suave de um texto em Libras é muito importante e o ato de
soletrar acaba mudando o ritmo dos sinais, uma vez que as regras da
estrutura da palavra são muito diferentes dos sinais não soletrados. Os
textos literários educacionais e as narrativas de experiência pessoal,
contudo, podem ter mais soletração do que os textos culturais cujo
objetivo principal é a diversão. Na narrativa *[Eu x
Rato](#ytModal-UmsAxQB5NQA),* o ator Rodrigo
Custódio da Silva soletra as palavras “ferramentas” e “rato” no início
da história, explica o que significa “ferramenta” e quais os sinais que
ele vai usar para falar dos conceitos. Porém, depois disso, a história
continua até o final sem mais soletrações, de maneira altamente visual e
com um ritmo divertido.

Muitos contos e outras histórias na língua portuguesa usam nomes de
pessoas. Em Libras, podemos soletrar esses nomes e os de locais, mas
isso acontece pouco em histórias literárias. Sabemos que há sinais na
comunidade surda para identificar as pessoas e que cada membro da
comunidade tem seu sinal particular. Alguns personagens ficcionais nas
histórias escritas também têm sinais por meio da tradução para Libras.
Branca de Neve, Peter Pan, Harry Potter e Cinderela têm seus próprios
sinais em Libras, que são importantes especialmente para os títulos das
histórias. Com certeza, o uso do sinal de identificação é culturalmente
mais adequado numa história em que há soletração, mas a cultura e a
literatura surdas costumam usar pouco os nomes. Se utilizados, servem,
por exemplo, para identificar uma pessoa que não está presente, mas,
fora isso, são pouco usados. Em muitas narrativas em Libras os nomes
simplesmente não importam.

Nas piadas em português ou em Libras, por convenção, não usamos nomes.
Ao dizermos “um homem entrou num bar” ou “uma mulher estava caminhando
na rua”, seus nomes não são evidenciados. Esse tipo de identificação
geral é muito mais comum nos gêneros literários de Libras, talvez por
causa das opções disponíveis para as duas línguas. No português,
sobretudo na língua escrita, temos poucas opções para a incorporação dos
personagens.[21](#tooltip) Assim, num conto, por exemplo, é importante saber os
nomes para que o leitor possa seguir os eventos e as ações dos
personagens. Talvez o personagem Mário seja um jovem cheio de
autoconfiança, Clarice uma idosa teimosa e João uma pessoa otimista que
sempre sorri. Essas características são mostradas em Libras na
incorporação de cada pessoa e não há necessidade de se usar nomes, nem
mesmo sinais identificadores.

### 9.4. Pode-se aumentar, dramatizar ou exagerar os personagens

Essa dica de Ryan mostra a importância de se criar uma imagem forte na
contação das narrativas. Em alguns contextos, não é adequado aumentar ou
exagerar os personagens e uma boa contadora (ou um bom contador) de
histórias sabe quando fazer, ou não, isso. As crianças gostam do exagero
e as histórias infantis muitas vezes geram sinais aumentados. A
descrição do sapo e do boi na fábula *[O Sapo e o
Boi](#ytModal-23HQFq0A4AY)* contada por
Nelson Pimenta é exagerada para atrair as crianças. Também vemos, nas
narrativas humorísticas ou assustadoras direcionadas para um público sem
especificação de idade, que o narrador pode aumentar e dramatizar as
descrições das personagens para intensificar as emoções do público. No
entanto, sem ter variação ou objetivo, o exagero cansa e é melhor
restringir o seu uso. Em uma contação de *Chapeuzinho Vermelho* podemos
exagerar na descrição do lobo mau, e talvez da vovó, mas não é preciso
dramatizar fortemente as personagens da mãe e do lenhador (ou caçador)
porque eles não são (geralmente) o foco da história.

Existem diversos recursos linguísticos para a intensificação dos
personagens. Quando se intensifica os sinais em Libras, há movimentos
mais lentos e com as partes iniciais e finais mais fortes e destacadas.
A parada (em inglês “hold”[22](#tooltip)) no final do movimento pode ser
prorrogada. Vemos movimentos maiores do que o normal e pode-se repetir o
movimento ou o sinal para enfatizar algo (VALLI, 1993). O
[*Jaguadarte*](#ytModal-DSSXTh0wriU),
de Aulio Nóbrega, faz tudo isso para criar pavor; o tempo de movimento
dos sinais no poema [*Meu Ser é
Nordestino*](#ytModal-t4SLooMDTiw), de Klícia
Campos, cria mais emoção a partir do sofrimento dos personagens; o
aumento dos sinais em
[*Tinder*](#vimeoModal-267275098), de Anna Luiza
Maciel, gera uma sensação leve que não pode ser feita com movimentos
menores.

O uso dos elementos não manuais também é muito importante no exagero. A
expressão facial, a abertura dos olhos, os movimentos do corpo, todos
geram imagens visuais fortes e emoções intensas quando aumentados.

### 9.5. Usar o corpo tanto quanto as mãos

Esse lembrete faz muita diferença nas histórias. Já vimos, em muitas
passagens deste livro, que as imagens visuais ficam mais fortes com a
incorporação dos personagens. As informações verbais estão nos sinais
manuais e as proposições são raramente feitas além das mãos, todavia a
parte emocional fica fora delas. O olhar do narrador sobre as mãos cria
um efeito no público, convidando-o a assistir às mãos da mesma maneira.
Além disso, os olhos criam um efeito de espaço e dão coerência à
história através da direção do olhar (veja mais sobre isso na seção
seguinte). A abertura dos olhos mostra as emoções por incorporação dos
personagens e o narrador pode usar essa parte do corpo para sugerir as
emoções que ele quer gerar no público.

A boca é, muitas vezes, “esquecida” por novos contadores. Talvez eles
articulem apenas as palavras da língua portuguesa, mas quem estuda
cuidadosamente a boca nas narrativas e nos poemas em Libras vai perceber
que as informações que ela carrega são riquíssimas, tanto nas
incorporações dos personagens quanto nas emoções apresentadas pelo
narrador.

Além dos olhos e da boca, Ryan fala do “corpo”, que pode incluir o
tronco e os ombros. Quanto mais a narrativa se aproxima das técnicas
teatrais, mais veremos o uso do peito, dos ombros, do tronco e até das
pernas e dos pés. Em [*Saci*](#ytModal-4UBwn9242gA), Fernanda
Machado usa muito os ombros e o tronco para apoiar os sinais manuais. Na
narrativa poética cinematográfica [O conto VV](#ytModal-0WfXyP4DoIg),
Lúcio Macedo usa muito o corpo, inclusive os pés, e se desloca no palco
durante a performance. O uso do corpo abaixo da cintura é uma
característica da mímica, ou às vezes do gênero VV. Normalmente, as
narrativas e os poemas em Libras não usam as pernas (ver capítulo 10).

### 9.6. Usar o espaço de forma clara, mostrando o espaço do mundo das personagens

Falaremos muito mais sobre o uso literário do espaço no capítulo 15,
mas, por enquanto, destacaremos a importância de uma disposição espacial
clara nas narrativas em Libras - devido às demandas da literatura em
Libras em criar imagens visuais.

Quando olhamos para uma imagem, esperamos que as coisas sejam dispostas
em uma ordem coerente, geralmente que esta represente a disposição dos
objetos no mundo real. Quando assistimos a um filme, esperamos a mesma
coisa, que ele seja coerente, porque estamos acostumados à gramática dos
filmes. Por exemplo, imagine um filme em que esposa e marido conversam
em um carro. A esposa está dirigindo e o marido é o passageiro.
Esperamos que a motorista olhe (brevemente!) na direção do passageiro e
este olhe para a motorista. Numa versão desse filme em Libras, mesmo que
só possamos ver a esposa na incorporação pelo sinalizante, sabemos que
ela está conversando com o marido porque ela olha para o lado direito;
sabemos que o marido está falando com ela porque ele olha para o lado
esquerdo. Se a motorista olhar para a esquerda enquanto estiver falando,
ela estará olhando pela janela do carro, e não para o marido. Isso cria
um sentido visual perfeito em um filme. Também faz todo o sentido em uma
narrativa em Libras, e os bons contadores de histórias cuidarão para que
a interação com as pessoas deixe claro quem está olhando e conversando
com quem.

Em uma obra cinematográfica, também, esperamos ver a mesma cena a partir
de perspectivas diferentes. Podemos ver o carro de fora quando o casal
entra nele, depois os dois dentro do carro e num outro momento ver
apenas uma pessoa. Também é possível vermos o que eles veem quando olham
para fora do carro. Todas essas coisas também são mostradas nas
narrativas de Libras quando o contador faz a narração com uso dos
classificadores e da incorporação. O narrador nos mostra onde estão os
diferentes objetos e as pessoas, por meio de classficadores, e vemos
como essas pessoas interagem com o espaço ao seu redor pela
incorporação, especialmente pela direção do olhar[23](#tooltip).

No cinema, como nas narrativas em Libras, às vezes também precisamos
entender a disposição de uma cena para que possamos compreender como a
ação se desenvolve. Um bom contador de histórias sabe o quanto dessas
informações devem ser dadas. Mas só vale a pena dar detalhes sobre
informações espaciais se forem relevantes. Em *[Leoa
Guerreira](#ytModal-rfnKoCXmSg4),* de Vanessa Lima, por exemplo,
há pouco esclarecimento sobre a disposição espacial da história. Só
importa que a leoa esteja correndo pela floresta. Quando ela encontra
outros leões à esquerda ou à direita, é com base na simetria da
história, porque alterar a ação à esquerda, depois à direita, depois à
esquerda e novamente à direita cria uma sensação agradável de simetria.
Em cada encontro, no entanto, é sempre claro onde a leoa está em relação
aos outros leões, porque a narradora move seu corpo para frente, para
trás, para a esquerda e para a direita.

Em [*Eu x Rato*](#ytModal-UmsAxQB5NQA),
diferentemente, Rodrigo Custódio da Silva expõe a cena na sala de
ferramentas muito claramente, porque precisamos dessas informações para
entender como a caçada aconteceu. Compreendemos onde a porta está e onde
estão as prateleiras e ele explica como viu o rabo do rato pendurado
embaixo de uma delas. As prateleiras estão à esquerda do personagem,
então é lá que ele mostra a captura do rato. Quando o bicho escapa, ele
salta da esquerda para a direita. Nos é dito, no início da história, que
a porta está à direita. Mais tarde, o narrador descreve a forma e a
localização da moldura da porta. Primeiro, ele a descreve colocando-a no
lado direito, depois ele olha para a câmera (seu público) e repete o
sinal na sua frente. Ele não quer dizer que há outra porta ali, mas ele
mostra claramente os sinais para o público. Sabemos disso porque ele
olha diretamente para o seu público durante a explicação. Isso resulta
numa boa narrativa visual. Quando vemos a forma do classificador do rato
em movimento seguindo o mesmo caminho que o narrador traçou, sabemos que
o animal corre para cima, atravessa e cai para debaixo da moldura da
porta.

Todos esses exemplos mostram a importância de se usar os classificadores
e a incorporação cuidadosamente, com a disposição espacial correta, para
se criar imagens visuais claras na literatura em Libras.

### 9.7. Usar muita repetição 

Vamos falar em mais detalhes sobre a repetição no captítulo 18. A
sugestão de Ryan de usar muitas repetições nas narrativas vem, em parte,
do objetivo de se criar imagens visuais e, por outro lado, da
necessidade que qualquer forma de linguagem não escrita tem de usar essa
estratégia. A reptição acumula ritmos à medida que padrões de sinais ou
movimentos repetidos se acumulam. Isso pode aumentar a emoção e tornar a
experiência de visualização mais agradável. Também pode ajudar os
espectadores a entenderem melhor a história.

O folclore usa muita repetição. Em sua publicação sobre folclore, o
pesquisador Axel Olrik (1909) fala a respeito desse recurso e destaca o
número “três” como o mais importante para o folclore europeu enquanto o
“quatro” é para outras tradições (por exemplo, na Índia). Stephanie Hall
(1986) apontou que o folclore de ASL usa mais as quatro repetições,
semelhantemente às tradições dos índios norte-americanos. Em Libras
vemos tanto três como quatro, em se tratando de repetições nas produções
dos artistas.

Olrik destacou, também, que os números “três”,
“sete” ou “doze” são importantes no folclore, sendo os dois últimos mais
abstratos. Por exemplo, um casal pode ter “sete filhos” ou um homem fica
preso por “sete anos”. A Branca Neve se encontra com “sete anões”. Olrik
percebeu que o foco nos contos, porém, é em “três personagens” e “três
eventos”. Em Libras, no entanto, não é tão recorrente o uso dos números
“sete” ou “doze”. Normalmente três pessoas se encaixam bem no espaço do
sinalizante porque temos três locais destacados no espaço – aos lados
esquerdo e direito e no centro – ou podemos usar uma mão para
representar cada pessoa e o corpo para a terceira.

Nos contos folclóricos descritos por Olrik, há muita repetição do número
“três”. Um rei tem “três filhos”, o príncipe precisa completar “três
tarefas” e assim por diante. Podemos ver isso na história [*A Rainha das
Abelhas*](#ytModal-nXI4aO2_G3E), traduzida para
Libras por Mariá de Rezende Araújo. Nesse conto, o casal tem “três”
filhos que saem para viajar. Na viagem, eles encontram “três” tipos de
animais – as formigas, os patos e as abelhas. Em cada encontro, os dois
irmãos mais velhos querem matá-los, o mais jovem se nega, eles desistem
e continuam a viagem. Ao chegar no castelo, o homem velho dá a eles
“três” tarefas impossíveis. Os dois irmãos mais velhos tentam, falham e
são transformados em estátuas de pedra; então o irmão mais novo faz a
“terceira” tentativa. Na “terceira” tarefa, ele deve escolher qual das
“três” princesas comeu o mel. Ao completar as “três” tarefas, ele quebra
o feitiço, todas as estátuas voltam a ser humanos e os “três” irmãos se
casam com as “três” princesas.

É muito importante que os alunos surdos aprendam essa estrutura de
conto, que é uma parte importante das literaturas mundiais (e por isso
as traduções são imprescindíveis). A Libras não tem essa mesma tradição
de contos e não usa a mesma “estrutura de três”. Às vezes, vemos isso
nas piadas (que estudaremos no capítulo 12), mas, apesar de tudo, a
literatura em Libras original não usa tanto a estrutura da repetição de
três personagens e três eventos nas narrativas. Porém, a produção de
mesmos sinais ou outros elementos sinalizados “três vezes” é muito comum
nas narrativas e nos poemas em Libras (ver capítulo 18).

Olrik falou da importância de se manter o foco
em apenas dois personagens em cada cena. Pode haver outras pessoas
presentes, mas apenas duas interagem por vez. No conto [*A Rainha das
Abelhas*](#ytModal-nXI4aO2_G3E), por exemplo, o
irmão mais novo conversa com uma formiga, um pato ou uma abelha. Às
vezes, os três irmãos estão presentes na cena, mas a narradora os
apresenta apenas como se fossem dois. Ela consegue, ao incorporar o mais
novo, falar dos dois irmãos mais velhos, mas incorpora apenas uma
pessoa, sendo uma representação dos dois. Nos outros contos já
destacados, vemos muitos deles com duas pessoas em uma cena. [*Bolinha
de Ping-pong*](#ytModal-VhGCEznqljo), de Rimar
Segala, tem quatro personagens (os dois jogadores, a bolinha e o juiz),
mas em qualquer interação o foco está em dois – por exemplo, nos dois
jogadores ou na bolinha e no juiz. No poema do
[*Saci*](#ytModal-4UBwn9242gA), de Fernanda Machado, a árvore
encontra o Saci, e diversos bichos, mas somente um por vez, sendo “a
árvore e mais um”. Na história [*Galinha
Fantasma*](#ytModal-isyT8-mgCDM),
de Bruno Ramos, o jovem e o tio interagem ou o jovem e as galinhas
descabeçadas interagem, mas nunca os três ao mesmo tempo. A
[*Leoa Guerreira*](#ytModal-rfnKoCXmSg4), de Vanessa Lima, interage
primeiro com um grupo de leões, depois com outro grupo, após com as
Misses ouvintes, mas há sempre apenas dois pontos de foco por cena. Na
interação com as Misses ouvintes, sabemos que os outros leões estão
presentes, mas eles não se manifestam.

Uma das funções de se ter duas personagens na cena é criar contraste.
Isso é muito importante nos contos folclóricos e nas narrativas visuais
em Libras. Vemos contrastes entre os irmãos mais velhos imprudentes e
arrogantes e o caçula gentil e humilde; o jogador e a jogadora; a árvore
grande e imóvel e os bichos menores que vêm e saem; o tio velho e calado
e o sobrinho jovem e curioso; a Leoa Guerreira e os leões preguiçosos, e
assim por diante. Podemos criar o contraste por meio de expressões
faciais e corporais e pelo tipo de movimento nos sinais.


<div class="float"></div>

![resumo](/images/resumo-parte2.png)

### 9.8. Resumo

Nesse capítulo, usamos a lista de dicas feita por Stephen Ryan para
compreender melhor como as narrativas em Libras podem se tornar cada vez
mais visuais e prazerosas para o público surdo. Alguns desses recursos
são semelhantes aos já evidenciados nos estudos de histórias folclóricas
dos ouvintes, porque a literatura em Libras e as histórias europeias de
folclore têm muito em comum. Porém, vimos que as necessidades visuais de
Libras tornam necessárias outras dicas, além daquelas encontradas no
folclore europeu. Podemos seguir as dicas de Ryan para entender melhor
as narrativas visuais, para compreender melhor como são feitas as
histórias em Libras e para facilitar a produção de uma literatura
fortemente visual nessa língua.

<div class="float"></div>

![atividade](/images/atividade-parte2.png)

### 9.9 Atividade

Escolha uma das obras em Libras destacadas no início do capítulo.

* Essa obra segue as dicas de Ryan?

* Qual o efeito visual ao seguir essas dicas?

<span class="tooltip-texto" id="tooltip-21">
Na escrita, fazemos isso pela forma da “pronúncia”, do dialeto ou
    do registro. Por essa razão, os textos escritos exigem mais a
    explicitação dos nomes dos personagens como modo de identificá-los.
</span>

<span class="tooltip-texto" id="tooltip-22">
Liddell e Johnson, 1989.
</span>

<span class="tooltip-texto" id="tooltip-23">
Nos países em que se dirige do lado esquerdo, tudo é ao contrário
    numa contação de histórias. Por exemplo, em BSL, o motorista olha
    para a esquerda numa conversa com o passageiro, porque os motoristas
    ingleses se sentam no lado direito do carro.
</span>
