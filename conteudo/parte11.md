---
capitulo: 11
parte: 2
titulo: A Estrutura das narrativas contadas em Libras
pdf: "http://files.literaturaemlibras.com/CP11_A_estrutura_das_narrativas_contadas_em_Libras.pdf"
video: IcgoxAJVXqQ
---

## 11. A Estrutura das narrativas contadas em Libras

<div class="float"></div>

![lista de videos](/images/videos-parte2.png)

### 11.1. Objetivo

Neste capítulo, falaremos um pouco sobre a
composição das narrativas contadas em Libras. Veremos que existe uma
sequência de estágios em muitas narrativas, mas que nem todas as
histórias seguem a mesma sequência.

Escolhemos oito histórias para falar sobre as suas
estruturas. [*A Rainha das
Abelhas*](#ytModal-nXI4aO2_G3E), de Mariá de
Rezende Araújo, [*O Curupira*](#vimeoModal-292526263),
de Fábio de Sá e [*A Formiga Indígena
Surda*](#vimeoModal-355984518) de Marina
Teles são destinadas às crianças de diversas idades. A primeira é uma
tradução de um conto de fadas europeu, a segunda é uma contação criada
usando um personagem folclórico brasileiro e a terceira é uma narrativa
curta e leve, criação da própria autora surda que mostra a importância
das configurações de mãos para crianças pequenas. [*O Negrinho do Pastoreio*](#vimeoModal-306082977), de Roger Prestes, conta uma lenda
tradicional do Rio Grande do Sul e é destinada a jovens, adolescentes ou
adultos. [*Bolinha de
Ping-pong*](#ytModal-VhGCEznqljo), de Rimar
Segala, [*Leoa Guerreira*](#ytModal-rfnKoCXmSg4), de Vanessa
Lima e [*Galinha
Fantasma*](#ytModal-isyT8-mgCDM),
de Bruno Ramos, são histórias criativas originais feitas por autores
surdos. [*Eu x Rato*](#ytModal-UmsAxQB5NQA), de
Rodrigo Custódio da Silva, é uma narrativa de experiência pessoal. Não
falaremos de todas as histórias em todas as seções, mas é possível ver
que qualquer narrativa em Libras tem elementos interessantes a serem
observados na sua estrutura.

Antes de falarmos do conteúdo das narrativas, podemos adotar outra
perspectiva, vendo-as dentro do contexto da contação de histórias. Por
isso, precisamos pensar um pouco sobre o conceito de *paratexto*.

### 11.2. O paratexto

É a parte de uma história que vai além do texto, por exemplo o título, a
capa de um livro e ilustrações como slides e imagens de vídeo. Veremos
também que, nas narrativas em Libras, o paratexto pode incluir saudações
e despedidas. Embora não seja parte do texto, o paratexto faz parte da
estrutura da obra como um todo.

#### 11.2.1. Título

Muitas histórias começam com um título. Esperamos um título para
qualquer texto escrito na literatura brasileira. Sabemos que a capa de
um livro mostra o título, mas raramente perguntamos por quê. Ele serve
para que o leitor possa encontrar a história e saber do que se trata. As
tradições de narrativas escritas precisam de títulos para serem
catalogadas e, assim, mais facilmente encontradas. Nos contextos
presenciais e orais, eles têm menor importância porque o contista já
sabe qual história vai narrar e o público pode confiar que o contista
vai contar o que eles querem.

Tradicionalmente, as histórias em Libras não tinham títulos. Porém, essa
identificação é necessária para se falar dos textos de uma forma
analítica (como neste livro) ou para a criação de qualquer coletânea de
histórias em Libras. Sem um título, não podemos identificar um trabalho
entre outros ou organizar uma coletânea. O título dos vídeos de obras em
Libras é especialmente importante para buscá-las na internet. Por
exemplo, no vídeo da narrativa [*Eu x
Rato*](#ytModal-UmsAxQB5NQA), Rodrigo Custódio da
Silva simplesmente fala que vai contar sobre uma coisa que aconteceu com
ele, mas não oferece um título. Este aparece apenas na descrição do
título do vídeo postado no YouTube, para que as pessoas possam
encontrá-lo.

Nas gravações das histórias em Libras, vemos hoje uma mistura de textos
escritos em português e sinalizados. Muitas vezes, a edição apresenta o
título escrito em português antes de o narrador apresentá-lo em Libras.

Precisamos lembrar que um título é apenas uma
maneira de identificar uma obra. Pode-se dizer também que uma obra
simplesmente “trata de um assunto”. O mesmo sinal em Libras traduz as
palavras “título” e “assunto” do português. No vídeo de Rimar Segala,
vemos as palavras escritas “Bolinha de ping-pong”, depois Rimar faz o
sinal de aspas (que tem diversas traduções em língua portuguesa como
<span style="font-variant:small-caps;">tema, assunto, sobre</span> ou
<span style="font-variant:small-caps;">título</span>), soletra as
palavras, diz o que é isso em português e finalmente sinaliza a bolinha
de pingue-pongue. Em [*O Negrinho do
Pastoreio*](#vimeoModal-306082977), Roger
Prestes dá o título em português traduzido literalmente para Libras
<span style="font-variant:small-caps;">pastoreio negro</span>, usando as
aspas para mostrar que é um título, e depois soletra as palavras. Mariá
de Rezende Araújo, na tradução do conto dos Irmãos Grimm [*A Rainha das
Abelhas*](#ytModal-nXI4aO2_G3E), sinaliza as
aspas e depois o título como é em português.

Por outro lado, muitas histórias na tradição de Libras ainda não têm
títulos. A pequena história *[A Formiga Indígena
Surda](#vimeoModal-355984518),* de Marina
Teles, começa com o sinal <span
style="font-variant:small-caps;">formiga</span>, para identificar que
ela incorpora uma formiga, mas não tem um título no vídeo. Bruno Ramos
começa [*Galinha
Fantasma*](#ytModal-isyT8-mgCDM) e
Vanessa Lima conta sobre a [*Leoa
Guerreira*](#ytModal-rfnKoCXmSg4) sem darem nenhum título.
Usamos apenas as identificações *A Formiga Indígena Surda, Galinha
Fantasma* e *Leoa Guerreira* porque os artistas combinaram assim com os
organizadores da antologia que gravaram as histórias.

#### 11.2.2. Imagens

O uso de vídeo oferece opções de paratextos
para os contistas que não estão disponíveis aos narradores que contam as
histórias ao vivo. Nesse sentido, podem-se incluir desenhos, imagens de
fotos ou de um filme antes da história ou durante ela. Nos vídeos *[A
Rainha das Abelhas](#ytModal-nXI4aO2_G3E)* e [*O Negrinho do
Pastoreio*](#vimeoModal-306082977), vemos
exemplos de imagens relacionadas às histórias antes de os contistas
começá-las. Depois de Marina Teles contar [*A* *Formiga
Indígena*](#vimeoModal-348607260) *Surda*, vemos a
imagem de uma formiga vestida como um índio que, podemos dizer, serve
como um tipo de título imagético.

#### 11.2.3. Saudações e despedidas

Vimos no capítulo 06 que um narrador pode começar com uma saudação
direta ao público. Rodrigo Custódio da Silva, contando [*Eu x
Rato*](#ytModal-UmsAxQB5NQA), inicia com uma
saudação e explica que vai contar uma história verídica. No final de [*O
Curupira*](#vimeoModal-292526263), o contador Fábio de
Sá acena com a mão para se despedir, incorporando o personagem do
Curupira que sai de cena.

### 11.3. Estrutura do enredo

O enredo é a organização de uma história, fala dos eventos e da relação
entre eles do início ao fim. Numa descrição mais simples de histórias em
geral, vemos três partes definidas: o início, a ação complicadora e o
fim - mas existem outras descrições mais detalhadas.

#### 11.3.1. Narratologia

Narratologia é o estudo da estrutura de narrativas, que objetiva
entender como estas criam os sentidos e tenta encontrar os mecanismos
fundamentais de todas as narrativas (BARRY, 2017). Na narratologia,
entendemos a história como os fatos e a sequência de eventos que
acontecem, mas é a narrativa a maneira de contar e apresentar esses
eventos. A abordagem procura ver as similaridades entre as narrativas,
mas não foca nos elementos específicos que as diferenciam; é uma forma
de entender sua estrutura.

O filósofo grego Aristóteles afirmou que uma história é uma interação
entre o caráter e a ação. Vemos o caráter do personagem pelas ações, que
é revelado durante a história. Essa ideia de desenvolvimento do caráter
sugere que o personagem muda de alguma maneira em função dos eventos que
acontecem. A proposta de Aristóteles tem foco no significado profundo de
uma história. Pelos eventos e pelas ações na história [*A Rainha das
Abelhas*](#ytModal-nXI4aO2_G3E), os irmãos mais
velhos aprendem sobre humildade e o caçula aprende a aceitar a ajuda
para superar as dificuldades. Na lenda *[O Negrinho do
Pastoreio](#vimeoModal-306082977),*
aprende-se que a crueldade que se fez ao menino não o matou e o deixou
sem todos os cavalos; o menino se transforma numa alma santa, é venerado
e liberado pelo seu sofrimento. Os caçadores em [*O
Curupira*](#vimeoModal-292526263), de Fábio de Sá, são
pessoas destruidoras, até que o Curupira os ensina a cuidar da floresta.

O formalista russo Vladimir Propp, já na primeira metade do século XX,
via a estrutura das narrativas como um conjunto de formas. Na teoria
formalista Saussuriana dos estudos linguísticos, entendemos que as
línguas são feitas de conjuntos de pequenas unidades de formas de
sentido, os morfemas. Propp sugeriu que uma narrativa também é uma
estrutura feita de uma seleção de seus elementos básicos, por exemplo:
“um membro da família sai da casa”, “o protagonista está proibido de
fazer algo”, “o protagonista faz algo proibido”, “o protagonista recebe
uma tarefa para cumprir”, “o protagonista luta contra um vilão”, “o
protagonista recebe ajuda de companheiros”, “a tarefa é cumprida” etc. A
proposta de Propp era de que qualquer história poderia ser criada a
partir dessas unidades. Não é preciso usá-las na totalidade (não
precisamos usar todos os morfemas de uma língua num discurso), mas temos
essas opções e os elementos escolhidos devem seguir uma ordem específica
(VIEIRA, 2001). Veremos exemplos das unidades de Propp nas narrativas
selecionadas aqui. Essa lista não é completa, mas serve para mostrar que
muitas narrativas em Libras seguem esses padrões apresentados pelo
autor.

Em [*Leoa Guerreira*](#ytModal-rfnKoCXmSg4), de Vanessa Lima, e
na história [*Galinha
Fantasma*](#ytModal-isyT8-mgCDM),
de Bruno Ramos, a leoa e o sobrinho saem de casa, respectivamente. O
sobrinho é proibido de abrir os olhos durante a noite, mas ele os abre.
Os protagonistas de [*Eu x
Rato*](#ytModal-UmsAxQB5NQA) e de [*O Negrinho do
Pastoreio*](#vimeoModal-306082977) recebem
uma tarefa – matar o rato, na primeira, e procurar o cavalo baio, na
segunda história – e as tarefas são cumpridas. [*O
Curupira*](#vimeoModal-292526263), de Fábio de Sá, luta
contra “vilões” caçadores e o protagonista de [*Eu x
Rato*](#ytModal-UmsAxQB5NQA), de Rodrigo Custódio
da Silva, traça um embate contra o rato. A leoa, em [*Leoa
Guerreira*](#ytModal-rfnKoCXmSg4), recebe ajuda de companheiros
na luta por ser uma Miss.

#### 11.3.2. Pirâmide de Freytag

É possível, também, utilizarmos uma análise de estrutura conhecida como
Pirâmide de Freytag que, normalmente, se organiza em até cinco partes.
Com estas podemos ver a evolução das emoções e da tensão, que vão
crescendo durante a história e depois sofrem uma redução até que o
enredo se feche com calma.

Na exposição, descreve-se a situação no início, antes dos eventos que
iremos contar. O narrador conta sobre “quem” e “onde”, apresentando os
personagens, o lugar e a situação onde os personagens se encontram.
Nessa parte, podemos ver mais descrições. Depois, chega o momento em que
acontece alguma coisa, como um evento ou uma realização que vai mudar a
situação original e criar o problema ou conflito que a narrativa vai
descrever.

No incremento da ação, vemos os problemas e os conflitos que acontecem
durante as mudanças da situação original, até que acontece o momento
mais importante, que revela o máximo da emoção ou ação, chamado de
*clímax*. Após, a narrativa apresenta o que acontece depois desse ponto
alto da história até a resolução dos problemas que fecham as ações. No
final de tudo, algum aspecto da situação inicial mudou.

#### 11.3.3. Início 

Às vezes, o narrador de Libras começa dizendo que vai contar uma
história e pode incluir uma explicação dizendo por que vai contá-la.
Rimar Segala fala “agora, vou contar uma metáfora...”, antes de começar
[*Bolinha de Ping-pong*](#ytModal-VhGCEznqljo).
Pode-se, ainda, informar se a história é verídica (ou não) ou se outra
pessoa já a contou e o narrador a está recontando (por exemplo, como em
*[O Armário](#vimeoModal-348652466),* de Juliana Lohn).

#### 11.3.4. Introdução

Uma narrativa geralmente começa com informações sobre “quem” e “onde”
para orientar inicialmente o leitor. Lembramos do capítulo 05, sobre a
importância das informações referentes à “quem” na contação de
narrativas em línguas de sinais. O conto tradicional europeu, assim como
o conto *[A Rainha das
Abelhas](#ytModal-nXI4aO2_G3E),* pode começar
falando que existia um lugar muito longe onde viveu um homem com três
filhos. *[O Negrinho do
Pastoreio](#vimeoModal-306082977),* de
Roger Prestes, começa com as informações de que existia um menino que
veio da África para o Rio Grande do Sul, que trabalhava para um
fazendeiro gaúcho cruel. [*O
Curupira*](#vimeoModal-292526263) inicia com a
apresentação da floresta e depois acrescenta que lá havia um menino. Nas
descrições desses personagens e lugares, podemos ver as imagens
fortemente visuais criadas com os sinais. Em [*Bolinha de
Ping-pong*](#ytModal-VhGCEznqljo), Rimar Segala
explica que acontece uma competição de pingue-pongue, descreve a arena
e, além de introduzir a bolinha e o juiz, detalha claramente os dois
jogadores.

As histórias em Libras contadas como narrativas pessoais não precisam de
uma descrição do personagem tão esmiuçada. Em *[Eu x
Rato](#ytModal-UmsAxQB5NQA)* é explicado
simplesmente que a mãe do narrador “eu” pediu para ele matar o rato na
sala de ferramentas e não descreve os personagens, mas sim o local. Além
disso, algumas narrativas em Libras que incorporam os personagens
(especialmente os não humanos) não precisam tanto de uma descrição
porque a própria incorporação já mostra as suas características. *[A
Formiga Indígena
Surda](#vimeoModal-355984518),* de Marina
Teles, é descrita nitidamente, mas Vanessa Lima simplesmente incorpora a
[*Leoa Guerreira*](#ytModal-rfnKoCXmSg4) sem descrição.

#### 11.3.5. Ponto de partida da ação

Depois de introduzir os personagens e o local, e de descrever a
situação, a narração conta um acontecimento importante que vai mudar a
situação e motivar os eventos que seguem. Esse é o início da
complicação, em que se apresenta um problema ou um desafio para o
personagem principal. Na história [*A Formiga Indígena
Surda*](#vimeoModal-355984518), a formiga
percebe que está nua e por causa disso começa a se vestir. Em [*Leoa
Guerreira*](#ytModal-rfnKoCXmSg4), a leoa acorda e sai em viagem
para encontrar outras leoas surdas. Na lenda *[O Negrinho do
Pastoreio](#vimeoModal-306082977),* o
cavalo foge. Na história da [*Galinha
Fantasma*](#ytModal-isyT8-mgCDM) o
ponto de partida é o momento em que o personagem abre os olhos durante a
noite e vê as cabeças das galinhas descabeçadas circulando em seu
quarto. Em *[O Curupira](#vimeoModal-292526263),* a
mudança ocorre quando os caçadores chegam na floresta.

A partir disso, começam as ações da narrativa
que respondem o que aconteceu e por quê.

#### 11.3.6. Ação, clímax e resolução

Conforme mencionamos, a ação pode aumentar até chegar ao momento mais
emocionante e forte da história: o clímax. Em [*Bolinha de
Ping-pong*](#ytModal-VhGCEznqljo), o jogo vai
ficando cada vez mais “quente” até que a bolinha sofrida pede ajuda ao
juiz e ele a tira do jogo. Na história de [*O
Curupira*](#vimeoModal-292526263), os caçadores
destroem cada vez mais a natureza até o ponto em que o Curupira grita
com eles (clímax), dando-lhes um grande susto e eles fogem da floresta.
Em [*Eu x Rato*](#ytModal-UmsAxQB5NQA), após o
rato fugir muitas vezes, o jovem consegue matá-lo com um pau. Na
história [*Galinha
Fantasma*](#ytModal-isyT8-mgCDM), o
sobrinho esfaqueia a imagem da tia e o fantasma desaparece junto com as
cabeças sem corpo das galinhas para deixar em paz a alma dela.

Apesar de esse padrão ser muito comum,
lembramos que nem todas as histórias têm um clímax. Talvez a sequência
de ações simplesmente termine de uma forma satisfatória para o público.
No final da história [*A Formiga Indígena
Surda*](#vimeoModal-355984518), depois que
a formiga coloca diversas roupas e pinta o rosto, ela fica completamente
vestida. Não há mais a necessidade de contar e assim a história termina.

#### 11.3.6. Conclusão - *coda*

A narrativa pode terminar com uma *coda*, que é a parte final em que se
oferece a conclusão[28](#tooltip). Pode ser uma observação sobre o que aconteceu
depois da narrativa, uma explicação, uma justificativa, uma opinião que
fecha a história ou uma mudança inesperada.

No desfecho do conto tradicional [*A Rainha das
Abelhas*](#ytModal-nXI4aO2_G3E), a coda se
localiza quando, depois de conseguir fazer as três tarefas, o irmão mais
novo se casa com a princesa e vira rei, enquanto os outros irmãos
aprendem a ter um bom coração e a serem justos; então estes se casam com
as duas irmãs da princesa e os três casais vivem felizes para sempre.
Essa coda “viver feliz para sempre” é tradicional dos contos de fadas,
ela fecha a história de uma forma eficaz para um público infantil.

No final da história da [*Leoa
Guerreira*](#ytModal-rfnKoCXmSg4), sabemos que depois de motivar
as outras leoas a conquistarem as coisas como ela fez, a leoa guerreira
viveu bonita e orgulhosa até uma idade avançada.

Em algumas narrativas, especialmente nas fábulas, a coda é a explicação
para o público do que significa a história. Em [*O
Curupira*](#vimeoModal-292526263), o narrador explica
diretamente ao público que precisamos cuidar da natureza. Mas, nem
sempre recebemos as informações que esperamos na coda, e o narrador pode
usá-la para deixar a narrativa aberta. No final da história da [*Bolinha
de Ping-pong*](#ytModal-VhGCEznqljo), nenhum dos
jogadores aceita a bolinha, e a coda é uma pergunta (não manual)
simples: “e aí?”. O público que assiste à história deve decidir como
resolver essa nova situação.

Vemos um exemplo de opinião dada na coda da história de Rodrigo Custódio
da Silva, [*Eu x Rato*](#ytModal-UmsAxQB5NQA), em
que ele explica que, quando a mãe, com orgulho dele, o agradece por
matar o rato, ele responde “sem problemas”, mas depois sacode fortemente
a cabeça para dizer “que saco, hein?”.

No desfecho da história [*Galinha
Fantasma*](#ytModal-isyT8-mgCDM),
de Bruno Ramos, há uma coda inesperada. O público entende que todas as
cabeças sem corpo das galinhas desapareceram quando a alma da tia ficou
em paz, mas o último sinal é de mais uma cabeça que vai diretamente à
câmera, até quase tocar na lente. A coda então é de uma pergunta: será
que *todas* as cabeças desapareceram?

### 11.4. Estrutura de narrativa pessoal

As narrativas pessoais normalmente não são ficções, mas ainda podem ter
uma estrutura parecida com outras narrativas fictícias. Nem todas as
histórias que falam de experiência pessoal são iguais. Algumas são
contadas e recontadas como uma história preparada, planejada e
aperfeiçoada durante os momentos de repetição. Pode ser que sejam
contadas por contadores com habilidades artísticas; e eles utilizam as
mesmas formas de linguagem literária que vemos nas narrativas de ficção.
Por outro lado, todos nós contamos as histórias da nossa vida, sem
preparação, simplesmente com o objetivo de compartilhar uma experiência.

O pesquisador linguista William Labov (1972) descreveu seis estágios de
uma narrativa pessoal que têm muita semelhança com a estrutura do enredo
descrito por Freytag, sendo eles: resumo, orientação, ação complicadora,
resolução, avaliação e coda. No resumo, o narrador começa a contar a
história; na orientação, sabemos quem ou o que participa da história,
onde e quando; na ação complicadora, aprendemos o que aconteceu; na
resolução vemos o que ocorreu no final para terminar a sequência de
eventos; na avaliação, o narrador explica porque isso importa; e, na
coda, o narrador oferece um comentário sobre a história em geral.

*[Eu x Rato](#ytModal-UmsAxQB5NQA)* é uma
narrativa de experiência pessoal que tem muitos elementos da Pirâmide de
Freytag. Embora não haja uma descrição do personagem, porque este é o
narrador, ele descreve claramente o local do evento. Um ponto
*climático* é quando a rato foge do pau e pula, mas isso não é o clímax,
que só acontece quando o narrador finalmente mata o rato.

Trazendo a história para o modelo de estágios destacados por Labov,
podemos ver o resumo, a orientação, a ação complicadora, a resolução, a
avaliação e a coda, deste modo:

<div class="identado"></div>

##### Resumo 

<div class="identado"></div>

O contador, Rodrigo, avisa que é uma narrativa real do que
aconteceu com ele.

<div class="identado"></div>

##### Orientação

<div class="identado"></div>

Ele explica que a mãe pediu para ele matar o rato na sala
de ferramentas. Ele descreve detalhadamente o interior do local.

<div class="identado"></div>

##### Ação complicadora

<div class="identado"></div>

Ele encontra o rato, mas não consegue bater nele com
o pau. Em um certo momento, consegue prender o rato pelo rabo com o
objeto, mas ao esticar o braço para pegar outro pedaço de madeira ele
move o pau que está prendendo o bicho e acaba soltando o rabo dele. O
rato pula perto do seu rosto. Daqui para frente as ações ficam sempre
mais intensas, até que ele mata o rato.

<div class="identado"></div>

##### Resolução

<div class="identado"></div>

Finalmente ele consegue matar o rato, limpar a sujeira e
contar para a mãe que conseguiu cumprir a tarefa.

<div class="identado"></div>

##### Avaliação

<div class="identado"></div>

A história importa porque a mãe fica aliviada e o agradece;
e ele diz que não tem problema, o que mostra que é um bom filho.

<div class="identado"></div>

##### Coda

<div class="identado"></div>

O comentário sobre tudo acontece quando ele sacode a cabeça, com
olhos esbugalhados, para dizer “que saco,
hein?”.

<div class="float"></div>

![resumo](/images/resumo-parte2.png)

### 11.5. Resumo

Nesse capítulo, vimos que muitas narrativas em Libras seguem algumas
regras básicas para estruturar seu enredo, para que o público possa
acompanhar e entender o que aconteceu e com quem. Antes de começar a
principal parte da história, vimos que pode haver alguns elementos
paratextuais, como um título e uma explicação. A ideia básica de se ter
um início, uma ação complicadora e um desfecho foi explorada aqui com a
análise de diversas histórias contadas em Libras. Vimos que, mesmo que
os públicos, os assuntos e os objetivos sejam diferentes, as estruturas
de narrativas em Libras são geralmente parecidas.

<div class="float"></div>

![atividade](/images/atividade-parte2.png)

### 11.6. Atividade

Escolha uma narrativa em Libras de criação original (não uma tradução de
uma narrativa em português) que você goste.

1. Liste os elementos paratextuais que você vê nela. Tem título,
imagens, saudações ou despedidas?

2. Tente fazer uma análise da narrativa, para ver seu início,
introdução, ponto de partida da ação, clímax, resolução e alguma
conclusão ou coda.

<span class="tooltip-texto" id="tooltip-28">
Não confundir essa coda com “CODA” (<em>Child of Deaf Adult</em>), que
    significa “filho de pais surdos”.
</span>
