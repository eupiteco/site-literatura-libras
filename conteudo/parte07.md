---
capitulo: 7
parte: 2
titulo: "Gêneros da literatura em Libras: definidos pelo grau de ficção e pela forma"
pdf: "http://files.literaturaemlibras.com/CP07_Generos_definidos_pelo_grau_de_ficcao_e_pela_forma.pdf"
video: oQL_w1B-rYY
---

## 7. Gêneros da literatura em Libras: definidos pelo grau de ficção e pela forma

<div class="float"></div>

![objetivo](/images/objetivo-parte2.png)

### 7.1. Objetivo

Para compreender melhor a riqueza da literatura em Libras e a relação da
literatura surda com a cultura surda, é útil vermos os diversos tipos de
literatura em Libras que existem e pensar sobre gêneros literários. No
capítulo 03, conhecemos um pouco sobre a ideia de um gênero literário.
Neste capítulo, vamos refletir sobre gêneros literários em Libras,
categorizados em função da veracidade (ou não) do conteúdo e de sua
forma. No próximo capítulo, falaremos dos gêneros baseados em origem,
conteúdo e público-alvo.

<div class="float"></div>

![lista de videos](/images/videos-parte2.png)

<div class="lista-de-videos"></div>


Para a leitura deste capítulo, vamos rever algumas obras as quais já
assistimos e conhecer outras novas. Veja agora:

* [Animais (números)](#vimeoModal-348189766), de Juliana Lohn.

* [*Arrumar, Passear...* *de A à Z Letras*](#ytModal-uciVF5oMqkc)*,* Jéssie Rezende (feito em
colaboração com Fernanda Bonfim, Nayara Aparecida, Suzana Alves e Thainã
Miranda).

* *[Julgar a Prostituta](#ytModal-SyG9yCkP_Qc),* de Maurício
Barreto.

* *[Lutas surdas](#ytModal-aOQx2YMj6Xc),* de Alan Henry Godinho.

* [Mãos em Fúrias](#ytModal-8sYWSq2pwhg), por Eduardo Tótoli, Luciano Canesso Dyniewicz,
Elissane Zimmerman Dyniewicz e Carlos Alexandre Silvestri, dirigido por
Giuliano Robert.

* *[Números em Libras](#ytModal-ad8k-rIq7Sw),* de Maurício
Barreto.

* *[Pássaro (números)](#vimeoModal-348080802),* de
Juliana Lohn.

* *[V & V](#vimeoModal-325444221)* de Fernanda Machado.

<div class="lista-de-videos sem-linha"></div>

Vamos revisitar os seguintes poemas:

* *[A Pedra Rolante](#ytModal-kPXWu5UCTzk),* de Sandro Pereira.

* *[O Farol da Barra](#ytModal-VXcKgO-jD9A),* de Maurício Barreto.

* *[Jaguadarte](#ytModal-DSSXTh0wriU),*
de Aulio Nóbrega.

* *[Lei de Libras](#vimeoModal-267274663),* de Anna Luiza
Maciel e Sara Theisen Amorim.

* *[Leoa Guerreira](#ytModal-rfnKoCXmSg4),* de Vanessa Lima.

* *[Meu Ser é Nordestino](#ytModal-t4SLooMDTiw),*
de Klícia Campos.

* *[O Modelo do Professor Surdo](#ytModal-rverroKm8Bg),* de Wilson
Santos Silva.

* *[Tinder](#vimeoModal-267275098),* de Anna Luiza
Maciel.

### 7.2. Alguns pensamentos sobre os gêneros literários em Libras

Uma definição de gênero destaca-o como um “tipo específico de texto de
qualquer natureza, literário ou não, oral ou escrito, formal ou
informal, caracterizado e reconhecido pela função específica e
organização retórica mais ou menos típica, e pelo(s) contexto(s) onde é
utilizado” (HEBERLE, 2011). Meurer e Dellagnelo (2008) citam exemplos de
diversos gêneros textuais: “entrevista, convite, ata, aviso, programa de
auditório, briga de namorados, bula, comédia, convênio, faroeste, filme
de terror, crônica, editorial, ementas, *e-mail*, circular, contrato
\[...\]”. (p.28)

Gêneros de literatura são um tipo específico dessa divisão, sendo
caracterizados pelo uso da linguagem para entretenimento ou por destacar
a linguagem criativa. Esses gêneros são divisões culturais; cada cultura
categoriza a sua própria literatura a sua maneira. Isso acontece porque
cada cultura valoriza e tem conhecimento de coisas diferentes; cada
cultura usa a língua de forma particular e os seus membros têm
experiências distintas daquelas de pessoas de outras culturas. Por isso,
os gêneros de literatura em Libras não seriam necessariamente iguais aos
gêneros de literatura em português brasileiro.

Tendo em conta todos esses elementos, podemos dizer que um gênero
literário é uma **categoria** da produção artística linguística[15](#tooltip) de
uma cultura, caracterizada por similaridades em forma, estilo, assunto
ou conteúdo e público-alvo. Além disso, a origem da literatura é uma
base para se criar uma categoria ou um gênero. Ressaltamos que não
existe uma forma certa ou errada de identificar, nomear e criar gêneros
e que os sistemas de classificação mudam entre culturas, sociedades e
épocas. O que serviu para os gregos antigos, por exemplo, ou para os
franceses do século XVIII, não necessariamente serve para a literatura
brasileira em português no início do século XXI e nenhum dos sistemas
essencialmente ajudará em uma categorização dos textos em Libras.

Lembramos que os critérios para decidir o gênero de um texto em Libras
não são tão bem definidos como são em português (nem são perfeitos
dentro da própria língua portuguesa). A pesquisadora surda americana
Cynthia Peters (2000) observa que os gêneros literários em línguas de
sinais não se encaixam facilmente dentro dos gêneros literários criados
para as línguas orais.

Apesar de todos os conceitos diferentes, podemos tentar identificar os
gêneros de um texto literário da Libras conforme trata de verdade ou
ficção, pela sua forma, pelo seu modo de apresentação, de acordo com seu
tema, sua origem e seu público-alvo. Vamos começar.

#### 7.2.1. Ficção ou não ficção: é uma história verdadeira? 

Um gênero de literatura bem conhecido é o da ficção, ou seja, uma
criação imaginária, que contrasta com o gênero de não ficção, que é o
relato de fatos verdadeiros. Quando pensamos em literatura muitas vezes
pensamos em ficção e a literatura em Libras tem muitos exemplos disso.
Porém, as pessoas acostumadas às tradições literárias de culturas como
as do Brasil contemporâneo, da Europa ou dos EUA podem se surpreender
com o fato de as histórias fictícias originais em Libras não serem tão
difundidas quanto deveriam.

Histórias de experiências verdadeiras (também chamadas de “**Narrativas
de Experiência Pessoal**” ou NEP) ou traduções de histórias fictícias de
textos em português, filmes ou outros meios visuais são bastante
difundidas e muitas vezes contadas de uma forma altamente estética e
divertida. Por outro lado, histórias imaginárias, sobre coisas
imaginárias, que acontecem com pessoas que na verdade não existem, em
lugares irreais, não são tão comuns em narrativas em Libras.

Muitas crianças surdas integradas ao sistema escolar regular estudam e
conhecem apenas a literatura escrita em língua portuguesa nesse
contexto. Os alunos que estudam literatura em Libras aprendem
frequentemente através de traduções de textos escritos originalmente em
português, talvez com a ajuda de um intérprete em sala de aula. Qualquer
tentativa de usar a linguagem de forma criativa e com imaginação é
direcionada para o português, o que se torna um grande desafio para
muitas crianças surdas e elas acabam perdendo a motivação. O resultado é
que não há, atualmente, uma forte tradição de ficção original criativa
em Libras.

Dito isso, há alguns excelentes exemplos da ficção original em Libras.
Há uma crescente conscientização sobre a literatura em Libras,
especialmente entre os surdos que estudam nas escolas bilíngues e nas
faculdades que têm cursos de Letras Libras.

#### 7.2.2. Não ficção

Lembrando que um dos traços fundamentais da literatura é o uso da
linguagem de maneira interessante ou estética; podemos identificar uma
abundância de não ficção na literatura feita em Libras.
**Oratória**[16](#tooltip) é a habilidade de apresentação em público em que uma
pessoa dirige um discurso a um grande grupo de pessoas. Esse tipo de
discurso usa a retórica como uma forma de arte da comunidade surda. A
retórica em Libras usa a linguagem especificamente para persuadir as
pessoas sobre as ideias do falante, por isso ocorre em sermões,
discursos, reuniões políticas ou cerimônias públicas. Essa habilidade
inclui o uso de recursos de linguagem como a repetição e o ritmo para
gerar emoções. Pessoas surdas com habilidade na oratória de Libras dão
discursos e sermões emocionantes que são memoráveis não apenas pelo que
dizem, mas pela maneira como dizem. A obra literária *[Lutas
surdas](#ytModal-aOQx2YMj6Xc),* de Alan Henry Godinho (meio
poema, meio discurso informativo), é um exemplo de um texto em Libras
com foco político que usa retórica.

**Autobiografia** é outra forma de não ficção literária em que as
pessoas usam uma forma de Libras em primeiro plano para contar as
histórias de suas vidas. É geralmente contada como uma narrativa e há
muitas narrativas de experiências pessoais contadas em Libras. O gênero
de autobiografias escritas (em português brasileiro, por exemplo) cobre
um longo período da vida de uma pessoa, às vezes a vida inteira até o
momento da escrita, ou um período específico como a infância. Fala de
muitos tópicos, mas com um foco particular no que o escritor espera que
os leitores achem interessante, tais como crescer em um lugar incomum ou
em um tempo em que as coisas eram diferentes ou, ainda, como as
experiências de vida do autor levaram ele a se tornar um artista,
surfista ou político famoso.

Müller e Karnopp (2015) fizeram um levantamento de autobiografias
escritas por surdos em português. Esses textos seguem a mesma estrutura
dos textos de autobiografias escritas por ouvintes, embora muitas vezes
se concentrem na experiência do autor de ser surdo. Histórias
autobiográficas contadas em Libras contam mais sobre um evento escolhido
na vida de uma pessoa ou focam em um único tópico, como seu processo
educacional ou sua experiência de trabalho. Embora a linguagem usada em
autobiografias sinalizadas ou escritas por pessoas surdas possa não ser
especialmente literária, essas histórias são tão centrais para as
tradições narrativas da comunidade surda que devemos colocá-las
firmemente entre os gêneros da literatura em Libras. São de tanta
importância que às vezes a expressão “narrativas surdas” significa esse
tipo de história. Conforme Vieira-Machado (2008, p. 226), “Contar suas
histórias, narrar suas lembranças e memórias fazem desses narradores,
autores não só de si, mas de todos que são parte do coletivo que é o
movimento surdo.”

Um exemplo de narrativa de experiência pessoal surda é a de [Clóvis Albuquerque dos Santos](#ytModal-uP3q5kOcXxs), de Manaus, que conta sua experiência ao ir do
Amazonas ao INES, no Rio de Janeiro, no início dos anos 1960. A história
apresenta sinais de sua época, conta sobre lugares, atividades
cotidianas, atitudes e costumes que agora mudaram e que fazem parte da
história social dos surdos brasileiros.

Há uma linha tênue entre o que é “verdadeiro” e o que poderia ser
verdade e muitas histórias que parecem ser “autobiográficas” são ficção,
ou uma mistura de fato e ficção. Na literatura escrita, os leitores
estão acostumados à ideia de que o "eu" do narrador da história não é o
"eu" do autor da história. Também, entendem que esse "eu" não é o "eu"
da voz interior que está lendo a história dentro da cabeça do leitor.
Por exemplo, no livro clássico brasileiro *Grande Sertão: Veredas*,
sabemos que o “eu” narrador é Riobaldo, o jagunço aposentado que é o
protagonista da história. O “eu” não é o autor do livro, João Guimarães
Rosa, nem o “eu” da voz que um ouvinte tem em sua mente enquanto lê. Na
literatura não escrita, no entanto, é mais provável que o público pense
que o “eu” que está contando a história é o mesmo “eu” que teve a
experiência e o “eu” que a escreveu. Dessa forma, o contador, o
personagem e o autor estão muito mais ligados, como uma única pessoa.
Nas narrativas em Libras, as pessoas são muito mais propensas a supor
que uma história é verdadeira e é menos claro se a narrativa da
experiência pessoal aconteceu com o contador ou com alguém parecido com
essa pessoa ou, ainda, se poderia ter acontecido, mas nunca aconteceu. O
importante não são os fatos verdadeiros, mas os que relatam a
experiência dos surdos.

A **literatura religiosa** em Libras é
importante para muitos surdos. É uma área complexa para se identificar
um único gênero. Muitas igrejas no Brasil fornecem intérpretes de
Português-Libras nos cultos e nas orações; os sermões, hinos e textos
bíblicos são traduzidos da língua portuguesa e, às vezes, são criados
diretamente em Libras. As pessoas se esforçam muito ​​para traduzir
literatura religiosa em Libras e esse material é abundante na internet.
Grande parte é traduzida da forma mais fiel possível, seguindo os textos
em português, especialmente a Bíblia ou os hinos das religiões cristãs
(muitas vezes considerados gêneros literários no português brasileiro).
Mas existem traduções que criam efeitos estéticos em Libras colocando a
linguagem em primeiro plano. Poemas baseados em crenças religiosas podem
usar recursos da Libras muito estéticos. *[O Farol da Barra](#ytModal-VXcKgO-jD9A),* de Maurício Barreto, é um desses exemplos. Ele também reconta
histórias bíblicas tradicionais, como a intitulada [*Julgar a Prostituta*](#ytModal-SyG9yCkP_Qc), de forma muito estética e
visual, através do VV (veja a seguir a discussão sobre VV). Podemos
também recontar as histórias e adaptá-las para que haja um surdo nelas,
o que permite que o público surdo sinta uma maior conexão com o texto.

Além das categorias de ficção e não ficção, existem outros gêneros
fundamentais na literatura em Libras. Vale lembrar que eles são
determinados pelo seu conteúdo, sua forma e origem ou pelo público
(BAHAN, 2006). Os exemplos aqui não são exaustivos, mas mostram uma
ampla gama dos gêneros em Libras. A seguir, vamos nos dedicar aos
gêneros definidos a partir da forma e, no próximo capítulo, estudaremos
aqueles determinados pelo seu conteúdo, sua origem e pelo público.

### 7.3. Gêneros definidos pela forma

#### 7.3.1 Poesia em Libras

Na poesia em Libras, os artistas apresentam novas ideias de novas
maneiras usando formas originais da língua. O foco está na linguagem
estética que, geralmente, é fortemente visual e cuidadosamente
construída para maximizar o impacto dos sentidos. A forma é na maioria
das vezes curta, raramente composta por mais de três minutos e
normalmente com cerca de dois minutos. É visualmente muito intensa e
muitas vezes o seu significado não é muito claro, de modo que o público
precise se esforçar, pensar sobre a forma da linguagem para entender o
significado. Assim, a linguagem é trazida para o primeiro plano
(SUTTON-SPENCE; QUADROS, 2006). Os poemas de Libras expressam e refletem
a identidade surda da comunidade. Estudos da poesia em Libras mostram
que essa é uma forma de arte com regras e padrões próprios que está
crescendo e mudando rapidamente.

Muitas pessoas que aprenderam poesia escrita na escola estão acostumadas
com a ideia da poesia lírica, na qual o “eu poético” fala de suas
emoções ou de seus pontos de vista. As letras das músicas são um bom
exemplo, de modo que às vezes é um desafio encontrar uma canção que não
contenha morfemas na primeira pessoa em verbos, ou palavras como "eu"
"meu / minha" ou "eu / mim". Há exceções, com certeza; o “eu” não está
tão presente em poemas narrativos (como ocorre nas histórias contadas em
poemas de cordel), poemas descritivos (como haicais) ou poemas visuais
(como os de Paulo Leminski ou Augusto de Campos).

Há menos poemas líricos em Libras. Os poemas *[Meu Ser é
Nordestino](#ytModal-t4SLooMDTiw),* de Klícia
Campos, e *[Lei de Libras](#vimeoModal-267274663),* por
Anna Luiza Maciel e Sara Theisen Amorim, são mais parecidos com os
poemas líricos, uma vez que mostram o ponto de vista do “eu” poeta. Mas
já estudamos poemas como *[O Modelo do Professor
Surdo](#ytModal-rverroKm8Bg),* de Wilson Santos Silva;
*[Tinder](#vimeoModal-267275098),* de Anna Luiza Maciel
ou o
*[Jaguadarte](#ytModal-DSSXTh0wriU),*
de Aulio Nóbrega, que contam ou descrevem, mas não são líricos no
sentido de apresentar emoções ou sentimentos relacionados ao “eu”.

Vamos explorar a poesia em Libras nos capítulos posteriores e veremos
que, dentro do gênero “poesia”, existem diferentes gêneros poéticos.

#### 7.3.2. Vernáculo Visual 

O Vernáculo Visual (também conhecido como VV) é a técnica de contar
histórias de uma forma muito visual sem utilizar o vocabulário de
sinais. É um estilo que tem as raízes na tradição surda de contar de
modo cinematográfico histórias, em que todos os personagens, a paisagem
e o narrador são apresentados pelo contador. O VV não é nem exatamente
Libras nem totalmente mímica. Algumas pessoas entendem que o VV foi
criado pelo ator surdo americano Bernard Bragg, porém a ideia de contar
histórias com sinais e gestos fortemente visuais não vem apenas dele.
Bernard Bragg afirmou:

> *Marcel Marceau*[17](#tooltip) *me convidou para estudar mímica com ele em
> Paris. Eu criei uma outra técnica de performance baseada no método
> dele. Desenvolvi algo que chamei VV – que é uma forma de mímica. Não é
> uma estrutura tradicional de mímica. Eu diminuí o tamanho do quadro e
> utilizei técnicas de filme. Usei edições e cortes, close-ups e
> perspectivas de distância. Eu fui o primeiro a usar esse estilo que
> chamei de Vernáculo Visual por falta de um termo melhor*
> (NATHAN-LERNER; FEIGEL, 2009, [The Heart of the Hydrogen Jukebox](#ytModal-aJ0Y-luT5_w) ,
> 00:19:53, tradução nossa das legendas).

Vale a pena assistir a essa entrevista no vídeo de Nathan-Lerner e
Feigel e assistir também à performance de *O Caçador e o Cão* (*The
Hunter and the Dog*, em inglês) por Bragg, no trecho 00:20:26.

O poeta surdo americano Peter Cook explicou com
muita clareza a diferença entre o Vernáculo Visual e o uso de
classificadores em línguas de sinais. Nessa explicação, ele mostra
também elementos de mímica (ver o capítulo 10), um pouco diferentes da
definição de Bragg. Vale a pena assistir ao vídeo curto [O vernáculo
visual e o uso de classificadores em línguas de
sinais](#ytModal-lfNVeK-24cA), de Peter Cook (ele usa uma
mistura de ASL, Libras e sinais internacionais, mas quem conhece a
Libras é capaz de entender).

> Os classificadores pertencem à linguística. O VV, por outro lado, é
> como a atuação no teatro. É uma técnica de teatro. Existe uma escala
> entre os dois. Numa extremidade da escala temos os classificadores. Com
> os classificadores, por exemplo, eu posso mostrar uma pessoa caminhando
> e avançando, junto a uma expressão facial de descuido, mas o
> classificador significa que a pessoa está caminhando a pé. Um pouco mais
> na direção do VV, aumento o uso de expressão facial e o movimento do
> corpo. Mais próximo ao VV ainda, continuo com o classificador na mão,
> mas uso mais o corpo e até mexo as pernas. Finalmente, na extremidade da
> escala do VV, posso tirar o classificador e usar os braços, as mãos e
> até as pernas e os pés para mostrar o verdadeiro corpo do personagem
> caminhando. Assim, temos uma escala e podemos escolher o que queremos de
> qualquer ponto dessa escala. Mas a extremidade de VV não é linguística e
> na extremidade linguística temos os classificadores. São bastante
> diferentes, mas há muita flexibilidade onde podemos escolher os sinais
> nos pontos da escala.(COOK, 2018, tradução nossa).

O
*[Jaguadarte](#ytModal-DSSXTh0wriU),*
de Aulio Nóbrega, e *[O Modelo do Professor
Surdo](#ytModal-rverroKm8Bg),* de Wilson Santos, são dois
exemplos de VV (bastante diferentes nos tópicos e nos modos de
apresentação).

#### 7.3.3. Histórias delimitadas 

Os poemas de Libras são cuidadosamente construídos e seguem regras ou
restrições estritas, mas também há histórias cuja forma é limitada por
regras que estão fora da estrutura da história, especialmente pelas
configurações de mão.

*Histórias ABC* são um exemplo de histórias delimitadas. Susan
Rutherford (1993, p. 68) relata que as *histórias ABC* existiam nos EUA
no início do século XX. Embora tenham sido desenvolvidas na América do
Norte, hoje são cada vez mais comuns no Brasil. O artista de Libras
Nelson Pimenta estudou com poetas e artistas de ASL nos EUA e publicou
sua obra "O Pintor de A a Z" em Libras, em 1999. Essa história e o
conceito geral das *histórias ABC* foram ensinados em todo o Brasil nos
primeiros cursos de Letras Libras em 2006 e 2008 e agora são amplamente
vistos como entretenimento e como uma ferramenta de ensino de Libras
como primeira e segunda línguas.

Nas *histórias ABC*, cada sinal sucessivo usa a forma de letras do
alfabeto manual, de A até Z. Usar sinais com a configuração de mão certa
é mais importante que a trama em si, embora a história deva fazer
sentido. Rutherford menciona que muitas dessas histórias eram
tradicionalmente feitas por adolescentes, então, grande parte delas têm
tópicos tabus que apelam para sua faixa etária, como sexo e drogas, ou
qualquer outro assunto, desde que seja parte de uma experiência surda.

[*Arrumar, Passear*](#ytModal-uciVF5oMqkc), de Jéssie Rezende, é
uma *história ABC* feminina. A história é contada por classificadores –
alguns representando instrumentos, outros objetos inteiros e outros a
manipulação do objeto - e a contadora sempre usa o corpo como sujeito,
incorporando a mulher que se arruma. A história tem um enredo simples,
mas coerente, em que a mulher se arruma, se veste, ajeita os cabelos, se
maquia etc., terminando com a escolha da bolsa antes de sair de casa.
Tem um ritmo destacado e muita simetria nos sinais, criando uma
narrativa com muitos elementos poéticos.

Embora as *histórias ABC* sejam geralmente
categorizadas como "histórias", porque elas frequentemente têm ação,
elementos não manuais e mudança de papéis, sua forma disciplinada e
restrita as tornam mais próxima de poemas. Peters (2000), no entanto,
duvida que as *histórias ABC* sejam histórias, poemas ou mesmo peças de
teatro. Talvez sejam os três, ou talvez elas pertençam a sua própria
categoria.

Um exemplo de uma história desse tipo, que mostra essa mistura de
gêneros, encontra-se no filme [Mãos em Fúrias](#ytModal-8sYWSq2pwhg),
por Eduardo Tótoli, Luciano Canesso Dyniewicz, Elissane Zimmerman
Dyniewicz e Carlos Alexandre Silvestri, dirigido por Giuliano Robert.
Esse filme de curta metragem mistura os elementos linguísticos de
narração, teatro e filmagem para criar um efeito emocional muito forte.
No filme, o protagonista surdo sonha que está atirando em ouvintes que
fazem *bullying* com ele, e acorda assustado. Como todas as *histórias
de ABC*, o filme segue bem a ordem alfabética de A a Z, mas usa diversos
recursos que vão além de uma simples apresentação em Libras.

Há alguns sinais no filme que usam as configurações de mãos das letras.
Por exemplo, <span style="font-variant:small-caps;">surdo</span> usa a
configuração de mão de G, <span
style="font-variant:small-caps;">mudar-ideia</span> usa a configuração
de mão (e o movimento) de H e <span
style="font-variant:small-caps;">dor</span> usa a configuração de mão de
T. Também, conforme a tradição das *histórias ABC*, existem sinais que
usam as configurações de mãos das letras em classificadores como
instrumentos. Por exemplo, as configurações em L, N e Q mostram a arma
na mão a partir de diversas perspectivas. Configurações de mão de
classificadores entidades de objetos inteiros também fazem parte da
história, mas nesses exemplos vemos a mescla de linguagem e a parte mais
fílmica e de edição, porque vemos os objetos reais também. Na letra B,
por exemplo, as mãos mostram as portas do elevador abrindo, enquanto
vemos as portas verdadeiras; a mão que mostra a letra M cria uma
configuração de mão num classificador de três pessoas avançando juntas,
enquanto vemos as reais pernas das pessoas, desfocadas no fundo,
avançando até ficarem em foco. A configuração de mão da letra O cria o
contorno do cano da arma, num círculo que mostra o ponto de vista da
bala dentro do cano - uma perspectiva conhecida pelo público de filmes
hoje, mas impossível de se mostrar apenas em sinais. O outro uso da
letra O também mostra o contorno da bala, porém, usando um recurso mais
teatral, esse sinal é feito pelo ator no papel da vítima atingida pelo
projétil, e não pelo personagem principal.

Os elementos de teatro são também vistos nos classificadores em que o
protagonista toca ou manipula os objetos. A configuração de mão de A
realmente aperta o botão do elevador, a configuração de mão em C
realmente puxa uma cadeira e as configurações de mão D, E, F, I e J
mexem com um pó que entendemos ser alguma droga. A configuração de mão
em Z toca o corpo do sinalizante de uma forma linguística normal, mas a
de U empurra a testa de um outro ator. Finalmente, vemos algumas letras
que vão além da língua de sinais e não são feitas pelo alfabeto manual
da Libras, mas pelas imagens do teatro. As letras W, X e Y são criadas
pelos braços, pelas pernas e pelo corpo dos atores no papel das vítimas
no sonho.

O artista também pode criar um conto usando as formas manuais de letras
de uma palavra na linguagem escrita. Essas histórias são frequentemente
usadas para fins de ensino e são uma forma importante de desenvolver a
consciência da linguagem. Por exemplo, um aluno na escola ou um
estudante de Libras pode escolher uma pessoa famosa na comunidade surda
e descrevê-la usando sinais da forma de cada uma das letras do nome da
pessoa. As histórias também são usadas ​​em eventos que celebram uma
pessoa, como em um aniversário; em um evento comemorativo diverso ou até
mesmo em um velório.

As *histórias numéricas* seguem uma ideia semelhante à das *histórias
ABC* e são limitadas pelas formas das mãos dos números, que geralmente
são realizadas de 0 (ou 1) a 10 e, às vezes, de trás para frente ou em
ordem crescente e decrescente na mesma história. Há muito mais histórias
desse tipo na internet. Maurício Barreto criou [uma história de
números](#ytModal-ad8k-rIq7Sw) em Libras falando de um tiroteio
que vai de 0 a 10 e de volta para o 0. As histórias [Pássaro
(números)](#vimeoModal-348080802) e
[Animais (números)](h#vimeoModal-48189766),
de Juliana Lohn, são direcionadas a surdos muito jovens.

As histórias com uma configuração de mão são limitadas justamente pela
regra que diz que se deve usar apenas uma única configuração de mão. O
texto [*V & V*](#vimeoModal-325444221), de Fernanda Machado, é um
exemplo. Ela usa somente a configuração de mão na forma V. É importante
notar que algumas configurações de mão são mais comuns em Libras do que
outras. Histórias contadas apenas com sinais que usam a mão com todos os
cinco dedos esticados são relativamente fáceis de fazer, mas a forma de
"ILY" (veja a tabela na seção *Convenções,* no início do livro) é rara e
muito mais difícil de ser usada para criar uma história. Essas histórias
exigem o uso extensivo de elementos não manuais para as passagens que
normalmente utilizam outra configuração de mão.

Nos casos em que é muito difícil manter as limitações do gênero, podemos
“flexibilizar as regras” e criar sinais não existentes com determinada
configuração de mão. *A [Leoa Guerreira](#ytModal-rfnKoCXmSg4)*,
de Vanessa Lima, é um exemplo, pois usa muitas vezes as mãos abertas em
garras até para sinais como <span
style="font-variant:small-caps;">surdo</span> e <span
style="font-variant:small-caps;">desistir,</span> que usam originalmente
outras configurações.

<div class="float"></div>

![resumo](/images/resumo-parte2.png)

### 7.4. Resumo

Nesse capítulo, vimos exemplos de diferentes gêneros de literatura em
Libras. Entendemos que não há apenas uma maneira de dividir os gêneros,
mas sabemos que existem gêneros de literatura surda produzidos em
português e em Libras que são de ficção e outros que são de não ficção.
Pensando sobre alguns gêneros delimitados pela forma das obras,
destacamos como exemplos de poemas o Vernáculo Visual e as histórias
delimitadas. No próximo capítulo veremos outros tipos de gêneros em
Libras.

<div class="float"></div>

![atividade](/images/atividade-parte2.png)


### 7.5. Atividade

1. Assista a três histórias delimitadas em Libras. Quais são as regras
que delimitam a história?

2. Crie sua própria história delimitada:

<div class="lista-com-letras"></div>

1. crie uma história de ABC;

1. crie uma história de números que vai de 1 a 9; e

1. crie uma história com **uma** configuração de mão.

**Quais os desafios que você encontrou na criação dessas histórias?**



<span class="tooltip-texto" id="tooltip-15">
Os gêneros podem categorizar outras produções culturais
    artísticas, também, tais como a música ou a pintura.
</span>

<span class="tooltip-texto" id="tooltip-16">
Destacada como um gênero de literatura em ASL pela pesquisadora
    americana Nancy Frishberg em 1988.
</span>

<span class="tooltip-texto" id="tooltip-17">
Um famoso ator mímico francês que popularizou a técnica da mímica
    especialmente nas décadas 1950-1970.
</span>
