---
capitulo: 12
parte: 2
titulo: Piadas surdas e humor surdo 
pdf: "http://files.literaturaemlibras.com/CP12_Piadas_surdas_e_humor_surdo.pdf"
video: ZGO14FERkH4
---

## 12. Piadas surdas e humor surdo 

<div class="float"></div>

![objetivo](/images/objetivo-parte2.png)

### 12.1. Objetivo

Neste capítulo iremos explorar o conteúdo humorístico nas produções da
comunidade surda brasileira, em particular o formato da piada, que,
nesse contexto, é um tipo de narrativa de intenção humorística,
brevíssima e muitas vezes que tem uma forma de repetição que se destaca
e uma virada no final.

O humor está presente em outras formas, mas, especificamente as piadas,
são úteis para se estudar o humor em Libras. Veremos que o conteúdo das
piadas surdas e de outras formas de humor - como histórias engraçadas -
baseia-se na experiência visual que os surdos têm do mundo. Embora nosso
foco neste capítulo seja as piadas e o humor da comunidade surda
brasileira, as pesquisas sobre humor ao redor do mundo mostram que boa
parte dele é compartilhada internacionalmente e se espalha entre os
surdos através de contatos pessoais e da internet. A estrutura, o
conteúdo e a função do humor nas sociedades dos surdos e dos ouvintes
são semelhantes, porque todos os seres humanos acham graça das mesmas
coisas (BERGSON, 1983; RASKIN, 1985), mas o humor que se origina dentro
de qualquer comunidade surda é influenciado pelo conhecimento de mundo e
pela experiência surda (RUTHERFORD, 1989 para a ASL; SUTTON-SPENCE;
NAPOLI, 2012 para a ASL e BSL; MORGADO, 2011 para LGP, a língua de
sinais de Portugal; POL, 2014 para LIS, a língua de sinais italiana;
KARNOPP; SILVEIRA, 2014 para Libras).

<div class="float"></div>

![lista de videos](/images/videos-parte2.png)

<div class="lista-de-videos"></div>

* Vamos assistir ao vídeo *[O Encontro](#ytModal-eDHKhLjyIGw),* de Fabio de Sá.
 

### 12.2. Humor surdo

O humor surdo é o humor feito pelos surdos, destinado aos surdos ou que
trate sobre eles e, muitas vezes, apresentado em Libras. Provém da
cultura e da história surda; frequentemente é político, acentuando a
relação entre a comunidade surda, minoritária e oprimida, e a comunidade
dominante ouvinte, que exerce uma relação de domínio sobre a experiência
dos surdos (BIENVENU, 1993; POL, 2014; KARNOPP; SILVEIRA, 2014).

Não obstante, sinalizantes que contam piadas ou histórias jocosas que
criam tal humor, com frequência não têm nenhuma intenção política
explícita. As piadas surdas ajudam a dar suporte às identidades
linguística, social e cultural dos surdos ao se referirem à Libras e à
cultura surda. A identidade linguística de um surdo é resultado de sua
experiência com a Libras, também com a língua portuguesa escrita e
falada (STROBEL, 2007) e muito do humor surdo ou faz referência à Libras
ou é contado em língua de sinais. A identidade social dos surdos é
baseada no senso de pertencimento (ou não) à comunidade surda, e o humor
surdo ensina os surdos a pertencerem a sua comunidade e a se comportarem
dentro dela, reforçando e desafiando os seus comportamentos. A
identidade cultural surda vem do conhecimento que uma pessoa surda tem
do mundo e da experiência dos surdos (BAHAN, 1994), portanto as piadas e
histórias jocosas que descrevem a experiência e o conhecimento de mundo
dos surdos contribuem para esse humor.

Os surdos riem do humor visual em particular (SCHALLENBERGER, 2010) e
com frequência acham graça no humor de sua comunidade local ouvinte
quando este é acessível visualmente. Mas nem todas as piadas dos
ouvintes traduzidas para Libras divertem os surdos. Isso pode acontecer
em função de um conhecimento cultural diferente, ou por expectativas
divergentes com relação ao que define a graça de uma piada. Muitos
surdos dizem preferir algo que seja engraçado ao longo de toda uma
história do que uma piada que tenha graça apenas no ponto principal (em
inglês *punchline*). Quando piadas surdas e histórias jocosas em Libras
são traduzidas para o português, o humor conceitual (baseado no
conteúdo) pode ser traduzido, mas o público que tem acesso a elas pela
tradução talvez tenha dificuldade de apreciar o que os membros da
comunidade surda acham engraçado (MECKLER, 2007).

Alguns tipos de humor visual não precisam do intermédio da Libras para
fruição. Os elementos visuais das comédias pastelão em filmes e
programas de televisão feitos para a sociedade ouvinte em geral são
muito populares entre a comunidade surda, como os de Charlie Chaplin e
Mr. Bean ou as “videocassetadas” no YouTube ou na TV. O humor visual
maluco dos desenhos animados é tão popular entre crianças surdas como é
para as ouvintes. É importante, porém, para nossos estudos de
literatura, que piadas, jogos de linguagem, teatro, contação de
histórias, conversas humorísticas e comentários jocosos surdos sejam
sinalizados em Libras.

Precisamos lembrar que as pessoas frequentemente riem de piadas e
histórias engraçadas porque o conteúdo delas é produzido através do uso
cômico e espirituoso da Libras, mas o humor em Libras frequentemente vai
além do vocabulário. Ele é apresentado por sinalizantes habilidosos
através de uma produção incorporada para criar imagens altamente
divertidas (veja, entre outros, KLIMA e BELLUGI, 1979; BOUCHAUVEAU,
1994; MORGADO, 2011). Ainda iremos explorar esse aspecto em outro
capítulo, mas devemos lembrar que o humor surdo e o humor em Libras
estão intimamente ligados.

### 12.3. Para que serve o humor surdo?

Em qualquer cultura, o humor dá prazer aos envolvidos. Ele também
permite que os membros da comunidade compartilhem experiências, por isso
no humor surdo há uma ligação entre o sinalizante e a plateia (BIENVENU,
1994). Para a comunidade surda, essa troca de experiências é importante,
principalmente devido ao fato de que muitos surdos entram para a
comunidade surda relativamente tarde.

O humor também serve para reduzir a tensão, a ansiedade e o medo. O
cômico britânico Hal Draper contou sobre a sua experiência fazendo
comédias *stand-up* surdas:

> \[Os surdos nas plateias\] se identificaram com as experiências e
> mais, eles riram. Eles podiam assistir, rir e pensar “sim, eu lembro
> que o mesmo aconteceu comigo”. Também para alguns surdos que são novos
> na comunidade surda, ela traz muitas coisas lá do fundo deles mesmos.
> Eles veem as coisas que eles sentiram ou com as quais se constrangeram
> sendo representadas e se dão conta “eu não sou o único que tem esse
> problema – todos os surdos têm esse problema”. Assim, por um lado, a
> apresentação é sobre humor e fazer rir, mas por outro é um pouco de
> terapia para alguns surdos que encontram sua identidade[29](#tooltip).

O humor também pode suavizar as relações entre as pessoas. Pode servir,
por exemplo, para uma pessoa fazer uma rejeição polida - mas também pode
funcionar para o controle social pela ridicularização. Assim, o humor
pode promover ou discordar de normas sociais específicas. Muitas
culturas fazem piadas sobre estar atrasado e, caso alguém chegue tarde
em algum compromisso, o humor sobre a violação de uma regra social pode
assegurar que essa regra seja seguida (como sempre chegar na hora), mas
ajuda a continuar a comunicação sem que a pessoa atrasada fique
constrangida.

Na base de boa parte do humor existe a incongruência. O humor apresenta
alguma coisa incongruente, especialmente um “erro” que cria uma
expectativa e então subverte ou contradiz algo. O público nota a
contradição e tenta resolvê-la. As risadas que se seguem ocorrem quando
encontramos a “lógica do absurdo” dentro da incongruência.

A teoria do humor de Martineau (1972) focava na ideia do uso do humor
como uma maneira de se criar e manter uma identidade de grupo. Podemos
aplicar as teorias de Martineau no humor surdo. Ele cria e mantém uma
ideia de que os surdos são todos membros de um mesmo grupo, ou “nós”,
que é às vezes chamado de *grupo interno*. Pessoas que não fazem parte
desse grupo são “os outros” e pertencem ao que pode se chamar de *grupo
externo*. As piadas surdas dão apoio ao grupo interno surdo ao ensinar
aos novos membros da comunidade as regras da sociedade surda e
reforçá-las para os membros mais antigos. O humor surdo também pode
provocar o grupo interno para expor os aspectos menos desejáveis da
própria cultura surda, surdos que negam que são surdos ou rejeitam a
herança cultural. Na cultura surda, o principal grupo externo geralmente
é a sociedade ouvinte (BAHAN, 2006).

Embora muitos surdos não se considerem deficientes físicos, há piadas
surdas com um cego, um cadeirante e um surdo como personagens[30](#tooltip). A
piada seguinte mostra a rejeição da deficiência física pela comunidade
em geral ao debochar do modo como o surdo manipula a sociedade ouvinte
que o trata como deficiente físico:

> Um surdo, um cego e um cadeirante amigos estão juntos num bar. Eles
> reclamam que a cerveja está ruim e o bar está muito cheio. De repente,
> Deus entra no bar. Ele vê que eles estão infelizes, então vai até a
> mesa deles e diz ao cadeirante: “esteja curado!”. O cadeirante se
> levanta e sai correndo do bar gritando “estou curado! Glória a Deus!”.
> Deus então diz ao cego: “esteja curado!”, e o cego passa a ver, sai
> correndo do bar gritando “estou curado! Glória a Deus!”. Deus então se
> volta para o surdo, mas antes que possa dizer qualquer coisa, o surdo
> apavorado diz: “não me cure, por favor! Não quero perder meu passe
> livre de ônibus!”. [31](#tooltip)

### 12.4. Piadas em Libras traduzidas do português

Há muitas piadas que podem simplesmente ser traduzidas do português para
a Libras. Muitas contadas em Libras vêm do português e podem não estar
tão centradas no senso de humor surdo, mas os surdos ainda assim
conseguem rir delas. Isso porque, em parte, brasileiros surdos e
ouvintes compartilham a mesma cultura nacional que se estende ao senso
de humor e, por outro lado, porque bons sinalizantes de Libras
transformam as piadas originais em algo “seu”, até que deixam de ser
piadas de ouvintes e suas versões visuais ficam muito mais aceitáveis
para um público surdo. Os protagonistas das piadas podem frequentemente
ser caracterizados como surdos (embora não seja assim nas piadas
originais), permitindo que a plateia possa se identificar mais com eles.
A simples ação de se contar uma piada em Libras já pode sugerir que o
protagonista é surdo.

### 12.5. Piadas em Libras oriundas da comunidade surda

As piadas surdas parecem se espalhar rapidamente pelo mundo, havendo
versões diferentes da mesma piada, às vezes adaptadas, em muitas línguas
de sinais diferentes (SILVEIRA, 2015). Muitas piadas e histórias jocosas
surdas tratam de tópicos importantes para a comunidade. Os mesmos
conteúdos de piadas ocorrem em outros gêneros de literatura não
humorística em Libras, como contação de histórias, poesia e teatro. Hall
(1989) identificou temas tradicionais em comum como problemas
enfrentados por casais, encontros com médicos e outros profissionais em
posições de poder (como intérpretes, professores e fonoaudiólogas) e
viajantes surdos que encontram ouvintes e passam a perna nestes ou
resolvem algum tipo de problema de alguma forma inusitada para os
ouvintes.

Em muitas dessas piadas, o som e a fala são postos em oposição à visão e
à sinalização. Piadas sobre desentendimentos e problemas de comunicação
com ouvintes também mostram o lado visual da vida dos surdos. Ouvintes
que mostram sua ignorância acerca das necessidades dos surdos e surdos
“pagando mico” ao tentarem se passar por ouvintes, são exemplos de
conteúdos de piadas que satirizam o grupo externo e membros rebeldes do
grupo interno. Piadas com esses temas com frequência também se voltam
aos contrastes entre som e visão.

Karnopp e Silveira (2014) descrevem diferentes versões da piada do leão
surdo, que existem não somente em Libras, mas também nas línguas de
sinais de diversos outros países. Observamos que essa piada tem uma
estrutura de três eventos parecidos e a virada acontece no terceiro
(como a piada acima). A piada mostra o contraste entre o que podemos
esperar do comportamento de ouvintes e surdos (ou leões). Veja:

> Um violinista vai para o campo para poder tocar sua música em paz. Um
> leão faminto se aproxima, mas, apaziguado pela doçura da música, cai
> no sono. Outro leão se aproxima e o homem toca o mais suavemente
> possível, fazendo o segundo leão adormecer. Antes que o violinista
> possa escapar, um terceiro leão aparece. O violinista toca o melhor
> que pode, mas é comido pelo leão mesmo assim. O leão era surdo.

Essa mesma piada básica tem várias formas. Às vezes não é um leão, mas
um touro na arena, e o violinista é apenas arremessado pelo touro. Em
outras versões, o violinista percebe que o leão é surdo e sinaliza para
que ele durma ou o leão encontra um surdo com um intérprete e este é
comido pelo leão, mas deixa o surdo fugir.

Já vimos, no capítulo 08, a piada surda clássica que tem muitas
variantes, a da árvore surda. Repetimos aqui:

> Um lenhador corta uma árvore. Ele grita: “Madeira!” e a árvore cai. Um
> dia ele corta uma árvore e grita: “Madeira!” mas ela não cai. Então
> ele chama um médico, que faz alguns exames, relata que a árvore é
> surda e orienta o lenhador a aprender a língua de sinais. O lenhador
> vai à associação de surdos e aprende Libras. Depois, ele volta à
> floresta e soletra M-A-D-E-I-R-A, e a árvore cai.

Em outras versões, a árvore é cega e surda, e por isso o lenhador
precisa soletrar fazendo contato com o tronco para que ela caia. Em
outra, o lenhador tenta fazer com que a árvore leia seus lábios
pronunciando a palavra “madeira” e ao inclinar-se para ver melhor sua
boca a árvore acaba caindo sobre ele.

Uma das piadas surdas internacionais mais famosas é às vezes chamada de
“a piada da lua-de-mel surda”:

> Um casal surdo em lua-de-mel está em um hotel. Eles ficam com fome e
> então o marido sai para buscar uma pizza. Quando volta ao hotel já é
> tarde e todas as luzes estão apagadas, por isso ele não consegue se
> lembrar do número do seu quarto. Daí ele toca a buzina do carro. As
> luzes de todos os quartos se acendem, exceto de um. Assim ele sabe
> onde sua esposa está.

Essa piada básica que se apoia na ideia de ouvintes escutando um som
inaudível para surdos tem uma gama enorme de variações.

Em [O Encontro](#ytModal-eDHKhLjyIGw), Fabio de Sá conta a versão de
um surdo que precisa encontrar outro surdo no aeroporto, porém não sabe
o seu nome ou a sua aparência. Ele grita o mais alto que pode, e todos
se viram para encará-lo. Todos, menos um, assim ele sabe que esta é a
pessoa que ele deveria encontrar.

Muitas piadas em Libras que foram criadas dentro da comunidade surda têm
tradução em português porque não dependem da forma exata da linguagem
usada para serem contadas, mas usam informação cultural para um completo
efeito humorístico. Esta piada, que provavelmente proveio da ASL, é
outro exemplo cuja estrutura envolve um surdo, um cego e um cadeirante:

> Um cego, um cadeirante e um surdo vão todos ao barbeiro. Ele corta o
> cabelo de todos de graça, como parte de seu compromisso com a semana
> nacional da deficiência física. Como forma de agradecimento, o cego
> lhe dá uma dúzia de rosas, o cadeirante lhe dá uma caixa com uma dúzia
> de bombons e o surdo conta para doze amigos, que também vêm aproveitar
> o corte de cabelo gratuito.  [32] tooltip quebrado aqui

Os ouvintes podem rir dessa piada porque veem o comportamento ingrato do
surdo como uma virada inesperada na trama. Os surdos, entretanto, podem
rir por dela depreenderem outros significados culturais como: o fato de
membros da comunidade surda saberem que surdos sempre contam uns aos
outros quando algo de bom acontece, o fato de gostarem de gratuidades e
o de que surdos gostam de tirar vantagem dos ouvintes, pois estes também
tiram muita vantagem dos surdos. Além disso, o modo como a piada é
contada frequentemente é engraçado, com a caracterização visual dos
personagens construída com exagero e caricatura. Piadas de ouvintes que
dependam unicamente do ponto principal (*punchline*) não são muito
divertidas, mesmo quando sinalizadas em Libras. Uma reação comum dos
surdos ao verem uma tradução de piada é de comentarem “entendo”, mas sem
risos. Para que a piada seja engraçada em seu ponto principal, é melhor
que seja também durante toda sua duração, com um uso cômico da Libras.
Veremos, entretanto, que piadas diferentes usam o recurso da Libras em
proporções diferentes.

### 12.6. Quanto precisamos da Libras para uma piada?

Algumas piadas contadas em Libras se apoiam em gestos para expressar seu
ponto principal. Quando eles são compartilhados entre surdos e ouvintes
da mesma sociedade (seja local, regional ou nacional), é possível
traduzir as piadas entre português e Libras com relativa facilidade. Há
piadas em língua oral que demandam gestos no ponto principal. Uma piada
que pode ser contada em português ou em Libras e que precisa de um gesto
que pode ser entendido de modo relativamente universal é a seguinte:

> Um padre está com uma dor de barriga terrível, mas mesmo assim vai
> rezar uma missa para sua congregação surda. Ele está com tanto
> desconforto que precisa peidar, mas acha que isso não será um
> problema, já que os surdos não vão ouvir. Porém, assim que ele o faz
> todos os surdos o encaram com um olhar de desaprovação e apontam para
> o que está as suas costas. Não entendendo como eles descobriram o que
> ele fez, o padre olha atrás e vê \[nesse ponto o sinalizante mostra\]
> Cristo crucificado tapando o nariz com os dedos em sinal de
> desaprovação.

Outras piadas surdas se apoiam na existência da língua de sinais como
parte da estrutura da piada, mas o elemento gestual por trás do humor
ainda é apreciado por sinalizantes e não sinalizantes desde que ambos se
lembrem que os surdos precisam das mãos para sinalizar.

> Dois amantes surdos se abraçam apaixonadamente. O homem tem a mulher
> em seus braços e quer dizer algo a ela, mas a deixa cair ao sinalizar,
> pois não consegue usar as mãos para fazer duas coisas ao mesmo tempo.
> Após isso, ela o deixa, furiosa.

Algumas piadas funcionam apenas em Libras e não em português, pois as
mãos do sinalizante mostram como os personagens usaram suas mãos e o que
sinalizaram. O exemplo mais conhecido desse tipo de piada é a do King
Kong (ou do gigante), também contada nos EUA, na Grã-Bretanha, na
França, em Portugal e em muitos outros países (para mais exemplos, vide
RUTHERFORD, 1993; SUTTON-SPENCE; NAPOLI, 2009; MORGADO 2011; KARNOPP;
SILVEIRA, 2014). Nessa piada, o gorila (ou gigante) está segurando uma
linda garota na palma da mão e diz que quer se casar com ela. Em Libras
(e em muitas outras línguas de sinais) o sinal <span
style="font-variant:small-caps;">casar</span> envolve um movimento com
as duas mãos, como uma batida de palmas seguida de uma mão segurando a
outra. Assim, ele mata a moça por engano ao sinalizar.

A próxima piada só tem graça se soubermos que um soldado teve que soltar
o outro quando sinalizou “não sei mesmo”, pois tal sinal requer as duas
mãos em sua forma enfática. A forma linguística dos sinais significa que
a piada realmente não funciona quando é traduzida para o português.

> Dois soldados inimigos saltam de paraquedas para o mesmo campo de
> batalha. O paraquedas de um deles não abre, e o outro soldado o segura
> com as mãos. Enquanto caem juntos, percebem que são ambos surdos. O
> soldado resgatado diz “nós dois somos surdos, devíamos ser amigos. Por
> que estamos em guerra?”. O soldado salvador percebe que o outro tem
> razão e diz enfaticamente “não sei, mesmo!”, e o deixa cair.

O sinalizante, nessa piada, mostra o soldado salvador segurando o outro
por incorporação. Quando os sinalizantes incorporam os personagens
através de uma transferência de pessoa, entende-se que seus movimentos
são os dos personagens. Os braços do sinalizante em torno de uma pessoa
imaginária são entendidos como os braços do soldado segurando seu
inimigo enquanto caem juntos de paraquedas. No entanto, as narrativas
com frequência utilizam um vocabulário padronizado de língua de sinais
no qual alguns sinais são feitos com uma mão e outros com as duas. A
mudança para o item de vocabulário <span
style="font-variant:small-caps;">não-sei</span> é o que faz com que a
piada funcione, como no caso do sinal <span
style="font-variant:small-caps;">casar</span> na piada do King Kong.

<div class="float"></div>

![resumo](/images/resumo-parte2.png)

### 12.7. Resumo

Vimos nesse capítulo que as piadas contadas em Libras são um tipo de
narrativa curta e humorística. O humor da comunidade surda é muito
parecido com o humor dos ouvintes. Algumas piadas podem ser contadas
igualmente em português ou em Libras, mas os tópicos muitas vezes trazem
conteúdos que mostram os interesses dos surdos. Embora elas tratem de
diversos assuntos, as piadas em Libras preferidas pelos surdos destacam
a linguagem criativa e humorística.

<div class="float"></div>

![atividade](/images/atividade-parte2.png)

### 12.8. Atividade

Pense em algumas piadas que você gosta. Para cada uma, considere o
seguinte:

* Qual é a base do humor?

* Há dois grupos (o de dentro e o de fora)? Se sim, quem são?

* A piada apoia ou insulta os que pertencem ao grupo? Ela apoia os de
fora do grupo ou os insulta? De que formas?




<span class="tooltip-texto" id="tooltip-29">
Reproduzida de Sutton-Spence e Napoli, 2012, p. 312 (tradução
    nossa).
</span>

<span class="tooltip-texto" id="tooltip-30">
Essa piada tem uma estrutura tripartida, presente nas piadas de
    diversas sociedades. É importante notar que tais piadas *não*
    satirizam as limitações físicas de outros, tendo o comportamento do
    surdo como alvo.
</span>

<span class="tooltip-texto" id="tooltip-31">
Reproduzida de Sutton-Spence e Napoli, 2012. (tradução nossa)
</span>

<span class="tooltip-texto" id="tooltip-32">
Reproduzida de Sutton-Spence e Napoli, 2012. (tradução nossa)
</span>