---
capitulo: 16
parte: 3
titulo: Metáfora 
pdf: "http://files.literaturaemlibras.com/CP16_Metafora.pdf"
video: F6qGX4QSjPA
---

## 16. Metáfora 

<div class="float"></div>

![objetivo](/images/objetivo-parte3.png)

### 16.1. Objetivo 

No capítulo anterior, começamos a falar sobre a metáfora. Neste,
conheceremos alguns outros aspectos das metáforas e da linguagem
metafórica e figurativa que vemos na literatura surda, especialmente na
literatura em Libras. Vamos descrever as funções das metáforas e os seus
sentidos, especialmente no contexto da literatura em Libras. Veremos a
importância da modalidade visual da Libras em metáforas conceituais e em
metáforas linguísticas. Pensaremos também como a iconicidade e a
incorporação afetam a linguagem figurativa da Libras.

<div class="float"></div>

![lista de vídeos](/images/videos-parte3.png)

<div class="lista-de-videos"></div>

Como preparação para este capítulo, você pode assistir aos seguintes
vídeos:

* [*A Pedra Rolante*](#ytModal-kPXWu5UCTzk), de Sandro Pereira.

* *[Bolinha de Ping-Pong](#ytModal-VhGCEznqljo),* de Rimar Segala.

* [*As Brasileiras*](#vimeoModal-242326425), de Anna Luiza Maciel e Klícia Campos.

* *[Como Veio Alimentação](#ytModal-nMOTYprbYoY),* de Fernanda
Machado.

* [*Doll*](#ytModal-5iM7zbis68w) (em português, *A Boneca*), de Paul Scott.

* [*O Farol da Barra*](#ytModal-VXcKgO-jD9A), de Maurício Barreto.

* *[Fazenda: Vaca](#ytModal-NtN98y67ukM),* de Rimar
Segala.

* *[Homenagem Santa Maria](#ytModal-9LtOP-LLx0Y)/RS,* de Alan
Henry Godinho.

* *[Lutas surdas](#ytModal-aOQx2YMj6Xc),* de Alan Henry Godinho.

* *[O passarinho
diferente](#ytModal-P_DR60BJuFI),* de
Nelson Pimenta.

* *[O Sapo e o Boi](#ytModal-23HQFq0A4AY),*
de Nelson Pimenta.

### 16.2. O que é metáfora?

Existem muitas definições de metáfora, mas, em resumo, podemos dizer que
na metáfora a essência é entender e experimentar um tipo de coisa em
outros termos. Assim, a coisa apresentada não é o seu significado. A
pessoa fala algo, mas tem a intenção de que a outra pessoa entenda o que
foi dito de outra maneira. Por isso, o sinal <span
style="font-variant:small-caps;">metáfora</span> em Libras é baseado na
ideia de se mostrar claramente uma coisa, mas também de que há outra
ideia importante por trás que devemos entender.

Geralmente, na metáfora, usamos termos concretos para significar um
conceito mais abstrato. Apresentamos o concreto – o real ou o físico –
para significar o abstrato – o que não é físico. Por exemplo, quando
usamos a frase em português “fiquei com o cabelo em pé” queremos dizer
que levamos um susto e usamos um sinal relacionado a essa expressão em
Libras para expressar a mesma ideia. Um susto é um conceito abstrato que
não podemos ver, mas os cabelos são concretos, são coisas reais que
podemos ver e tocar. Nossos cabelos podem ficar em pé por muitas razões
(por causa do vento, porque tiramos o boné com pressa ou porque os
cabelos estão despenteados) e não apenas por susto. Por outro lado,
quando nos assustamos, isso pode ter muitas causas e muitos efeitos
físicos e mentais, além dos cabelos se mexerem. A expressão “ficar com o
cabelo em pé” supõe uma razão para que os cabelos fiquem assim e usa um
traço da experiência do susto para criar a metáfora. É importante,
também, lembrar que os cabelos não estavam realmente em pé, mas quando
levamos o susto é como se estivessem.

A metáfora é um elemento importante nas literaturas de diversos tipos e
em diversas culturas. Muitos poetas usam uma imagem concreta para
descrever uma ideia abstrata, e a seleção da imagem certa para
relacionar à ideia de uma forma original e criativa é muito valorizada.
Alguns textos inteiros são metafóricos, ou ainda, dentro deles veremos
diversas metáforas menores. Ressaltamos, no entanto, que nem toda a
literatura é metafórica. No Japão, por exemplo, os poemas tradicionais
haicais não usam metáfora. O objetivo é criar uma imagem mental forte
por meio das palavras, mas elas não têm intenção de indicar outro
significado. Em Libras, muitos poemas não têm significados escondidos
porque os poetas apresentam diretamente o que eles querem dizer e a
criatividade está dentro da forma dos sinais e não no conteúdo. Mas, em
outros textos literários em Libras, vemos metáforas, como veremos a
seguir neste capítulo.

### 16.3. Exemplos de metáfora em Libras

Sandra Patrícia de Faria do Nascimento (2009), pesquisou sobre as
metáforas no léxico da Libras. Ela percebeu que muitas frases
metafóricas em português que usam o corpo têm empréstimos equivalentes
em Libras e que elas são apresentadas iconicamente usando o corpo. Já
vimos isso no exemplo dos cabelos em pé. Na expressão em português
“falar pelos cotovelos” (com sentido de falar sem parar) podemos
articular o sinal <span style="font-variant:small-caps;">falar</span> no
cotovelo. A outra pessoa precisa entender que os cotovelos não falam,
mas que a pessoa fala demais. Sinalizar “segurando uma vela” pode
significar que alguém realmente está segurando uma vela, mas o sentido
metafórico é o de uma pessoa solteira que está na presença de namorados
e se sente como se estivesse sobrando. Podemos criar alguns sinais de
uma forma mais visual juntando duas partes de um sinal. No sinal <span
style="font-variant:small-caps;">pau</span> ou <span
style="font-variant:small-caps;">madeira</span>, o punho bate o
antebraço. No sinal metafórico <span
style="font-variant:small-caps;">cara-de-pau</span>, o punho bate na
cara. Sem saber que o punho faz parte do sinal <span
style="font-variant:small-caps;">pau</span>, a outra pessoa não entende.
Além do mais, pode-se pensar que o rosto é feito de pau. O significado
certo será entendido apenas quando se souber que, em português, um “cara
de pau” é uma pessoa sem vergonha[41](#tooltip). Esses processos de criação de
metáforas em Libras são criativos, os quais observamos na literatura em
Libras quando os poetas brincam com os sinais.

### 16.4. Metonímia e sinédoque

Relacionadas ao conceito de metáfora, também temos as ideias de
metonímia e sinédoque. Nas duas, também existe a concepção de que aquilo
que a pessoa fala não é o que ela realmente quer dizer.

Metonímia é a utilização de uma palavra ligada a algum outro sentido
para expressar uma ideia. Por exemplo, no mês de abril temos o feriado
de Tiradentes, o revolucionário que foi condenado à morte por
enforcamento. O sinal para a pessoa, Tiradentes, para o feriado de
Tiradentes e até para o mês de abril é baseado na imagem do
enforcamento, que é ligada aos conceitos. Na Inglaterra, os guardas da
rainha usam uniformes com chapéu de pelo de urso com uma correia do
queixo. Essa correia é uma imagem ligada à Inglaterra e em Libras se
tonou o sinal que significa o país inteiro. Libras tem muitos sinais
metonímicos para conceitos abstratos, tais como meses, países, cidades,
estados, feriados e estações do ano.

Sinédoque é uma figura que consiste no uso do “todo pela parte” ou da
“parte pelo todo”. Por exemplo, em Libras usamos a imagem de um chifre
para falar de uma vaca. O chifre, na realidade, é apenas uma parte da
vaca, mas a sua figura substitui a vaca inteira. O vocabulário da Libras
usa muito a sinédoque para conceitos concretos. Muitos sinais para
animais, frutas e objetos cotidianos são baseados nessa figura.

Ambas, metonímia e sinédoque, são usadas na literatura em Libras e na
criação de sinais novos, e mostram imagens visuais para o público
entender que há diversos níveis de sentido.

### 16.5. A metáfora literária em Libras

A metáfora na literatura em Libras ocorre em vários níveis. Quando um
texto inteiro é uma metáfora, é chamado de **alegoria** ou **metáfora
estendida.** O público decide qual é a metáfora do poema, mas essa não é
uma decisão livre. Sempre deve ser justificada com base no nosso
entendimento da metáfora convencional, nosso conhecimento e na nossa
experiência de mundo e em qualquer mapeamento icônico entre forma e
significado (LAKOFF; TURNER, 1989, p. 146).

Nesta seção, investigamos exemplos de literatura em Libras em que o
texto inteiro, um trecho maior de um sinal ou de uma frase, carrega uma
metáfora. Isso quer dizer que vemos uma narrativa apresentada sobre um
assunto que na verdade tem a intenção de mostrar outro significado.

#### 16.5.1. A metáfora nas narrativas traduzidas

As fábulas e as parábolas das sociedades ouvintes usam um tipo de
metáfora: apresentam uma imagem, mas escondem uma moral ou um objetivo
que o público deve entender. Algumas dessas fábulas foram traduzidas
para Libras por Nelson Pimenta.

As fábulas são contos sobre um assunto concreto, mas que têm o objetivo
de mostrar outro significado mais abstrato. A fábula *A Lebre e a
Tartaruga* fala de uma corrida entre dois animais, uma lebre (por
natureza muito rápida) e uma tartaruga (por natureza muito devagar), que
ao final tem um resultado inesperado. Na fábula [*O Sapo e o
Boi*](#ytModal-23HQFq0A4AY), um sapinho
cheio de inveja de um boi imponente tenta ficar inchado até ficar do
tamanho do boi. Apesar de os amigos pedirem para que ele desista,
continua inchando até estourar. Nos dois contos, há muitas coisas
fictícias, aplicando-se diversas figuras antropomórficas aos animais
(ver capítulo 17). Mas, apesar de irreais, essas são histórias concretas
com animais que podemos ver e que fazem ações reais. Nelson Pimenta
conta essas fábulas com muitas figuras estéticas, seguindo as regras de
criação de histórias visuais em Libras e, por isso, são contos que
podemos valorizar como arte literária e divertida. No entanto, essas
histórias têm um sentido metafórico também.

Normalmente, o contador de uma fábula termina com o significado da
história, que revela a metáfora. A moral trazida ao final de *A* *Lebre
e a Tartaruga* explica, por exemplo, que uma pessoa com menor habilidade
natural, mas persistente (a tartaruga), pode vencer uma pessoa com mais
habilidade, mas menos aplicação e afinco (a lebre). A moral da história,
no final de *[O Sapo e o
Boi](#ytModal),* ensina que é
melhor se aceitar como se é e não tentar ser o que não se pode ser (um
sapo nunca pode atingir o tamanho de um boi).

Nas parábolas, como nas fábulas, o sentido não é aparente e os
personagens são simbólicos, embora eles sejam humanos e não animais. A
história [*Fazenda: Vaca*](#ytModal-NtN98y67ukM),
contada por Rimar Segala, é uma parábola. Nela, assim como em muitas
outras, o assunto da narrativa é concreto, mas a ideia contada é mais
abstrata. Nas parábolas, normalmente, o contador também explica os
significados. Por exemplo, na bíblia, onde são escritas as parábolas de
Jesus, depois de contar as histórias ele explica o que elas significam
para os discípulos.

Em [*Fazenda: Vaca*](#ytModal-NtN98y67ukM), um
mestre e um discípulo seguem por uma estrada com fome. Uma família
pobre, com apenas uma vaca, os recebe e dá leite e queijo para eles com
toda a sua generosidade, apesar de serem muito pobres. O pai da família
explica que graças a sua vaca continuam pobres, mas têm as necessidades
mais básicas supridas. O mestre manda seu discípulo empurrar a vaca de
um precipício e matá-la. Muitos anos depois, o discípulo se arrepende de
matar o único apoio que a família tinha e resolve voltar para pedir
desculpas. Lá ele encontra uma fazenda rica e a família confortável com
toda a comida que quisesse ter. O pai explica que, um dia, a única vaca
morreu por acidente e a família teve que que se esforçar para
sobreviver. Tentaram novos negócios, até que criaram uma fazenda
produtiva - o que nunca teria acontecido se a vaca continuasse a
fornecer somente o básico.

No final dessa parábola, Sueli Ramalho explica que a história mostra
para o surdo que não é bom sempre se fazer a mesma coisa de maneira
limitada, mas que se deve abrir a mente, se arriscar a fazer novas
coisas, se desenvolver e tentar ter mais sucesso.

#### 16.5.2. Narrativas surdas originais

As metáforas acontecem também nas narrativas originais dos surdos.
Nelas, o narrador não explica o significado da metáfora, mas deixa para
público a tarefa de entendê-la. Às vezes a metáfora é fácil de entender,
como veremos na narrativa [*O passarinho
diferente*](#ytModal-P_DR60BJuFI),
contada por Nelson Pimenta

A história é baseada numa versão em ASL do artista e pesquisador surdo
Ben Bahan. Nelson fez a sua tradução para Libras com adaptação à cultura
brasileira. O passarinho nasce dentro de uma família de águias, mas com
o bico pequeno e reto. A família percebe que ele é diferente e tenta
achar uma cura para que ele se torne uma águia com o bico grande e
curvado, ou, pelo menos, que ele aprenda a viver como elas, caçando
coelhos e voando alto. O passarinho vai a uma escola especial para
pássaros com bicos defeituosos como o dele para tentar remediar o
problema. Conhece um grupo de passarinhos como ele, vivendo de outra
maneira, comendo uvas e cantando; fica muito contente com eles, mas
acaba voltando para a sua família. Ele aceita então uma cirurgia para
que seu bico fique curvado e parecido com o de uma águia, mas fica
zangado porque parece mais um papagaio. Ele volta aos amigos
passarinhos, mas com a nova forma de seu bico ele não consegue mais
comer uvas nem cantar. No final, ele voa sozinho até o pôr do sol no
horizonte.

A narrativa fala das aves, mas entendemos que é uma metáfora da
sociedade humana. Para os surdos, fica claro que a narrativa é uma
metáfora para a experiência dos surdos numa sociedade de ouvintes. O
passarinho é o filho surdo e a família de águias (que ama muito o seu
filho) é a família ouvinte. As tentativas de curar o bico têm paralelo
com as tentativas de curar a surdez, e a escola para remediar o
passarinho tem professores, objetivos e falhas bem conhecidos pela
comunidade surda em sua educação. Os passarinhos que cantam e comem uvas
são uma metáfora para a comunidade surda, vivendo do jeito natural
deles. A cirurgia para curvatura do bico remete à cirurgia do implante
coclear e o resultado de criar um bicho nem de águia nem de passarinho é
o surdo que não se encaixa nem no mundo dos surdos tampouco no dos
ouvintes.

Mas nem todas as metáforas são tão fáceis de entender. A pesquisadora
surda norte-americana Phyllis Wilcox explica que públicos diferentes
podem ter opiniões distintas sobre o significado de uma metáfora
literária. Sua pesquisa mostrou o poema *Two Dogs* (*Dois cachorros*) da
poeta Ella Mae Lentz em ASL para surdos de três países. No poema, dois
cachorros estão amarrados por uma corrente. Um deles é de raça pura e
outro um vira-lata. Os dois se odeiam, mas não podem fugir, então
decidem aceitar suas diferenças e morar juntos. O poema é uma metáfora –
mas o que significa? Quem são os cachorros? O que significa a corrente?
Os surdos norte-americanos acharam que os cachorros significam os surdos
que usam ASL e os surdos oralizados, ambos ligados pela vida cotidiana
da comunidade surda. Os surdos na Suíça pensaram que o poema falava dos
ouvintes e dos surdos, ambos ligados pela humanidade. Já os italianos
entenderam que o poema falava dos nativos de um país e dos imigrantes
recém chegados, ligados pela mesma cidadania. Na verdade, qualquer
interpretação é válida.

Agora veremos o exemplo de um texto em Libras em que sabemos que existe
uma metáfora, mas não fica claro o que ela significa. Já assistimos ao
poema narrativo [*Bolinha de
Ping-pong*](#ytModal-VhGCEznqljo), de Rimar
Segala. Nesse texto, há uma competição de pingue-pongue entre duas
pessoas muito diferentes - um homem forte e mais grosseiro e uma mulher
mais refinada e aparentemente delicada. No desenvolvimento da história,
vemos que a bolinha sofre no jogo e pede para o juiz ajudá-la. No final
da competição, depois de lutar e de bater a bolinha de cá para lá,
nenhum dos participantes a quer mais. Então o narrador pergunta: “E
aí?”.

Essa é uma narrativa com um assunto concreto, em que as pessoas
interagem com um objeto, mas tem um sentido abstrato e conceitual que
não é tão fácil de mostrar visualmente. No início da história, Rimar
explica que se trata de uma metáfora, mas cada pessoa precisa decidir
qual é o significado que está por trás das imagens apresentadas. Os
públicos entendem que a metáfora mostra uma pessoa com pouco poder que
se sente pressionada por duas demandas mais poderosas que estão em
conflito. Algumas pessoas acham que a bolinha é surda e os dois
competidores são a sociedade ouvinte e a comunidade surda. Outras
pessoas dizem que são os sistemas de educação oral/inclusivo e bilíngue,
ou que a bola é um filho surdo e que a mãe e o pai lutam para controlar
a vida dele. Outra interpretação é a de que a bolinha pode ser qualquer
pessoa (surda ou ouvinte) que sinta a pressão de duas demandas opostas,
por exemplo estudar ou trabalhar, ou obedecer às regras da sociedade ou
seguir outro estilo de vida.

O prazer da metáfora literária está em se pensar no que ela significa,
além de apreciar as imagens concretas apresentadas para tentar entender
um significado abstrato.

Talvez o público nem perceba que um texto é uma metáfora. Veremos isso
no poema em BSL [*Doll*](#ytModal-5iM7zbis68w) (em português, *A
Boneca*) do poeta britânico Paul Scott. Embora seja destinado para um
público britânico de BSL, o poema usa apenas classificadores e
incorporação[42](#tooltip), então é facilmente entendido por usuários de Libras.
Fala de uma criança que arruma os cabelos da boneca, coloca uma
maquiagem nela e ao final arranca a sua cabeça. Muitas pessoas acham o
poema um conto curtinho e engraçado que fala do jeito das crianças de
destruírem seus brinquedos. Superficialmente, parece isso mesmo. Ainda
assim, é um poema rico pelo uso de uma linguagem poética. Porém, Paul
Scott explicou que essa é uma metáfora séria e pesada, que mostra a
crueldade da sociedade ouvinte que tenta transformar os surdos e os
oprime ao querer que sejam ouvintes contra suas vontades, até que
aceitam a sua destruição total. Quem percebe isso não ri mais, mas ao
assistir a um poema que parece tão inocente sendo tão cruel, emoções
fortes são instigadas.

O poema *[Homenagem Santa Maria](#ytModal-9LtOP-LLx0Y)/RS,* de
Alan Henry Godinho, mostra metáforas de outro tipo. É um lamento em
homenagem aos jovens da cidade de Santa Maria, no Rio Grande do Sul, que
morreram em um trágico incêndio em uma festa de formatura em 2013. Nesse
poema, o artista fala dos jovens como se fossem muitas flores ou
estrelas. Sobre as flores, sabemos que são lindas, mas que vivem pouco
tempo. Compara as flores com a vida dos jovens. Depois, fala deles como
se fossem estrelas. As estrelas também são lindas, mas elas duram muito
tempo. Então compara as estrelas com a memória dos jovens. Sabemos que
eles não são flores nem estrelas, tampouco pássaros que voam para o céu
(o que o poeta também menciona), mas as ideias dentro do poema são
metáforas concretas (flores, estrelas, pássaros) que mostram conceitos
abstratos (vida, memória e paz).

O poema [*Lutas surdas*](#ytModal-aOQx2YMj6Xc), também de Alan
Henry Godinho, descreve de forma poética o movimento de luta dos surdos
contra o fechamento do Instituto Nacional de Educação de Surdos, o INES.
O poema tem muitas metáforas visuais. Ao falar da marcha de protesto dos
surdos em Brasília, mostra visualmente a estrutura do Congresso Nacional
tremendo com as vibrações da manifestação. Os prédios não tremeram de
verdade, mas o poema os mostra de uma forma criativa, como se tremessem.
Alan Henry não usou o sinal <span
style="font-variant:small-caps;">tremer</span>, mas adicionou um
movimento de tremer dentro dos sinais classificadores que mostram as
edificações. Os prédios de Brasília são uma figura metonímica para
significar o governo. Um prédio não pode respeitar a Libras, mas as
pessoas poderosas dentro deles podem. Em outra metáfora, os surdos no
protesto fazem uma construção de pedras, representando a FENEIS (a
federação nacional dos surdos), as associações de surdos estaduais, a
cultura surda, as associações esportivas dos surdos e a sociedade que
apoia os surdos. Na verdade, todas as pedras da construção são coisas
abstratas – sabemos que associações, culturas e sociedades não têm forma
–, mas por metáfora são transformadas para serem concretas e mostrarem
que os conceitos abstratos também apoiaram a luta. Quando os surdos no
poema fazem essa construção referência, eles erguem uma bandeira enorme
em cima dela, que tremula com o orgulho da Libras. A bandeira é um
objeto concreto, mas a importância da língua de sinais é abstrata. A
criação do sinal da bandeira feita por mãos que sinalizam mostra a
importância da Libras para a comunidade surda. A bandeira não é de fato
feita por uma língua nem pode fazer um prédio tremer, mas através da
metáfora Alan Henry cria a ideia de que isso é possível.

No último exemplo, vemos um conceito visual importante de metáfora em
Libras, que é a ligação entre a forma dos sinais e a forma física do
referente. Uma bandeira tem uma área extensa de duas dimensões. A mão
aberta também tem uma área extensa bidimensional. O deslocamento da mão
no espaço traça uma área ainda maior, que cria uma bandeira maior. É
fundamental a essa metáfora em Libras que a forma dos sinais <span
style="font-variant:small-caps;">bandeira</span> e <span
style="font-variant:small-caps;">sinal</span>, e dos sinais <span
style="font-variant:small-caps;">bandeira-grande-tremulando</span> e
<span style="font-variant:small-caps;">mãos-sinalizando,</span> sejam
muito parecidas. Essa ligação de forma e conteúdo numa metáfora é uma
comparação ou um paralelo visual muito forte.

### 16.6. Metáforas conceituais

Na teoria de Lakoff e Johnson (1980) vemos uma diferença importante
entre a metáfora conceitual e a metáfora linguística. Segundo os
autores, a primeira é gerada por nossas associações entre conceitos
abstratos mapeados do abstrato ao concreto. Esses mapeamentos mentais
são realizados por meio de metáforas linguísticas. Por exemplo, na
metáfora conceitual podemos lidar com abstrações como tempo, causas,
relações sociais e emoções como se fossem coisas concretas. Embora as
metáforas conceituais não sejam representadas diretamente na literatura
em Libras, as representações linguísticas delas usam essas metáforas.
Falaremos um pouco sobre elas agora.

**Metáforas ontológicas**[43](#tooltip) são formas de conceber conceitos
abstratos tais como eventos, atividades e emoções como se fossem
entidades e substâncias reais. Na metáfora ontológica, a mente é uma
entidade que funciona como um recipiente. Podemos, literalmente, colocar
objetos concretos dentro de um recipiente e, metaforicamente, a
informação é colocada dentro dele e manejada por meio de vários
classificadores e configurações de mão (NASCIMENTO, 2009, p. 52-53).
Claro que a mente não é um recipiente de verdade, mas pensamos nos
processos intelectuais como se fossem objetos concretos que podemos
manipular e colocar na mente. No sinal <span
style="font-variant:small-caps;">aprendizagem</span> temos a ideia de
que é possível segurar uma informação com a mão e colocá-la dentro da
cabeça (onde se localiza a mente) e no sinal <span
style="font-variant:small-caps;">esquecer</span> imaginamos que podemos
tirar uma informação da cabeça com a mão.

Nas **metáforas estruturais**, um conceito é estruturado em termos de
outros (NASCIMENTO, 2009, p. 52). Nos poemas em Libras podemos ver
exemplos como CONHECIMENTO É LUZ e LIBERDADE É VOAR. Com o primeiro,
pode-se falar do sol ou de uma lanterna com a intenção de falar do
conhecimento ou do entendimento. O poema [*O Farol da
Barra*](#ytModal-VXcKgO-jD9A), de Maurício Barreto, que fala da
importância da Libras para os surdos, começa com sinais que falam da
escuridão da noite, seguido de um pouco de iluminação da lua e depois da
clareza do Farol. Esse poema também trata da metáfora A GRAÇA DE DEUS É
LUZ. O sinal <span style="font-variant:small-caps;">graça-divina</span>
é apresentado sendo semelhante ao sinal <span
style="font-variant:small-caps;">luz-do-farol</span>, para que o poeta
possa usar os dois sinais para dizer que a graça de Deus é como a luz do
farol, não apenas conceitualmente, mas também porque o *sinal* <span
style="font-variant:small-caps;">graça</span> é como o sinal <span
style="font-variant:small-caps;">luz-do-farol.</span>

**Metáforas espaciais** ou orientacionais organizam todo um sistema de
conceitos em relação a outro a partir de bases físicas, sociais e
culturais possíveis que estão enraizadas na experiência física e
cultural. Por exemplo, locações ou movimentos para cima mostram a ideia
de estar bem, de coisas positivas, do que é bom; os mesmos movimentos
para baixo representam a ideia de estar mal, de coisas negativas, do que
é ruim. Essas metáforas orientacionais são muito importantes para falar
de poder e opressão.

Como vimos no capítulo anterior, no poema [*As
Brasileiras*](#vimeoModal-242326425), de Anna Luiza
Maciel e Klícia Campos, os sinais da poderosa Paulistana são articulados
acima dos sinais da pobre Nordestina. Não falam diretamente do poder,
mas a locação dos sinais mostra essa desigualdade metaforicamente. No
poema *[Como Veio Alimentação](#ytModal-nMOTYprbYoY),* de
Fernanda Machado, o olhar, o corpo, a locação e a direção do movimento
dos sinais relacionados ao trabalhador rural pobre são mais baixos do
que os do consumidor urbano rico.

A configuração de mão pode ter sentido simbólico. Os sinais com dedos
curvados “em garras” têm sentidos mais negativos do que aqueles feitos
com os dedos esticados. Os sinais em Libras <span
style="font-variant:small-caps;">guerra, raiva, angústia</span> e <span
style="font-variant:small-caps;">violência</span>, têm essa configuração
de mão, e essa associação permite novos sentidos nos poemas. Em *[Como
Veio Alimentação](#ytModal-nMOTYprbYoY),* os dedos dos sinais
articulando o trabalhador são curvados e os dedos usados para
representar o consumidor são retos. Em [*Meu Ser é
Nordestino*](#ytModal-t4SLooMDTiw), sinais com
mão em garras, sendo eles normalmente negativos ou não, dão a impressão
de sofrimento, como <span
style="font-variant:small-caps;">sol-forte</span>, <span
style="font-variant:small-caps;">seca, árvore-torta</span> e até <span
style="font-variant:small-caps;">rural</span>. Vamos falar mais sobre
isso no capítulo 18.

Lembramos que nem todos os movimentos e as locações ou configurações de
mão têm sentido metafórico. Por exemplo, [*A Pedra
Rolante*](#ytModal-kPXWu5UCTzk), de Sandro Pereira, tem sinais
de diversas alturas e configurações de mão, mas não têm sentido
metafórico.

<div class="float"></div>

![resumo](/images/resumo-parte3.png)

### 16.7. Resumo

Nesse capítulo, investigamos a importância da metáfora para a literatura
em Libras. Vimos que a metáfora é uma maneira pela qual a pessoa fala
uma coisa, mas tem a intenção de ser entendida de outra forma. Em
Libras, assim como em português, muitas vezes falamos de ideias
abstratas por meio de coisas concretas. Todavia, a Libras usa essa
figura muito mais do que a língua portuguesa por ser uma língua visual
em que o vocabulário é baseado na forma de imagens concretas de coisas
que são abstratas.

Muitos textos literários em Libras não usam metáforas, mas em outras
produções textuais na língua o significado apresentado pelo conteúdo não
é o sentido único ou verdadeiro do texto. Vimos exemplos de fábulas e
parábolas contadas em Libras em que a narrativa tem um significado além
da história, e que geralmente faz parte desta. Observamos, também, que
existem outras histórias e outros poemas metafóricos que não explicam a
metáfora e fica a cargo do público resolver o enigma. Talvez a resolução
não seja fácil, ou não seja definitiva ou, talvez, o público nem perceba
a presença de uma metáfora, mas tudo isso faz parte da rica experiência
do público literário de Libras.

<div class="float"></div>

![atividade](/images/atividade-parte3.png)

### 16.8. Atividade

1. Crie um poema curto em Libras para cada estação do ano (primavera,
verão, outono e inverno).

Ajuste a velocidade (rápida ou lenta) e o tamanho (grande ou pequeno)
dos sinais e use metáforas orientacionais para ilustrar simbolicamente
as diferenças entre as quatro estações.

Por exemplo, seu poema sobre a primavera pode ter movimentos rápidos
para sugerir que é uma estação de mudanças; já o verão pode ter
movimentos lentos para sugerir o calor.

2. Pense em sinais com mãos em garra com sentido negativo.

    - Pense em sinais com mãos em garra com sentido positivo.

    - Pense em sinais com mãos abertas com sentido negativo.

    - Pense em sinais com mãos abertas com sentido positivo.

    - Pense em sinais com locação alta e/ou movimento para cima com sentido
negativo.

    - Pense em sinais com locação alta e/ou movimento para cima com sentido
positivo.

    - Pense em sinais com locação baixa e/ou movimento para baixo com
 sentido negativo.

    - Pense em sinais com locação baixa e/ou movimento para baixo com
sentido positivo.

    - Agora, crie um poema curto em Libras que use esses sinais de forma
metafórica.

<span class="tooltip-texto" id="tooltip-41">
A metáfora não se traduz para outras línguas orais. Por exemplo
    “wooden faced” – a tradução literal de cara de pau para inglês,
    significa que a pessoa não mostra as emoções (em português isso
    seria “cara de samambaia” ou “cara de paisagem”).
</span>

<span class="tooltip-texto" id="tooltip-42">
Fora dos sinais em BSL que ele repete: “Coitada da boneca.”
</span>

<span class="tooltip-texto" id="tooltip-43">
A ontologia estuda os seres e a relação entre conceitos de
    sistemas diferentes e pergunta “O que é isso?”
</span>
