---
capitulo: 22
parte: 4
titulo: Tradução de literatura em Libras 
pdf: "http://files.literaturaemlibras.com/CP22_Traducao_de_literatura_em_Libras.pdf"
video: msq-hqKH1k4
---

## 22. Tradução de literatura em Libras 


<div class="float"></div>

![objetivo](/images/objetivo-parte4.png)

### 22.1. Objetivo

Neste capítulo pensaremos sobre tradução de literatura: de literatura em
português para a Libras ou de literatura em Libras para o português.
Vamos pensar sobre as opções que existem para se levar uma narrativa ou
um poema de uma língua para outra e sobre alguns desafios nesse
processo. Lembramos que as traduções literárias entre Libras e língua
portuguesa são feitas por ouvintes ou surdos, ou por tradutores ouvintes
e surdos trabalhando em parceria.

<div class="float"></div>

![lista de videos](/images/videos-parte4.png)

<div class="lista-de-videos"></div>

Usaremos os seguintes vídeos:

* *[Como Veio Alimentação](#ytModal-nMOTYprbYoY),* de Fernanda
Machado.

* [*Golf Ball*](#ytModal-Gl3vqLeOyEE), de Stefan Goldschmidt.

* *[Lei de Libras](#vimeoModal-267274663),* de Sara
Theisen Amorim e Anna Luiza Maciel.

* *[O Negrinho do Pastoreio](#vimeoModal-306082977),* de
Roger Prestes.

* *[O Pequeno Príncipe](#ytModal-foMiwFlVHCc),* Projeto
Acessibilidade em Bibliotecas Públicas.

* *[Peixe](#ytModal-LEDC479z_vo),* de Renato Nunes.

* *[A Rainha das Abelhas](#ytModal-nXI4aO2_G3E),*
de Mariá de Rezende Araújo.

* *[Vagalume](#ytModal-JG8xrh1az_g),* de Tom Min
Alves.

### 22.2. Reconto, adaptação e tradução

É comum, quando falamos de literatura em Libras, dividi-la entre peças
traduzidas, adaptadas e originais (MOURÃO, 2011). Neste capítulo, como
vamos focar na tradução, não falaremos mais de criação original - além
de destacar que a diferença entre tradução, adaptação e criação original
não é tão claramente delineada, pois todas as traduções necessitam uma
adaptação e têm um elemento de criação original por parte do tradutor.
Além disso, podemos acrescentar um tipo de literatura em Libras que não
é totalmente de origem surda, o reconto.

#### 22.2.1. Reconto

Uma das principais características do folclore (dos contos de fadas, das
lendas folclóricas e das piadas), é que cada pessoa pode recontar essas
histórias do seu próprio jeito, desde que o enredo principal seja
mantido, porque não existe uma versão “certa”. Retomando as ideias de
Vladimir Propp (que vimos no capítulo 11), podemos dizer que as unidades
da história são iguais, embora a maneira de contar seja diferente. Por
exemplo, na fábula de Esopo *A lebre e a Tartaruga,* é importante
informar que os dois animais concorrem, a lebre dorme e a tartaruga
alcança antes dela a linha de chegada. Além disso, os outros fatos e as
palavras que usamos para contar a fábula não importam muito. Cada
reconto mantém o conteúdo básico dos originais, tanto em Libras quanto
em português (ou em qualquer outra língua). Os tópicos são iguais, os
personagens principais e os eventos fundamentais - até a estrutura geral
– se mantém, mas o narrador tem a liberdade de contar como quiser.

Existem muitos contos, diversas histórias, lendas e piadas mundiais que
vêm das tradições brasileiras da sociedade ouvinte e que são também
contados em Libras pelos surdos. Vemos muitos exemplos de recontos em
língua brasileira de sinais, nos contos de fadas (como *Chapeuzinho
Vermelho, João e Maria, João e o Pé de Feijão*). O conto
[A Rainha das Abelhas](#ytModal-nXI4aO2_G3E), de Mariá de Rezende
Araújo, segue as informações, a estrutura e o padrão da história
original, mas a versão exata é a criação da contadora. Existem muitas
variações da lenda [O Negrinho do Pastoreio](#vimeoModal-306082977),
que é contada em Libras por Roger Prestes. Ele escolheu a sua forma
preferida de contar de uma maneira visual, estética e clara em Libras,
mas a lenda é basicamente igual às outras versões. Nas fábulas de Esopo
(como *O Sapo e o Boi e A lebre e a Tartaruga*) e nas histórias da
bíblia (por exemplo as de Noé, David ou Moisés, ou as parábolas de
Jesus) temos a mesma flexibilidade de contar o enredo com as nossas
próprias palavras, porque elas fazem parte da tradição oral (não
escrita) que, por sua vez, fazem parte do folclore. Esse tipo de reconto
pertence à vida e à identidade cultural e social dos brasileiros e é
importante que exista também em Libras, para que as pessoas surdas
possam ter as mesmas informações e o mesmo prazer que os ouvintes têm em
conhecer as histórias.

#### 22.2.2. Adaptação

As adaptações de histórias traduzidas para
Libras são destacadas especialmente por adaptarem seu enredo para
incluírem nele personagens surdos (MOURÃO, 2011). Quando contamos os
contos tradicionais em língua de sinais, apesar de mantermos o enredo
básico, adicionamos esses elementos culturais da comunidade surda. Já
vimos no capítulo 8, que no conto *Cinderela Surda*, a maior parte do
enredo fica igual ao tradicional (a Cinderela é proibida de ir ao baile,
mas a madrinha a ajuda ir, ela encontra o Príncipe, foge à meia noite
etc.), mas existem adaptações feitas especialmente para os surdos, como
aquela em que a Cinderela e o Príncipe são surdos e, em vez de perder o
sapato, ela perde a luva, que é uma marca surda porque enfoca as mãos
(em outras ela perde o aparelho auditivo). Nas versões diferentes do
conto *Os Três Porquinhos*, ou todos são surdos, com comportamento
surdo, ou os porquinhos são surdos e o lobo é ouvinte.

Essas alterações nem sempre eram comuns e não são iguais em todos os
países. Um surdo britânico observou que, até os anos 80, os surdos que
contavam histórias clássicas em BSL não as adaptavam, porque acreditavam
que isso era uma falta de respeito aos ouvintes por estarem mexendo nas
suas narrativas. Isso também acontece com as histórias da bíblia.
Algumas pessoas acham que não devem adaptá-las, mas há surdos que já
contaram adaptações em que foram incluídos personagens também surdos. A
surda britânica, Penny Beschizza, contou a história de Moisés, a
travessia do Mar Vermelho e a divisão das águas, mas incluiu no enredo
um grupo de surdos. Estes ficaram por último na travessia e demoraram
muito porque estavam sinalizando entre si e admirando os peixes na água.
Estavam em perigo de os egípcios os pegarem ou de morrerem afogados
quando as águas se fechassem, mas Deus mandou alguns raios e relâmpagos
como distratores e eles conseguiram chegar na praia vivos. Depois desse
reconto com adaptação, um surdo disse que foi a primeira vez na sua vida
em que sentiu que a bíblia realmente o tocou como surdo.

#### 22.2.3. Tradução 

Às vezes, usamos o termo tradução de uma forma mais ampla para falar de
qualquer forma de se levar uma história de origem não surda para a
comunidade surda. Assim, o reconto e a adaptação se incluem nessa
definição e são tipos de tradução. Mas, também, podemos partir de uma
perspectiva mais estrita, usando a ideia da tradução para as
transposições que seguem mais fielmente as palavras de um texto. Vemos
muito disso nas traduções de materiais educacionais que têm o objetivo
de ensinar português através da Libras. Nesse caso, muitas vezes, vemos
as palavras do texto ao lado dos sinais, como num texto em português
escrito e em Libras escrita (em SignWriting), a exemplo das lendas *Onze
Histórias e Um Segredo: desvendando as Lendas Amazônicas* (SALES, 2016).
Porém, muitas vezes, vemos vídeos com o texto em português escrito e a
tradução em Libras sinalizada, como [O Pequeno Príncipe](#ytModal-foMiwFlVHCc), feito pelo Projeto
Acessibilidade em Bibliotecas Públicas, ou as traduções dos contos de
literatura brasileira (como os de Machado de Assis) feitas pela Arara
Azul em 2005. Alguns são simplesmente um vídeo da contação em Libras com
legendas do texto original em português e outros como um e-book, com um
trecho de texto maior em uma parte da tela e a versão em Libras na
outra. Nessas traduções, os textos escritos em português e os textos
sinalizados em Libras são mais parecidos, porque o principal objetivo da
tradução é recriar, o máximo possível, as mesmas informações produzidas
no texto em português na versão em Libras. Isso também acontece nas
traduções de letras de música em vídeos do YouTube. Há traduções de uma
forma de Libras muito estética e criativa e que não seguem as palavras
uma a uma, mas sempre têm uma relação mais de perto com o português
porque as palavras do texto em língua portuguesa são reconhecidas como
importantes. A tradução da canção
[Vagalume](#ytModal-JG8xrh1az_g), de Tom Min Alves (da música
Vagalumes, do grupo Pollo) é um bom exemplo desse tipo de tradução.

Até agora, falamos de recontos, adaptações e traduções do português para
Libras, mas lembramos que eles também existem na outra direção, de
Libras para português, embora as suas gravações sejam menos comuns.
Existem pesquisas sobre as narrativas pessoais dos surdos feitas em
Libras e escritas em português (MATOS, 2008). Algumas histórias contadas
em Libras já foram escritas em livros. A piada no livro *Adão e Eva*
(KARNOPP; SILVEIRA, 2014) foi contada originalmente em Libras e depois
publicada num livro escrito em língua portuguesa. Veremos também que, em
certas ocasiões, os intérpretes são chamados para interpretar os eventos
literários de Libras na modalidade vocal, ou seja, para o português.

### 22.3. Tradução literária

Antes de fazer uma tradução de português para Libras, sempre precisamos
refletir sobre o objetivo desse trabalho. É didático, talvez para
ensinar a língua portuguesa através da Libras ou o conteúdo da obra
literária? É para fornecer aos surdos o acesso às informações do texto
em português? Ou para proporcionar prazer ou gerar outras emoções no
público surdo iguais às geradas no público ouvinte?

Numa tradução literária, o foco não é tanto o conteúdo ou as
informações, mas sim os efeitos estéticos. Para traduzir textos
artísticos entre duas línguas, é preciso entender as normas literárias
dessas duas línguas. Simplesmente traduzir um texto literário de
português para uma forma de Libras cotidiana ou não estética pode levar
o conteúdo, mas não a beleza, até o público surdo, de modo que ele possa
dizer “entendi” - mas esse público não vai sentir as mesmas emoções do
ouvinte. Por isso, os tradutores e intérpretes de Libras devem entender
bem as normas literárias da Libras e da comunidade surda antes de fazer
as traduções planejadas.

Além de perguntar qual o objetivo da tradução, podemos também
questionar: qual o objetivo original da obra literária? É para o público
rir, sorrir, chorar ou admirar? É para aprender novas coisas, pensar com
outro ponto de vista e entender a vida e as experiências de outras
pessoas? A resposta a essas perguntas vai influenciar na tradução.

#### 22.3.1. Tradução literária de português para Libras

Já vimos, nos capítulos anteriores, que existem elementos linguísticos e
culturais que fazem parte das normas surdas literárias em Libras. Para
se fazer uma boa tradução literária de português para Libras, precisamos
entender bem as regras literárias de ambas as línguas. Sabemos que em
língua brasileira de sinais devemos considerar o ritmo, as configurações
das mãos, o espaço, a velocidade, as perspectivas múltiplas e
simultâneas, a incorporação, o uso de expressão facial e do corpo e os
classificadores. Thatiane do Prado Barros (2015) oferece um bom resumo
dessas considerações para a tradução em sua pesquisa, na qual traduziu
os poemas de Carlos Drummond de Andrade para Libras.

Klícia Campos (2017), em sua investigação sobre a tradução de literatura
de cordel, fala das etapas necessárias para tradutores (sejam eles
surdos ou ouvintes) atingirem uma boa tradução literária em Libras. Ela
sugere que se pode começar com uma tradução intralingual, de português
literário e estético para o português não literário - nos casos em que a
linguagem do texto de origem apresente elementos difíceis, como palavras
arcaicas ou regionais. O próximo passo é fazer uma tradução
interlingual, do português para a Libras. E a última etapa seria mais
uma tradução intralingual, de Libras não estética para Libras estética,
seguindo as normas literárias da comunidade surda brasileira.

O trabalho de Campos foca nas competências tradutórias necessárias para
se fazer a tradução de um folheto de cordel para Libras, com olhar
especial para os desafios que esta cria para os tradutores surdos. A
comunidade surda tem acesso limitado à herança cultural da região
nordeste, e isso já é um desafio a ser enfrentado pelos tradutores. Por
exemplo, alguns surdos não têm conhecimento, como os demais brasileiros
têm, sobre a vida e os dialetos nordestinos por não receberem uma
educação consistente e completa no sistema oralista ou no inclusivo.
Esse conhecimento é importante para qualquer tradução, ou seja, para a
tradução literária em Libras também. Talvez um público surdo não tenha
conhecimento de conceitos daquela região do Brasil (como o de “sertão”),
ou eles até os conheçam, mas não sabem quais os sinais que já existem
para falar dessas coisas. Além disso, alguns tradutores surdos que
conhecem bem a língua e a cultura da região não estudaram as normas
literárias da Libras, porque elas são pouco debatidas, não fazem parte
do currículo dos alunos surdos na escola, portanto, não sabem como criar
uma tradução estética.

#### 22.3.2. Traduções e interpretações de Libras para o português

As traduções de Libras para o português são ainda mais raras do que as
realizadas na direção contrária, mas são importantes para que o público
ouvinte que não domina a língua de sinais entenda a riqueza da
literatura em Libras.

Uma tentativa de criar uma tradução literária
de Libras para o português veio do tradutor e professor Markus Weininger
com o poema haicai [Peixe](#ytModal-LEDC479z_vo), de Renato Nunes. A
tradução dele segue a forma tradicional desse gênero e captura o
conteúdo. A tradução cria um poema agradável em português, mas não vemos
nada nela que sugira que há tanta ambiguidade na imagem, ou que haja
apenas uma configuração de mão e que cada sinal começa no ponto final do
sinal anterior, porque não é possível incluir todos esses elementos numa
só tradução.

<div class="poesia"></div>

> Ondas do sol irradiando\
> Peixe flutua na água, liberdade\
> Choque na parede invisível

O público ouvinte, embora não entenda Libras, pode ver a produção no
corpo do artista e entender muitas coisas - como a emoção - simplesmente
por ver a performance (SUTTON-SPENCE; QUADROS, 2014; FELÍCIO, 2017). É
comum ver as traduções de literatura em Libras produzidas juntamente à
fonte original. Por exemplo, num vídeo com legendas o leitor pode ver o
poema e ler a tradução simultaneamente. Numa interpretação vocalizada,
isso também acompanha a performance incorporada de Libras e o espectador
pode ver o poema e ouvir a tradução. Nessas situações, as informações
disponíveis ao público são maiores.

Às vezes, as pessoas até perguntam se é realmente necessário traduzir as
obras literárias de Libras que são muito visuais, porque, com certeza,
os ouvintes conseguem entender, não é? Mas pesquisas mostram que, apesar
de um poema ou uma narrativa ser muito visual, os ouvintes que não
dominam Libras não os entendem bem. Quem não domina a língua de sinais
não sabe se a mão do artista faz um sinal ou um classificador, ou se é
parte da incorporação (SUTTON-SPENCE; QUADROS, 2014; FELÍCIO, 2017).

Às vezes, apenas o título basta para ajudar o público a entender a obra,
porque ele norteia o espectador a usar o conhecimento de mundo para
entender os sinais no contexto. A obra [*Golf Ball*](#ytModal-Gl3vqLeOyEE), de Stefan Goldschmidt, apresenta imagens fortemente visuais, mas
não usa vocabulário de nenhuma língua de sinais. Quando sabemos que se
trata de uma bolinha de golfe, isso já abre um contexto para se imaginar
tacos de golfe, o anteparo, a bandeirinha e o buraco. Sabendo tudo isso,
fica fácil acompanhar. Sem saber que se trata de golfe, interpretá-la é
um desafio para quem não domina Libras.

Há um equilíbrio entre a informação visual e linguística nos textos
literários e o que é necessário para a compreensão. Poemas ou narrativas
com maior informação linguística vão exigir mais interpretação
linguística, mas textos com menos informação desse tipo e mais
informação semiótica vão exigir menos interpretação linguística. Nesses
casos, o tradutor-intérprete pode deixar o público ouvinte que não sabe
Libras focar nos elementos visuais, sem a tradução para português.

Como exemplo, podemos comparar o poema [*Lei de
Libras*](#vimeoModal-267274663), de Sara Theisen Amorim
e Anna Luiza Maciel, com o poema *[Como Veio
Alimentação](#ytModal-nMOTYprbYoY),* de Fernanda Machado. *Lei
de Libras* tem muitos sinais de vocabulário, como <span
style="font-variant:small-caps;">libras, lei, adquirir, produção,
sinalizar, comunicar, expressão-facial, valorizar, poesia, humor,
teatro, vernáculo-visual, é, sempre, nós</span> e, assim, podemos
traduzir as palavras para que o público entenda o conteúdo e deixar as
pessoas curtirem o ritmo e a performance elegante e agradável. Por outro
lado, nenhum sinal no poema *[Como Veio
Alimentação](#ytModal-nMOTYprbYoY)* é um sinal do próprio
vocabulário da Libras. Cada sinal necessita de uma sentença curta na
tradução em língua portuguesa, o que “atrapalha” a poesia do texto. Mas
o poema é muito visual, então a poeta pode explicar, antes da
apresentação, que o poema trata da desigualdade entre os trabalhadores
rurais e os habitantes urbanos e até pode avisar que é preciso perceber
o comportamento dos dois personagens incorporados no poema. Depois, não
é necessária uma tradução durante a sua apresentação.

Alguns artistas surdos (e seu público) preferem não ter interpretação
durante uma performance literária, porque a voz tira a atenção do corpo
do artista, onde fica a produção. Nessa situação, o artista pode
apresentar uma tradução escrita antes da sua apresentação e depois o
público assiste à performance sem interpretação. Uma opção para a
tradução escrita é de seguir uma forma tradicional, usando as normas da
literatura de língua portuguesa. Por exemplo, podemos traduzir um poema
em Libras para o português escrito com versos e estrofes para lembrar ao
público que eles irão assistir a um poema. Outra opção é a de o artista
explicar os sinais que o público precisa saber e depois deixar as
pessoas usarem essa explicação durante a apresentação.

Como sabemos que a literatura sinalizada é bastante visual, uma proposta
que veio de Kenny Lerner (da dupla americana *Flying Words Project*) é
falar apenas as palavras necessárias para esclarecer o suficiente para o
público poder “ver” e entender a imagem que o artista apresenta. Assim,
as palavras são “dicas”, suficientes para esclarecer a imagem.

Até este ponto, falamos da tradução entre duas línguas, mas existe
também a opção de tradução de textos literários por meio de imagem. Já
sabemos, contudo, que as ilustrações ajudam o leitor a entender melhor
um texto. Vamos falar mais sobre isso no próximo capítulo.

<div class="float"></div>

![resumo](/images/resumo-parte4.png)

### 22.4. Resumo

A tradução de literatura é uma parte importante dos estudos de
literatura em Libras. Historicamente, a tradução da literatura do
português para a Libras ajudou no desenvolvimento das normas literárias
da comunidade surda com o uso da linguagem estética. A origem não surda
de uma obra não necessariamente significa que esta não utiliza as normas
surdas. Os recontos têm o objetivo de recontar uma história tradicional
de Libras em qualquer forma e, muitas vezes, têm a intenção de entreter.
As adaptações incluem personagens ou comportamentos surdos para criar
uma ligação mais forte com o público surdo. A tradução mais fiel é mais
comum nos contextos educacionais. As traduções podem acontecer do
português para a Libras e vice-versa, cada uma trazendo seus próprios
desafios. Vimos que os desafios para tradutores surdos e ouvintes vêm
dos textos e das necessidades dos públicos surdos e ouvintes. De
qualquer forma, uma tradução bem-feita pode abrir novos caminhos para a
criação de literatura em Libras e até mesmo em português.

<div class="float"></div>

![atividade](/images/atividade-parte4.png)

### 22.5. Atividade

Assista ao vídeo do poema de Fernanda Machado, *[Como Veio
Alimentação](#ytModal-nMOTYprbYoY).*

1.  Pense nas ideias apresentadas nesse capítulo sobre tradução e
    interpretação.

2.  Quais as possibilidades que você viu para fazer uma tradução ou
    interpretação para o português?

3.  Quais os desafios enfrentados pelo tradutor ou intérprete, e pelos
    públicos?

4.  Qual você acha a melhor opção, para qual público?

<!-- -->

