const path = require('path')
const { createFilePath } = require(`gatsby-source-filesystem`)

// Adicionando a url do caítulo pra cada post
exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions
  // Ensures we are processing only markdown files
  if (node.internal.type === "MarkdownRemark") {
    // Use `createFilePath` to turn markdown files in our `data/faqs` directory into `/faqs/slug`
    const relativeFilePath = createFilePath({
      node,
      getNode,
      basePath: "conteudo",
    })

    createNodeField({
      node,
      name: "slug",
      value: relativeFilePath,
    })
  }
}

exports.createPages = ({graphql, actions}) => {
  const { createPage } = actions

  return graphql(`
    {
      allMarkdownRemark {
        edges {
          node {
            fields {
              slug
            }
          }
        }
      }
    }
  `).then(result => {
    result.data.allMarkdownRemark.edges.forEach( ({node}) => {
      createPage({
        path: `/leitura-online${node.fields.slug}`,
        component: path.resolve('./src/templates/capitulo.js'),
        context: {
          slug: node.fields.slug,
        }
      })
    })
  })

}
