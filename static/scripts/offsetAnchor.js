export default function (hasHash, hasVideo) {
	if (hasHash) {
		const mobileVideoOffset = hasVideo ? document.querySelector('aside').offsetHeight : 0
		const offset = window.innerWidth >= 992 ? -140 : -103 - mobileVideoOffset
		window.scrollBy(0, offset)
	}
}
