function getBackdrop() {
  return document.querySelector('.vimeo.video-modal_backdrop');
}
function getIframe() {
  return document.querySelector('.vimeo.video-modal_iframe');
}
function getCloseButton() {
  return document.querySelector('.vimeo.video-modal_close-button');
}
function getVimeoLinks() {
  return document.querySelectorAll('a[href^="#vimeoModal"]')
}

function openModal(id) {
  var iframe = getIframe();
  var backdrop = getBackdrop();
  iframe.setAttribute('src', `https://player.vimeo.com/video/${id}?transparent=0`)
  backdrop.style.display = 'flex'
}

function closeModal() {
  var iframe = getIframe();
  var backdrop = getBackdrop();
  backdrop.style.display = 'none';
  iframe.setAttribute('src', '')
}

function vimeoEvent(e) {
  e.preventDefault();
  const link = e.target.localName === "em" ? e.target.parentNode : e.target
  var videoId = link.attributes.href.value.slice(12); 
  openModal(videoId);
}

function closeEvent(e) {
    e.preventDefault();
    closeModal();
}

function escEvent(e) {
  if (e.key === 'Escape') closeModal();
}

export function addVimeoListeners() {
  var vimeoLinks = getVimeoLinks();
  var backdrop = getBackdrop();
  var closeButton = getCloseButton();
  vimeoLinks.forEach(function(vLink) {
    vLink.addEventListener('click', vimeoEvent)
  })
  closeButton.addEventListener('click', closeEvent)
  backdrop.addEventListener('click', closeEvent)
  document.addEventListener('keyup', escEvent) 
}

export function removeVimeoListeners() {
  var vimeoLinks = getVimeoLinks();
  var backdrop = getBackdrop();
  var closeButton = getCloseButton();
  vimeoLinks.forEach(function(vLink) {
    vLink.removeEventListener('click', vimeoEvent)
  })
  closeButton.removeEventListener('click', closeEvent)
  backdrop.removeEventListener('click', closeEvent)
  document.removeEventListener('keyup', escEvent) 
}
