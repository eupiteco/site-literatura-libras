function getTooltip() {
	return  document.querySelector(".tooltip");
}

function getTooltipLinks() {
	return document.querySelectorAll('a[href="#tooltip"]');
}

function positionTooltip(x, y, id) {
	var tooltip = getTooltip();
	var content = document.querySelector("#tooltip-"+id)
	var xOffset = 18;
	var yOffset = 15;
	tooltip.style.display = "block";
	tooltip.innerHTML = content.innerHTML;
	const tooltipHeight = tooltip.offsetHeight;
	tooltip.style.left = (x - xOffset)+"px";
	tooltip.style.top = (y - yOffset - tooltipHeight)+"px";
}

function hideTooltip() {
	var tooltip = getTooltip();
	tooltip.style.display = "none";
	tooltip.style.left = "unset";
	tooltip.style.top = "unset";
}

function tooltipEvent(e) {
	e.preventDefault();
	e.stopPropagation();
	const link = e.target
	var linkData = {
		x: link.offsetLeft,
		y: link.offsetTop,
		id: e.target.innerText,
	};
	positionTooltip(linkData.x, linkData.y, linkData.id);
}

function closeTooltipEvent(e) {
	var tooltip = getTooltip();
	if (e.target !== tooltip) {
		hideTooltip()
	}
}

function escEvent(e) {
	if (e.key === "Escape") hideTooltip()
}

export function addTooltipListeners() {
	var links = getTooltipLinks()
	links.forEach(function(link) {
		link.addEventListener("click", tooltipEvent);
	});
	document.addEventListener('click', closeTooltipEvent);

	document.addEventListener('keyup', escEvent);
}

export function removeTooltipListeners() {
	var links = getTooltipLinks()
	links.forEach(function(link) {
		link.removeEventListener("click", tooltipEvent);
	});
	document.removeEventListener('click', closeTooltipEvent);

	document.removeEventListener('keyup', escEvent);
}
