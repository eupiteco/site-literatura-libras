function getBackdrop() {
  return document.querySelector('.youtube.video-modal_backdrop');
}
function getIframe() {
  return document.querySelector('.youtube.video-modal_iframe');
}
function getCloseButton() {
  return document.querySelector('.youtube.video-modal_close-button');
}
function getYoutubeLinks() {
  return document.querySelectorAll('a[href^="#ytModal"]')
}

function openModal(id) {
  var iframe = getIframe();
  var backdrop = getBackdrop();
  iframe.setAttribute('src', 'https://www.youtube.com/embed/' + id)
  backdrop.style.display = 'flex'
}

function closeModal() {
  var iframe = getIframe();
  var backdrop = getBackdrop();
  backdrop.style.display = 'none';
  iframe.setAttribute('src', '')
}

function youtubeEvent(e) {
  e.preventDefault();
  e.stopPropagation();
  const link = e.target.localName === "em" ? e.target.parentNode : e.target
  var videoId = link.attributes.href.value.slice(9);
  openModal(videoId);
}

function closeEvent(e) {
    e.preventDefault();
    closeModal();
}

function escEvent(e) {
  if (e.key === 'Escape') closeModal();
}

export function addYoutubeListeners() {
  var youtubeLinks = getYoutubeLinks();
  var backdrop = getBackdrop();
  var closeButton = getCloseButton();
  youtubeLinks.forEach(function(yLink) {
    yLink.addEventListener('click', youtubeEvent)
  })
  closeButton.addEventListener('click', closeEvent)
  backdrop.addEventListener('click', closeEvent)
  document.addEventListener('keyup', escEvent) 
}

export function removeYoutubeListeners() {
  var youtubeLinks = getYoutubeLinks();
  var backdrop = getBackdrop();
  var closeButton = getCloseButton();
  youtubeLinks.forEach(function(yLink) {
    yLink.removeEventListener('click', youtubeEvent)
  })
  closeButton.removeEventListener('click', closeEvent)
  backdrop.removeEventListener('click', closeEvent)
  document.removeEventListener('keyup', escEvent) 
}
